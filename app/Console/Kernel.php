<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
            //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function () {
            $data = DB::table('admin.all_tempfiles')->where('processed', 0)->limit(90)->get();
            foreach ($data as $value) {
                if ($value->controller == 'PaymentController') {
                    $class =new \App\Http\Controllers\PaymentController();
                     $class->{$value->method}($value->data,$value->id,$value->schema_name);
                }
            }
        })->everyMinute();
    }

    

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__ . '/Commands');
        require base_path('routes/console.php');
    }

}







