<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\QueryException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Mail;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Validation\ValidationException;
use Closure;
use Throwable;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
//        \Illuminate\Auth\AuthenticationException::class,
//        \Illuminate\Auth\Access\AuthorizationException::class,
//        \Symfony\Component\HttpKernel\Exception\HttpException::class,
//        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
//        ValidationException::class,
//        Illuminate\Foundation\Validation\ValidationException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    function createLog($e, $notify_admins = FALSE) {
        //if (!preg_match('/Router.php/',$e->getTrace()[0]['file'])) {
        if (!preg_match('/pipeline/i', @$e->getTrace()[0]['file'])) {
            $line = @$e->getTrace()[0]['line'];
            $err = "<br/><hr/><ul>\n";
            $err .= "\t<li>date time " . date('Y-M-d H:m', time()) . "</li>\n";
            $err .= "\t<li>Made By: " . session('id') . "</li>\n";
            $err .= "\t<li>usertype " . session('usertype') . "</li>\n";
            $err .= "\t<li>error msg: [" . $e->getCode() . '] ' . $e->getMessage() . ' on line ' . $line . ' of file ' . @$e->getTrace()[0]['file'] . "</li>\n";
            $err .= "\t<li>url: " . url()->current() . "</li>\n";
            $err .= "\t<li>Controller route: " . createRoute() . "</li>\n";
            $err .= "\t<li>Error from which host: " . gethostname() . "</li>\n";
            $err .= "\t<li>Error from username: " . session('username') . "</li>\n";
            $err .= "</ul>\n\n";

            $filename = set_schema_name() . '_' . str_replace('-', '_', date('Y-M-d')) . '.html';
            if (gethostname() == 'INETS-COMPANY' || set_schema_name() == 'beta_testing.' || set_schema_name() == 'public.') {
                //(isset($line) && $line == 546) ? '' : error_log($err, 3, dirname(__FILE__) . "/../../storage/logs/" . $filename);
            }

            error_log($err, 3, dirname(__FILE__) . "/../../storage/logs/" . $filename);


            if ($notify_admins == TRUE) {
                //$this->sendLog($err);
            }
        }
        // }
    }

    public function sendLog($err) {
        return DB::table("email")->insert(array(
                    'body' => $err,
                    'subject' => 'Error Occurred at ' . set_schema_name(),
                    'email' => 'inetscompany@gmail.com')
        );
    }

    //  public function report(Exception $e) {
//        parent::report($e);
    //   }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function action($action) {
        if (request()->ajax()) {
            return response()->json(['error' => 'Not Found'], 404);
        } else {
            return $action;
        }
    }

    function createErrorLog($e) {
        //  return $this->createLog($e);
        $line = @$e->getTrace()[0]['line'];
        $object = [
            'error_message' => $e->getMessage() . ' on line ' . $line . ' of file ' . @$e->getTrace()[0]['file'],
            'file' => @$e->getTrace()[0]['file'],
            'route' => createRoute(),
            "url" => url()->current(),
            'error_instance' => get_class($e),
            'request' => json_encode(request()->all()),
            "schema_name" => str_replace('.', null, set_schema_name()),
            'created_by' => session('id'),
            'created_by_table' => session('table')
        ];
        $this->createLog($e);
        if (preg_match('/query_wait_timeout/i', $e->getMessage()) || preg_match('/the database system is in recovery mode/i', $e->getMessage()) || preg_match('/connection refused/i', $e->getMessage()) || preg_match('/the database system is starting up/i', $e->getMessage()) || preg_match('/pgbouncer cannot connect/i', $e->getMessage())) {
            $this->automateDatabaseRecovery();
            $db_log = dirname(__FILE__) . '/../../storage/logs/db_log.txt';
            $handle = fopen($db_log, 'w');
            fwrite($handle, 'stop');
            fclose($handle);
            shell_exec("bash /usr/scripts/dbstatus.sh 2>&1");
            sleep(20);
            // die('Sorry, un planned server upgrade is on progress');
        } else {
            $db_log = dirname(__FILE__) . '/../../storage/logs/db_log.txt';
            $handle = fopen($db_log, 'w');
            fwrite($handle, '');
            fclose($handle);

            $wp_log = dirname(__FILE__) . '/../../storage/logs/wp_log.txt';
            if (file_exists($wp_log)) {
                unlink($wp_log); 
            }
        }

        //   if (!preg_match('/ValidatesRequests.php/i', @$e->getTrace()[0]['file']) || !preg_match('/Router.php/i', @$e->getTrace()[0]['file']) || !preg_match('/framework/src/Illuminate/Routing/Router.php/i', @$e->getTrace()[0]['file'])) {
        if (!preg_match('/sql/i', $e->getMessage())) {
            DB::table('admin.error_logs')->insert($object);
        }
        //  $this->allocateToPmp($e, $line);
        //  }

        if (preg_match('/api/i', url()->current())) {
            //this might be the api requests
            echo json_encode([
                'status' => 500,
                'message' => 'Server Error'
            ]);
            exit;
        }
    }

    public function automateDatabaseRecovery() {
        // First Notify key people
        //try to see if we can restart our db here
        $db_log = dirname(__FILE__) . '/../../storage/logs/wp_log.txt';
        if (!file_exists($db_log)) {
            $handle = fopen($db_log, 'w');
            fwrite($handle, 'stop');
            fclose($handle);

            $message = 'Database System is down and needs your immediate attention. Thanks';
            $whatsapp_numbers = ['255714825469', '255744158016', '255684033878', '255652160360'];
            foreach ($whatsapp_numbers as $number) {
                $chat_id = $number . '@c.us';
                $this->sendMessage($chat_id, $message);
            }
        }
        //system("service postgresql-12 stop");
        //  return system("service postgresql-12 restart");
//        $karibusms = new \karibusms();
//        $karibusms->API_KEY = '25336025463';
//        $karibusms->API_SECRET = '1cb066306b7c36d3e665228a50ceca939609864d';
//        $karibusms->set_name(strtoupper('SHULESOFT'));
//        $karibusms->karibuSMSpro = 1;
//        (object) json_decode($karibusms->send_sms('255714825469', $message, 'SHULESOFT_recovery' . time()));
//
//        $data = ['content' => $message, 'link' => 'demo.',
//            'photo' => 'shulesoft.png', 'sitename' => 'ShuleSoft', 'name' => ''];
//        $mes = [];
//        $emails = ['email' => 'ephraim@shulesoft.com', 'email' => 'swillae1@gmail.com'];
//        foreach ($emails as $mail) {
//            \Mail::send('email.default', $data, function ($m) use ($mail) {
//                $m->from('noreply@shulesoft.com', 'ShuleSoft');
//                $m->to($mail['email'])->subject('Database System is down and needs your immediate attention');
//            });
//        }
    }

    public function sendMessage($chatId, $text) {
        $data = array('chatId' => $chatId, 'body' => $text);
        $this->sendRequest('message', $data);
    }

    public function sendRequest($method, $data) {
        $APIurl = 'https://eu4.chat-api.com/instance210904/';
        $token = 'h67ddfj89j8pm4o8';
        $url = $APIurl . $method . '?token=' . $token;
        if (is_array($data)) {
            $data = json_encode($data);
        }
        $options = stream_context_create(['http' => [
                'method' => 'POST',
                'header' => 'Content-type: application/json',
                'content' => $data]]);
        $response = file_get_contents($url, false, $options);
    }

    public function allocateToPmp($e, $line) {
        $err = $e->getMessage() . ' on line ' . $line . ' of file ' . @$e->getTrace()[0]['file'];
        $title = createRoute() . ' ' . get_class($e);
        $check_tasks = DB::connection('mysql')->table('easycases')->where('title', $title)->first();



        if (!empty($check_tasks)) {
            DB::connection('mysql')->table('easycases')->where('title', $title)->update([
                'message' => $check_tasks->message . '<br/>' . $err,
                'case_count' => $check_tasks->case_count + 1,
                'status' => 0
            ]);
        } else {
            $min_user_id = \collect(DB::connection('mysql')->select('select assign_to from easycases where project_id=29 group by assign_to order by count(*) asc limit 1 '))->first();
            DB::connection('mysql')->table('easycases')->insert([
                'title' => $title, 'message' => $err, 'due_date' => date('Y-m-d'), 'status' => 0, 'legend' => 1, 'assign_to' => count($min_user_id) == 1 ? $min_user_id->assign_to : 1, 'user_id' => 1, 'case_count' => 1, 'project_id' => 29
            ]);
        }
    }

    public function render($request, Throwable $exception) {

        $this->createErrorLog($exception);

        if (preg_match('/does not exist on/', $exception->getMessage())) {
            return redirect(url('dashboard'))->with('warning', 'Page Supplied with name does not exists and you have redirected to this home page. Please try to type correctly url or contact your administrator if problem persist');
        }
        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
            return redirect()->back()->with('warning', 'This page has expired, please reload this page');
        }

        if ($exception instanceof ModelNotFoundException or $exception instanceof NotFoundHttpException) {

            return $this->action(response()->view('errors.404', [], 404));
        } else if ($exception instanceof QueryException) {

            //catch Database Errors
            return $this->databaseErrors($exception);
        } else if ($exception instanceof FatalErrorException) {

            return $this->action(redirect()->back()->with('error', "Sorry, we are experiencing difficulty processing your request "));
        } else if ($exception instanceof \ErrorException) {


            return $this->action(redirect()->back()->with('error', 'Sorry, we are experiencing difficulty processing your request'));
        } else if ($exception instanceof \LogicException) {

            return $this->action(response()->view('errors.fatal', [], 500));
        }
        return parent::render($request, $exception);
    }

    public function databaseErrors($e) {
        $error = $e->getMessage();
        $error_number = 'Message # : S001';
        $message = 'System cannot save information because it seems there are some invalid information supplied or required records are missing. Please refresh and try again or check the information you submit and if problem persist ';
        if (preg_match('/Undefined column/i', $error)) {
            //try to create a column 
        } else if (preg_match('/Undefined table/i', $error)) {
            //try to create a table 
        } else if (preg_match('/Invalid text representation/i', $error) || preg_match('/Syntax error/i', $error)) {
            $error_number = 'Message # : S009';
            $message = 'System cannot save information because it seems you try to save information which is not in a valid format or contains invalid characters. ';
        } else if (preg_match('/Foreign key violation/i', $error)) {
            $error_number = 'Message # : S007';
            $message = 'System cannot save information because it seems you try to save or edit or delete information which has a relationship with other information that exists in the system.';
            echo '<script>
            sweetAlert({
                confirmButtonText: "Yes",
                icon: "success",
                showCancelButton: true,
                title: "Can Your Suggest Subject/Topic",
                text: "Sorry; No video has been uploaded to this class yet. If you wish to learn specific topic/subject, Kindly click okay ."
                });
                <script>';
            return redirect()->back()->with('error', $message . $error_number);
        } else if (preg_match('/Unique violation/', $error)) {
            $error_number = 'Message # : S009';
            $message = 'System cannot save information because it seems you try to save information which already exists. ';
        } else if (preg_match('/Datetime field overflow:/i', $error) || preg_match('/Invalid datetime format/i', $error)) {
            $error_number = 'Message # : S008';
            $message = 'System cannot save information because it seems date you provided is not in a valid format. Please write date in (YYYY-MM-DD) format correctly and try again.';
        } else if (preg_match('/Deadlock detected/i', $error)) {
            //more serious, send internet sms notifications TO all programmers and try to restart a server  
        } else if (preg_match('/Check violation/i', $error)) {
            $error_number = 'Message # : S010';
            $message = 'System cannot save information because it seems you try to save information which exceed a valid specified range.';
        } else if (preg_match('/the database system is in recovery mode/i', $error) || preg_match('/Connection refused/i', $error)) {
            return $this->action(response()->view('errors.downtime', [], 500));
        }
        return redirect()->back()->with('error', $message . $error_number);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception) {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        // return redirect()->guest(route('login'))->with('warning', 'Your session had expired, please login again');
    }

    public function handle($request, Closure $next) {
        $response = $next($request);

        if (!method_exists($response, 'render')) {
            return $response;
        }

        $content = $response->render();

        // ... do stuff to $content

        return $content;
    }

    public function report(Throwable $exception) {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

}
