<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use App\Notifications\FcmNotification;
use App;
use Image;

class Admin_Controller extends Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    public $user_access;
    public $settings;

    function __construct() {

        parent::__construct();
        $this->data['all'] = array();
        $this->data['alert'] = array();
        $this->lang->load('topbar_menu');
        $this->lang->load('email');

        $this->data["siteinfos"] = \App\Model\Setting::first();

        $this->settings = $this->data["siteinfos"];

        $this->data['setup_instruction'] = $this->setupInstructions();
        $this->data['add_payment'] = $this->checkPaymentStatus();
        $this->data['user_access'] = 1;

        $check_tips = \DB::table('admin.page_tips')->where('controller_page', str_replace('@', '/', createRoute()))->first();
        $this->data['page_tip'] = '';
        $this->data['page_tip_content'] = '';
        if (!empty($check_tips)) {
            $this->data['page_tip'] = '<button class="btn-default btn-cs btn-sm-cs" data-toggle="modal" data-target="#page_tips" onmousedown="count_page_tip(' . $check_tips->id . ')"><span class="fa fa-info"></span> Tips</button>';
            $this->data['page_tip_content'] = $check_tips->content;
        }
    
    }

    public function setupInstructions() {
//check priorities in messages

        if (\App\Model\Classlevel::count() == 0) {
            $status = array('title' => 'Class Level(s)', 'message' => 'You need to add Class level(s) in your school. The System cannot function effectively without the Class level.', 'link' => base_url("classlevel/index"));
        } else if (\App\Model\AcademicYear::count() == 0) {
            $status = array('title' => 'Academic Year', 'message' => 'You need to add academic year(s). The System cannot function effectively without academic year(s)', 'link' => base_url("setting/index#academic"));
        } else if (\App\Model\Teacher::count() == 0) {
            $status = array('title' => 'Teachers', 'message' => 'You need to add teacher(s) in the system. ShuleSoft cannot function effectively without  teacher(s).', 'link' => base_url("teacher/index"));
        } else if (\App\Model\Classes::count() == 0) {
            $status = array('title' => 'Class', 'message' => 'You need to add classes in the system. The System cannot function effectively without  Classes.', 'link' => base_url("classes/add"));
        } else if (\App\Model\Section::count() == 0) {
            $status = array('title' => 'Class Section/Stream', 'message' => 'You need to add Section/Stream(s)  in the system. The System cannot function effectively without Section/Stream(s).', 'link' => base_url("section/index"));
        } else if (\App\Model\Parents::count() == 0) {
            $status = array('title' => 'Parents', 'message' => 'You need to add parents available in the system. You cannot add students without first adding parents in the system.', 'link' => base_url("parents/add"));
        } else if ((int) $this->parentWithNoStudents() > 0) {
            $status = array('title' => 'Parents', 'message' => $this->parentWithNoStudents() . ' parents have no students associated with their accounts. Please specify students correctly or add students for these parents in the system.', 'link' => base_url("parents/nostudent"));
        } else if ((int) $this->studentWithNoParents() > 0) {
            $status = array('title' => 'Students', 'message' => $this->studentWithNoParents() . ' students have no parents associated with their accounts. Please specify parents correctly for these students in the system.', 'link' => base_url("student/noparent"));
        } else {
            $status = array();
        }

        $status = array();
        return $status;
    }

    public function parentWithNoStudents() {
        return \App\Model\Parents::whereNotIn('parentID', \App\Model\StudentParent::get(['parent_id']))->count();
    }

    public function studentWithNoParents() {
        return \App\Model\Student::whereNotIn('student_id', \App\Model\StudentParent::get(['student_id']))->where('status', 1)->count();
    }

    public function duplicatedRecords() {
        $users = \App\Model\User::all();
        $duplicate = [];
        foreach ($users as $user) {
            $valid = validate_phone_number($user->phone);
            if (is_array($valid) && count($valid) == 2 && $user->phone != $valid[1]) {
                $duplicate[$user->id][$user->table] = [];
                $check = DB::table($user->table)->where('phone', $valid[1])->first();
                if (!empty($check)) {
                    $status = ['status' => '<p color="red">Duplicate found for phone  ' . $valid[1] . ' to user ' . $user->name . ',id=' . $user->id . ' in table' . $user->table . ' |== With existing users ' . $check->name . ', id=' . $check->{$user->table . 'ID'} . ',table=' . $user->table . '<p>'];
                    array_push($duplicate[$user->id][$user->table], $status);
                }
            }
        }
        return $duplicate;
    }

    function checkPaymentStatus() {
//        return false;

        $client = DB::table('admin.clients')->where('username', str_replace('.', '', set_schema_name()))->first();

        if (!empty($client)) {
            $account_year = DB::table('admin.account_years')->where('name', date('Y'))->first();
            if (!empty($account_year)) {
                $account_year_id = $account_year->id;
            } else {
                $account_year_id = DB::table('admin.account_years')->insert(['name' => date('Y')]);
            }

            $invoice = DB::table('admin.invoices')->where('client_id', $client->id)->where('account_year_id', $account_year_id)->first();

            //check payments as well
            if (!empty($invoice)) {
                $invoice_amount = DB::table('admin.invoice_fees')->where('invoice_id', $invoice->id)->sum('amount');
                $ids = DB::table('admin.invoice_fees')->where('invoice_id', $invoice->id)->get(['id']);


                $paid_amount = \collect(DB::select('select sum(paid_amount) from admin.invoice_fees_payments where invoice_fee_id in (select id from admin.invoice_fees where invoice_id=' . $invoice->id . ') '))->first()->sum;
                //add conditions, if you issue an invoice and it stays more than 30days ahead, put uggly red warning that appear in all pages, not being able to be removed, appear in all printing 
                //check if school pays alread, lets know when and if they need to pay again or not
                //get how many month school has used our solution , check how much school pay per month, check paid amount

                $now = date('Y-m-d'); // or your date as well
                $your_date = date('Y-m-d', strtotime($client->created_at));
                $date1 = new \DateTime($now);
                $date2 = new \DateTime($your_date);
                $days = $date2->diff($date1)->format("%a");

                //use the system x days
                $usage_month = ceil($days / 29);
            }

            if ((!empty($invoice) && (int) $paid_amount == 0)) {


                //check if payment is below 30 days to show that banner notice

                return array('due_date' => $invoice->due_date, 'days_remains' => $days, 'expired' => 1);
            } else if (!empty($invoice) && (int) $paid_amount < (int) $invoice_amount) {
                return array('due_date' => $invoice->due_date, 'days_remains' => $days, 'expired' => 3);
            } else {
                return $this->defaultPaymentCheck();
            }
        } else {
            return $this->defaultPaymentCheck();
        }
    }

    public function defaultPaymentCheck() {
        return FALSE;
        $setting = DB::table('setting')->first();
        if (!empty($setting)) {
            if ($setting->payment_deadline_date == '') {
                $paydate = date('Y-m-d', strtotime('30 days'));
                DB::table('setting')->update(['payment_deadline_date' => $paydate]);
            }
            if (isset($setting->payment_status) && (int) $setting->payment_status == 1) {
                //dd($setting->payment_status);
                return FALSE;
            }
            $now = date('Y-m-d'); // or your date as well
            if (isset($setting->payment_deadline_date)) {
                $your_date = date('Y-m-d', strtotime($setting->payment_deadline_date));
            } else {
                $your_date = $now;
            }
            $date1 = new \DateTime($now);
            $date2 = new \DateTime($your_date);
            $days = $date2->diff($date1)->format("%a");
            if (isset($setting->payment_status) && (int) $setting->payment_status == 2) {
                //extended due date due to a request
                $return = array('due_date' => $setting->payment_deadline_date, 'days_remains' => $days, 'expired' => 2);
            } else if (strtotime($your_date) < time() || (int) $days < 0) {
                // application expired
                $return = array('due_date' => $setting->payment_deadline_date, 'days_remains' => $days, 'expired' => 1);
            } else {
                //we have a warning for payments to be done on due date
                $return = array('due_date' => $setting->payment_deadline_date, 'days_remains' => $days, 'expired' => 0);
            }
            return $return;
        } else {
            $this->session->set_flashdata('error', 'Please define first School Infromation to Proceed');
            return redirect(base_url('install/site'));
        }
    }

    public function send_email($email, $subject, $message) {

        if ($this->settings->email_enabled == 1 && filter_var($email, FILTER_VALIDATE_EMAIL) && !preg_match('/shulesoft/', $email) && !in_array($email, ['inetscompany@gmail.com'])) {
            $user = \App\Model\User::where('email', $email)->first();
            $obj = array('body' => is_array($message) ? $message['message'] : $message, 'subject' => $subject, 'email' => $email);
            if (!empty($user)) {
                $_array = array('user_id' => $user->id, 'table' => $user->table);
                (int) $user->userInfo(DB::table($user->table))->email_valid == 1 ? DB::table('email')->insert(array_merge($_array, $obj)) : '';
            } else {
                DB::table('email')->insert($obj);
            }
        }

        return $this;
    }

    public function getSmsKey() {

        $check_keys = DB::table('sms_keys')->first();
        if ($check_keys) {
            $sms_keys_id = $check_keys->id;
        } else {
            $background = new \App\Http\Controllers\Background();
            $background->createSmsSetting();
            $sms_keys_id = DB::table('sms_keys')->first()->id;
        }

        return $sms_keys_id;
    }

    /**
     * ping a phone and check if its connected with internet
     */
    public function checkPhoneStatus() {
        $sms = DB::table('sms_keys')->where('name', 'phone-sms')->first();
        try {
            $karibusms = new \karibusms();
            $karibusms->API_KEY = $sms->api_key;
            $karibusms->API_SECRET = $sms->api_secret;
            $result = (object) json_decode($karibusms->check_phone_status());
            //2020-08-06 20:07:28
            $last_online = isset($result->last_online) ? $result->last_online : date('d-m-Y H:i');

            $time = strtotime($last_online);
            $tz_date = strtotime('-7 hours', $time);


            $current_time = date('d-m-Y H:i', strtotime('+3 hours', time()));
            $sms_time = date('d-m-Y H:i', $tz_date);


            $to_time = strtotime($current_time);
            $from_time = strtotime($sms_time);
            $minutes = round(abs($to_time - $from_time) / 60, 2);
            if ((int) $minutes > 60) {
                $message = "Hello, Your Phone was last active on " . date('d M Y H:i', strtotime($sms_time)) . ",  (about " . $minutes . " minutes) ago. kindly make sure your phone is connected with internet.";
            } else {
                $message = '';
            }
        } catch (Exception $exc) {
            $message = $exc->getTraceAsString();
        }
        return $message;
    }

    public function checkChannelStatus($key) {
        if ($key == 'whatsapp') {
            $schema = str_replace('.', '', set_schema_name());
            $client = DB::table('admin.clients')->where('username', $schema)->first();
            //check addons payments
            $addons = DB::table('admin.addons_payments')->where('client_id', $client->id)->where('end_date', '>=', date('Y-m-d H:i', time()))->first();
            return empty($addons) ? FALSE : TRUE;
        }
        if ($key == 'quick-sms') {
            $schema = str_replace('.', null, set_schema_name());
            $client = DB::connection('karibusms')->table('client')->where('keyname', $schema)->first();
            if (empty($client)) {
                $message_left = (new \App\Http\Controllers\Background())->createSmsSetting();
            } else {

                $quick_sms = DB::connection('karibusms')->table('sms_status')->where('client_id', $client->client_id)->first();
                $message_left = !empty($quick_sms) ? $quick_sms->message_left : 0;
            }
            return $message_left;
        }

        if ($key == 'phone-sms') {
            $phone_status = $this->checkPhoneStatus();
            return strlen($phone_status) > 10 ? FALSE : TRUE;
        }
    }

    public function getValidChannels() {
        $checks = [];
        $whatsapp = $this->checkChannelStatus('whatsapp');
        $whatsapp == FALSE ? array_push($checks, ['whatsapp']) : '';

        $quick = $this->checkChannelStatus('quick-sms');
        (int) $quick = 0 ? array_push($checks, ['quick-sms']) : '';

        $keys_values = request('sms_keys_id');
        if(isset($keys_values) && !empty($keys_values)){
          
        foreach ($keys_values as $key => $value) {
            if (in_array($value, $checks)) {
                unset($keys_values[$key]);
            }
        }
    
        return $keys_values;
    } 
    }

    public function send_sms($phone_number, $message, $priority = 0, $sms_content_id = null) {
        if ($this->settings->sms_enabled == 1 && (strlen($phone_number) > 6 && strlen($phone_number) < 20) && $message != '') {
            $user = \App\Model\User::where('phone', $phone_number)->first();
            $type = $this->data['siteinfos']->sms_type;
            
            if ($sms_content_id == null) {

                $channels = request('sms_keys_id');
            if(isset($channels) && !empty($channels)){

                    $sms_content_id = DB::table('sms_content')->insertGetId(['message' => $message,
                    'created_by' => session('id'),
                    'created_by_table' => session('table'),
                    'channels' => implode(',', request('sms_keys_id'))]);

               
                    foreach ($channels as $channel) {
                        $sms_key = DB::table('sms_keys')->where('name', strtolower($channel))->first();
                        $sms_key_id = !empty($sms_key) ? $sms_key->id : DB::table('sms_keys')->insertGetId(['name' => strtolower($channel)]);
                        DB::table('sms_content_channels')->insert([
                            'sms_keys_id' => $sms_key_id,
                            'sms_content_id' => $this->sms_content_id
                        ]);
                    }
            } else {
                    //send via default message
                    $sms_key = DB::table('sms_keys')->where('name', 'phone-sms')->first();

                        if(isset( $sms_key) && !empty( $sms_key)){
                            DB::table('sms_content_channels')->insert([
                            'sms_keys_id' => $sms_key->id,
                            'sms_content_id' => $sms_content_id
                            ]);
                        }
                        
                    }
            }

            $sms_keys_id = $this->getSmsKey();
            $valid_channels = $this->getValidChannels();
            if(!empty($valid_channels)){

            $sent_from=  implode(',', $valid_channels) ;
            } else{

                $sent_from='phonesms';   
            }

            if ($user) {   
                //skip default teacher               
                if (!preg_match('/655406004/i', $phone_number)) {
                    $sms_id = DB::table('sms')->insertGetId(array('phone_number' => $phone_number,
                        'body' => $message,
                        'user_id' => $user->id,
                        'table' => $user->table,
                        'sms_content_id' => $sms_content_id,
                        'sent_from' =>$sent_from,
                        'subject' => request('email_subject'),
                        'type' => $type, 'priority' => $priority, 'sms_keys_id' => $sms_keys_id), 'sms_id');

                    
                }
            } else {

                $sms_id = DB::table('sms')->insertGetId(array('phone_number' => $phone_number,
                    'subject' => request('email_subject'),
                    'sms_content_id' => $sms_content_id,
                    'sent_from' => $sent_from,
                    'body' => $message,
                    'type' => $type, 'priority' => $priority, 'sms_keys_id' => $sms_keys_id), 'sms_id');

            }
        }

        return $this;
    }

    public function sendDesktopNotification($fcm_token, $message) {
        $fcm = new FcmNotification();
        isset($fcm_token) && strlen($fcm_token) > 4 ?
        $fcm->notify($fcm_token, 'Message From ShuleSoft', substr($message, 0, 25), url()->current()):'';
    }

    public function get_setting() {
        return \App\Model\Setting::first();
    }

    public function fileload($filename) {

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($filename);
        return $spreadsheet;
    }

    public function uploadExcel($sheet_name = null) {
        //        $file = request()->file('file');
        //        $data = $this->fileload($file);
        //        dd($data);
        //        exit;
        //        $this->load->library('PHPExcel');
        try {
            // it will be your file name that you are posting with a form or c
            //an pass static name $_FILES["file"]["name"]
            $folder = "storage/uploads/media/";
            if (!is_dir($folder)) {
                mkdir($folder, 0777, true);
            }
            //is_dir($folder) ? '' : mkdir($folder, 0777,True);
            $file = request()->file('file');
            //$name=  str_replace('.'.$file->guessClientExtension(), '', $file->getClientOriginalName());
            $name = time() . rand(4343, 3243434) . '.' . $file->guessClientExtension();
            $move = $file->move($folder, $name);
            $path = $folder . $name;
            if (!$move) {
                die('upload Error');
            } else {
                $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
            }
        } catch (Exception $e) {
            $this->resp->success = FALSE;
            $this->resp->msg = 'Error Uploading file';
            echo json_encode($this->resp);
        }
        $sheets = $objPHPExcel->getSheetNames();
        if ($sheet_name == null) {
            return $this->getDataBySheet($objPHPExcel, 0);
        } else {
            $data = [];
            foreach ($sheets as $key => $value) {
                $data[$value] = [];
            }
            foreach ($sheets as $key => $value) {
                $excel_data = $this->getDataBySheet($objPHPExcel, $key);
                count($excel_data) > 0 ? array_push($data[$value], $excel_data) : '';
            }
            return $data;
        }
        unlink($path);
    }

    public function getDataBySheet($objPHPExcel, $sheet_id) {
        $sheet = $objPHPExcel->getSheet($sheet_id);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1, NULL, TRUE, FALSE);
        $data = array();
        for ($row = 2; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData[0] = array_combine($headings[0], $rowData[0]);
            $data = array_merge_recursive($data, $rowData);
        }
        return $data;
    }

    public function removeSpecialChar($input_string) {

        // Using str_replace() function  
        // to replace the word  
        $output_string = str_replace(array('\'', '"', ',', ';', '<', '>', '&'), ' ', $input_string);
        return $output_string;
    }

    public function exportExcel($data_array, $title, $file_name = 'file') {
        $table = '<table><tr>';
        foreach ($title as $key) {
            $key = $this->removeSpecialChar($key);
            $table .= '<td>' . $key . '</td>';
        }
        $table .= '</tr>';
        foreach ($data_array as $key1 => $_value) {
            $table .= '<tr>';
            foreach ($_value as $key => $value) {
                $table .= '<td>' . $value . '</td>';
            }
            $table .= '</tr>';
        }
        $table .= '</table>';

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $spreadsheet = $reader->loadFromString($table);

        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename = "' . $file_name . '.xls' . '"');
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
// $writer->save($file_name . '.xls');
        $writer->save('php://output');
    }

    public function exportExcelOld($data_array, $title, $file_name = 'file') {
//$this->load->library('PHPExcel', NULL, 'excel');
        $columns = array_keys($data_array);

//activate worksheet number 1
        $excel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $excel->setActiveSheetIndex(0);
//name the worksheet

        $excel->getActiveSheet()->setTitle('Users list');

//set the title values	
        $excel->getActiveSheet()->fromArray($title, NULL, 'A1');

//$this->PHPExcel->getActiveSheet()->SetCellValue('A1', 'STUDENT REPORT');
// read data to active sheet
        $ex_data = array();
        foreach ($data_array as $key1 => $_value) {
            $in_data = array();
            foreach ($_value as $key => $value) {
                $in_data = array_merge($in_data, [$key => $value]);
            }

            array_push($ex_data, $in_data);
        }
//echo json_encode($ex_data,JSON_PRETTY_PRINT); exit;

        $$excel->getActiveSheet()->fromArray($ex_data, null, 'A2');

        $filename = $file_name . '.xls'; //save our workbook as this file name
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $spreadsheet = $reader->loadFromString($htmlString);

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save($filename);
        exit;
        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename = "' . $filename . '"'); //tell browser what's the file name
///header('Cache-Control: max-age=0'); //no cache
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
// $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($excel, 'Xlsx');
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xls($excel);

        $writer->save("05featuredemo.xls");
        exit;
//force user to download the Excel file without writing it to server's HD
        $writer->save('php://output');
    }

    function createDate($date, $format = 'm-d-Y', $return = 'Y-m-d') {
        return date('Y-m-d', strtotime($date));
    }

    public function uploadImage($path = "./storage/uploads/images/", $filename = NULL) {


        $config = array(
            'name' => $_FILES['image']['name'],
            'required_extension' => array("gif", "jpg", "png", "jpeg"),
            'path' => $path,
            'tmp_name' => $_FILES['image']['tmp_name']
        );

        $upload = $this->uploadFile($config, $filename);
        return (empty($upload)) ? FALSE : $upload['name'];
    }

    public function uploadFile($config, $filename = NULL) {
        if (isset($config['name'])) {
            $name = explode(".", $config['name']);
            $ext = count($name) > 1 ? strtolower($name[1]) : '';

            if (isset($config['required_extension']) && in_array($ext, $config['required_extension'])) {
                $file = $filename == NULL ? time() . '.' . $ext : $filename;
                $path = $config['path'] . $file;

                !is_dir($config['path']) ? mkdir($config['path'], 0755) : '';

                $move = move_uploaded_file($config['tmp_name'], $path);
                if (!$move) {
                    return array();
                } else {
                    return array('name' => $file);
                }
            }
        }
    }

    /**
      @uses : It will be used in all table updates and table deletes
     * @param $array (
      'table_name'=>'table name affected',
      'column_name'=>'column affected',
      'status'=> 0 if update, 1 if data deleted
      'column_value_from'=> 'original value before change',
      'column_final_value'=> 'final value after change',
      )
     *
     */
    public function track($array) {
        $id = $this->session->userdata("id");
        $usertype = Auth::user()->usertype;
        return $this->db->insert('track', array_merge(array('user_id' => $id, 'user_type' => $usertype), $array));
    }

    function ip_details($ip) {
        $json = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip={$ip}"));
//$details = json_decode($json, true);
        return (object) $json;
    }

    public function curlPage($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        return clean_htmlentities($data);
    }

    public function withJob($type = 'sms') {
        return null;
    }

    function getStudentByYearClass($class_id, $academic_year_id, $section_id = NULL) {
        $where = ($section_id == NULL || (int) $section_id == 0 ) ? '' :
                ' AND a.section_id=' . $section_id;

        $sql = 'select a.*,b.name,b.photo,b.roll,c.section, c."classesID" from ' . set_schema_name() . 'student_archive a JOIN ' . '  ' . set_schema_name() . 'student b ON '
                . '      b."student_id"= a.student_id JOIN ' . set_schema_name() . 'section c
ON  c."sectionID"=a."section_id" WHERE b.status=1 AND '
                . '      a.academic_year_id=' . $academic_year_id . '  AND '
                . '      a.section_id IN (SELECT "sectionID" FROM ' . set_schema_name() . 'section WHERE "classesID"=' . $class_id . ') ' . $where;
        return DB::select($sql);
    }

    function modify_keys_to_upper_and_underscore($value_array) {

        $new_array = array_change_key_case($value_array, CASE_LOWER);
        $keys = str_replace(' ', '_', array_keys($new_array));
        $value = array_combine($keys, array_values($new_array));
        return $value;
    }

    public function ajaxTable($table, $columns, $custom_sql = null, $order_name = null, $count = null) {
## Read value
        if (isset($_POST) && request()->ajax() == true) {
            $draw = $_POST['draw'];
            $row = $_POST['start'];
            $rowperpage = $_POST['length']; // Rows display per page
            $columnIndex = $_POST['order'][0]['column']; // Column index
            $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
            $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
            $searchValue = $_POST['search']['value']; // Search value
## Search 
            $searchQuery = " ";
            if ($searchValue != '') {
                $searchQuery = " and ( ";
                $list = '';
                foreach ($columns as $column) {
                    $list .= 'lower(' . $column . "::text) like '%" . strtolower($searchValue) . "%' or ";
                }
                $searchQuery = $searchQuery . rtrim($list, 'or ') . ' )';
            }

## Total number of records without filtering
// $sel = DB::select("select count(*) as allcount from employee");
## Total number of record with filtering
## Fetch records
            $columnName = strlen($columnName) < 1 ? '1' : $columnName;
            $total_records = 0;
            if (strlen($custom_sql) < 2) {
// strlen($searchQuery); exit;
                $sel = \collect(DB::select("select count(*) as count from " . $table . " WHERE true " . $searchQuery))->first();
                $total_records = $sel->count;

                $empQuery = "select * from " . $table . " WHERE true " . $searchQuery . " order by \"" . $columnName . "\" " . $columnSortOrder . " offset  " . $row . " limit " . $rowperpage;
            } else {
                $empQuery = $custom_sql . " " . $searchQuery . " order by \"" . $columnName . "\" " . $columnSortOrder . " offset  " . $row . " limit " . $rowperpage;

                $total_records = $count == null ? count(DB::select($custom_sql)) : $count;
            }
            $empRecords = DB::select($empQuery);


## Response
            $response = array(
                "draw" => intval($draw),
                "iTotalRecords" => $total_records,
                "iTotalDisplayRecords" => $total_records,
                "aaData" => $empRecords
            );

            return json_encode($response);
        }
    }

    public function saveFile($file, $subfolder = null, $local = null) {
           if ($local == TRUE) { 
            $url = $this->uploadFileLocal($file);
            DB::table('files')->insert([
                'mime' => $file->getClientOriginalExtension(),
                'name' => $file->getClientOriginalName(),
                'display_name' => $file->getClientOriginalExtension(),
                'user_id' => session('id'),
                'table' => session('table'),
                'size' => 0,
                'caption' => $file->getRealPath(),
                'path' => $url
            ]);
            return $url;
        } else {  
            $path = \Storage::disk('s3')->put($subfolder, $file);
            $url = \Storage::disk('s3')->url($path);

            if (strlen($url) > 10) {
                DB::table('files')->insert([
                    'name' => $file->getClientOriginalName(),
                    'display_name' => $file->getClientOriginalExtension(),
                    'user_id' => session('id'),
                    'table' => session('table'),
                    'size' => $file->getSize(),
                    'caption' => $file->getRealPath(),
                    'path' => $url
                ]);
                return $url;
            } else {
                return false;
            }
        }
    }

    public function uploadFileLocal($file) {
//Move Uploaded File
        $schema = str_replace('.', null, set_schema_name());
        $destinationPath = 'storage/uploads/attach/' . $schema;
        !is_dir($destinationPath) ? mkdir($destinationPath) : '';
        $filename = rand(145, 87998) . time() . '.' . $file->getClientOriginalExtension();
        $file->move($destinationPath, $filename);
        return url($destinationPath . '/' . $filename);
    }

    public function curlServer($fields, $url) {
// Open connection
        $ch = curl_init();
// Set the url, number of POST vars, POST data

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'application/x-www-form-urlencoded'
        ));

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /*
      public function notify($title, $message) {
      $siteinfos = $this->data['siteinfos'];
      return view('components.notify', compact('title', 'message', 'siteinfos'));
      }
     */

    public function curlPrivate($fields, $url = null) {
// Open connection
        $url = 'http://51.91.251.252:8081/api/payment';
        $ch = curl_init();
// Set the url, number of POST vars, POST data

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'application/x-www-form-urlencoded'
        ));

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    ##################################### function for email and sms templates ######################################

    public function message($template, $language) {
        $get_template = DB::table('constant.mailandsmstemplate')->where('name', $template)->where('lang', $language)->first();
        $get_default_template = DB::table('constant.mailandsmstemplate')->where('name', $template)->where('lang', 'sw')->first();
        if ($get_template) {
            return $get_template->template;
        } else if ($get_default_template) {
            return $get_default_template->template;
        } else {
            return '';
        }
    }

    ##################################function for selecting languages for email and sms ############################

    public function message_language() {
        return DB::table('setting')->first() ? DB::table('setting')->first()->sms_lang : session('lang');
    }

    public function moveLocalFiles() {

        //  $load_users = \App\Model\User::get();
        $load_users = \App\Model\Sponsor::get();
        foreach ($load_users as $user) {
            if (file_exists('storage/uploads/images/' . $user->photo)) {

                //$file = base_url('storage/uploads/images/' . $user->photo); 
                $contents = base_url('storage/uploads/images/' . $user->photo);
                echo $contents . '<br>';

                echo '<img src="' . $contents . '"><br>';

//            dd($contents);
                $path = \Storage::disk('s3')->put('shulesoft', $contents);
                $url = \Storage::disk('s3')->url($path);
                echo $url;

                exit;
                \App\Model\Sponsor::where('id', $user->id)->update(['photo' => $url]);

                if (strlen($url) > 10) {

                    DB::table('files')->insert([
                        'name' => $file->getClientOriginalName(),
                        'display_name' => $file->getClientOriginalExtension(),
                        'user_id' => session('id'),
                        'table' => session('table'),
                        'size' => $file->getSize(),
                        'caption' => $file->getRealPath(),
                        'path' => $url
                    ]);

                    $user->table == 'student' ? $table_id = 'student_id' : $table_id = $user->table . 'ID';
                    \DB::table($user->table)->where($table_id, $user->table)->update('photo', $url);
                }
            }
        }
    }

}
