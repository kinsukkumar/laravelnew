<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Auth;
use Lang;
use App;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    /**
      SILLY FUNCTIONS THAT FAKE CI TO LARAVEL
     */

    /**
     * $load Variable : Overwrite codeigniter load variable
     */
    public $load;

    /**
     * $db Variable : Overwrite codeigniter DB variable
     */
    public $db;

    /**
     * $session Variable : Overwrite codeigniter session variable
     */
    public $session;

    /**
     * $lang Variable : Overwrite codeigniter language variable
     */
    public $lang;

    /**
     * $data Variable : Overwrite codeigniter data variable
     */
    public $data;

    /**
     * $form_validation Variable : Overwrite codeigniter form validation variable
     */
    public $form_validation;

    /**
     * $input Variable : Overwrite codeigniter input variable
     */
    public $input;

    /**
     * $url Variable : Overwrite codeigniter url mapping variable
     */
    public $uri;

    /**
     * $language_files Variable : Overwrite codeigniter language files variable
     */
    public $language_files = array();

    /**
     * $op Variable : object sql catching variable
     */
    private $op;

    /**
     * $op $custom_validation_message : Message that user will read when error occurs
     */
    public $custom_validation_message = [
        'required' => 'The :attribute field is required.',
        'photo' => 'The :attribute is too big.',
        'max' => 'The :attribute must be at maximum :max.',
        'min' => 'The :attribute must be specified correctly.',
        'same' => 'The :attribute and :other must match.',
        'numeric' => 'The :attribute must be a valid number.',
        'unique' => 'This :attribute is already in use',
        'iunique' => 'This :attribute is already in use',
        'email' => 'This :attribute is not valid',
        'alpha' => 'The :attribute field must only contain alphabets and numbers',
        'integer' => 'This :attribute must be a valid number',
        'alpha_dash' => 'The :attribute can only contain alphabets and underscores',
        'after' => 'The :attribute must be a date after :date.',
        'greater_than_field' => 'The :attribute must be greater than :field',
        'date_format' => 'The :attribute does not match the format :format.',
        'regex' => 'The :attribute format is invalid.',
        'before' => 'The :attribute must be a date before :date.',
        'mimes' => 'The :attribute must be a file of type: :values.',
        'mimetypes' => 'The :attribute must be a file of type: :values.',
         'gt' => 'The :attribute must be greater than :value',
    ];

    public function __construct() {

        $this->load = $this;
        $this->db = new \CiDatabase();
        $this->session = $this;
        $this->lang = $this;
        $this->uri = $this;
        $this->input = new \Input();
        $this->form_validation = $this;
        $this->data['data'] = $this;
        $this->data['TITLE'] = 'SHULESOFT';
        $this->data['bn_number'] = '888999';
        DB::statement("SET TIME ZONE 'Africa/Dar_es_Salaam'");
        $this->ciModels();
    }

    public function set_flashdata($key = null, $value = null) {
        return session(['flash' => array($key, $value)]);
    }

    function changeLanguage() {

        $loc = $this->segment(3);

        if (session('lang') == null || $loc != '') {

            $lang = $loc == 'sw' ? 'kiswahili' : 'english';
        } else {
            $lang = session('lang') == 'kiswahili' ? 'kiswahili' : 'english';
        }

        session([
            'lang' => $lang
        ]);

        App::setLocale(session('lang'));
        echo $lang;
    }

    /**
     * 
     * @param type $value : Object value from Excel
     * @param type $required : Keys required to match in excel file
     * @return null If all keys match, otherwise string message
     */
    public function excelCheckKeysExists($value, $required) {
        if (!is_array($value)) {
            return 'File uploaded is not valid';
        }
        $data = array_shift($value);
        if (count(array_intersect_key(array_flip($required), $data)) === count($required)) {
            //All required keys exist! 
            $status = 1;
        } else {
            $missing = array_intersect_key(array_flip($required), $data);
            $data_miss = array_diff(array_flip($required), $missing);
            $status = ' Column with title <b>' . implode(', ', array_keys($data_miss)) . '</b>  miss from Excel file. Please make sure file is in the same format as a sample file';
        }
        return $status;
    }

    public function createdBy() {
        return '{' . session('id') . ',' . session('table') . '}';
    }

    public function segment($id) {
        $enc = mb_detect_encoding(trim(request()->segment($id)), "UTF-8,ISO-8859-1");
        return iconv($enc, "UTF-8", trim(request()->segment($id)));
    }

    public $error_validation;

    public function set_rules($rules) {
        $this->error_validation = [];
        if (is_array($rules)) {
            foreach ($rules as $key => $value) {
                $obj_rules = [
                    isset($value['field']) ? $value['field'] : '' => isset($value['rules']) ? $value['rules'] : ''
                ];
                $this->error_validation = array_merge($this->error_validation, $obj_rules);
            }
            return $this;
        }
    }

    public function run() {

        $return = Validator::make(request()->all(), $this->error_validation, $this->custom_validation_message)->validate();
        return $return == null ? true : false;
    }

    public function ciModels() {
        /*
          | -------------------------------------------------------------------
          |  Auto-load Models from CI
          | -------------------------------------------------------------------
         */
        $return = array('notice_m', 'student_m', 'teacher_m', 'parents_m', 'sattendance_m', 'grade_m',  'book_m', 'category_m', 'dashboard_m', 'eattendance_m', 'exam_m', 'examschedule_m', 'expense_m', 'hmember_m', 'hostel_m', 'issue_m', 'lmember_m', 'mailandsms_m', 'mailandsmstemplate_m', 'mailandsmstemplatetag_m', 'mark_m', 'media_category_m', 'media_m', 'media_share_m', 'message_m', 'parents_info_m', 'payment_m', 'payment_modal', 'promotionsubject_m', 'reply_msg_m', 'reset_m', 'routine_m', 'sattendance_m', 'section_m', 'setting_m', 'signin_m', 'site_m', 'smssettings_m', 'student_info_m', 'subject_m', 'tattendance_m', 'tmember_m', 'transport_m', 'user_m', 'vendor_m', 'item_m', 'classlevel_m', 'examreport_m', 'bankaccount_m', 'academic_year_m', 'automation_shudulu_m', 'role_m', 'academic_year_m', 'health_conditions_m', 'countries_m', 'religions_m', 'parent_types_m', 'characters_m', 'character_categories_m', 'student_characters_m', 'character_grading_m', 'general_character_assessment_m', 'invoice_fee_m', 'semester_m', 'book_quantity_m', 'fee_m', 'fee_discount_m', 'invoices_m', 'fee_installment_m');

        foreach ($return as $key => $value) {
            if (class_exists($value)) {
                $this->{$value} = new $value;
            }
        }
    }

    /**
     * @access NULL object
     */
    public function library($library = null) {
        $this->{$library} = new $library;
    }

    /**
     * @access NULL object
     */
    public function helper($helper_file) {
        
    }

    /**
     * @access NULL object
     */
    public function database() {
        
    }

    public function userdata($session_name = null) {
        return session($session_name);
    }

    public function load($file = null, $state = null) {
        array_push($this->language_files, $file . '_lang');
        return $this->data['data_language'] = $this;
    }

    /**
     * Fetch a single line of text from the language array
     *
     * @access  public
     * @param string  $line the language line
     * @return  string
     */
    function line($line = '') {
        $lang = '';
        foreach ($this->language_files as $value) {
            if (Lang::has($value . '.' . $line)) {
                $lang = $value;
            }
        }
        return __($lang . '.' . $line);
    }

    public function userInformation() {
        $user_information = array();
        if (session('table') != null) {
            $id = trim(session('table')) == 'student' ? 'student_id' : session('table') . 'ID';
            $user_information = \collect(DB::select('select * FROM ' . set_schema_name() . session('table') . ' where "' . $id . '"=\'' . session('id') . '\' '))->first();
        }
        return $user_information;
    }

    public function view($view = null, $data = null) {
        $user_info['user_info'] = $this->userInformation();
        $return = (!empty($data) && is_array($data) ) ? array_merge($user_info, $data) : array();
        if (request('email') == 'send_view') {
            $page = view('email.page', $return);
            // DB::table('email')->insert(array('email' => 'swillae1@gmail.com', 'subject' => 'testing sending email', 'body' => $page));
        } else {
            echo request()->ajax() == true ? view($this->data['subview'], $return) : \View::make($view, $return)->render();
            // echo view($view, $return);
        }
    }

    public function model($model = null) {
        if (class_exists($model)) {
            $this->{$model} = (new $model);
        }
    }

//    public function ajaxTable($table, $columns, $custom_sql = null, $order_name = null) {
//        ## Read value
//        if (isset($_POST) && request()->ajax() == true) {
//            $draw = $_POST['draw'];
//            $row = $_POST['start'];
//            $rowperpage = $_POST['length']; // Rows display per page
//            $columnIndex = $_POST['order'][0]['column']; // Column index
//            $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
//            $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
//            $searchValue = $_POST['search']['value']; // Search value
//## Search 
//            $searchQuery = " ";
//            if ($searchValue != '') {
//                $searchQuery = " and ( ";
//                $list = '';
//                foreach ($columns as $column) {
//                    $list .= 'lower(' . $column . "::text) like '%" . strtolower($searchValue) . "%' or ";
//                }
//                $searchQuery = $searchQuery . rtrim($list, 'or ') . ' )';
//            }
//
//## Total number of records without filtering
//            // $sel = DB::select("select count(*) as allcount from employee");
//## Total number of record with filtering
//## Fetch records
//            $columnName = strlen($columnName) < 1 ? '1' : $columnName;
//            if (strlen($custom_sql) < 2) {
//                // strlen($searchQuery); exit;
//                $sel = DB::select("select * from " . $table . " WHERE true " . $searchQuery);
//
//                $empQuery = "select * from " . $table . " WHERE true " . $searchQuery . " order by \"" . $columnName . "\" " . $columnSortOrder . " offset  " . $row . " limit " . $rowperpage;
//            } else {
//                $empQuery = $custom_sql . " " . $searchQuery . " order by \"" . $columnName . "\" " . $columnSortOrder . " offset  " . $row . " limit " . $rowperpage;
//                $sel = DB::select($custom_sql);
//            }
//            $empRecords = DB::select($empQuery);
//
//
//## Response
//            $response = array(
//                "draw" => intval($draw),
//                "iTotalRecords" => count($sel),
//                "iTotalDisplayRecords" => count($sel),
//                "aaData" => $empRecords
//            );
//
//            return json_encode($response);
//        }
//    }
}
