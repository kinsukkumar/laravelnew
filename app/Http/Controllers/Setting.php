<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use DB;
use Input;
use Image;

Class Setting extends Admin_Controller {

    /**
     * -----------------------------------------
     *
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     *
     *
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     *
     *
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
        parent::__construct();
        $language = $this->session->userdata('lang');
        $this->lang->load('setting', $language);
        $this->lang->load('signature', $language);
    }

    public function guides() {

        $this->data["subview"] = "setting/parent_guide";
        $this->data['siteinfos'] = $this->data['setting'] = \App\Model\Setting::first();
        $this->load->view('_layout_main', $this->data);
    }

    public function createBarCode($user_id, $table, $number = null) {
        $user_number = $number == null ?
                \App\Model\User::where('id', $user_id)->where('table', $table)->first()->number :
                $number;
        $barcode = new BarcodeGenerator();
        $barcode->setText('SS' . $user_id);
        $barcode->setType(BarcodeGenerator::Code128);
        $barcode->setScale(1);
        $barcode->setThickness(15);
        $barcode->setFontSize(7);
        return $barcode->generate();
    }

    protected function rules() {
        $rules = array(
            array(
                'field' => 'sname',
                'label' => $this->lang->line("setting_school_name"),
                'rules' => 'required|max:128'
            ),
            array(
                'field' => 'name',
                'label' => $this->lang->line("setting_name"),
                'rules' => 'required|max:128'
            ),
            array(
                'field' => 'username',
                'label' => $this->lang->line("setting_username"),
                'rules' => 'required|max:128'
            ),
            array(
                'field' => 'phone',
                'label' => $this->lang->line("setting_school_phone"),
            ),
            array(
                'field' => 'email',
                'label' => $this->lang->line("setting_school_email"),
                'rules' => 'required|email|max:40'
            ),
            array(
                'field' => 'headname',
                'label' => "Head teacher title. Example, Headmaster, Headmistress etc",
                'rules' => 'required|max:40'
            ),
            array(
                'field' => 'automation',
                'label' => $this->lang->line("setting_school_automation"),
                'rules' => 'required|max:2|numeric'
            ),
            array(
                'field' => 'currency_code',
                'label' => $this->lang->line("setting_school_currency_code"),
                'rules' => 'required|min:1'
            ),
            array(
                'field' => 'currency_symbol',
                'label' => $this->lang->line("setting_school_currency_symbol"),
                'rules' => 'required|max:3'
            ),
            array(
                'field' => 'email_list',
                'label' => $this->lang->line("mailing_list"),
                'rules' => ''
            ),
            array(
                'field' => 'currency_rounding',
                'label' => $this->lang->line("setting_school_currency_rounding"),
                'rules' => 'required|min:1'
            ),
            array(
                'field' => 'show_bank',
                'label' => $this->lang->line("setting show_bank"),
                'rules' => 'max:1'
            ),
            array(
                'field' => 'sub_invoice',
                'label' => $this->lang->line("setting sub_invoice"),
                'rules' => 'max:1'
            ),
            array(
                'field' => 'footer',
                'label' => $this->lang->line("setting_school_footer"),
                'rules' => 'max:200'
            ),
            array(
                'field' => 'address',
                'label' => $this->lang->line("setting_school_address"),
                'rules' => 'required|max:200'
            ),
            array(
                'field' => 'api_key',
                'label' => 'karibuSMS API KEY',
                'rules' => 'required|max:200'
            ),
            array(
                'field' => 'api_secret',
                'label' => 'karibuSMS API SECRET',
                'rules' => 'required|max:200'
            ),
            array(
                'field' => 'website',
                'label' => 'School Website',
                'rules' => 'required|max:240'
            ),
            array(
                'field' => 'box',
                'label' => 'School P.O BOX',
                'rules' => 'required|max:240'
            ),
            array(
                'field' => 'roll_no_initial',
                'label' => 'Roll No Initial',
                'rules' => 'max:240'
            ),
            array(
                'field' => 'headname',
                'label' => 'Head of School Tittle',
                'rules' => 'max:240'
            ),
            array(
                'field' => 'motto',
                'label' => 'School Motto',
                'rules' => 'required|max:250'
            ),
            array(
                'field' => 'school_format',
                'lable' => 'school Curriculum',
                'rules' => 'max:25'
            ),
            array(
                'field' => 'online_admission',
                'lable' => 'Online Admission'
            ),
            array(
                'field' => 'other_learning_material',
                'lable' => 'Online Admission'
            ),
            array(
                'field' => 'sms_enabled',
                'label' => 'SMS Enabled',
                'rules' => 'max:2'
            ),
            array(
                'field' => 'email_enabled',
                'label' => 'Email Enabled',
                'rules' => 'max:2'
            ),
            array(
                'field' => 'sms_type',
                'label' => 'SMS Type',
                'rules' => 'max:2'
            ),
            array(
                'field' => 'sms_lang',
                'label' => 'Select Language'
            ),
            array(
                'field' => 'registration_number',
                'label' => 'Registration Number',
                'rules' => 'required'
            ),
            array(
                'field' => 'exam_avg_format',
                'label' => 'Exam average format',
                'rules' => 'max:2'
            )
            ,
            array(
                'field' => 'show_report_to',
                'label' => 'Show Report To',
                'rules' => 'max:2'
            ),
            array(
                'field' => 'custom_to',
                'label' => 'Custom To',
                'rules' => ''
            ),
            array(
                'field' => 'custom_to_amount',
                'label' => 'Custom to Amount',
                'rules' => ''
            ),
            array('field' => 'publish_exam',
                'label' => 'publish exam',
                'rules' => ''),
            array('field' => 'show_payment_plan',
                'label' => 'show_payment_plan',
                'rules' => ''),
            array('field' => 'enable_payment_delete',
                'label' => 'enable_payment_delete',
                'rules' => ''),
        );
        return $rules;
    }

    /**
     * 
     * create installments named LiveStudy_Month_name, put a status, 1, with specified start date
     * create a fee named LiveStudy and associate it with classes and installments
     * add amount in fee_installment_classes 
     * add create seperate invoice for parents to pay
     * now, create a background operations to sync invoice and notify parents
     * done
     */
    public function livestudySettings($student_id = null, $amount = null) {
        $academic_years = DB::select('select id from ' . set_schema_name() . ' academic_year where \'' . date("Y-m-d") . '\' between start_date and end_date');
        if (empty($academic_years)) {
            return redirect()->back()->with('warning', 'No Academic Year have been Defined in this current year');
        }
        foreach ($academic_years as $year) {
            //$check = \App\Model\Installment::where('name', 'LiveStudy ' . date('M'))->where('academic_year_id', $year->id)->count();

            $check = \collect(DB::select("select * from " . set_schema_name() . " installments where  academic_year_id=" . $year->id . " and lower(name) like '%livestudy%' and CURRENT_DATE BETWEEN start_date and end_date"))->count();




            if ($check < 1) {
                \App\Model\Installment::create(array("name" => 'LiveStudy ' . date('M'), "start_date" => date("Y-m-d", strtotime(request("start_date"))), "end_date" => date("Y-m-d", strtotime("+1 month", strtotime(request("start_date")))), "academic_year_id" => $year->id, 'status' => '2'
                ));
            }
        }

        $check_fee = \App\Model\Fee::where('name', 'LiveStudy Fee')->first();

        //$insts = \App\Model\Installment::where("name", 'LiveStudy ' . date('M'));
        $inst = \collect(DB::select("select * from " . set_schema_name() . " installments where  academic_year_id=" . $year->id . " and lower(name) like '%livestudy%' and CURRENT_DATE BETWEEN start_date and end_date"))->first();
        if (empty($check_fee)) {
            $fee = new \App\Http\Controllers\Fee();
            $fee->add();
            \DB::table('fees')->where('name', 'LiveStudy Fee')->update(['id' => 3000]); //update fee id to be 3000
            //------------Done with Fee Creation --------------------//
        } else if (!empty($check_fee) && $check_fee->id != 3000) {
            \App\Model\Fee::where('name', 'LiveStudy Fee')->update(['id' => 3000]);
        }
        $fee_detail = new \App\Http\Controllers\Fee_detail();


        $classes = \App\Model\Classes::whereIn('classlevel_id', \App\Model\AcademicYear::where('id', $inst->academic_year_id)->get(['class_level_id']))->get();
        foreach ($classes as $class) {
            $fee_detail->add_installment_amount($class->classesID, 3000, $inst->id, $amount);
        }


        //-------------------done with amount allocation-----------//
        //now create invoice
        $where = $student_id == null ? '' : 'and a.student_id=' . $student_id;
        $cond = '';
        if (set_schema_name() == 'rightwayschools.') {
            $cond = ' and a.student_id in (select a.student_id from (select student_id, sum(balance) as balance from rightwayschools.invoice_balances where academic_year_id in (11,12) and installment_id in (29,30,25,26) and fee_id <>3000  group by student_id order by student_id) a join student b using(student_id) where balance =0)';
        }
        $fee_installment_ids = DB::select("select distinct a.student_id,a.name as student_name, a.academic_year_id, fees_installment_id,installment_id from " . set_schema_name() . "student a join " . set_schema_name() . "student_all_fees_subscribed b on (a.student_id=b.student_id and a.academic_year_id=b.academic_year_id) where b.fee_id=3000 and a.status=1 and installment_id= " . $inst->id . " and a.student_id not in (select student_id from " . set_schema_name() . "invoices where reference like '%TZ%')  " . $where . " " . $cond . " order by student_id");



        if (!empty($fee_installment_ids)) {
            $max_invoice = \collect(DB::SELECT('select max(id) as max_id from invoices'))->first();
            $bank = \collect(DB::SELECT('select a.* from ' . set_schema_name() . ' bank_accounts_integrations a join ' . set_schema_name() . ' bank_accounts_fees_classes b on b.bank_account_id=a.bank_account_id  join ' . set_schema_name() . 'fees_classes c on c.id=b.fees_classes_id order by c.fee_id desc'))->first();
            if (!empty($max_invoice) && !empty($bank)) {
                $invoice_object = [];
                $i = 1;
                foreach ($fee_installment_ids as $fees_installment) {

                    $reference = $bank->invoice_prefix . ($max_invoice->max_id + 230 + $i);
                    $array = array(
                        'student_id' => $fees_installment->student_id,
                        'academic_year_id' => $fees_installment->academic_year_id,
                        'created_at' => 'now()',
                        'status' => 0,
                        'token' => null,
                        'amount' => request('amount'),
                        'date' => 'now()',
                        'reference' => $reference . 'TZ',
                        'prefix' => $bank->invoice_prefix
                    );
                    $invoice = \App\Model\Invoice::create($array);
                    $check_fee_existance = \App\Model\InvoicesFeesInstallment::where('invoice_id', $invoice->id)->where('fees_installment_id', $fees_installment->fees_installment_id)->count();
                    if ($check_fee_existance == 0) {
                        DB::table('invoices_fees_installments')->insert(['fees_installment_id' => $fees_installment->fees_installment_id, 'invoice_id' => $invoice->id]);
                    }
                    array_push($invoice_object, $array);
                    $i++;
                }
            } else {
                echo ('Please Check if Bank Intergration Setup is done properly. This part supports only schools with ');
            }
        }
    }

    public function index() {

        $usertype = $this->session->userdata("usertype");
        if (can_access('view_system_setting')) {

            $this->data['setting'] = \App\Model\Setting::first();
            $this->data['languages'] = DB::table('constant.languages')->get();
            $this->data['client'] = DB::table('admin.clients')->where('username', str_replace('.', NULL, set_schema_name()))->first();
            $this->data['nmb_connection'] = DB::table('admin.integration_requests')->where('refer_bank_id', 22)->where('client_id', $this->data['client']->id)->first();
            $this->data['crdb_connection'] = DB::table('admin.integration_requests')->where('refer_bank_id', 8)->where('client_id', $this->data['client']->id)->first();
            $this->data['academic_years'] = $this->academic_year_m->get_all_years();
            $this->data['class_levels'] = $this->classlevel_m->get_classlevel();

            if (is_object($this->data['setting'])) {

                if ($_POST) {

                    if (strlen(request('karibusms_password')) > 3) {

                        $number = validate_phone_number(preg_replace('/[^0-9]/', '', request('karibusms_phone_number')));
                        $phone_number = $number[1] == null ? rand(245, 2457) . time() : $number[1];
                        $schema = str_replace('.', null, set_schema_name());
                        $check_phone = DB::connection('karibusms')->table('client')->where('phone_number', ltrim($phone_number, '+'))->first();
                        if (!empty($check_phone)) {
                              DB::connection('karibusms')->table('client')->where('keyname', $schema)->update([
                                'password' => sha1(md5(sha1(request('karibusms_password')))),
                            ]);
                        } else {
                            DB::connection('karibusms')->table('client')->where('keyname', $schema)->update([
                                'phone_number' => ltrim($phone_number, '+'),
                                'password' => sha1(md5(sha1(request('karibusms_password')))),
                            ]);
                        }
                        return redirect()->back()->with('success', 'success');
                    }
                    $posted_data = array_keys($_POST);
                    $rules = $this->rules();

                    foreach ($rules as $key => $value) {
                        if (!in_array($value['field'], $posted_data)) {
                            unset($rules[$key]);
                        }
                    }

                    if ((int) request('sub_invoice') == 1) {
                        DB::table('invoices')->where('sync', 1)->update(['sync' => 0]);
                    }

                    if (request('online_setting') == 1) {
                        $enable_live = (int) request('enable_parent_charging') == 1 ? 1 : 0;
                        $collection_method = (int) request('collection_method') == 1 ? 1 : 0;
                        $enable_self_registration = (int) request('enable_self_registration') == 1 ? 1 : 0;
                        DB::table('setting')->update(['enable_parent_charging' => $enable_live, 'collection_method' => $collection_method, 'enable_self_registration' => $enable_self_registration]);
                        if ((int) request('amount') > 0) {
                            $this->livestudySettings();
                            return redirect()->back()->with('success', 'Information Updated, Amount and date are not be updated');
                        } else {
                            return redirect()->back()->with('success', 'success');
                        }
                    }

                    if (request('show_report_to') == 0) {
                        $custom_to = request('custom_to');

                        DB::table('setting')->update(['custom_to' => $custom_to, 'show_report_to' => 0]);
                    } else {

                        DB::table('setting')->update(['show_report_to' => 1]);
                    }


                    $array = array();
                    $rules = reset_keys($rules);

                    for ($i = 0; $i < count($rules); $i++) {
                        $array[$rules[$i]['field']] = request($rules[$i]['field']);
                        if (request('exam_avg_format') == 1) {

//$this->optimize_tabledb();
                        }
//                        if (request('enable_payment_delete') == 1) {
//                            $q = DB::select("SELECT *  FROM pg_catalog.pg_rules where rulename='payment_del_protect' and schemaname='" . str_replace('.', '', set_schema_name()) . "'");
//                            if (!empty($q) > 0) {
//                                DB::select('DROP Rule payment_del_protect ON ' . set_schema_name() . 'payments');
//                            }
//                        } else if (request('enable_payment_delete') == 0) {
//                            DB::select('CREATE OR REPLACE RULE payment_del_protect AS  ON DELETE TO ' . set_schema_name() . 'payments DO INSTEAD NOTHING');
//                        }
                    }

                    if (isset($_FILES["image"]['name'])) {

                        $upload = $this->uploadImage();
                        $array['photo'] = $upload;

                        if ($upload == FALSE) {
                            $this->data["image"] = $this->upload->display_errors();
                            $this->data["subview"] = "setting/index";
                            $this->load->view('_layout_main', $this->data);
                        } else {

                            //$data = array("upload_data" => $this->upload->data());
                            \DB::table('setting')->update($array);
                            if (isset($array['username'])) {
                                session(['username' => $array['username']]);
                            }

                            $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                            return redirect(base_url("setting/index"));
                        }
                    } else {
                        if (isset($array['show_payment_plan'])) {
                            $show_p_array = array(
                                'show_payment_plan' => $array['show_payment_plan'],
                                'show_banks' => $array['show_bank']
                            );
                            DB::table('invoice_settings')->update($show_p_array);
                        }

                        //DB::table('setting')->update($array);

                        \DB::table('setting')->update($array);
                        if (isset($array['username'])) {
                            session(['username' => $array['username']]);
                        }
                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                        return redirect(base_url("setting/index"));
                    }
                } else {
                    $this->data["subview"] = "setting/index";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function optimize_tabledb() {

        //important consideration to ensure data are in the correct format

        /**
         * 1. Ensure table student_subject is filled with the correct data matched with mark table
         * a) Subjects that are options, are matched with mark table
          b) Subjects that are core, are not in option tables
         */
//$this->db->query();
        /**
         * 2. Ensure all students in this academic year are in student_archive table
         */
        /**
         * 3. Ensure core subjects are not in student_subject table and option subjects are not in core subject subscription
         */
        $this->db->query('delete from ' . set_schema_name() . 'subject_section where subject_id IN (select "subjectID" from ' . set_schema_name() . "subject where subject_type='Option')");


        //option for core subjects also
        $this->db->query('delete from ' . set_schema_name() . 'subject_student where subject_id IN (select "subjectID" from ' . set_schema_name() . "subject where subject_type='Core')");

        /**
         * 4. Ensure all student section match with section ID in student table
         */
        $this->db->query('UPDATE ' . set_schema_name() . 'student a set section=(select section from ' . set_schema_name() . 'section where "sectionID"=a."sectionID")');

        /**
         * procedures to optimize report
          1. check if option subjects are speciefied as core subjects in subject section, if yes, delete them
          2. check if core subjects are in subject student table, if yes, delete them

          3. check if student is in mark table for certain option subject but that student id is not in subject student table. if yes, add that student in subject student table

          //this sql get all subjects and students that are core subjects but are not allocated in any section
         */
        $core_status = $this->db->query('select * from  ' . set_schema_name() . 'mark where "subjectID" IN (SELECT "subjectID" FROM  ' . set_schema_name() . "subject WHERE subject_type='core') AND " . NULL . ' "subjectID" NOT IN (SELECT subject_id FROM  ' . set_schema_name() . 'subject_section)');
        if (!empty($core_status)) {

            //we have a problem for that subject, we need to specify that subject is taken by which sections.
            //login into the system and specify this subject is taken by which section
        }


// core subjects that appear in student subject subscription 
        $core_in_option_status = $this->db->query('select * from  ' . set_schema_name() . 'mark where "subjectID" IN (SELECT "subjectID" FROM  ' . set_schema_name() . "subject WHERE subject_type='Core') AND " . NULL . ' "subjectID" IN (SELECT subject_id FROM  ' . set_schema_name() . 'subject_student)');
        if (!empty($core_in_option_status)) {

            //we have a problem for that subject, we need to remove core subjects that appear in student subject table
        }

//viceversa for option subject

        $option_status = $this->db->query('select * from  ' . set_schema_name() . 'mark where "subjectID" IN (SELECT "subjectID" FROM  ' . set_schema_name() . "subject WHERE subject_type='Option') AND  " . NULL . '  "subjectID" NOT IN (SELECT subject_id FROM  ' . set_schema_name() . 'subject_student)');
        if (!empty($option_status)) {

            //we have a problem for that subject, we need to specify those students that they study this option subject
            $this->db->query('INSERT INTO ' . set_schema_name() . 'subject_student (subject_id, student_id, academic_year_id)
select "subjectID","student_id", academic_year_id from ' . set_schema_name() . 'mark where "subjectID" IN (SELECT "subjectID" FROM ' . set_schema_name() . "subject WHERE subject_type='Option') AND " . NULL . '  "subjectID" NOT IN (SELECT subject_id FROM ' . set_schema_name() . 'subject_student)');
        }

        /**

          4. do the same in source codes and remember to update or delete when conditions changes. for example, a certain subject is not counted as option subject
         */
//$this->db->query("delete from ".set_schema_name()."subject_section where subject_id IN (select "\"subjectID"\" from ".set_schema_name()."subject where subject_type='Option'");
//$this->db->query('delete from '.set_schema_name().'subject_section where subject_id IN (select '\"subjectID"\' from '.set_schema_name().'subject where subject_type="Option"');
    }

    public function permissionUpdate() {
        $status = clean_htmlentities($this->uri->segment(3));
        $id = clean_htmlentities($this->uri->segment(4));
        if (strtolower($status) == 'delete') {
            $this->db->query('delete from ' . set_schema_name() . 'role where id=' . $id);
            $this->session->set_flashdata('success', $this->lang->line('menu_success'));
            return redirect(base_url("setting/permission"));
        } else {
            $name = request('name');
            $role_id = request('id');
            $this->db->query('update ' . set_schema_name() . 'role set name=\'' . $name . '\' where id=' . $role_id);
            echo 'Name Changed';
        }
    }

    public function permission() {
        $this->load->model('permission_m');
        $usertype = $this->session->userdata("usertype");
        $id = clean_htmlentities($this->uri->segment(3));
        if (isset($_POST) && $_POST) {
            $name = strtolower(request('role_name'));
            $find = DB::select('select * from ' . set_schema_name() . 'role where lower(name)=\'' . $name . '\'');
            if (!$find) {
                DB::table('role')->insert(array('name' => ucfirst($name)));
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
            } else {
                $this->session->set_flashdata('error', 'Name Exists');
            }
        }
        if ((int) $id) {
            $sql = 'SELECT * from constant.permission a where a.id IN (select permission_id FROM ' . set_schema_name() . 'role_permission WHERE role_id=(select id FROM ' . set_schema_name() . 'role where id=' . $id . ' ) ) order by arrangement';
        } else {
            $sql = 'SELECT * from constant.permission a where a.id IN (select permission_id FROM ' . set_schema_name() . 'role_permission WHERE role_id=(select id FROM ' . set_schema_name() . 'role where lower(name)=\'' . strtolower($usertype) . '\'  )) order by arrangement';
        }
        $this->data['user_permissions'] = DB::select($sql);
        $this->data['user_permission_groups'] = DB::select("select * from constant.permission_group where lower(name) not like '%trainings%' ");
        $this->data['permissions'] = DB::select('select * from constant.permission order by id asc');
        $this->data['user_role'] = DB::select('select * from ' . set_schema_name() . 'role ');
        $this->data["subview"] = "setting/permission";
        $this->load->view('_layout_main', $this->data);
    }

    public function addPermission() {
        if ((int) request('group_id') > 0) {
            $permissions = \App\Model\Permission::where('permission_group_id', request('group_id'))->get(['id']);
            \App\Model\RolePermission::whereIn('permission_id', $permissions)->where('role_id', request('role_id'))->delete();
            foreach ($permissions as $permission) {
                \App\Model\RolePermission::create(array(
                    'role_id' => request('role_id'),
                    'permission_id' => $permission->id
                ));
            }
            echo 'success';
        } else {
            \App\Model\RolePermission::create(array(
                'role_id' => request('role_id'),
                'permission_id' => request('id')
            ));
            echo 'success';
        }
    }

    public function removePermission() {
        if ((int) request('group_id') > 0) {
            $permissions = \App\Model\Permission::where('permission_group_id', request('group_id'))->get(['id']);
            \App\Model\RolePermission::whereIn('permission_id', $permissions)->delete();
            echo 'success';
        } else {
            \App\Model\RolePermission::where(array(
                'role_id' => request('role_id'),
                'permission_id' => request('id')
            ))->delete();
            echo 'success';
        }
    }

    public function siteImage() {
        if (isset($_FILES["image"]['name'])) {
            $setting = \App\Model\Setting::first();
            $upload = $this->uploadImage();
            $array['photo'] = $upload;

            if ($upload == FALSE) {
                $this->session->set_flashdata('error', 'Image upload error');
                return redirect(base_url("setting/index"));
            } else {
                \DB::table('setting')->update($array);
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url("setting/index"));
            }
        }
    }

    public function academic_year() {
        $data = array();
        parse_str(request('formdata'), $data);
        //check if data exist
        unset($data['_token']);
        $check = \App\Model\AcademicYear::where(array('name' => $data['name'], 'class_level_id' => $data['class_level_id']))->get();
        if (count($check) == 0) {
            $start_date = date('Y-m-d', strtotime($data['start_date']));
            $end_date = date('Y-m-d', strtotime($data['end_date']));
            if (strtotime($start_date) >= strtotime($data['end_date'])) {
                die('Please correct dates. Start date must be less than end date');
            }
            $id = DB::table('academic_year')->insertGetId(array('name' => $data['name'], 'class_level_id' => $data['class_level_id'], 'start_date' => $start_date, 'end_date' => $end_date));
            if ($id > 0) {
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
            } else {
                echo 'Academic year failed to be added. Please try again later';
            }
        } else {
            echo 'Academic year NAME of the same class level already exists';
        }
    }

    public function system_deadline() {

        $data = array();
        parse_str(request('formdata'), $data);
        //check if data exist
        unset($data['_token']);

        $payment_deadline_date = date('Y-m-d', strtotime($data['payment_deadline_date']));
        DB::table('setting')->update(['payment_deadline_date' => $payment_deadline_date]);
        echo 'Deadline set successfully';
    }

    public function account_setting() {
        $data = array();
        parse_str(request('formaccountdata'), $data);

        //check if data exist
        unset($data['_token']);
//print_r($data);exit;
        // echo $data['academic_level'];exit;


        if (count($data) > 0) {
            DB::SELECT('SELECT * FROM ' . set_schema_name() . ' shift_installments(' . $data['to_academic_id'] . ',' . $data['academic_level'] . ')');

            DB::SELECT('SELECT * FROM ' . set_schema_name() . ' transfer_transport_members(' . $data['from_academic_id'] . ',' . $data['to_academic_id'] . ')');

            echo '1';
        } else {
            echo 'No data selected';
        }
    }

    public function edit_academic() {
        $id = clean_htmlentities(($this->uri->segment(3)));
        $this->data['academic_year'] = $this->academic_year_m->get_single_year(array('id' => $id));
        if ($_POST) {
            // DB::enableQueryLog();
            $this->validate(request(), [
                'class_level_id' => 'required|numeric|min:1',
                'name' => 'required',
                'start_date' => 'date|required',
                'end_date' => 'date|required|after:start_date'
                    ], $this->custom_validation_message);
            $check = \App\Model\AcademicYear::where(array('name' => request('name'), 'class_level_id' => request('class_level_id')))->where('id', '!=', $id)->get();
            if (count($check) > 0) {
                return redirect()->back()->with('error', 'Academic Name must be unique for class level');
            }

            //dd(DB::getQueryLog());
            $this->academic_year_m->update_academic(request()->except('_token'), $id);
            $this->session->set_flashdata('success', $this->lang->line('menu_success'));
            return redirect(base_url('setting/index#academic'));
        } else {
            $this->data['class_levels'] = $this->classlevel_m->get_classlevel();
            $this->data["subview"] = "setting/academic_edit";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function definition() {
        $this->data['setting'] = \App\Model\Setting::first();
        $this->data['academic_years'] = \App\Model\AcademicYear::all();
        $this->data['class_levels'] = \App\Model\ClassLevel::all();
        $this->data["subview"] = "setting/definition";
        $this->load->view('_layout_main', $this->data);
    }

    public function getDefinitions() {
        $required_table = array(
            'refer_countries', 'refer_cities', 'refer_health_conditions', 'refer_religions', 'refer_professions'
        );
        $table = request('pg');
        if ($table == 'refer_cities') {
            $sql = 'select a.city, b.country FROM constant.refer_cities a JOIN constant.refer_countries b on b.id=a.countryid';
        } else {
            $sql = 'select * from constant.' . $table;
        }

        $contents = in_array($table, $required_table) ? DB::select($sql) : array();
        return $contents ? view('setting.definition_content', compact('contents')) : 0;
    }

    public function signature() {
        $this->data["signature"] = $this->retrieve_signature();
        $this->data["subview"] = "setting/signature";
        $this->load->view('_layout_main', $this->data);
    }

    public function upload_signature() {
        $this->data["signature"] = $this->retrieve_signature();
        $this->data["subview"] = "setting/upload_signature";
        $this->load->view('_layout_main', $this->data);
    }

    public function upload_stamp() {

        if (request()->file('stamp')) {

            // $this->validate(\request(), ['stamp' => 'max:300'], ['photo' => 'The stamp size must be less than 300KB']);
            $filename = 'stamp_' . time() . rand(11, 8844) . '.' . request()->file('stamp')->guessExtension();
            Image::make(request()->file('stamp'))->resize(300, 300)->save('storage/uploads/images/' . $filename);
            \App\Model\Classlevel::find(request('classlevel_id'))->update(['stamp' => $filename]);
            return redirect()->back()->with('success', 'Your stamp has been successfully uploaded!');
        }
        return redirect()->back()->with('warning', 'Please add image first');
    }

    public function upload_certificate() {

        if (Input::hasFile('stamp')) {

            //$this->validate(\request(), ['stamp' => 'max:300'], ['photo' => 'The stamp size must be less than 300KB']);
            $file = request()->file('stamp');

            $filename = str_replace('.', null, set_schema_name()) . '_' . request('classlevel_id') . '_certificate' . '.' . request()->file('stamp')->guessExtension();
            $filepath = 'storage/app/' . $filename;
            if (file_exists($filepath)) {
                unlink($filepath);
            } else {
                echo 'not file';
                exit;
            }
            $file->move('storage/app/', $filename);
            \App\Model\Classlevel::find(request('classlevel_id'))->update(['leaving_certificate' => $filename]);
            return redirect()->back()->with('success', 'Your stamp has been successfully uploaded!');
        }
        return redirect()->back()->with('warning', 'Please add image first');
    }

    public function save_signature() {
        $signature = str_replace(' ', '+', request('signature'));
        $file_name = time() . ".png";
        $table = session("table");
        $id = session("id");
        $table_column = session("table") == 'student' ? 'student_id' : session('table') . 'ID';
        $update = DB::table($table)->where([$table_column => $id])->update(['signature' => $signature, 'signature_path' => $file_name]);
        if ($update == 1) {
            echo "SUCCESS";
        } else {
            echo "ERROR";
        }
    }

    public function retrieve_signature() {
        $table = session("table");
        $id = session("id");
        $table_column = session("table") == 'student' ? 'student_id' : session('table') . 'ID';
        return  DB::table($table)->where([$table_column => $id])->first()->signature;
        //\DB::table('setting')->first()->signature;        
    }

    public function pcode_validation($pcode) {

        return TRUE;
    }

    public function show() {
        $p = (new \App\Http\Controllers\Profile())->index();
    }

    protected function priority_rules() {
        $rules = array(
            array(
                'field' => 'fee_priority',
                'label' => $this->lang->line("fees_priority"),
                'rules' => 'required'
            )
        );
        return $rules;
    }

    Function set_fee_priority() {

        if (can_access('view_feetype')) {
            $this->data['feetypes'] = $this->fee_m->get_fee_by_priority();
            $this->data['fees'] = $this->fee_m->get_fee();
            if ($_POST) {
                $rules = $this->priority_rules();
                $this->form_validation->set_rules($rules);
                if ($this->form_validation->run() == FALSE) {
                    $this->data["subview"] = "setting/set_fee_priority";
                    $this->load->view('_layout_main', $this->data);
                } else {
                    $hidden = $_POST['multiple_value']; //get the values from the hidden field
                    $hidden_in_array = explode(",", $hidden); //convert the values into array
                    $filter_array = array_filter($hidden_in_array); //remove empty index
                    $fee_priority = array_values($filter_array); //reset the array key

                    foreach ($fee_priority as $key => $value) {
                        DB::table('fee')->where('id', $value)->update(['priority' => $key]);
                    }
                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                    return redirect(base_url("setting/set_fee_priority"));
                }
            } else {
                $this->data["subview"] = "setting/set_fee_priority";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function replace_empty_mark() {
        $empty_mark['empty_mark'] = request('empty_mark');
        \DB::table('setting')->update($empty_mark);
        return $empty_mark;
    }

    public function daily_report() {
        // users
        $this->data['users'] = DB::table('users')->where('status', 1)->count();
        $this->data['added_users'] = DB::table('parent')->where(DB::raw('created_at::date'), date('Y-m-d'))->count() + DB::table('teacher')->where(DB::raw('created_at::date'), date('Y-m-d'))->count() + DB::table('student')->where(DB::raw('created_at::date'), date('Y-m-d'))->count();
        //logs
        $this->data['logs'] = DB::table('log')->where(DB::raw('created_at::date'), date('Y-m-d'))->count();
        $this->data['log_parents'] = DB::table('log')->where(DB::raw('created_at::date'), date('Y-m-d'))->where('table', 'parent')->count();
        $this->data['sms'] = DB::table('sms')->where(DB::raw('created_at::date'), date('Y-m-d'))->count();
        $this->data['email'] = DB::table('email')->where(DB::raw('created_at::date'), date('Y-m-d'))->count();
        $this->data['revenue'] = \collect(DB::select("select sum(amount) from " . set_schema_name() . "total_revenues where date::date='" . date('Y-m-d') . "'"))->first();
        $this->data['expense'] = \collect(DB::select("select sum(amount) from " . set_schema_name() . "total_revenues where date::date='" . date('Y-m-d') . "'"))->first();
        $sql = "select  c.name as parent_name, d.classes, a.dob,a.\"classesID\", a.section, a.\"student_id\", a.name as student_name,c.phone as parent_phone FROM " . set_schema_name() . "student a join " . set_schema_name() . "student_parents b on b.student_id=a.\"student_id\" JOIN " . set_schema_name() . "parent c on c.\"parentID\"=b.parent_id join " . set_schema_name() . "classes d on d.\"classesID\"=a.\"classesID\" WHERE 
                    DATE_PART('day', a.dob) = date_part('day', CURRENT_DATE) 
                    AND DATE_PART('month', a.dob) = date_part('month', CURRENT_DATE)";
        $this->data['birthday'] = count(DB::select($sql));

        return view('email.daily_report', $this->data);
    }

    public function update_transaction() {
        \App\Model\Setting::first()->update(['transaction_charges_to_parents' => request('transaction_charges_to_parents')]);
        $status = request('transaction_charges_to_parents') == 1 ? 'Parent' : 'School';
        echo '<span class="label label-success">Success : ' . $status . ' will be charged</span>';
    }

    public function testPassword() {
        $pass = rand(1, 999) . substr(str_shuffle('abcdefghkmnp'), 0, 3);
        $password = bcrypt($pass);
        $user_info = DB::table($users->table)->where($users->table . 'ID', $users->id);
        $user_info->update(['password' => $password, 'default_password' => $pass]);
        $parents = DB::select('select  p.* FROM parent p, setting s where p."parentID" NOT IN (SELECT user_id from log where user_id is not null and "user"=\'Parent\') and p.status=1 and p."parentID" IN (
SELECT parent_id from student_parents where student_id in (select "student_id" from student_exams ) )');
        foreach ($parents as $parent) {
            if (Auth::attempt(['username' => $parent->username, 'password' => $parent->default_password])) {
                echo 1;
            } else {
                return $this->sendDefaultPassword($users);
            }
        }
    }

    public function addcategory() {
        switch (request('type')) {
            case 'physical_condition':
                \App\Model\PhysicalCondition::create(request()->all());
                $requests = \App\Model\PhysicalCondition::all();
                echo ' <select name="physical_condition_id">';
                foreach ($requests as $value) {
                    echo '<option value="' . $value->id . '">' . $value->name . '</option>';
                }
                echo '</select>';
                break;
            case 'profession_id':
                \App\Model\ReferProfession::create(request()->all());
                $requests = \App\Model\ReferProfession::all();
                echo ' <select name="profession_id">';
                foreach ($requests as $value) {
                    echo '<option value="' . $value->id . '">' . $value->name . '</option>';
                }
                echo '</select>';
                break;
            case 'insurance':
                \App\Model\HealthInsurance::create(request()->all());
                $requests = \App\Model\HealthInsurance::all();
                echo ' <select name="health_insurance_id">';
                foreach ($requests as $value) {
                    echo '<option value="' . $value->id . '">' . $value->name . '</option>';
                }
                echo '</select>';
                break;

            default:
                break;
        }
    }

    public function CallUsers() {
        $usertype_id = request('usertype_id');

        $users = DB::select('select * from ' . set_schema_name() . ' users where role_id=' . $usertype_id . '');

        if (empty($users)) {
            echo '0';
        } else {
            echo "<option value='" . 0 . "'>All</option>";

            foreach ($users as $user) {
                echo "<option value='" . $user->id . "'>" . $user->name . "</option>";
            }
        }
    }

    public function createApiKeys() {

        //
//        $karibu_username = request('username');
//        $karibu_password = request('password');
        $schema = str_replace('.', null, set_schema_name());
//        $password = sha1(md5(sha1($karibu_password)));
//        $number = validate_phone_number($karibu_username);

        $results = DB::connection('karibusms')->table('client')->where('keyname', $schema)->first();

        if (!empty($results)) {

            //this user is successfully login
            $app_name = str_replace('.', null, set_schema_name()) . '_' . $results->phone_number;
            $app_info = DB::connection('karibusms')->table('developer_app')->where('name', $app_name)->first();

            if (!empty($app_info)) {
                $api_key = $app_info->api_key;
                $api_secret = $app_info->api_secret;
            } else {
                $api_key = time() * date('y');
                $api_secret = sha1(md5($api_key));
                DB::connection('karibusms')->table('developer_app')->insert([
                    'name' => $app_name,
                    'client_id' => $results->client_id,
                    'api_key' => $api_key,
                    'api_secret' => $api_secret
                ]);
            }
            DB::table('sms_keys')->insert(['api_key' => $api_key,
                'api_secret' => $api_secret, 'phone_number' => $results->phone_number]);
            echo 1;
        } else {

            echo '<b class="alert alert-danger">Wrong username or password</b>';
        }
    }

    public function unlinkPhone() {
        $id = request()->segment(3);
        DB::table('sms_keys')->where('id', $id)->delete();
        return redirect()->back()->with('success', 'success');
    }

    public function pageTipView() {
        if (strlen(request('page')) > 3) {
            $check_tips = \DB::table('admin.page_tips')->where('controller_page', str_replace('@', '/', request('page')))->first();
            if (!empty($check_tips)) {
                $check = DB::table('page_tips_viewers')->where('user_id', session('id'))->where("table", session('table'))->where('page_tip_id', $check_tips->id)->first();
                if (empty($check)) {
                    //insert
                    DB::table('page_tips_viewers')->insert(array(
                        'user_id' => session('id'),
                        "table" => session('table'),
                        'is_helpful' => (int) request('status'),
                        'page_tip_id' => $check_tips->id));
                    echo 'insert';
                } else {
                    DB::table('page_tips_viewers')->where('user_id', session('id'))->where("table", session('table'))->where('page_tip_id', $check_tips->id)->update(['is_helpful' => (int) request('status')]);
                    echo 'updated';
                }
            }
        } else {
            return DB::table('page_tips_viewers')->insert(array(
                        'user_id' => session('id'),
                        "table" => session('table'),
                        'page_tip_id' => request('id')));
        }
    }

}
