<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;
use App\Model\Tour;
use App\Model\User;

class Signin extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {

        parent::__construct();
   
    }

    protected $redirectTo = 'dashboard/index';
    public $timestamps = false;

    protected function rules() {
        $this->validate(\request(), ['username' => 'required', 'password' => 'required'], $this->custom_validation_message);
    }

    public function reset_password() {
        $this->data['form_validation'] = "";
        $this->data['siteinfos'] = $this->reset_m->get_admin();
        $number1 = rand(1, 9);
        $number2 = rand(1, 9);

        $this->data['firstnumber'] = $number1;
        $this->data['secondnumber'] = $number2;

// !isset($_POST) ? $this->load->view('reset/index', $this->data) : '';

        if ($_POST) {

            $this->rules();
            $first_number = request('firstNumber');
            $second_number = request('secondNumber');
            $capture_number = request('answer');
            $sum = $first_number + $second_number;

            if ($capture_number == $sum) {
// we have validated that this is a valid user, so let us reset pswd
                $phone = request('phone');
                $valid = validate_phone_number($phone);


                if (count($valid) == 2) {
                    $users = \App\Model\User::where('phone', $valid[1])->where('table', '!=', 'student')->first();
                } else {
                    $users = array();
                }

                if (!empty($users)) {
                    $this->sendDefaultPassword($users);
                    return redirect('signin/new_login')->with('success', $this->lang->line('reset_success'));
                } else {


                    $super_user = \App\Model\Setting::where('phone', $phone)->first();


                    if (!empty($super_user)) {
                        $this->sendDefaultPassword($super_user);
                        return redirect('signin/new_login')->with('reset_success', $this->lang->line('reset_success'));
                    } else {
                        $this->data['form_validation'] = 'Phone number is not found!';
                        return $this->load->view('reset_password', $this->data);
                    }
                }
            } else {
                $this->data['form_validation'] = 'Please write a valid answer in calculation';
                return $this->load->view('reset_password', $this->data);
            }
        } else {
            return $this->load->view('reset_password', $this->data);
        }
    }

    public function index() {

        $this->lang->load('topbar_menu');
        if (request('action') == 'google') {
            return $this->tokenSignin();
        }
        $id = clean_htmlentities(($this->uri->segment(3)));
        $number1 = rand(1, 9);
        $number2 = rand(1, 9);
        $this->data['random_string'] = random_string('alnum', 4);
        $this->data['firstnumber'] = $number1;
        $this->data['secondnumber'] = $number2;

        if ($id == 'test') {
// return $this->testCorrectAuth();
        } else {
            if ($_POST) {

                $this->rules();
                $old_password = trim(request('password'));
                $username = trim(request('username'));
                $with_username = array('username' => $username, 'password' => $old_password);

                if ($this->loginByUsername($with_username) == true) {

                    return $this->logUser();
                } else if ($this->loginByPhone() == true) {
// Auth::logoutOtherDevices($old_password);
                    return $this->logUser();
                } else if ($this->loginByEmail() == true) {

// Auth::logoutOtherDevices($old_password);
                    return $this->logUser();
                } else {
                    DB::table('login_attempts')->insert(array('username' => request('username'), 'wrong_password' => request('password')));

                    return back()->with(['username' => $username, 'error' => 'Wrong Password or Username.If you don\'t remember your password, please click on the forgot password above']);
                }
            } else {

                if ((int) session('id') > 0) {
                    return redirect()->intended('dashboard/index')->header('Cache-Control', 'no-store');
                } else {

//$this->data["subview"] = "signin/index";
                    return view('signin.signin', $this->data);
                }
            }
        }
    }

    public function updatePassword($user, $new_password) {
        return DB::table($user->table)->where($user->table . "ID", $user->id)->update(['password' => bcrypt($new_password)]);
    }

    public function logUser() {

        $data = array("id", "usertype", "username", "table");
        foreach ($data as $value) {
            if (isset(Auth::user()->{$value})) {
                session([$value => Auth::user()->{$value}]);
                $this->createCookie($value, Auth::user()->{$value});
            }
        }
        session(['loggedin' => 1]);
        if (isset(Auth::user()->id)) {
            $this->saveUserLocation(Auth::user()->id, Auth::user()->table);
        }
        $url = session('previus_url') != null && !preg_match('/signin/i', session('previus_url')) ? session('previus_url') : 'dashboard/index';
        session('previus_url', NULL);
        return redirect()->intended($url);
    }

    public function loginByUsername($with_username) {

        if (Auth::attempt($with_username, false)) {

            if ((int) Auth::user()->status <> 1 && strtolower(Auth::user()->table) != 'parent') {
                Auth::logout();
                return redirect()->back()->with('error', 'Sorry, your account has been disabled. Please contact your Administrator or call us for more information');
            } else {


//            if(Auth::user()->table == 'student')  {  
//            Auth::logoutOtherDevices($with_username['password']); 
//            
//            }  

                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    public function loginByPhone() {
        $valid_phone = validate_phone_number(request('username'));
        if (!empty($valid_phone) <> 2) {
            return false;
        }
        $phone = $valid_phone[1];
        $with_phone = array('phone' => $phone, 'password' => trim(request('password')));
        if (Auth::attempt($with_phone, false)) {

            if ((int) Auth::user()->status <> 1 && strtolower(session('table')) != 'parent') {
                Auth::logout();
                return back()->with('warning', 'Sorry, your account has been disabled. Please contact your Administrator or call us for more information');
            }
            if (Auth::user()->table == 'student') {
                Auth::logoutOtherDevices($with_phone['password']);
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function loginByEmail() {
        $valid_email = filter_var(request('username'), FILTER_VALIDATE_EMAIL);
        if (!$valid_email) {
            return false;
        }
        $with_email = array('email' => $valid_email, 'password' => trim(request('password')));
        if (Auth::attempt($with_email)) {
            if ((int) Auth::user()->status <> 1 && strtolower(session('table')) != 'parent') {
                Auth::logout();
                return back()->with('warning', 'Sorry, your account has been disabled. Please contact your Administrator or call us for more information');
            }

            if (Auth::user()->table == 'student') {
                Auth::logoutOtherDevices($with_email['password']);
            }

            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function cpassword() {
        $this->data['success'] = '';
        $this->data['form_validation'] = 'No';
        if ($_POST) {
            $old_password = request('old_password');
            $old_pass = hash("sha512", $old_password . '536482f036ca01e8af8db01e26efd34');

            $alluserdata = DB::table(session('table'))->where(session('table') . "ID", session('id'))->where("password", $old_pass)->first();

            if (empty($alluserdata)) {
                //try with bcrypt

                $alluserdata = array("username" => session('username'), "password" => $old_password);
                if (Auth::attempt($alluserdata, false)) {
                    $pstatus = 2;
                } else {
                    $pstatus = 0;
                }
            } else {
                $pstatus = 2;
            }
            $new_password = request('new_password');
            $re_password = request('re_password');
            $status = 1;

            if ($pstatus != 2) {
                $this->data['form_validation'] = 'Old Passwords does not match';
                $status = 0;
            } else if ($new_password != $re_password) {

                $this->data['form_validation'] = 'Passwords (New Password and Re-Password) fields does not match';
                $status = 0;
            } elseif (strlen($new_password) < 6) {
                $this->data['form_validation'] = 'Passwords length must be at least 6 characters';
                $status = 0;
            }
            if ($status == 0) {
                $this->data["subview"] = "signin/cpassword";
                $this->load->view('_layout_main', $this->data);
            } else {
                DB::table(session('table'))->where(session('table') . "ID", session('id'))->update(['password' => bcrypt($new_password)]);

                \request()->session()->flush();
                return redirect('signin/index')->with('success', 'Password Reset Successfully! Please login to continue');
            }
        } else {
            $this->data["subview"] = "signin/cpassword";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function createCookie($cookie_name, $cookie_value) {
        return setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
    }

    public function signout() {

        //Auth::logout();
        \request()->session()->flush();
        return redirect(base_url('signin/index'));
    }

    public function saveUserLocation($user_id, $table, $action = null) {
        $this->locate();
        return DB::table('login_locations')->insert([
                    'ip' => $this->ip,
                    'city' => $this->city,
                    'region' => $this->region,
                    'country' => $this->countryName,
                    'latitude' => $this->latitude,
                    'longtude' => $this->longitude,
                    'timezone' => $this->timezone,
                    'user_id' => $user_id,
                    "table" => $table,
                    'action' => $action,
                    'continent' => $this->continentName,
                    'currency_code' => $this->currencyCode,
                    'currency_symbol' => $this->currencySymbol,
                    'currency_convert' => $this->currencyConverter,
                    'location_radius_accuracy' => $this->locationAccuracyRadius]);
    }

    //the geoPlugin server
    public $host = 'http://www.geoplugin.net/php.gp?ip={IP}&base_currency={CURRENCY}&lang={LANG}';
    //the default base currency
    public $currency = 'USD';
    //the default language
    public $language = 'en';
    //initiate the geoPlugin publics
    public $ip = null;
    public $city = null;
    public $region = null;
    public $regionCode = null;
    public $regionName = null;
    public $dmaCode = null;
    public $countryCode = null;
    public $countryName = null;
    public $inEU = null;
    public $euVATrate = false;
    public $continentCode = null;
    public $continentName = null;
    public $latitude = null;
    public $longitude = null;
    public $locationAccuracyRadius = null;
    public $timezone = null;
    public $currencyCode = null;
    public $currencySymbol = null;
    public $currencyConverter = null;

    /*     * *
     * in updated mode, we will use https://ipstack.com/ to check these information instead of this geoplugin
     */

    public function locate($ip = null) {
        global $_SERVER;
        if (is_null($ip)) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $host = str_replace('{IP}', $ip, $this->host);
        $host = str_replace('{CURRENCY}', $this->currency, $host);
        $host = str_replace('{LANG}', $this->language, $host);

        $data = array();
        $response = $this->fetch($host);

        $data = unserialize($response);

        //set the geoPlugin publics
        $this->ip = $ip;
        $this->city = isset($data['geoplugin_city']) ? $data['geoplugin_city'] : '';
        $this->region = isset($data['geoplugin_region']) ? $data['geoplugin_region'] : '';
        $this->regionCode = isset($data['geoplugin_regionCode']) ? $data['geoplugin_regionCode'] : '';
        $this->regionName = isset($data['geoplugin_regionName']) ? $data['geoplugin_regionName'] : '';
        $this->dmaCode = isset($data['geoplugin_dmaCode']) ? $data['geoplugin_dmaCode'] : '';
        $this->countryCode = isset($data['geoplugin_countryCode']) ? $data['geoplugin_countryCode'] : '';
        $this->countryName = isset($data['geoplugin_countryName']) ? $data['geoplugin_countryName'] : '';
        $this->inEU = isset($data['geoplugin_inEU']) ? $data['geoplugin_inEU'] : '';
        $this->euVATrate = isset($data['geoplugin_euVATrate']) ? $data['geoplugin_euVATrate'] : '';
        $this->continentCode = isset($data['geoplugin_continentCode']) ? $data['geoplugin_continentCode'] : '';
        $this->continentName = isset($data['geoplugin_continentName']) ? $data['geoplugin_continentName'] : '';
        $this->latitude = isset($data['geoplugin_latitude']) ? $data['geoplugin_latitude'] : '';
        $this->longitude = isset($data['geoplugin_longitude']) ? $data['geoplugin_longitude'] : '';
        $this->locationAccuracyRadius = isset($data['geoplugin_locationAccuracyRadius']) ? $data['geoplugin_locationAccuracyRadius'] : '';
        $this->timezone = isset($data['geoplugin_timezone']) ? $data['geoplugin_timezone'] : '';
        $this->currencyCode = isset($data['geoplugin_currencyCode']) ? $data['geoplugin_currencyCode'] : '';
        $this->currencySymbol = isset($data['geoplugin_currencySymbol']) ? $data['geoplugin_currencySymbol'] : '';
        $this->currencyConverter = isset($data['geoplugin_currencyConverter']) ? $data['geoplugin_currencyConverter'] : '';
    }

    function fetch($host) {

        if (function_exists('curl_init')) {

            //use cURL to fetch data
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $host);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'geoPlugin PHP Class v1.1');
            $response = curl_exec($ch);
            curl_close($ch);
        } else if (ini_get('allow_url_fopen')) {

            //fall back to fopen()
            $response = file_get_contents($host, 'r');
        } else {

            trigger_error('geoPlugin class Error: Cannot retrieve data. Either compile PHP with cURL support or enable allow_url_fopen in php.ini ', E_USER_ERROR);
            return;
        }

        return $response;
    }

    public function testCorrectAuth() {
        $sql_updated = "select  p.username, p.default_password,p.phone, p.\"parentID\" FROM " . set_schema_name() . "parent p, " . set_schema_name() . "setting s where p.\"parentID\" NOT IN (SELECT user_id from " . set_schema_name() . "log where user_id is not null and \"user\"='Parent') and p.status=1 and p.\"parentID\" IN (
SELECT a.parent_id from " . set_schema_name() . "student_parents a join " . set_schema_name() . "student b on b.student_id=a.student_id   where b.status=1 and a.student_id  in (
select \"student_id\" from " . set_schema_name() . "student_exams ) ) ";
        $return = DB::select($sql_updated);
        foreach ($return as $value) {
            $with_username = array('username' => $value->username, 'password' => $value->default_password);

            if (Auth::attempt($with_username, false)) {

                echo 'Parent username ' . $value->username . ' and password ' . $value->default_password . ' is valid <br/>';
            } else {
                DB::table('parent')->where("parentID", $value->parentID)->update(['password' => bcrypt($value->default_password)]);
                echo 'NOT CORRECT username ' . $value->username . ' and password ' . $value->default_password . ' is valid <br/>';
            }
        }
    }

    public function newlogin() {

        return view('signin.newlogin');
    }

    public function new_login() {
        $email = request('email');
        $check = DB::table('users')->where('email', $email)->first();
        if (!empty($check)) {
            //user exists, so log him/her inside and update his/her profile
            if ((int) $check->status <> 1 && $check->table != 'parent') {
                return 2;
            }
            $data = array("id", "usertype", "username", "table");
            foreach ($data as $value) {
                isset($check->{$value}) ? session([$value => $check->{$value}]) : '';
            }
            if ($check->photo == 'defualt.png') {
                $col = $check->table == 'student' ? 'student_id' : $check->table . 'ID';
                DB::table($check->table)->where($col, $check->id)->update(['photo' => request('photo')]);
            }
            Auth::loginUsingId($check->id);
            session(['loggedin' => 1]);
            $this->saveUserLocation($check->id, $check->table, 'google');
            return 1;
        } else {
            DB::table('login_attempts')->insert(array('username' => request('email'), 'wrong_password' => 'google login'));
            return 3;
        }
    }

    public function valid() {
        $password = '%943_))(_(&(^^###@__)*';
        $email = openssl_decrypt(request('valid'), "AES-128-ECB", $password);
        $users = DB::table('admin.all_users')->where('email', $email)->get();
        $schools = '';
        if (!empty($users)) {
            foreach ($users as $user) {
                $schools .= $user->schema_name . ',';
                DB::table($user->schema_name . '.' . $user->table)->where('email', $email)->update(['email_valid' => 1]);
            }
            return redirect('signin/index')->with('success', 'Email has been validated successfully; Schools affected :' . trim($schools, ',') . 'Kindly proceed with login');
        } else {
            return redirect('signin/index')->with('error', 'Invalid url provided. Kindly type url correctly');
        }
    }

}
