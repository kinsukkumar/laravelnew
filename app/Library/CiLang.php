<?php  

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Language Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Language
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/language.html
 */
class CiLang {

	/**
	 * List of translations
	 *
	 * @var array
	 */
	public $lang;

	/**
	 * List of translations
	 *
	 * @var array
	 */
	public $language	= array();
	/**
	 * List of loaded language files
	 *
	 * @var array
	 */
	public $is_loaded	= array();


	public $lang_file=array();
	/**
	 * Constructor
	 *
	 * @access	public
	 */
	function __construct($language_file=null)
	{
		$this->lang=$this;
		array_push($this->lang_file, $language_file);
	}

	
/**
 * Determine if phrase exist.
 * 
 * @param  string $phrase
 * @param  string $lang
 * @param  string $default
 * @return boolean
 */
function langHas($phrase, $lang, $default = 'en')
{
    if(Lang::has($phrase, $lang) == false || Lang::get($phrase, $lang) == Lang::get($phrase, $default)){
        return false;
    }

    return true;
}
}
// END Language Class

/* End of file Lang.php */
/* Location: ./system/core/Lang.php */
