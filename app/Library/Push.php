<?php

namespace App\Library;

class Push {

	private $title;
	private $message;
	private $data;
        private $link;

	/**
	 * @param $title
	 */
	public function setTitle( $title ) {
		$this->title = $title;
	}
/**
	 * @param $link
	 */
	public function setLink( $link ) {
		$this->link = $link;
	}
	/**
	 * @param $message
	 */
	public function setMessage( $message ) {
		$this->message = $message;
	}

	/**
	 * @param $data
	 */
	public function setPayload( $data ) {
		$this->data = $data;
	}

	/**
	 * @return array
	 */
	public function getPush() {
		$res                      = array();
		$res['title']     = $this->title;
		$res['body']   = $this->message;
		$res['payload']   = $this->data;
                $res['click_action']   = $this->link;
		$res['timestamp'] = date( 'Y-m-d G:i:s' );

		return $res;
	}

}
