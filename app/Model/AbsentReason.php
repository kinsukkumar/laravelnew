<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AbsentReason extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.absent_reasons';
    protected $fillable = ['id', 'reason'];


    public function students() {
        return $this->belongsToMany(\App\Model\Student::class, 'sattendances', 'absent_reason_id', 'student_id');
}

    public function sattendances() {
        return $this->hasMany(\App\Model\Sattendance::class, 'absent_reason_id', 'id');
    }


}
