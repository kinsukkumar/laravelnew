<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AcademicYear extends Model {

    /**
     * Generated
     */
    protected $table = 'academic_year';
    protected $fillable = ['id', 'name', 'class_level_id', 'status', 'start_date', 'end_date'];

    public function classlevel() {
        return $this->belongsTo(\App\Model\Classlevel::class, 'class_level_id', 'classlevel_id');
    }

    public function subjects() {
        return $this->belongsToMany(\App\Model\Subject::class, 'subject_topic', 'academic_year_id', 'subjectID');
    }

    public function invoices() {
        return $this->hasMany(\App\Model\Invoice::class, 'academic_year_id', 'id');
    }

    public function semesters() {
        return $this->hasMany(\App\Model\Semester::class, 'academic_year_id', 'id');
    }

    public function subjectStudents() {
        return $this->hasMany(\App\Model\SubjectStudent::class, 'academic_year_id', 'id');
    }

    public function examReports() {
        return $this->hasMany(\App\Model\ExamReport::class, 'academic_year_id', 'id');
    }

    public function studentArchives() {
        return $this->hasMany(\App\Model\StudentArchive::class, 'academic_year_id', 'id');
    }

    public function installments() {
        return $this->hasMany(\App\Model\Installment::class, 'academic_year_id', 'id');
    }

    public function marks() {
        return $this->hasMany(\App\Model\Mark::class, 'academic_year_id', 'id');
    }

    public function reminderTemplates() {
        return $this->hasMany(\App\Model\ReminderTemplate::class, 'academic_year_id', 'id');
    }

    public function sectionSubjectTeachers() {
        return $this->hasMany(\App\Model\SectionSubjectTeacher::class, 'academic_year_id', 'id');
    }

    public function subjectTopics() {
        return $this->hasMany(\App\Model\SubjectTopic::class, 'academic_year_id', 'id');
    }

    public function classExams() {
        return $this->hasMany(\App\Model\ClassExam::class, 'academic_year_id', 'id');
    }

    public function students() {
        return $this->hasMany(\App\Model\Student::class, 'academic_year_id', 'id');
    }

    public static function getCurrentYear($class_level_id) {
        return $this->db->query('select * FROM ' . set_schema_name() . 'academic_year WHERE class_level_id=' . $class_level_id . ' ORDER BY  end_date DESC LIMIT 1')->row();
    }

}
