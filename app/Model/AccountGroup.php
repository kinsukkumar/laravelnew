<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AccountGroup extends Model {

    /**
     * Generated
     */
    protected $table = 'account_groups';
    protected $fillable = ['id', 'name', 'note', 'financial_category_id'];


    public function financialCategory() {
        return $this->belongsTo(\App\Model\FinancialCategory::class, 'financial_category_id', 'id')->withDefault(['name'=>'unknown']);
    }

}
