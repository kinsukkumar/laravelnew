<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model {

    /**
     * Generated
     */
    protected $table = 'admin.users';
    protected $fillable = [
        'email', 'password', 'firstname', 'lastname', 'phone', 'created_by', 'type', 'rolename'
    ];

}
