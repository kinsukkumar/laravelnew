<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Admission extends Model {

    /**
     * Generated
     */
    protected $table = 'admissions';
    protected $fillable = [
        'id', 'student_id', 'status', 'user_id', 'time', 'created_by', 'updated_at'
    ];

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }
}
