<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdvancePayment extends Model {

    /**
     * Generated
     */

    protected $table = 'advance_payments';
    public $timestamps=false;
    protected $fillable = ['id', 'student_id', 'fee_id', 'payment_id', 'amount'];


    public function fee() {
        return $this->belongsTo(\App\Model\Fee::class, 'fee_id', 'id');
    }

    public function payment() {
        return $this->belongsTo(\App\Model\Payment::class, 'payment_id', 'id')->withDefault(['amount'=>0]);
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function invoicesFeesInstallments() {
        return $this->belongsToMany(\App\Model\InvoicesFeesInstallment::class, 'advance_payments_invoices_fees_installments', 'advance_payment_id', 'invoices_fees_installments_id');
    }

    public function advancePaymentsInvoicesFeesInstallments() {
        return $this->hasMany(\App\Model\AdvancePaymentsInvoicesFeesInstallment::class, 'advance_payment_id', 'id');
    }


}
