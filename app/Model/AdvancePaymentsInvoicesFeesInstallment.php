<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdvancePaymentsInvoicesFeesInstallment extends Model {

    /**
     * Generated
     */

    protected $table = 'advance_payments_invoices_fees_installments';
    protected $fillable = ['id', 'invoices_fees_installments_id', 'advance_payment_id', 'amount'];


    public function advancePayment() {
        return $this->belongsTo(\App\Model\AdvancePayment::class, 'advance_payment_id', 'id');
    }

    public function invoicesFeesInstallment() {
        return $this->belongsTo(\App\Model\InvoicesFeesInstallment::class, 'invoices_fees_installments_id', 'id');
    }


}
