<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model {

    /**
     * Generated
     */

    protected $table = 'alert';
    protected $fillable = ['id', 'notice_id', 'username', 'usertype', 'notice_for'];


    public function notice() {
        return $this->belongsTo(\App\Model\Notice::class, 'notice_id', 'noticeID');
    }


}
