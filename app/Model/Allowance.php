<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Allowance extends Model {

    /**
     * Generated
     */

    protected $table = 'allowances';
    protected $fillable = ['id', 'name', 'amount', 'percent', 'description', 'is_percentage', 'type', 'pension_included','category'];


    public function userAllowances() {
        return $this->hasMany(\App\Model\UserAllowance::class, 'allowance_id', 'id');
    }


}
