<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Application extends Model {

    /**
     * Generated
     */

    protected $table = 'application';
    protected $fillable = ['id', 'student_id', 'apply_for', 'created_by'];

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }


}
