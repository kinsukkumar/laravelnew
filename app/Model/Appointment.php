<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model {

    /**
     * Generated
     */

    protected $table = 'appointments';
    protected $fillable = ['id', 'date', 'time', 'to_user_id', 'to_table', 'from_user_id', 'from_table', 'message', 'status'];



}
