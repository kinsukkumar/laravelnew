<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model {

    /**
     * Generated
     */

    protected $table = 'assignments';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'title', 'note', 'due_date','subject_id', 'attach', 'attach_file_name','exam_group_id'];

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subject_id', 'subjectID');
    }

    
    public function examGroup() {
        return $this->belongsTo(\App\Model\ExamGroup::class, 'exam_group_id', 'id')->withDefault(['name'=>'Home package']);;
    }


}
