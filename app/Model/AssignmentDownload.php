<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AssignmentDownload extends Model {

    /**
     * Generated
     */

    protected $table = 'assignment_downloads';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'assignment_id', 'counter', 'created_by', 'created_by_table', 'updated_at', 'created_at'];

    public function assignment() {
        return $this->belongsTo(\App\Model\Assignment::class, 'assignment_id', 'id');
    }

    public function user() {
        return \App\Model\User::where('id', $this->attributes['created_by'])->where('table', $this->attributes['created_by_table'])->first();
    }

}
