<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AssignmentFile extends Model {

    /**
     * Generated
     */

    protected $table = 'assignment_files';
    protected $primaryKey = 'id';
    protected $fillable = ['id','assignment_id', 'attach', 'attach_file_name','status'];

    public function assignment() {
        return $this->belongsTo(\App\Model\Assignment::class, 'assignment_id', 'id');
    }

    
}
