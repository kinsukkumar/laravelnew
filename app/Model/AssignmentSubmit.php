<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AssignmentSubmit extends Model {

    /**
     * Generated
     */

    protected $table = 'assignments_submitted';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'student_id', 'assignment_id', 'score_mark', 'note', 'attach', 'attach_file_name'];

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function assignment() {
        return $this->belongsTo(\App\Model\Assignment::class, 'assignment_id', 'id');
    }


}
