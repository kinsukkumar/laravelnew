<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model {

    /**
     * Generated
     */

    protected $table = 'bank_accounts';
    protected $fillable = ['id', 'name', 'number', 'branch', 'refer_bank_id', 'opening_balance', 'note','refer_currency_id'];


    public function referBank() {
        return $this->belongsTo(\App\Model\ReferBank::class, 'refer_bank_id', 'id')->withDefault(['name'=>'Not defined']);
    }

     public function referCurrency() {
        return $this->belongsTo(\App\Model\ReferCurrency::class, 'refer_currency_id', 'id')->withDefault(['symbol'=>'Not Defined']);
    }
    public function referExpenses() {
        return $this->belongsToMany(\App\Model\ReferExpense::class, 'expense', 'bank_account_id', 'refer_expense_id');
    }

    public function feesClasses() {
        return $this->belongsToMany(\App\Model\FeesClass::class, 'bank_accounts_fees_classes', 'bank_account_id', 'fees_classes_id');
    }

    public function expenses() {
        return $this->hasMany(\App\Model\Expense::class, 'bank_account_id', 'id');
    }

    public function revenues() {
        return $this->hasMany(\App\Model\Revenue::class, 'bank_account_id', 'id');
    }

    public function bankAccountsFeesClasses() {
        return $this->hasMany(\App\Model\BankAccountsFeesClass::class, 'bank_account_id', 'id');
    }

    public function payments() {
        return $this->hasMany(\App\Model\Payment::class, 'bank_account_id', 'id');
    }


}
