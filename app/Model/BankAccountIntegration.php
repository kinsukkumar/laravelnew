<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BankAccountIntegration extends Model {

    /**
     * Generated
     */

    protected $table = 'bank_accounts_integrations';
    protected $fillable = ['id', 'bank_account_id', 'api_username', 'api_password', 'invoice_prefix', 'created_at', 'updated_at','sandbox_api_username','sandbox_api_password'];


    public function bankAccount() {
        return $this->belongsTo(\App\Model\BankAccount::class)->withDefault(['name'=>'Not defined']);
    }
}
