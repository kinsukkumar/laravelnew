<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BankAccountsFeesClass extends Model {

    /**
     * Generated
     */

    protected $table = 'bank_accounts_fees_classes';
    protected $fillable = ['id', 'bank_account_id', 'fees_classes_id'];

 
    public function bankAccount() {
        return $this->belongsTo(\App\Model\BankAccount::class, 'bank_account_id', 'id');
    }

    public function feesClass() {
        return $this->belongsTo(\App\Model\FeesClass::class, 'fees_classes_id', 'id');
    }


}
