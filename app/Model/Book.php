<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Book extends Model {

    /**
     * Generated
     */

    protected $table = 'book';
    protected $fillable = ['id', 'name', 'author', 'rack', 'edition', 'user_id', 'serial_no', 'subject_id', 'class_id', 'whois', 'quantity', 'book_for', 'due_quantity'];



    public function subject() {
        return $this->belongsTo(\App\Model\ReferSubject::class, 'subject_id', 'subject_id');
    }

    public function bookQuantitiesIssued() {
        return $this->belongsToMany(\App\Model\BookQuantity::class, 'issue', 'book_id', 'book_quantity_id');
    }

    public function bookQuantities() {
        return $this->hasMany(\App\Model\BookQuantity::class, 'book_id', 'id');
    }

    public function bookClasses() {
        return $this->hasMany(\App\Model\BookClass::class, 'book_id', 'id');
    }

    public function issues() {
        return $this->hasMany(\App\Model\Issue::class, 'book_id', 'id');
    }


}
