<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BookClass extends Model {

    /**
     * Generated
     */

    protected $table = 'book_class';
    protected $fillable = ['id', 'classes_id', 'book_id','created_at','updated_at'];


    public function book() {
        return $this->belongsTo(\App\Model\Book::class, 'book_id', 'id')->withDefault(['id'=>1,'name'=>'Not Defined']);
    }

    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class);
    }


}
