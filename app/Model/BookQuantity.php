<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BookQuantity extends Model {

    /**
     * Generated
     */

    protected $table = 'book_quantity';
    protected $fillable = ['id', 'book_id', 'status', 'book_condition', 'bID'];


    public function book() {
        return $this->belongsTo(\App\Model\Book::class, 'book_id', 'id');
    }

    public function books() {
        return $this->belongsToMany(\App\Model\Book::class, 'issue', 'book_quantity_id', 'book_id');
    }

    public function issues() {
        return $this->hasMany(\App\Model\Issue::class, 'book_quantity_id', 'id');
    }


}
