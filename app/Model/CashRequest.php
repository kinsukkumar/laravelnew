<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class CashRequest extends Model {

    /**
     * Generated
     */
    protected $table = 'cash_requests';
    protected $fillable = [
        'id', 'amount',
        'requested_by',
        'requested_by_table',
        'requested_date',
        'checked_by',
        'checked_by_table',
        'checked_date',
        'approved_by',
        'approved_by_table',
        'approved_date',
        'received_by',
        'received_by_table',
        'received_date', 'created_at', 'updated_at', 'particulars'
    ];

    public function requestedBy() {
        return (int) $this->attributes['requested_by'] > 0 ? DB::table($this->attributes['requested_by_table'])->where($this->attributes['requested_by_table'] . 'ID', $this->attributes['requested_by'])->first() : (object) ['name' => 'Not Defined'];
    }

    public function checkedBy() {
        return  (int) $this->attributes['checked_by'] > 0 ?DB::table($this->attributes['checked_by_table'])->where($this->attributes['checked_by_table'] . 'ID', $this->attributes['checked_by'])->first() : (object) ['name'=>'Not Checked'];
    }

    public function approvedBy() {
        return (int) $this->attributes['approved_by'] > 0 ? DB::table($this->attributes['approved_by_table'])->where($this->attributes['approved_by_table'] . 'ID', $this->attributes['approved_by'])->first(): (object) ['name'=>'Not Approved'];
    }

    public function receivedBy() {
        return (int) $this->attributes['received_by'] > 0 ? DB::table($this->attributes['received_by_table'])->where($this->attributes['received_by_table'] . 'ID', $this->attributes['received_by'])->first(): (object) ['name'=>'Not Received'];
    }

}
