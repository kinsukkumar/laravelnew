<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Character extends Model {

    /**
     * Generated
     */

    protected $table = 'characters';
    protected $fillable = ['id', 'code', 'description', 'created_by', 'created_by_id', 'character_category_id', 'position'];


    public function characterCategory() {
        return $this->belongsTo(\App\Model\CharacterCategory::class, 'character_category_id', 'id');
    }

    public function classes() {
        return $this->belongsToMany(\App\Model\Classes::class, 'character_classes', 'character_id', 'class_id');
    }

    public function studentCharacters() {
        return $this->hasMany(\App\Model\StudentCharacter::class, 'character_id', 'id');
    }

    public function characterClasses() {
        return $this->hasMany(\App\Model\CharacterClass::class, 'character_id', 'id');
    }


}
