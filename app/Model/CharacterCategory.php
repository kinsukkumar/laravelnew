<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CharacterCategory extends Model {

    /**
     * Generated
     */

    protected $table = 'character_categories';
    protected $fillable = ['id', 'character_category', 'based_on', 'position'];


    public function characters() {
        return $this->hasMany(\App\Model\Character::class, 'character_category_id', 'id');
    }


}
