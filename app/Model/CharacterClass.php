<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CharacterClass extends Model {

    /**
     * Generated
     */

    protected $table = 'character_classes';
    protected $fillable = ['id', 'character_id', 'class_id', 'created_by', 'created_by_table'];


    public function character() {
        return $this->belongsTo(\App\Model\Character::class, 'character_id', 'id');
    }

    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'class_id', 'classesID');
    }


}
