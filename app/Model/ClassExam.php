<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClassExam extends Model {

    /**
     * Generated
     */

    protected $table = 'class_exam';
      protected $primaryKey = 'class_exam_id';
    protected $fillable = ['class_exam_id', 'class_id', 'exam_id', 'academic_year_id'];


    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }

    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'class_id', 'classesID');
    }

    public function exam() {
        return $this->belongsTo(\App\Model\Exam::class, 'exam_id', 'examID')->withDefault(['exam'=>'undefined']);
    }


}
