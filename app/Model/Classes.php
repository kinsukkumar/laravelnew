<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model {

    /**
     * Generated
     */

    protected $table = 'classes';
    protected $primaryKey = 'classesID';
    protected $fillable = ['classesID', 'classes', 'classes_numeric', 'teacherID', 'note', 'classlevel_id', 'special_grade_name_id'];


    public function classlevel() {
        return $this->belongsTo(\App\Model\Classlevel::class, 'classlevel_id', 'classlevel_id');
    }

    public function specialGradeName() {
        return $this->belongsTo(\App\Model\SpecialGradeName::class, 'special_grade_name_id', 'id');
    }

    public function teacher() {
        return $this->belongsTo(\App\Model\Teacher::class, 'teacherID', 'teacherID')->withDefault(['name'=>'Not Defined']);
    }

    public function characters() {
        return $this->belongsToMany(\App\Model\Character::class, 'character_classes', 'class_id', 'character_id');
    }

    public function booksClass() {
        return $this->belongsToMany(\App\Model\Book::class, 'book_class', 'class_id', 'book_id');
    }

    public function feesInstallments() {
        return $this->belongsToMany(\App\Model\FeesInstallment::class, 'fees_installments_classes', 'class_id', 'fees_installment_id');
    }

    public function invoices() {
        return $this->hasMany(\App\Model\Invoice::class, 'classesID', 'classesID');
    }

    public function sections() {
        return $this->hasMany(\App\Model\Section::class, 'classesID', 'classesID');
    }

    public function books() {
        return $this->hasMany(\App\Model\Book::class, 'class_id', 'classesID');
    }

    public function characterClasses() {
        return $this->hasMany(\App\Model\CharacterClass::class, 'class_id', 'classesID');
    }

    public function examReports() {
        return $this->hasMany(\App\Model\ExamReport::class, 'classes_id', 'classesID');
    }

    public function examschedules() {
        return $this->hasMany(\App\Model\Examschedule::class, 'classesID', 'classesID');
    }

    public function feesClasses() {
        return $this->hasMany(\App\Model\FeesClass::class, 'class_id', 'classesID');
    }

    public function subjects() {
        return $this->hasMany(\App\Model\Subject::class, 'classesID', 'classesID');
    }

    public function promotionsubjects() {
        return $this->hasMany(\App\Model\Promotionsubject::class, 'classesID', 'classesID');
    }

    public function routines() {
        return $this->hasMany(\App\Model\Routine::class, 'classesID', 'classesID');
    }

    public function sectionSubjectTeachers() {
        return $this->hasMany(\App\Model\SectionSubjectTeacher::class, 'classesID', 'classesID');
    }

    public function subjectMarks() {
        return $this->hasMany(\App\Model\SubjectMark::class, 'classesID', 'classesID');
    }

    public function bookClasses() {
        return $this->hasMany(\App\Model\BookClass::class, 'class_id', 'classesID');
    }

    public function classExams() {
        return $this->hasMany(\App\Model\ClassExam::class, 'class_id', 'classesID');
    }

    public function feesInstallmentsClasses() {
        return $this->hasMany(\App\Model\FeesInstallmentsClass::class, 'class_id', 'classesID');
    }

    public function students() {
        return $this->hasMany(\App\Model\Student::class, 'classesID', 'classesID');
    }


}
