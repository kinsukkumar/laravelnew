<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Classlevel extends Model {

    /**
     * Generated
     */
    protected $table = 'classlevel';
    protected $primaryKey = 'classlevel_id';
    protected $fillable = ['classlevel_id', 'name', 'span_number', 'note', 'result_format', 'terms', 'stamp', 'head_teacher_title', 'school_level_id', 'school_id', 'pass_mark', 'school_level_id','reg_form'];

    public function specialGradeNames() {
        return $this->belongsToMany(\App\Model\SpecialGradeName::class, 'special_grades', 'classlevel_id', 'special_grade_name_id');
    }

    public function classes() {
        return $this->hasMany(\App\Model\Classes::class, 'classlevel_id', 'classlevel_id');
    }

    public function semesters() {
        return $this->hasMany(\App\Model\Semester::class, 'class_level_id', 'classlevel_id');
    }

    public function grades() {
        return $this->hasMany(\App\Model\Grade::class, 'classlevel_id', 'classlevel_id');
    }

    public function necta() {
        return $this->hasMany(\App\Model\Nectum::class, 'class_level_id', 'classlevel_id');
    }

    public function referExams() {
        return $this->hasMany(\App\Model\ReferExam::class, 'classlevel_id', 'classlevel_id');
    }

    public function academicYears() {
        return $this->hasMany(\App\Model\AcademicYear::class, 'class_level_id', 'classlevel_id');
    }

    public function specialGrades() {
        return $this->hasMany(\App\Model\SpecialGrade::class, 'classlevel_id', 'classlevel_id');
    }

    public function school() {
        return $this->belongsTo(\App\Model\School::class);
    }

    public function schoolLevel() {
        return $this->belongsTo(\App\Model\SchoolLevel::class, 'school_level_id','id');
    }

}
