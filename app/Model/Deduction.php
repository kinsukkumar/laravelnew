<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Deduction extends Model {

    /**
     * Generated
     */

    protected $table = 'deductions';
    protected $fillable = ['id', 'name', 'percent', 'amount', 'description','account_number','bank_account_id', 'is_percentage', 'category','employer_percent','employer_amount','gross_pay'];


    public function salaries() {
        return $this->belongsToMany(\App\Model\Salary::class, 'salary_deductions', 'deduction_id', 'salary_id');
    }

    public function userDeductions() {
        return $this->hasMany(\App\Model\UserDeduction::class, 'deduction_id', 'id');
    }

    public function salaryDeductions() {
        return $this->hasMany(\App\Model\SalaryDeduction::class, 'deduction_id', 'id');
    }

     public function bankAccount() {
        return $this->belongsTo(\App\Model\BankAccount::class,'bank_account_id','id')->withDefault(['name'=>'']);
    }

}
