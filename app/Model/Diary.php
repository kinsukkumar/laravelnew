<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Diary extends Model {

    /**
     * Generated
     */
    protected $table = 'diaries';
    protected $fillable = ['id', 'student_id', 'teacher_id', 'work_title', 'description', 'end_date', 'start_date', 'created_at', 'updated_at', 'book_id', 'subject_id', 'book_chapter'];

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function teacher() {
        return $this->belongsTo(\App\Model\Teacher::class, 'teacher_id', 'teacherID')->withDefault(['name' => 'Admin', 'photo' => 'default.png']);
    }

    public function comment() {
        return $this->hasMany(\App\Model\DiaryComment::class);
    }

    public function book() {
        return $this->belongsTo(\App\Model\Book::class, 'book_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subject_id', 'subjectID')->withDefault(['subject' => 'Not Defined']);
    }

}
