<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DiaryComment extends Model {

    /**
     * Generated
     */
    protected $table = 'diary_comments';
    protected $fillable = ['id', 'user_id', 'table','comment', 'diary_id','opened',  'created_at', 'updated_at'];

    public function diary() {
        return $this->belongsTo(\App\Model\Diary::class);
    }

}
