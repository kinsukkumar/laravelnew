<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DiscountFeesInstallment extends Model {

    /**
     * Generated
     */

    protected $table = 'discount_fees_installments';
    protected $fillable = ['id', 'fees_installment_id', 'amount', 'student_id', 'note'];


    public function feesInstallment() {
        return $this->belongsTo(\App\Model\FeesInstallment::class, 'fees_installment_id', 'id');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }


}
