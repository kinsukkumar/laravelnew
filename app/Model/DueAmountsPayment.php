<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DueAmountsPayment extends Model {

    /**
     * Generated
     */

    protected $table = 'due_amounts_payments';
    protected $fillable = ['id', 'payment_id', 'due_amount_id', 'amount'];


    public function payment() {
        return $this->belongsTo(\App\Model\Payment::class, 'payment_id', 'id');
    }

    public function dueAmount() {
        return $this->belongsTo(\App\Model\DueAmount::class, 'due_amount_id', 'id');
    }


}
