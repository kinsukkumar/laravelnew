<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Duty extends Model {

    protected $table = 'duties';
    protected $fillable = ['id', 'start_date', 'end_date', 'name', 'created_by', 'created_at', 'updated_at'];

    public function studentDuty() {
        return $this->hasMany(\App\Model\StudentDuty::class, 'student_id', 'id');
    }

    public function teacherDuty() {
        return $this->hasMany(\App\Model\TeacherDuty::class, 'teacher_id', 'id');
    }

    public function students() {
        return $this->belongsToMany(\App\Model\Student::class, 'student_duties', 'duty_id', 'student_id');
    }

    public function teachers() {
        return $this->belongsToMany(\App\Model\Teacher::class, 'teacher_duties','duty_id',  'teacher_id');
    }

}
