<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DutyReport extends Model {

    protected $table = 'duty_reports';
    protected $fillable = ['id', 'date', 'transport','feed','special_event','tod_comment', 'headteacher_comment', 'created_by', 'created_at', 'updated_at','created_by_table'];

    public function duty() {
        return $this->belongsTo(\App\Model\Duty::class);
    }

}
