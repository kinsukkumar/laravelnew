<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Eattendance extends Model {

    /**
     * Generated
     */

    protected $table = 'eattendance';
    protected $fillable = ['eattendanceID', 'examID', 'classesID', 'subjectID', 'date', 'student_id', 's_name', 'eattendance', 'year', 'eextra'];



}
