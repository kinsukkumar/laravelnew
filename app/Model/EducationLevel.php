<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EducationLevel extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.education_level';
    protected $fillable = ['id', 'name'];



}
