<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Email extends Model {

    /**
     * Generated
     */
    protected $table = 'email';
    protected $primaryKey = 'email_id';
    protected $fillable = ['email_id', 'body', 'subject', 'user_id', 'status', 'email', 'table'];

}
