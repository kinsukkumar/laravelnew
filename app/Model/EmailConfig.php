<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmailConfig extends Model {

    /**
     * Generated
     */

    protected $table = 'admin.email_config';
    protected $fillable = ['id', 'email', 'username', 'password', 'host', 'status'];


    public function sentEmails() {
        return $this->hasMany(\App\Model\SentEmail::class, 'email_config_id', 'id');
    }


}
