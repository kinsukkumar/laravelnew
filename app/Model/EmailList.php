<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmailList extends Model {

    /**
     * Generated
     */

    protected $table = 'email_lists';
    protected $fillable = ['id', 'email', 'created_by'];



}
