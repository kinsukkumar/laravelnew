<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmploymentType extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.employment_type';
    protected $fillable = ['id', 'name'];


    public function teachers() {
        return $this->hasMany(\App\Model\Teacher::class, 'employment_type_id', 'id');
    }


}
