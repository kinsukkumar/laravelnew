<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ErrorLog extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.error_log';
    protected $fillable = ['id', 'type', 'madeby', 'username', 'logmessage', 'last_sql', 'called_class', 'called_method', 'request_url', 'filename', 'line_number', 'user_agent', 'user_platform', 'schema_name', 'email_sent'];



}
