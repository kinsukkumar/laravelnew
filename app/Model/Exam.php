<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model {

    /**
     * Generated
     */
    protected $table = 'exam';
    protected $primaryKey = 'examID';
    protected $fillable = ['examID', 'exam', 'date', 'note', 'abbreviation', 'semester_id', 'refer_exam_id', 'show_division', 'email_send', 'special_grade_name_id'];

    public function semester() {
        return $this->belongsTo(\App\Model\Semester::class, 'semester_id', 'id');
    }

    public function referExam() {
        return $this->belongsTo(\App\Model\ReferExam::class, 'refer_exam_id', 'id');
    }

    public function students() {
        return $this->belongsToMany(\App\Model\Student::class, 'exam_special_cases', 'exam_id', 'student_id');
    }

    public function studentCharacters() {
        return $this->hasMany(\App\Model\StudentCharacter::class, 'exam_id', 'examID');
    }

    public function examschedules() {
        return $this->hasMany(\App\Model\Examschedule::class, 'examID', 'examID');
    }

    public function examSpecialCases() {
        return $this->hasMany(\App\Model\ExamSpecialCase::class, 'exam_id', 'examID');
    }

    public function marks() {
        return $this->hasMany(\App\Model\Mark::class, 'examID', 'examID');
    }

    public function subjectMarks() {
        return $this->hasMany(\App\Model\SubjectMark::class, 'examID', 'examID');
    }

    public function classExams() {
        return $this->hasMany(\App\Model\ClassExam::class, 'exam_id', 'examID');
    }

}
