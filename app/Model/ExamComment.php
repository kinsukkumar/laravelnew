<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExamComment extends Model {

    /**
     * Generated
     */
    protected $table = 'exam_comments';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'body', 'student_id', 'exam_report_id', 'user_id', 'user_table', 'created_at', 'updated_at','academic_year_id','status'];

    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }

    public function exam() {
        return $this->belongsTo(\App\Model\Exam::class, 'exam_report_id', 'examID');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function user() {
        return \App\Model\User::where('id', $this->attributes['user_id'])->where('table', $this->attributes['user_table'])->first();
    }
}
