<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExamGroup extends Model {

    /**
     * Generated
     */

    protected $table = 'exam_groups';
    protected $fillable = ['id', 'name', 'weight', 'note', 'created_at','updated_at'];

    public function referExams() {
        return $this->hasMany(\App\Model\ReferExam::class);
    }


}
