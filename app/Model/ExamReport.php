<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExamReport extends Model {

    /**
     * Generated
     */

    protected $table = 'exam_report';
    protected $fillable = ['id', 'classes_id', 'combined_exams', 'name', 'sms_sent', 'exam_id', 'email_sent', 'academic_year_id', 'combined_exam_array', 'reporting_date', 'percent', 'semester_id'];


    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'classes_id', 'classesID');
    }

    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }

    public function semester() {
        return $this->belongsTo(\App\Model\Semester::class, 'semester_id', 'id');
    }


}
