<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExamReportSetting extends Model {

    /**
     * Generated
     */

    protected $table = 'exam_report_settings';
    protected $fillable = ['id', 'exam_type', 'average_column_name', 'show_teacher', 'show_remarks', 'show_teacher_sign', 'show_grade', 'show_pos_in_stream', 'show_csee_division', 'show_acsee_division', 'show_subject_point', 'show_division_by_exam', 'show_division_total_points', 'show_overall_division', 'show_pos_in_class', 'show_pos_in_section', 'show_subject_pos_in_class', 'show_subject_pos_in_section', 'semester_average_name', 'single_semester_avg_name', 'overall_semester_avg_name', 'show_classteacher_name', 'show_classteacher_phone', 'class_teacher_remark', 'head_teacher_remark', 'show_all_signature_on_the_footer', 'show_student_attendance', 'show_overall_grade','show_average_row','show_percentage_on_mark_column','subject_order_by_arrangement','show_total_marks','show_subject_total_across_exams','show_parent_comment','show_marks'];



}
