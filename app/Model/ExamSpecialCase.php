<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExamSpecialCase extends Model {

    /**
     * Generated
     */

    protected $table = 'exam_special_cases';
    protected $fillable = ['id', 'student_id', 'exam_id', 'description', 'special_exam_reason_id', 'created_by', 'created_by_table', 'subjects_excluded', 'subjects_ids_excluded'];


    public function exam() {
        return $this->belongsTo(\App\Model\Exam::class, 'exam_id', 'examID');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }


}
