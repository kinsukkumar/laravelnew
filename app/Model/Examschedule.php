<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Examschedule extends Model {

    /**
     * Generated
     */

    protected $table = 'examschedule';
      protected $primaryKey = 'examscheduleID';
    protected $fillable = ['examscheduleID', 'examID', 'classesID', 'sectionID', 'subjectID', 'edate', 'examfrom', 'examto', 'room', 'year'];


    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'classesID', 'classesID');
    }

    public function exam() {
        return $this->belongsTo(\App\Model\Exam::class, 'examID', 'examID');
    }

    public function section() {
        return $this->belongsTo(\App\Model\Section::class, 'sectionID', 'sectionID');
    }

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subjectID', 'subjectID');
    }


}
