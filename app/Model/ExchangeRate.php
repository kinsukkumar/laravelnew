<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model {

    /**
     * Generated
     */

    protected $table = 'exchange_rates';
    protected $fillable = ['id', 'from_currency', 'to_currency', 'rate', 'note', 'created_by_id', 'created_by_table', 'status'];



}
