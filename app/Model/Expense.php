<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model {

    /**
     * Generated
     */

    protected $table = 'expense';
       protected $primaryKey = 'expenseID';
       public $timestamps=false;
    protected $fillable = ['expenseID', 'create_date', 'date', 'expense', 'userID', 'uname', 'usertype', 'expenseyear', 'note', 'categoryID', 'is_depreciation', 'amount', 'depreciation', 'refer_expense_id', 'ref_no', 'payment_method', 'created_by', 'bank_account_id', 'transaction_id', 'created_by_table', 'reconciled','voucher_no','recipient','payer_name','payment_type_id'];


    public function referExpense() {
        return $this->belongsTo(\App\Model\ReferExpense::class, 'refer_expense_id', 'id')->withDefault(['name'=>'Not Defined']);
    }
    
     public function productpurchase() {
        return $this->hasMany(\App\Model\ProductPurchase::class, 'expense_id', 'expenseID');
    }

    public function bankAccount() {
        return $this->belongsTo(\App\Model\BankAccount::class, 'bank_account_id', 'id')->withDefault(['name'=>'Not Defined']);
    }

 public function paymentType() {
        return $this->belongsTo(\App\Model\PaymentType::class, 'payment_type_id', 'id')->withDefault(['name'=>'No Payment type defined']);
    }
}
