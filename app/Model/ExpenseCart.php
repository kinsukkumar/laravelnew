<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExpenseCart extends Model {

    /**
     * Generated
     */
    protected $table = 'expense_cart';
    protected $fillable = ['id', 'note', 'refer_expense_id', 'date', 'name', 'amount', 'created_by','expense_id'];

    public function expenses() { 
        return $this->belongsTo(\App\Model\Expense::class, 'expense_id', 'expenseID');
    }

    public function referExpense() { 
        return $this->belongsTo(\App\Model\ReferExpense::class, 'refer_expense_id', 'id')->withDefault(['name' => 'Not Defined']);
    }
    
}
