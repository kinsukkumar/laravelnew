<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FailedJob extends Model {

    /**
     * Generated
     */

    protected $table = 'admin.failed_jobs';
    protected $fillable = ['id', 'connection', 'queue', 'payload', 'exception', 'failed_at'];



}
