<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model {

    /**
     * Generated
     */

    protected $table = 'admin.faq';
    protected $fillable = ['id', 'question', 'answer', 'created_by'];



}
