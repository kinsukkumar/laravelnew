<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model {

    /**
     * Generated
     */

    protected $table = 'fees';
    protected $fillable = ['id','name', 'priority'];


    public function advancePayments() {
        return $this->hasMany(\App\Model\AdvancePayment::class, 'fee_id', 'id');
    }
    public function invoiceFeesBalance() {
        return $this->hasMany(\App\Model\InvoicesFeesInstallmentsBalance::class, 'fee_id', 'id');
    }
    public function dueAmounts() {
        return $this->hasMany(\App\Model\DueAmount::class, 'fee_id', 'id');
    }

    public function feesClasses() {
        return $this->hasMany(\App\Model\FeesClass::class, 'fee_id', 'id');
    }


}
