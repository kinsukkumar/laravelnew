<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.feedback';
    protected $fillable = ['id', 'feedback', 'username', 'schema', 'user_id', 'table', 'opened'];


    public function feedbackReplies() {
        return $this->hasMany(\App\Model\feedbackReply::class, 'feedback_id', 'id');
    }


}
