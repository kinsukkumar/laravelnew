<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FeedbackReply extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.feedback_reply';
    protected $fillable = ['id', 'feedback_id', 'message', 'user_id'];


    public function feedback() {
        return $this->belongsTo(\App\Model\feedback::class, 'feedback_id', 'id');
    }


}
