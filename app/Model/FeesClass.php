<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FeesClass extends Model {

    /**
     * Generated
     */

    protected $table = 'fees_classes';
    protected $fillable = ['id', 'fee_id', 'class_id'];


    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'class_id', 'classesID');
    }

    public function fee() {
        return $this->belongsTo(\App\Model\Fee::class, 'fee_id', 'id');
    }

    public function bankAccounts() {
        return $this->belongsToMany(\App\Model\BankAccount::class, 'bank_accounts_fees_classes', 'fees_classes_id', 'bank_account_id');
    }

    public function bankAccountsFeesClasses() {
        return $this->hasMany(\App\Model\BankAccountsFeesClass::class, 'fees_classes_id', 'id');
    }


}
