<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FeesInstallment extends Model {

    /**
     * Generated
     */

    protected $table = 'fees_installments';
    protected $fillable = ['id', 'fee_id', 'installment_id'];

    public function fee() {
        return $this->belongsTo(\App\Model\Fee::class);
    }

    public function studentsWithDiscount() {
        return $this->belongsToMany(\App\Model\Student::class, 'discount_fees_installments', 'fees_installment_id', 'student_id');
    }

    public function hostels() {
        return $this->belongsToMany(\App\Model\Hostel::class, 'hostel_fees_installments', 'fees_installment_id', 'hostel_id');
    }

    public function studentsUnsubscribed() {
        return $this->belongsToMany(\App\Model\Student::class, 'student_fees_installments_unsubscriptions', 'fees_installment_id', 'student_id');
    }

    public function classes() {
        return $this->belongsToMany(\App\Model\Classes::class, 'fees_installments_classes', 'fees_installment_id', 'class_id');
    }

    public function transportRoutes() {
        return $this->belongsToMany(\App\Model\TransportRoute::class, 'transport_routes_fees_installments', 'fees_installment_id', 'transport_route_id');
    }

    public function discountFeesInstallments() {
        return $this->hasMany(\App\Model\DiscountFeesInstallment::class, 'fees_installment_id', 'id');
    }

    public function hostelFeesInstallments() {
        return $this->hasMany(\App\Model\HostelFeesInstallment::class, 'fees_installment_id', 'id');
    }

    public function studentFeesInstallmentsUnsubscriptions() {
        return $this->hasMany(\App\Model\StudentFeesInstallmentsUnsubscription::class, 'fees_installment_id', 'id');
    }

    public function invoicesFeesInstallments() {
        return $this->hasMany(\App\Model\InvoicesFeesInstallment::class, 'fees_installment_id', 'id');
    }

    public function feesInstallmentsClasses() {
        return $this->hasMany(\App\Model\FeesInstallmentsClass::class, 'fees_installment_id', 'id');
    }

    public function transportRoutesFeesInstallments() {
        return $this->hasMany(\App\Model\TransportRoutesFeesInstallment::class, 'fees_installment_id', 'id');
    }


}
