<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FeesInstallmentsClass extends Model {

    /**
     * Generated
     */

    protected $table = 'fees_installments_classes';
    protected $fillable = ['id', 'fees_installment_id', 'class_id', 'amount'];


    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'class_id', 'classesID');
    }

    public function feesInstallment() {
        return $this->belongsTo(\App\Model\FeesInstallment::class, 'fees_installment_id', 'id');
    }


}
