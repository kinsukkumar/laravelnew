<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

    /**
     * Generated
     */

    protected $table = 'files';
    protected $fillable = ['id', 'mime', 'file_folder_id', 'name', 'display_name', 'user_id', 'table', 'size', 'caption', 'path'];



}
