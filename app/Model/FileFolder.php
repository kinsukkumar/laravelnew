<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FileFolder extends Model {

    /**
     * Generated
     */

    protected $table = 'file_folder';
    protected $fillable = ['id', 'user_id', 'table', 'name'];



}
