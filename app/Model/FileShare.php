<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FileShare extends Model {

    /**
     * Generated
     */

    protected $table = 'file_share';
    protected $fillable = ['id', 'classesID', 'public', 'file_or_folder', 'item_id'];



}
