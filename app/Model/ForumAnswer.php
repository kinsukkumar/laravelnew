<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ForumAnswer extends Model {

    /**
     * Generated
     */
    protected $table = 'forum_answers';
    protected $fillable = ['id', 'forum_question_id', 'answer', 'created_by', 'created_by_table', 'created_at', 'updated_at','is_correct','teacher_id'];

    public function forumQuestion() {
        return $this->belongsTo(\App\Model\ForumQuestion::class);
    }

    public function user() {
        return \App\Model\User::where('id', $this->attributes['created_by'])->where('table', $this->attributes['created_by_table'])->first();
    }
     public function forumAnswerVotes() {
        return $this->hasMany(\App\Model\ForumAnswerVote::class);
    }

}
