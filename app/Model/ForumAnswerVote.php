<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ForumAnswerVote extends Model {

    /**
     * Generated
     */

     protected $table = 'forum_answer_votes';
    protected $fillable = ['id', 'forum_answer_id', 'vote_type', 'created_by', 'created_by_table', 'created_at', 'updated_at'];

    public function forumAnswer() {
        return $this->belongsToMany(\App\Model\ForumAnswer::class);
    }

    public function user() {
        return \App\Model\User::where('id', $this->attributes['created_by'])->where('table', $this->attributes['created_by_table'])->first();
    }

}
