<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ForumCategory extends Model
{
    protected $table = 'forum_categories';
    public $timestamps = true;
    //public $with = 'parents';

    public function discussions()
    {
        return $this->hasMany(App\Model\ForumDiscussion::class,'forum_category_id');
    }

    public function parents()
    {
        return $this->hasMany(Self::class, 'parent_id')->orderBy('order', 'asc');
    }
}
