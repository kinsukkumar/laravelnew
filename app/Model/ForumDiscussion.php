<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumDiscussion extends Model
{
    use SoftDeletes;
    
    protected $table = 'forum_discussion';
    public $timestamps = true;
    protected $fillable = ['title', 'forum_category_id', 'user_id','user_table', 'slug', 'color'];
    protected $dates = ['deleted_at', 'last_reply_at'];

    public function user()
    {
       return \App\Model\User::where('id', $this->attributes['user_id'])->where('table', $this->attributes['user_table'])->first();
    }

    public function category()
    {
        return $this->belongsTo(\App\Model\ForumCategory::class,'forum_category_id','id');
    }

    public function posts()
    {
        
        return $this->hasMany(\App\Model\Post::class, 'forum_discussion_id','id');
    }

    public function post()
    {
       // return $this->hasMany(Models::className(Post::class), 'chatter_discussion_id')->orderBy('created_at', 'ASC');
         return $this->hasMany(\App\Model\Post::class, 'forum_discussion_id','id')->orderBy('created_at', 'ASC');
    }

    public function postsCount()
    {
        return $this->posts()
        ->selectRaw('forum_discussion_id, count(*)-1 as total')
        ->groupBy('forum_discussion_id');
    }

    public function users()
    {
        //return $this->belongsToMany(config('chatter.user.namespace'), 'forum_user_discussion', 'discussion_id', 'user_id');
    }
}
