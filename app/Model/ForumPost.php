<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumPost extends Model
{
    use SoftDeletes;
    
    protected $table = 'forum_post';
    public $timestamps = true;
    protected $fillable = ['forum_discussion_id', 'user_id','user_table', 'body', 'markdown'];
    protected $dates = ['deleted_at'];

    public function discussion()
    {
        return $this->belongsTo(\App\Model\ForumDiscussion::class, 'forum_discussion_id');
    }

    public function user()
    {
        return \App\Model\User::where('id', $this->attributes['user_id'])->where('table', $this->attributes['user_table'])->first();
    }
}
