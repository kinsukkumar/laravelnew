<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ForumQuestion extends Model {

    /**
     * Generated
     */
    protected $table = 'forum_questions';
    protected $fillable = ['id', 'syllabus_topic_id', 'title', 'question', 'created_by', 'created_by_table', 'status', 'created_at', 'updated_at'];

    public function syllabusTopic() {
        return $this->belongsTo(\App\Model\SyllabusTopic::class);
    }

    public function user() {
        return \App\Model\User::where('id', $this->attributes['created_by'])->where('table', $this->attributes['created_by_table'])->first();
    }

    public function forumAnswers() {
        return $this->hasMany(\App\Model\ForumAnswer::class);
    }

    public function forumQuestionViewer() {
        return $this->hasMany(\App\Model\ForumQuestionViewer::class);
    }

}
