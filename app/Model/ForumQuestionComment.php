<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ForumQuestionComment extends Model {

    /**
     * Generated
     */

    protected $table = 'forum_question_comments';
    protected $fillable = ['id', 'forum_question_id','content', 'created_by', 'created_by_table', 'created_at', 'updated_at'];


    public function forumQuestion() {
        return $this->belongsTo(\App\Model\ForumQuestion::class);
    }

   public function user() {
        return \App\Model\User::where('id', $this->attributes['created_by'])->where('table', $this->attributes['created_by_table'])->first();
    }


}
