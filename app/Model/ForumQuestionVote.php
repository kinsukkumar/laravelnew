<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ForumQuestionVote extends Model {

    /**
     * Generated
     */

    protected $table = 'forum_questions_votes';
    protected $fillable = ['id', 'forum_question_answer_id','vote_type','created_by','created_by_table','created_at','updated_at'];


    public function forumQuestionAnswer() {
        return $this->hasMany(\App\Model\ForumAnswer::class, 'forum_question_answer_id', 'id');
    }


}
