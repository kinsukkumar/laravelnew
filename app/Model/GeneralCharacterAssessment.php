<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GeneralCharacterAssessment extends Model {

    /**
     * Generated
     */

    protected $table = 'general_character_assessment';
    protected $fillable = ['id', 'student_id', 'semester_id', 'class_teacher_id', 'class_teacher_comment', 'head_teacher_id', 'head_teacher_comment', 'class_teacher_created_at', 'head_teacher_created_at', 'class_teacher_updated_at', 'head_teacher_updated_at', 'exam_id'];


    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function teacher() {
        return $this->belongsTo(\App\Model\Teacher::class, 'class_teacher_id', 'teacherID');
    }


}
