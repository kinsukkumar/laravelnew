<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model {

    /**
     * Generated
     */

    protected $table = 'grade';
      protected $primaryKey = 'gradeID';
    protected $fillable = ['gradeID', 'grade', 'point', 'gradefrom', 'gradeupto', 'note', 'classlevel_id', 'overall_note', 'overall_academic_note'];


    public function classlevel() {
        return $this->belongsTo(\App\Model\Classlevel::class, 'classlevel_id', 'classlevel_id');
    }


}
