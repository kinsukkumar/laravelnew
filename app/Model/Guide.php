<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model
{
    protected $table = 'constant.guides';
    public $timestamps=false;
    protected $fillable = [
        'permission_id', 'content', 'created_by','language'
    ];
    
    public function permission() {
        return $this->belongsTo(\App\Model\Permission::class);
    }
    public function guidePageVisit() {
        return $this->hasMany(\App\Model\GuidePageVisit::class);
    }
}
