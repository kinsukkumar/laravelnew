<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HealthInsurance extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.health_insurance';
    protected $fillable = ['id', 'name'];


    public function teachers() {
        return $this->hasMany(\App\Model\Teacher::class, 'health_insurance_id', 'id');
    }

    public function students() {
        return $this->hasMany(\App\Model\Student::class, 'health_insurance_id', 'id');
    }


}
