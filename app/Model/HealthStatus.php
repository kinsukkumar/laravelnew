<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HealthStatus extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.health_status';
    protected $fillable = ['id', 'name'];



}
