<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Hmember extends Model {

    /**
     * Generated
     */
    protected $table = 'hmembers';
    protected $fillable = ['id', 'student_id', 'hostel_category_id', 'jod', 'hostel_id','bed_id'];

    public function hostelCategory() {
        return $this->belongsTo(\App\Model\HostelCategory::class, 'hostel_category_id', 'id')->withDefault(['htype' => 'Not defined']);
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }
    public function hostelbeds() {
        return $this->belongsTo(\App\Model\HostelBeds::class, 'bed_id', 'id')->withDefault(['htype' => 'Not defined']);
    }
    public function installments() {
        return $this->belongsTo(\App\Model\Installment::class, 'installment_id', 'id');
    }
    public function hostel() {
        return $this->belongsTo(\App\Model\Hostel::class);
    }
}
