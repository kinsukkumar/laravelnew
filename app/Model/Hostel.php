<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Hostel extends Model {

    /**
     * Generated
     */

    protected $table = 'hostels';
    protected $fillable = ['id', 'name', 'htype', 'address', 'note','user_id','usertype','beds_no'];



    public function feesInstallments() {
        return $this->belongsToMany(\App\Model\FeesInstallment::class, 'hostel_fees_installments', 'hostel_id', 'fees_installment_id');
    }

    public function hostelCategories() {
        return $this->hasMany(\App\Model\HostelCategory::class, 'hostel_id', 'id');
    }

    public function hmembers() {
        return $this->hasMany(\App\Model\Hmember::class, 'hostel_id', 'id');
    }

    public function hostelFeesInstallments() {
        return $this->hasMany(\App\Model\HostelFeesInstallment::class, 'hostel_id', 'id');
    }


}
