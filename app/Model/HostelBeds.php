<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HostelBeds extends Model {

    /**
     * Generated
     */
    protected $table = 'hostel_beds';
    protected $fillable = ['id', 'name', 'hostel_id'];

    public function hostel() {
        return $this->belongsTo(\App\Model\Hostel::class, 'hostel_id', 'id');
    }
}
