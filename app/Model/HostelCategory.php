<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HostelCategory extends Model {

    /**
     * Generated
     */
    protected $table = 'hostel_category';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'hostel_id', 'class_type', 'hbalance', 'note'];

    public function hostel() {
        return $this->belongsTo(\App\Model\Hostel::class, 'hostel_id', 'id');
    }

    public function students() {
        return $this->belongsToMany(\App\Model\Student::class, 'hmembers', 'hostel_category_id', 'student_id');
    }

}
