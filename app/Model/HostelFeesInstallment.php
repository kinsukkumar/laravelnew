<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HostelFeesInstallment extends Model {

    /**
     * Generated
     */

    protected $table = 'hostel_fees_installments';
    protected $fillable = ['id', 'hostel_id', 'fees_installment_id', 'amount'];


    public function feesInstallment() {
        return $this->belongsTo(\App\Model\FeesInstallment::class, 'fees_installment_id', 'id');
    }

    public function hostel() {
        return $this->belongsTo(\App\Model\Hostel::class, 'hostel_id', 'id');
    }


}
