<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class IdCard extends Model
{
    protected $table = 'id_cards';
    protected $fillable = ['show_gender', 'show_birthday', 'show_admission_number', 'show_class', 'show_issue_date','show_barcode','show_payment_status','show_watermark','show_year_range'];
}
