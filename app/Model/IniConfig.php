<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class IniConfig extends Model {

    /**
     * Generated
     */

    protected $table = 'ini_config';
    protected $fillable = ['configID', 'type', 'config_key', 'value'];



}
