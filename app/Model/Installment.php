<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Installment extends Model {

    /**
     * Generated
     */

    protected $table = 'installments';
    protected $fillable = ['id', 'academic_year_id', 'start_date', 'end_date', 'name'];


    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }
  public function feesInstallments() {
        return $this->hasMany(\App\Model\FeesInstallment::class, 'installment_id', 'id');
    }

}
