<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class IntegrationRequest extends Model {

    /**
     * Generated
     */
    protected $table = 'admin.integration_requests';
    protected $fillable = ['id', 'client_id', 'user_id', 'table', 'shulesoft_approved', 'bank_approved', 'created_at', 'updated_at', 'bank_accounts_integration_id', 'schema_name', 'approval_user_id'];

    public function bankAccountIntegration() {
        return $this->belongsTo(\App\Model\BankAccountIntegration::class,'bank_accounts_integration_id','id');
    }

    public function user() {
        return \App\Model\User::where('id', $this->attributes['user_id'])->where('table', $this->attributes['table'])->first();
    }

}
