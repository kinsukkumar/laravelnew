<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model {

    /**
     * Generated
     */
    protected $table = 'invoices';
    public $timestamps=false;
    protected $fillable = ['id', 'reference', 'student_id', 'date', 'created_at', 'updated_at',  'sync', 'return_message', 'push_status','prefix','academic_year_id','amount'];

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }
    

    public function invoicesFeesInstallments() {
        return $this->hasMany(\App\Model\InvoicesFeesInstallment::class, 'invoice_id', 'id');
    }

    public function invoicesFeesBalance() {
        return $this->hasMany(\App\Model\InvoicesFeesInstallmentsBalance::class, 'invoice_id', 'id');
    }

    public function feeExclude() {
        return $this->student()->first()->studentFeesInstallmentsUnsubscriptions()->get(['fees_installment_id']);
    }

    /**
     * paid invoice fee installment + Advance payments
     */
    public function paidAmount() {
        // 
        $invoices_fees_installments = $this->invoicesFeesInstallments()->whereNotIn('fees_installment_id', $this->feeExclude())->get();
        $total_amount = 0;
        foreach ($invoices_fees_installments as $inst) {

            $total_amount += isset($inst->paymentsInvoicesFeesInstallments) ? $inst->paymentsInvoicesFeesInstallments->sum('amount') : 0;
        }
        return $total_amount + $this->paidAdvancedAmount();
    }

    /**
     * check in discount table
     */
    public function discountAmount() {
        //
        $invoices_fees_installments = $this->invoicesFeesInstallments()->whereNotIn('fees_installment_id', $this->feeExclude())->get();
        $total_amount = 0;
        foreach ($invoices_fees_installments as $inst) {

            $total_amount += $inst->feesInstallment->discountFeesInstallments->sum('amount');
        }
        return $total_amount;
    }

    /**
     * get total invoice amount
     * Total Invoice Fee Installment Amount - Discount Amount + Due Amount -Paid Due Amount
     */
    public function amount() {
        //
        $invoices_fees_installments = $this->invoicesFeesInstallments()->whereNotIn('fees_installment_id', $this->feeExclude())->get();
        $total_amount = 0;
        foreach ($invoices_fees_installments as $inst) {
            if ($inst->feesInstallment->fee_id == 1000) {
                //this is transport fee

                $transport = $inst->feesInstallment->transportRoutesFeesInstallments()->whereIn('transport_route_id', $this->student->tmembers()->get(['student_id']))->first();


                $total_amount += count($transport) > 0 ? $transport->amount : 0;
            } else if ($inst->feesInstallment->fee_id == 2000) {
                //this is hostel fee  
                $total_amount += $inst->feesInstallment->hostelFeesInstallments()->first()->amount;
            } else {
                $total_amount += $inst->feesInstallment->feesInstallmentsClasses()->first()->amount;
            }
        }
        return $total_amount-$this->discountAmount();
    }

    /**
     * 
     * @return Due Amount (total)
     */
    public function dueAmount() {
        return $this->student->DueAmounts()->sum('amount');
    }

    /**
     * 
     * @return Paid Due Amount (total)
     */
    public function paidDueAmount() {
        return \App\Model\DueAmountsPayment::whereIn('due_amount_id', $this->student->DueAmounts()->get(['id']))->sum('amount');
    }

    /**
     * 
     * @return Total Advanced Paid Amount
     */
    public function paidAdvancedAmount() {
        $invoices_fees_installments = $this->invoicesFeesInstallments()->whereNotIn('fees_installment_id', $this->feeExclude())->get();
        $total_amount = 0;
        foreach ($invoices_fees_installments as $inst) {

            $total_amount += isset($inst->advancePaymentsInvoicesFeesInstallments) ? $inst->advancePaymentsInvoicesFeesInstallments()->sum('amount') : 0;
        }
        return $total_amount;
    }

    public function advancePayment() {
        return $this->student->advancePayments()->sum('amount');
    }

    public function balance() {
        return $this->amount() - $this->discountAmount() - $this->paidAmount();
    }

}
