<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoiceSetting extends Model {

    /**
     * Generated
     */

    protected $table = 'invoice_settings';
    protected $fillable = ['id', 'title', 'reference_naming', 'show_banks', 'show_payment_plan','show_different_header', 'show_mobile_payment'];



}
