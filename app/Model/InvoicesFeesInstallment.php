<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoicesFeesInstallment extends Model {

    /**
     * Generated
     */

    protected $table = 'invoices_fees_installments';
    protected $fillable = ['id', 'invoice_id', 'fees_installment_id'];


    public function feesInstallment() {
        return $this->belongsTo(\App\Model\FeesInstallment::class, 'fees_installment_id', 'id');
    }

    public function invoice() {
        return $this->belongsTo(\App\Model\Invoice::class, 'invoice_id', 'id');
    }

    public function payments() {
        return $this->belongsToMany(\App\Model\Payment::class, 'payments_invoices_fees_installments', 'invoices_fees_installment_id', 'payment_id');
    }

    public function advancePayments() {
        return $this->belongsToMany(\App\Model\AdvancePayment::class, 'advance_payments_invoices_fees_installments', 'invoices_fees_installments_id', 'advance_payment_id');
    }

    public function paymentsInvoicesFeesInstallments() {
        return $this->hasMany(\App\Model\PaymentsInvoicesFeesInstallment::class, 'invoices_fees_installment_id', 'id');
    }

    public function advancePaymentsInvoicesFeesInstallments() {
        return $this->hasMany(\App\Model\AdvancePaymentsInvoicesFeesInstallment::class, 'invoices_fees_installments_id', 'id');
    }


}
