<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoicesFeesInstallmentsBalance extends Model {

    protected $table = 'invoices_fees_installments_balance';

    public function invoice() {
        return $this->hasMany('\App\Model\invoice', 'id', 'invoice_id');
    }

    public function fee() {
        return $this->belongsTo(\App\Model\Fee::class, 'fee_id', 'id');
    }

    public function installment() {
        return $this->hasone(\App\Model\Installment::class, 'installment_id', 'id');
    }

}
