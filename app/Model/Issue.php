<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model {

    /**
     * Generated
     */
    protected $table = 'issue';
    protected $fillable = ['id', 'lmember_id', 'issue_date', 'due_date', 'return_date', 'fine', 'note', 'is_returned', 'book_quantity_id', 'created_by', 'created_by_table', 'type', 'book_id'];

    public function bookQuantity() {
        return $this->belongsTo(\App\Model\BookQuantity::class, 'book_quantity_id', 'id');
    }

    public function book() {
        return $this->belongsTo(\App\Model\Book::class, 'book_id', 'id');
    }

    public function lmember() {
        return $this->belongsTo(\App\Model\Lmember::class, 'lmember_id', 'id');
    }

}
