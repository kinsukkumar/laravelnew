<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

    /**
     * Generated
     */

    protected $table = 'item';
    protected $fillable = ['item_id', 'name', 'batch_number', 'quantity', 'vendor_id', 'contact_person_name', 'contact_person_number', 'status', 'price', 'comments', 'depreciation', 'current_price', 'date_purchased'];


    public function vendor() {
        return $this->belongsTo(\App\Model\Vendor::class, 'vendor_id', 'vendor_id');
    }


}
