<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LessonPlan extends Model {

    /**
     * Generated
     */

    protected $table = 'lesson_plan';
    protected $fillable = ['id', 'syllabus_objective_id', 'stage_position', 'stage_name', 'activity', 'time_taken', 'resource', 'teacher_id', 'created_by_id', 'created_by_table'];


    public function lessonPlan() {
        return $this->belongsTo(\App\Model\LessonPlan::class, 'syllabus_objective_id', 'id');
    }

    public function teacher() {
        return $this->belongsTo(\App\Model\Teacher::class, 'teacher_id', 'teacherID');
    }

    public function teachers() {
        return $this->belongsToMany(\App\Model\Teacher::class, 'lesson_plan', 'syllabus_objective_id', 'teacher_id');
    }

    public function lessonPlans() {
        return $this->hasMany(\App\Model\LessonPlan::class, 'syllabus_objective_id', 'id');
    }


}
