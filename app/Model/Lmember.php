<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lmember extends Model {

    /**
     * Generated
     */
    protected $table = 'lmember';
    protected $fillable = ['id', 'lID', 'lbalance', 'ljoindate', 'status', 'user_table', 'user_id'];

    public function user() {
        return \DB::table('users')->where('id', $this->attributes['user_id'])->where('table', $this->attributes['user_table'])->first();
    }

}
