<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Log extends Model {

    /**
     * Generated
     */

    protected $table = 'log';
    public $timestamps=false;
    protected $fillable = ['id', 'url', 'user_agent', 'platform', 'platform_name', 'source', 'user', 'user_id', 'country', 'city', 'region', 'isp', 'table', 'controller', 'method', 'is_ajax', 'request'];



}
