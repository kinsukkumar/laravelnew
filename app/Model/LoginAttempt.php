<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LoginAttempt extends Model {

    /**
     * Generated
     */

    protected $table = 'login_attempts';
    protected $fillable = ['id', 'username', 'wrong_password'];



}
