<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mailandsm extends Model {

    /**
     * Generated
     */

    protected $table = 'mailandsms';
      protected $primaryKey = 'mailandsmsID';
    protected $fillable = ['mailandsmsID', 'users', 'type', 'message', 'create_date', 'year'];



}
