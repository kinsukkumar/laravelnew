<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mailandsmstemplate extends Model {

    /**
     * Generated
     */

    protected $table = 'mailandsmstemplate';
    protected $fillable = ['id', 'name', 'type', 'template', 'create_date', 'user'];



}
