<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mailandsmstemplatetag extends Model {

    /**
     * Generated
     */

    protected $table = 'mailandsmstemplatetag';
     protected $primaryKey = 'mailandsmstemplatetagID';
    protected $fillable = ['mailandsmstemplatetagID', 'usersID', 'name', 'tagname', 'mailandsmstemplatetag_extra', 'create_date'];



}
