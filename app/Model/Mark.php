<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mark extends Model {

    /**
     * Generated
     */

    protected $table = 'mark';
     protected $primaryKey = 'markID';
     public $timestamps=false;
    protected $fillable = ['markID', 'examID', 'exam', 'student_id', 'classesID', 'subjectID', 'subject', 'mark', 'year', 'postion', 'academic_year_id', 'status', 'created_by', 'table'];


    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }

    public function exam() {
        return $this->belongsTo(\App\Model\Exam::class, 'examID', 'examID');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subjectID', 'subjectID');
    }


}
