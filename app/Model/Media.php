<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Media extends Model {

    /**
     * Generated
     */
    protected $table = 'medias';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'size', 'created_by', 'media_category_id', 'title', 'description', 'created_at', 'updated_at', 'created_by_table', 'url', 'name', 'syllabus_topic_id', 'status','syllabus_subtopic_id','media_link'];

    public function mediaCategory() {
        return $this->belongsTo(\App\Model\MediaCategory::class);
    }

    public function syllabusTopic() {
        return $this->belongsTo(\App\Model\SyllabusTopic::class);
    }

    public function user() {
        return \App\Model\User::where('id', $this->attributes['created_by'])->where('table', $this->attributes['created_by_table'])->first();
    }

    public function mediaViewes() {
        return $this->hasMany(\App\Model\MediaView::class);
    }

    public function mediaLikes() {
        return $this->hasMany(\App\Model\MediaLike::class);
    }

    public function mediaComments() {
        return $this->hasMany(\App\Model\MediaComment::class);
    }

    public function mediaLive() {
        return $this->hasOne(\App\Model\MediaLive::class);
    }
}
