<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MediaCategory extends Model {

    /**
     * Generated
     */

    protected $table = 'media_categories';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'name', 'updated_at', 'created_at'];



}
