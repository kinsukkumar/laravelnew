<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MediaLike extends Model {

    /**
     * Generated
     */
    protected $table = 'media_likes';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'media_id', 'created_by', 'created_by_table', 'updated_at', 'created_at','type','schema_name'];

    public function user() {
        return \App\Model\User::where('id', $this->attributes['created_by'])->where('table', $this->attributes['created_by_table'])->first();
    }

    public function media() {
        return $this->belongsTo(\App\Model\Media::class);
    }

}
