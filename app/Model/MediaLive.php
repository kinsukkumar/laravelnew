<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MediaLive extends Model {

    /**
     * Generated
     */
    protected $table = 'media_live';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'media_id','date', 'start_time','end_time','source', 'streaming_url', 'updated_at', 'created_at'];

    public function media() {
        return $this->belongsTo(\App\Model\Media::class);
    }

     public function mediaLiveComments() {
        return $this->hasMany(\App\Model\MediaLiveComment::class);
    }
}
