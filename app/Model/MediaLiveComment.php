<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MediaLiveComment extends Model {

    /**
     * Generated
     */
    protected $table = 'media_live_comments';
    protected $primaryKey = 'id';
    public $timestamps=false;
    protected $fillable = ['id', 'media_live_id', 'comment', 'created_by', 'created_by_table', 'updated_at', 'created_at','schema_name'];

    public function user() {
        return \App\Model\User::where('id', $this->attributes['created_by'])->where('table', $this->attributes['created_by_table'])->first();
    }

    public function mediaLive() {
        return $this->belongsTo(\App\Model\MediaLive::class);
    }
}
