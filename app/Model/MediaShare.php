<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MediaShare extends Model {

    /**
     * Generated
     */

    protected $table = 'media_share';
    protected $primaryKey = 'shareID';
    protected $fillable = ['shareID', 'classesID', 'public', 'file_or_folder', 'item_id', 'create_time'];



}
