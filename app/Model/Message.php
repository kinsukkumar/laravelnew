<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {

    /**
     * Generated
     */

    protected $table = 'message';
    protected $primaryKey = 'messageID';
    protected $fillable = ['messageID', 'email', 'receiverID', 'receiverType', 'subject', 'message', 'attach', 'attach_file_name', 'userID', 'usertype', 'useremail', 'year', 'date', 'create_date', 'read_status', 'from_status', 'to_status', 'fav_status', 'fav_status_sent', 'reply_status', 'receiverTable'];



}
