<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Metric extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.metrics';
    protected $fillable = ['id', 'name', 'abbreviation'];
    

}
