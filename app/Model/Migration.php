<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Migration extends Model {

    /**
     * Generated
     */

    protected $table = 'migrations';
    protected $fillable = ['batch', 'migration', 'version'];



}
