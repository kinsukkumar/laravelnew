<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MinorExam extends Model {

    /**
     * Generated
     */
    protected $table = 'minor_exams';
    protected $fillable = ['id', 'subject_id', 'exam_group_id', 'date', 'note', 'created_by', 'created_by_table', 'created_at', 'updated_at','syllabus_topic_id','total_question','total_time'];

    public function minorExamMarks() {
        return $this->hasMany(\App\Model\minorExamMarks::class);
    }

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subject_id', 'subjectID');
    }

    public function examGroup() {
        return $this->belongsTo(\App\Model\ExamGroup::class);
    }

    public function createdBy() {
        return \App\Model\User::where('table', $this->attributes['created_by_table'])->where('id', $this->attributes['created_by'])->first();
    }

    public function syllabusTopic() {
        return $this->belongsTo(\App\Model\SyllabusTopic::class, 'syllabus_topic_id', 'id');
    }
}
