<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MinorExamMark extends Model {

    /**
     * Generated
     */
    protected $table = 'minor_exam_marks';
    protected $fillable = ['id', 'minor_exam_id', 'student_id', 'mark', 'academic_year_id','status', 'created_by', 'created_by_table', 'created_at', 'updated_at'];

    public function minorExam() {
        return $this->belongsTo(\App\Model\minorExam::class);
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function createdBy() {
        return \App\Model\User::where('table', $this->attributes['created_by_table'])->where('id', $this->attributes['created_by'])->first();
    }

}
