<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Module extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.modules';
    protected $fillable = ['id', 'name', 'price'];


    public function permissionGroups() {
        return $this->hasMany(\App\Model\PermissionGroup::class, 'module_id', 'id');
    }


}
