<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nectum extends Model {

    /**
     * Generated
     */

    protected $table = 'necta';
    protected $fillable = ['id', 'name', 'class_level_id', 'url', 'year', 'regional_position', 'national_position', 'candidate_type', 'centre_gpa', 'created_by_id', 'created_by_user_table'];


    public function classlevel() {
        return $this->belongsTo(\App\Model\Classlevel::class, 'class_level_id', 'classlevel_id');
    }


}
