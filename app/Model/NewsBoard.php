<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewsBoard extends Model {

    /**
     * Generated
     */

    protected $table = 'news_board';
    public $timestamps = true;
    protected $fillable = ['id', 'title', 'body', 'create_date', 'role_id', 'status','attachment'];

    public function alerts() {
        return $this->hasMany(\App\Model\Alert::class, 'news_board_id', 'id');
    }
    
    public function role() {
        return $this->belongsTo(\App\Model\Role::class);
    }


}
