<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model {

    /**
     * Generated
     */

    protected $table = 'notice';
    protected $primaryKey = 'noticeID';
    protected $fillable = ['noticeID', 'title', 'notice', 'year', 'date', 'create_date', 'class_id', 'to_roll_id', 'status', 'notice_for'];


    public function alerts() {
        return $this->hasMany(\App\Model\Alert::class, 'notice_id', 'noticeID');
    }


}
