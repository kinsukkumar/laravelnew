<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ParentDocument extends Model {

    /**
     * Generated
     */

    protected $table = 'parent_documents';
    protected $primaryKey = 'id';
    protected $fillable = ['id','parent_id', 'attach', 'attach_file_name','type'];

    public function parent() {
        return $this->belongsTo(\App\Model\Parent::class, 'parent_id', 'parentID');
    }

    
}
