<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ParentPhone extends Model {

    /**
     * Generated
     */

    protected $table = 'parent_phones';
    protected $fillable = ['id', 'parent_id', 'phone'];


    public function parents() {
        return $this->belongsTo(\App\Model\Parents::class, 'parent_id', 'parentID');
    }


}
