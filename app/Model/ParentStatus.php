<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ParentStatus extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.parent_status';
    protected $fillable = ['id', 'reason', 'is_report'];



}
