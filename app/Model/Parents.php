<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Parents extends Model {

    /**
     * Generated
     */
    protected $table = 'parent';
    protected $primaryKey = 'parentID';
    public $timestamps = false;
    protected $fillable = ['parentID', 'name', 'father_profession', 'mother_profession', 'email', 'phone', 'address', 'photo', 'username', 'password', 'usertype', 'other_phone', 'status', 'employer', 'guardian_profession', 'dob', 'sex', 'relation', 'physical_condition_id', 'default_password', 'language', 'denomination', 'box', 'city_id', 'profession_id', 'remember', 'status_id', 'location', 'region', 'employer_type_id', 'country_id'];

    public function students() {
        return $this->belongsToMany(\App\Model\Student::class, 'student_parents', 'parent_id', 'student_id');
    }

    public function parentPhones() {
        return $this->hasMany(\App\Model\ParentPhone::class, 'parent_id', 'parentID');
    }

    public function studentParents() {
        return $this->hasMany(\App\Model\StudentParent::class, 'parent_id', 'parentID');
    }

    public function profession() {
        return $this->belongsTo(\App\Model\ReferProfession::class, 'profession_id', 'id')->withDefault(['name'=>'Unknown']);
    }

}
