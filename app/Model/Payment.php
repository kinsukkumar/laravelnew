<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {
    /**
     * Generated
     */

    protected $table = 'payments';
     public $timestamps=false;
    protected $fillable = ['id', 'student_id', 'amount', 'payment_type_id', 'date', 'transaction_id', 'cheque_number', 'bank_account_id', 'payer_name', 'mobile_transaction_id', 'transaction_time', 'account_number', 'token', 'reconciled'];


    public function bankAccount() {
        return $this->belongsTo(\App\Model\BankAccount::class, 'bank_account_id', 'id')->withDefault(['name'=>'Not Defined']);
    }

    public function paymentType() {
        return $this->belongsTo(\App\Model\PaymentType::class, 'payment_type_id', 'id')->withDefault(['name'=>'cash']);
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function dueAmounts() {
        return $this->belongsToMany(\App\Model\DueAmount::class, 'due_amounts_payments', 'payment_id', 'due_amount_id');
    }

    public function invoicesFeesInstallments() {
        return $this->belongsToMany(\App\Model\InvoicesFeesInstallment::class, 'payments_invoices_fees_installments', 'payment_id', 'invoices_fees_installment_id');
    }

    public function advancePayments() {
        return $this->hasMany(\App\Model\AdvancePayment::class, 'payment_id', 'id');
    }

    public function dueAmountsPayments() {
        return $this->hasMany(\App\Model\DueAmountsPayment::class, 'payment_id', 'id');
    }

    public function paymentsInvoicesFeesInstallments() {
        return $this->hasMany(\App\Model\PaymentsInvoicesFeesInstallment::class, 'payment_id', 'id');
    }


}
