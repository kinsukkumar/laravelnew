<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentFee extends Model {

    /**
     * Generated
     */

    protected $table = 'payment_fee';
    protected $fillable = ['invoice_fee_id', 'id', 'payment_id', 'date', 'current_paid_amount'];



}
