<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.payment_types';
    protected $fillable = ['id', 'name'];


    public function payments() {
        return $this->hasMany(\App\Model\Payment::class, 'payment_type_id', 'id');
    }


}
