<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentsInvoicesFeesInstallment extends Model {

    /**
     * Generated
     */

    protected $table = 'payments_invoices_fees_installments';
    protected $fillable = ['id', 'invoices_fees_installment_id', 'payment_id', 'amount'];


    public function invoicesFeesInstallment() {
        return $this->belongsTo(\App\Model\InvoicesFeesInstallment::class, 'invoices_fees_installment_id', 'id');
    }

    public function payment() {
        return $this->belongsTo(\App\Model\Payment::class, 'payment_id', 'id');
    }


}
