<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pension extends Model {

    /**
     * Generated
     */

    protected $table = 'pensions';
    protected $fillable = ['id', 'name', 'employer_percentage', 'employee_percentage', 'address','refer_pension_id','status'];


    public function userPensions() {
        return $this->hasMany(\App\Model\UserPension::class, 'pension_id', 'id');
    }

  public function salaries() {
        return $this->belongsToMany(\App\Model\Salary::class, 'salary_pensions', 'pension_id', 'salary_id');
    }

    public function salaryPensions() {
        return $this->hasMany(\App\Model\SalaryPension::class, 'pension_id', 'id');
    }

}
