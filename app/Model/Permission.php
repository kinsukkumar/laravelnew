<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.permission';
    protected $fillable = ['id', 'name', 'display_name', 'description', 'is_super', 'permission_group_id', 'arrangement'];


    public function permissionGroup() {
        return $this->belongsTo(\App\Model\Constant.permissionGroup::class, 'permission_group_id', 'id');
    }

    public function roles() {
        return $this->belongsToMany(\App\Model\Role::class, 'role_permission', 'permission_id', 'role_id');
    }

    public function rolePermissions() {
        return $this->hasMany(\App\Model\RolePermission::class, 'permission_id', 'id');
    }


}
