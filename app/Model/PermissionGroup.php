<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.permission_group';
    protected $fillable = ['name', 'id', 'module_id'];


    public function module() {
        return $this->belongsTo(\App\Model\Module::class, 'module_id', 'id');
    }

    public function permissions() {
        return $this->hasMany(\App\Model\Permission::class, 'permission_group_id', 'id');
    }


}
