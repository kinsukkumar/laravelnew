<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PhysicalCondition extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.physical_condition';
    protected $fillable = ['id', 'name'];


    public function teachers() {
        return $this->hasMany(\App\Model\Teacher::class, 'physical_condition_id', 'id');
    }

    public function students() {
        return $this->hasMany(\App\Model\Student::class, 'physical_condition_id', 'id');
    }


}
