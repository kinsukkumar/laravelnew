<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductCart extends Model {

    /**
     * Generated
     */
    protected $table = 'product_cart';
    protected $fillable = ['id', 'product_alert_id','quantity','amount', 'name','revenue_id'];
  
    public function revenues() { 
        return $this->belongsTo(\App\Model\Revenue::class, 'revenue_id', 'id');
    }

    public function productQuantity() { 
        return $this->belongsTo(\App\Model\ProductQuantity::class, 'product_alert_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

}
