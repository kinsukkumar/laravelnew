<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductPurchase extends Model {

    /**
     * Generated
     */
    protected $table = 'product_purchases';
    protected $fillable = ['id', 'product_alert_id', 'created_by', 'created_by_table', 'quantity', 'amount', 'vendor_id', 'selling_price', 'note', 'expense_id', 'date','unit_price'];

    public function productQuantity() {
        return $this->belongsTo(\App\Model\ProductQuantity::class, 'product_alert_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

    public function vendor() {
        return $this->belongsTo(\App\Model\Vendor::class)->withDefault(['name'=>'Not Defined']);
    }

    public function expenses() {
        return $this->belongsTo(\App\Model\Expense::class, 'expense_id', 'expenseID');
    }

}
