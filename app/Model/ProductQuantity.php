<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductQuantity extends Model {

    /**
     * Generated
     */
    protected $table = 'product_alert_quantity';
    protected $fillable = ['id', 'product_register_id', 'name', 'note', 'metric_id','refer_expense_id','open_blance'];
  
    public function metric() { 
        return $this->belongsTo(\App\Model\Metric::class, 'metric_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

    public function referExpense() { 
        return $this->belongsTo(\App\Model\ReferExpense::class, 'refer_expense_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

    public function productRegister() { 
        return $this->belongsTo(\App\Model\ProductRegister::class, 'product_register_id', 'id')->withDefault(['name' => 'Not Defined']);
    }
}
