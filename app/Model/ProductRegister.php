<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductRegister extends Model {

    /**
     * Generated
     */
    protected $table = 'constant.product_registers';
    protected $fillable = ['id', 'metric_id', 'name', 'comment', 'created_at', 'updated_at','schema_name'];


    public function metric() {
        return $this->belongsTo(\App\Model\Metric::class, 'id', 'metric_id')->withDefault(['name' => 'Not Defined']);
    }

}
