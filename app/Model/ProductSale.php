<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductSale extends Model {

    /**
     * Generated
     */
    protected $table = 'product_sales';

    protected $fillable = ['id', 'product_alert_id', 'quantity', 'selling_price', 'revenue_id', 'date','created_by','created_by_table','created_at','updated_at','note','user_id','user_table'];
   
    public function productQuantity() {
        return $this->belongsTo(\App\Model\ProductQuantity::class, 'product_alert_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

    public function revenue() {
        return $this->belongsTo(\App\Model\Revenue::class, 'revenue_id','id');
    }

}
