<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Promotionsubject extends Model {

    /**
     * Generated
     */

    protected $table = 'promotionsubject';
    protected $fillable = ['promotionSubjectID', 'classesID', 'subjectID', 'subjectCode', 'subjectMark'];


    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'classesID', 'classesID');
    }

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subjectID', 'subjectID');
    }


}
