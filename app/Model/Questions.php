<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model {

    /**
     * Generated
     */

    protected $table = 'questions';
      protected $primaryKey = 'id';
    protected $fillable = ['minor_exam_id', 'question', 'weight','attach','type'];

    public function minorExam() {
        return $this->belongsTo(\App\Model\MinorExam::class, 'minor_exam_id', 'id');
    }

}
