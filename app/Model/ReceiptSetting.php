<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReceiptSetting extends Model {

    /**
     * Generated
     */

    protected $table = 'receipt_settings';
    protected $fillable = ['id', 'show_installment', 'show_class', 'template',
        'available_templates', 'show_single_fee','show_digital_signature',
        'copy_to_print','show_balance','show_school_stamp','show_class','show_stream','show_fee_amount'];


}
