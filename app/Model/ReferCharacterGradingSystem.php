<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferCharacterGradingSystem extends Model {

    /**
     * Generated
     */

    protected $table = 'refer_character_grading_systems';
    protected $fillable = ['id', 'grade_remark', 'grade', 'description', 'points'];



}
