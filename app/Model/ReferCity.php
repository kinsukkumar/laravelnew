<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferCity extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.refer_cities';
    protected $fillable = ['id', 'countryid', 'city'];


    public function referCountry() {
        return $this->belongsTo(\App\Model\ReferCountry::class, 'countryid', 'id');
    }


}
