<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferCountry extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.refer_countries';
    protected $fillable = ['id', 'country_code', 'dialling_code', 'country'];


    public function referCities() {
        return $this->hasMany(\App\Model\ReferCity::class, 'countryid', 'id');
    }


}
