<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferCurrency extends Model {

    protected $table = 'constant.refer_currencies';
    protected $fillable = ['id', 'country_origin', 'currency', 'symbol', 'unit'];

    public function bankAccounts() {
        return $this->hasMany(\App\Model\BankAccount::class, 'refer_currency_id', 'id');
    }

}
