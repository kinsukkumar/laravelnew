<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferExam extends Model {

    /**
     * Generated
     */
    protected $table = 'refer_exam';
    protected $fillable = ['id', 'name', 'classlevel_id', 'note', 'abbreviation', 'exam_group_id','created_at','updated_at'];

    public function classlevel() {
        return $this->belongsTo(\App\Model\Classlevel::class, 'classlevel_id', 'classlevel_id');
    }

    public function exams() {
        return $this->hasMany(\App\Model\Exam::class, 'refer_exam_id', 'id');
    }

    public function examGroup() {
        return $this->belongsTo(\App\Model\ExamGroup::class,'exam_group_id','id')->withDefault(['name'=>'Not Defined']);
    }

}
