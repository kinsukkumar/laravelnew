<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferExpense extends Model {

    /**
     * Generated
     */

    protected $table = 'refer_expense';
    protected $fillable = ['id', 'name', 'create_date', 'financial_category_id', 'note', 'status', 'code', 'open_balance', 'account_group_id','chart_no'];


    public function financialCategory() {
        return $this->belongsTo(\App\Model\FinancialCategory::class, 'financial_category_id', 'id');
    }

    public function accountGroup() {
        return $this->belongsTo(\App\Model\AccountGroup::class, 'account_group_id', 'id');
    }

    public function bankAccounts() {
        return $this->belongsToMany(\App\Model\BankAccount::class, 'revenues', 'refer_expense_id', 'bank_account_id');
    }

    public function expenses() {
        return $this->hasMany(\App\Model\Expense::class, 'refer_expense_id', 'id');
    }

    public function revenues() {
        return $this->hasMany(\App\Model\Revenue::class, 'refer_expense_id', 'id');
    }

    public function productRegister(){
         return $this->belongsTo(\App\Model\ProductRegister::class)->withDefault(['name'=>'Not Defined']);
    }

}
