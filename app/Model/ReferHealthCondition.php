<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferHealthCondition extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.refer_health_conditions';
    protected $fillable = ['id', 'health_condition', 'description'];



}
