<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferParentType extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.refer_parent_types';
    protected $fillable = ['id', 'parent_type'];


    public function students() {
        return $this->hasMany(\App\Model\Student::class, 'parent_type_id', 'id');
    }


}
