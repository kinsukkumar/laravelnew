<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferProfession extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.refer_professions';
    protected $fillable = ['id', 'name'];



}
