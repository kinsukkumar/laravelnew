<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferReligion extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.refer_religions';
    protected $fillable = ['id', 'religion', 'description'];


    public function teachers() {
        return $this->hasMany(\App\Model\Teacher::class, 'religion_id', 'id');
    }

    public function students() {
        return $this->hasMany(\App\Model\Student::class, 'religion_id', 'id');
    }


}
