<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferSubject extends Model {

    /**
     * Generated
     */

    protected $table = 'refer_subject';
    protected $primaryKey = 'subject_id';
    protected $fillable = ['subject_id', 'subject_name', 'code', 'arrangement'];


    public function subjects() {
        return $this->hasMany(\App\Model\Subject::class, 'subject_id', 'subject_id');
    }

    public function sectionSubjectTeachers() {
        return $this->hasMany(\App\Model\SectionSubjectTeacher::class, 'refer_subject_id', 'subject_id');
    }


}
