<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferSubjectDefinition extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.refer_subject_definition';
    protected $fillable = ['id', 'name', 'code', 'arrangement', 'classlevel', 'country_id'];



}
