<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model {

    protected $table = 'reminders';
    public $timestamps = false;
    protected $fillable = ['id', 'user_id', 'role_id', 'date', 'time', 'mailandsmstemplate_id', 'title', 'is_repeated', 'days'];

    public function mailandsmstemplate() {
        return $this->belongsTo(\App\Model\Mailandsmstemplate::class, 'mailandsmstemplate_id', 'id')->withDefault(['template' => 'unknown']);
    }

    public function role() {
        return $this->belongsTo(\App\Model\Role::class, 'role_id', 'id')->withDefault(['name' => 'unknown']);
    }

}
