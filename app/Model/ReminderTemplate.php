<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReminderTemplate extends Model {

    /**
     * Generated
     */

    protected $table = 'reminder_template';
    protected $fillable = ['id', 'template', 'type', 'academic_year_id'];


    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }


}
