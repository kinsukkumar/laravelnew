<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReplyMsg extends Model {

    /**
     * Generated
     */

    protected $table = 'reply_msg';
    protected $primaryKey = 'replyID';
    protected $fillable = ['replyID', 'messageID', 'reply_msg', 'status', 'create_time', 'sender_id', 'sender_table'];



}
