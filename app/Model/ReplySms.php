<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReplySms extends Model {

    /**
     * Generated
     */

    protected $table = 'reply_sms';
    public $timestamps=false;
    protected $fillable = ['id', 'secret', 'from', 'message_id', 'message', 'sent_to', 'device_id', 'user_id', 'table', 'sent_timestamp', 'opened'];



}
