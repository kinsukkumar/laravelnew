<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Reset extends Model {

    /**
     * Generated
     */

    protected $table = 'reset';
    protected $primaryKey = 'resetID';
    protected $fillable = ['resetID', 'keyID', 'email', 'code'];



}
