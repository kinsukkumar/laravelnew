<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Revenue extends Model {

    /**
     * Generated
     */
    protected $table = 'revenues';
    protected $fillable = ['id', 'payer_name', 'payer_phone', 'payer_email', 'refer_expense_id', 'amount', 'created_by_id', 'created_by_table', 'payment_type_id', 'payment_method', 'transaction_id', 'bank_account_id', 'invoice_number', 'note', 'date', 'user_in_shulesoft', 'user_id', 'user_table', 'reconciled'];

    public function referExpense() {
        return $this->belongsTo(\App\Model\ReferExpense::class, 'refer_expense_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

    public function productsale() {
        return $this->hasOne(\App\Model\ProductSale::class, 'revenue_id', 'id');
    }

    public function bankAccount() {
        return $this->belongsTo(\App\Model\BankAccount::class, 'bank_account_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

    public function paymentType() {
        return $this->belongsTo(\App\Model\PaymentType::class, 'payment_type_id', 'id');
    }

}
