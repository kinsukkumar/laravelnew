<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RevenueCart extends Model {

    /**
     * Generated
     */
    
    protected $table = 'revenue_cart';
    
    protected $fillable = ['id', 'note', 'refer_expense_id', 'date', 'name', 'created_by_table', 'amount', 'created_by_id', 'revenue_id'];
    
    public function revenues() { 
        return $this->belongsTo(\App\Model\Revenue::class, 'revenue_id', 'id');
    }

    public function referExpense() { 
        return $this->belongsTo(\App\Model\ReferExpense::class, 'refer_expense_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

}
