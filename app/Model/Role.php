<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

    /**
     * Generated
     */

    protected $table = 'role';
    protected $fillable = ['id', 'name', 'sys_role', 'is_super'];


    public function permissions() {
        return $this->belongsToMany(\App\Model\Permission::class, 'role_permission', 'role_id', 'permission_id');
    }

    public function roles() {
        return $this->belongsToMany(\App\Model\Role::class, 'user_role', 'role_id', 'role_id');
    }

    public function users() {
        return $this->hasMany(\App\Model\User::class, 'role_id', 'id');
    }

    public function rolePermissions() {
        return $this->hasMany(\App\Model\RolePermission::class, 'role_id', 'id');
    }

    public function userRoles() {
        return $this->hasMany(\App\Model\UserRole::class, 'role_id', 'id');
    }


}
