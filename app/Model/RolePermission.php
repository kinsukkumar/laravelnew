<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model {

    /**
     * Generated
     */

    protected $table = 'role_permission';
    protected $fillable = ['id', 'role_id', 'permission_id'];


    public function permission() {
        return $this->belongsTo(\App\Model\Permission::class, 'permission_id', 'id');
    }

    public function role() {
        return $this->belongsTo(\App\Model\Role::class, 'role_id', 'id');
    }


}
