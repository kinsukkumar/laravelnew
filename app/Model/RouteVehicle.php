<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteVehicle extends Model {

    /**
     * Generated
     */

    protected $table = 'route_vehicle';
    protected $fillable = ['transport_id', 'vehicle_id', 'id'];


    public function transportRoute() {
        return $this->belongsTo(\App\Model\TransportRoute::class, 'transport_id', 'id');
    }

    public function vehicle() {
        return $this->belongsTo(\App\Model\Vehicle::class, 'vehicle_id', 'id');
    }


}
