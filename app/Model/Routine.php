<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Routine extends Model {

    /**
     * Generated
     */

    protected $table = 'routine';
    protected $fillable = ['routineID', 'classesID', 'sectionID', 'subjectID', 'day', 'start_time', 'end_time', 'room'];


    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'classesID', 'classesID');
    }

    public function section() {
        return $this->belongsTo(\App\Model\Section::class, 'sectionID', 'sectionID');
    }

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subjectID', 'subjectID');
    }


}
