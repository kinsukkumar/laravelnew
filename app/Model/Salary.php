<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model {

    /**
     * Generated
     */
    protected $table = 'salaries';
    protected $fillable = ['id', 'user_id', 'table', 'basic_pay', 'allowance', 'gross_pay', 'pension_fund', 'deduction', 'tax', 'paye', 'net_pay', 'payment_date', 'allowance_distribution', 'deduction_distribution', 'pension_distribution', 'reference'];

    public function pensions() {
        return $this->belongsToMany(\App\Model\Pension::class, 'salary_pensions', 'salary_id', 'pension_id');
    }

    public function deductions() {
        return $this->belongsToMany(\App\Model\Deduction::class, 'salary_deductions', 'salary_id', 'deduction_id');
    }

    public function salaryPensions() { 
        return $this->hasMany(\App\Model\SalaryPension::class, 'salary_id', 'id');
    }

    public function salaryDeductions() {
        return $this->hasMany(\App\Model\SalaryDeduction::class, 'salary_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\Model\User::class)->where('table', $this->attributes['table'])->where('id', $this->attributes['user_id'])->where('status',1)->withDefault(['name' => 'Unknown']);
    }

}
