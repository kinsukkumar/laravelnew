<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SalaryAllowance extends Model {

    /**
     * Generated
     */
    protected $table = 'salary_allowances';
    protected $fillable = ['id', 'salary_id', 'allowance_id', 'amount', 'created_by'];

    public function allowance() {
        return $this->belongsTo('App\Model\Allowance');
    }

     public function salary() {
        return $this->belongsTo('App\Model\Salary');
    }
}
