<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SalaryDeduction extends Model {

    /**
     * Generated
     */

    protected $table = 'salary_deductions';
    protected $fillable = ['id', 'salary_id', 'deduction_id', 'amount', 'created_by'];


    public function deduction() {
        return $this->belongsTo(\App\Model\Deduction::class, 'deduction_id', 'id');
    }

    public function salary() {
        return $this->belongsTo(\App\Model\Salary::class, 'salary_id', 'id');
    }


}
