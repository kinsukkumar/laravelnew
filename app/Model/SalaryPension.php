<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SalaryPension extends Model {

    /**
     * Generated
     */

    protected $table = 'salary_pensions';
    protected $fillable = ['id', 'salary_id', 'pension_id', 'amount', 'created_by', 'employer_amount'];


    public function pension() {
        return $this->belongsTo(\App\Model\Pension::class, 'pension_id', 'id');
    }

    public function salary() {
        return $this->belongsTo(\App\Model\Salary::class, 'salary_id', 'id');
    }


}
