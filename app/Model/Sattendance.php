<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sattendance extends Model {

    /**
     * Generated
     */

    protected $table = 'sattendances';
    protected $fillable = ['id', 'student_id', 'created_by', 'created_by_table', 'date', 'present', 'absent_reason', 'absent_reason_id'];


    public function absentReason() {
        return $this->belongsTo(\App\Model\AbsentReason::class, 'absent_reason_id', 'id');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }


}
