<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class School extends Model {

    protected $table = 'admin.schools';
    protected $fillable = ['region', 'district', 'ward', 'name', 'ownership', 'type'];

}
