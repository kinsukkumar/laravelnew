<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SchoolSession extends Model {

    /**
     * Generated
     */

    protected $table = 'school_sessions';
    protected $fillable = ['session_id', 'ip_address', 'user_agent', 'last_activity', 'user_data'];



}
