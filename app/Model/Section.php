<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Section extends Model {

    /**
     * Generated
     */

    protected $table = 'section';
    protected $primaryKey = 'sectionID';
    protected $fillable = ['sectionID', 'section', 'category', 'classesID', 'teacherID', 'note', 'extra','special_grade_name_id'];


    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'classesID', 'classesID');
    }

    public function teacher() {
        return $this->belongsTo(\App\Model\Teacher::class, 'teacherID', 'teacherID');
    }

    public function subjects() {
        return $this->belongsToMany(\App\Model\Subject::class, 'subject_section', 'section_id', 'subject_id');
    }

    public function subjectSections() {
        return $this->hasMany(\App\Model\SubjectSection::class, 'section_id', 'sectionID');
    }

    public function examschedules() {
        return $this->hasMany(\App\Model\Examschedule::class, 'sectionID', 'sectionID');
    }

    public function studentArchives() {
        return $this->hasMany(\App\Model\StudentArchive::class, 'section_id', 'sectionID');
    }

    public function routines() {
        return $this->hasMany(\App\Model\Routine::class, 'sectionID', 'sectionID');
    }

    public function sectionSubjectTeachers() {
        return $this->hasMany(\App\Model\SectionSubjectTeacher::class, 'sectionID', 'sectionID');
    }

    public function students() {
        return $this->hasMany(\App\Model\Student::class, 'sectionID', 'sectionID');
    }


}
