<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SectionSubjectTeacher extends Model {

    /**
     * Generated
     */

    protected $table = 'section_subject_teacher';
    protected $fillable = ['id', 'sectionID', 'academic_year_id', 'teacherID', 'refer_subject_id', 'subject_id', 'classesID'];


    public function referSubject() {
        return $this->belongsTo(\App\Model\ReferSubject::class, 'refer_subject_id', 'subject_id');
    }

    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'classesID', 'classesID');
    }

    public function section() {
        return $this->belongsTo(\App\Model\Section::class, 'sectionID', 'sectionID');
    }

    public function teacher() {
        return $this->belongsTo(\App\Model\Teacher::class, 'teacherID', 'teacherID');
    }

    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subject_id', 'subjectID');
    }


}
