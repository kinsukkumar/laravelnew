<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model {

    /**
     * Generated
     */

    protected $table = 'semester';
    protected $fillable = ['id', 'name', 'class_level_id', 'academic_year_id', 'start_date', 'end_date', 'study_days'];


    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }

    public function classlevel() {
        return $this->belongsTo(\App\Model\Classlevel::class, 'class_level_id', 'classlevel_id');
    }

    public function exams() {
        return $this->hasMany(\App\Model\Exam::class, 'semester_id', 'id');
    }

    public function examReports() {
        return $this->hasMany(\App\Model\ExamReport::class, 'semester_id', 'id');
    }


}
