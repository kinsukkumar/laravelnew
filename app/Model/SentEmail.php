<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SentEmail extends Model {

    /**
     * Generated
     */

    protected $table = 'admin.sent_emails';
    protected $fillable = ['id', 'sent', 'email_config_id'];


    public function emailConfig() {
        return $this->belongsTo(\App\Model\EmailConfig::class, 'email_config_id', 'id');
    }


}
