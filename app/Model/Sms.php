<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model {

    /**
     * Generated
     */

    protected $table = 'sms';
    protected $primaryKey = 'sms_id';
    public $timestamps=false;
    protected $fillable = ['sms_id', 'body', 'user_id', 'status', 'return_code', 'phone_number', 'type', 'table', 'priority', 'opened'];



}
