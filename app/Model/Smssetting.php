<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Smssetting extends Model {

    /**
     * Generated
     */

    protected $table = 'smssettings';
    protected $fillable = ['smssettingsID', 'types', 'field_names', 'field_values', 'smssettings_extra'];



}
