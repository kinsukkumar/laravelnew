<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpecialGrade extends Model {

    /**
     * Generated
     */

    protected $table = 'special_grades';
    protected $fillable = ['id', 'grade', 'point', 'gradefrom', 'gradeupto', 'note', 'classlevel_id', 'overall_note', 'overall_academic_note', 'special_grade_name_id'];


    public function specialGradeName() {
        return $this->belongsTo(\App\Model\SpecialGradeName::class, 'special_grade_name_id', 'id');
    }

    public function classlevel() {
        return $this->belongsTo(\App\Model\Classlevel::class, 'classlevel_id', 'classlevel_id');
    }


}
