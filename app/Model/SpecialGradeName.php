<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpecialGradeName extends Model {

    /**
     * Generated
     */

    protected $table = 'special_grade_names';
    protected $fillable = ['id', 'name', 'note', 'special_for'];


    public function classlevels() {
        return $this->belongsToMany(\App\Model\Classlevel::class, 'special_grades', 'special_grade_name_id', 'classlevel_id');
    }

    public function classes() {
        return $this->hasMany(\App\Model\Classes::class, 'special_grade_name_id', 'id');
    }

    public function specialGrades() {
        return $this->hasMany(\App\Model\SpecialGrade::class, 'special_grade_name_id', 'id');
    }


}
