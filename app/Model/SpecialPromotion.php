<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpecialPromotion extends Model {

    /**
     * Generated
     */

    protected $table = 'special_promotion';
    protected $fillable = ['id', 'student_id', 'from_academic_year_id', 'to_academic_year_id', 'pass_mark', 'remark', 'status'];


    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }


}
