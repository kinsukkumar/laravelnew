<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model {

    /**
     * Generated
     */

    protected $table = 'sponsors';
    protected $fillable = ['id', 'name', 'location_address', 'phone', 'sex', 'photo', 'created_at', 'email'];

    public function students() {
        return $this->belongsToMany(\App\Model\Student::class, 'student_sponsors', 'sponsor_id', 'student_id');
    }

    public function studentSponsors() {
        return $this->hasMany(\App\Model\StudentSponsor::class, 'sponsor_id', 'id');
    }


}
