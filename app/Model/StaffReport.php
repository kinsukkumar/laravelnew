<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StaffReport extends Model {

    /**
     * Generated
     */

    protected $table = 'staff_report';
    protected $primaryKey = 'id';
    protected $fillable = ['id','user_id', 'user_table', 'date', 'comment', 'title', 'attach', 'attach_file_name','status'];

    public function assignment() {
        return $this->belongsTo(\App\Model\Assignment::class, 'assignment_id', 'id');
    }

    
}
