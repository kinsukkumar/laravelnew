<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentAddress extends Model {

    /**
     * Generated
     */

    protected $table = 'student_addresses';
    protected $fillable = ['id', 'district_id', 'ward', 'village', 'student_id'];


    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }


}
