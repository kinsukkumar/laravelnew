<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentArchive extends Model {

    /**
     * Generated
     */

    protected $table = 'student_archive';
    protected $fillable = ['id', 'student_id', 'academic_year_id', 'section_id', 'due_amount', 'status', 'status_id'];


    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }

    public function section() {
        return $this->belongsTo(\App\Model\Section::class, 'section_id', 'sectionID');
    }

    public function studentStatus() {
        return $this->belongsTo(\App\Model\StudentStatus::class, 'status_id', 'id');
    }


}
