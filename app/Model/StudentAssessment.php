<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentAssessment extends Model {

    /**
     * Generated
     */

    protected $table = 'student_assessment';
      protected $primaryKey = 'id';
    protected $fillable = ['valid_answer_id','student_id', 'question_id', 'student_answer','attach','score'];

    public function question() {
        return $this->belongsTo(\App\Model\Questions::class, 'question_id', 'id');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }
    
    public function validAnswer() {
        return $this->belongsTo(\App\Model\ValidAnswer::class, 'valid_answer_id', 'id');
    }
    
}
