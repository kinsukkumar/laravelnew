<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentCharacter extends Model {

    /**
     * Generated
     */

    protected $table = 'student_characters';
    protected $fillable = ['id', 'student_id', 'exam_id', 'character_id', 'teacher_id', 'grade1', 'grade2', 'remark', 'classes_id', 'status', 'grade', 'semester_id'];


    public function exam() {
        return $this->belongsTo(\App\Model\Exam::class, 'exam_id', 'examID');
    }

    public function character() {
        return $this->belongsTo(\App\Model\Character::class, 'character_id', 'id');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }


}
