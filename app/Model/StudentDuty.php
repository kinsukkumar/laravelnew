<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentDuty extends Model {

    protected $table = 'student_duties';
    protected $fillable = ['id', 'duty_id', 'student_id', 'created_at', 'updated_at'];

    public function duty() {
        return $this->belongsTo(\App\Model\Duty::class);
    }

     public function student() {
        return $this->belongsTo(\App\Model\Student::class,'student_id','student_id');
    }
}
