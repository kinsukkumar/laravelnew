<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentFeesInstallmentsUnsubscription extends Model {

    /**
     * Generated
     */

    protected $table = 'student_fees_installments_unsubscriptions';
    protected $fillable = ['id', 'fees_installment_id', 'student_id'];


    public function feesInstallment() {
        return $this->belongsTo(\App\Model\FeesInstallment::class, 'fees_installment_id', 'id');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }


}
