<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentOther extends Model {

    /**
     * Generated
     */

    protected $table = 'student_other';
    protected $fillable = ['admitted_from', 'region', 'reg_number', 'student_id', 'year_finished', 'other_subject', 'school_id'];


    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function school() {
         return $this->belongsTo(\App\Model\School::class, 'school_id','id')->withDefault(['name'=>'Not defined','region'=>'not defined']);
    }

}
