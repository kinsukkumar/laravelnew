<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentParent extends Model {

    /**
     * Generated
     */

    protected $table = 'student_parents';
    protected $fillable = ['id', 'student_id', 'parent_id', 'relation'];


    public function parents() {
        return $this->belongsTo(\App\Model\Parents::class, 'parent_id', 'parentID');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }


}
