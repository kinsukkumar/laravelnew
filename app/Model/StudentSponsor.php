<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentSponsor extends Model {

    /**
     * Generated
     */

    protected $table = 'student_sponsors';
    protected $fillable = ['id', 'student_id', 'sponsor_id', 'status', 'created_at', 'updated_at'];


    public function sponsor() {
        return $this->belongsTo(\App\Model\Sponsor::class, 'sponsor_id', 'id');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }


}
