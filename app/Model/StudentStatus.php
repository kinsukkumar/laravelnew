<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentStatus extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.student_status';
      protected $primaryKey = 'id';
    protected $fillable = ['id', 'reason', 'is_report'];



}
