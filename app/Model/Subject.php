<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model {
    /**
     * Generated
     */

    protected $table = 'subject';
       protected $primaryKey = 'subjectID';
    protected $fillable = ['subjectID', 'classesID', 'teacherID', 'subject', 'subject_type', 'teacher_name', 'is_counted', 'is_penalty', 'pass_mark', 'grade_mark', 'subject_id', 'semester_id', 'credit_number', 'is_counted_indivision'];


    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'classesID', 'classesID');
    }

    public function referSubject() {
        return $this->belongsTo(\App\Model\ReferSubject::class, 'subject_id', 'subject_id');
    }

    public function teacher() {
        return $this->belongsTo(\App\Model\Teacher::class, 'teacherID', 'teacherID');
    }

    public function sections() {
        return $this->belongsToMany(\App\Model\Section::class, 'subject_section', 'subject_id', 'section_id');
    }

    public function classesPromotion() {
        return $this->belongsToMany(\App\Model\Classes::class, 'promotionsubject', 'subjectID', 'classesID');
    }

    public function academicYears() {
        return $this->belongsToMany(\App\Model\AcademicYear::class, 'subject_topic', 'subjectID', 'academic_year_id');
    }

    public function books() {
        return $this->hasMany(\App\Model\Book::class, 'subject_id', 'subjectID');
    }

    public function subjectSections() {
        return $this->hasMany(\App\Model\SubjectSection::class, 'subject_id', 'subjectID');
    }

    public function subjectStudents() {
        return $this->hasMany(\App\Model\SubjectStudent::class, 'subject_id', 'subjectID');
    }

    public function examschedules() {
        return $this->hasMany(\App\Model\Examschedule::class, 'subjectID', 'subjectID');
    }

    public function marks() {
        return $this->hasMany(\App\Model\Mark::class, 'subjectID', 'subjectID');
    }

    public function promotionsubjects() {
        return $this->hasMany(\App\Model\Promotionsubject::class, 'subjectID', 'subjectID');
    }

    public function routines() {
        return $this->hasMany(\App\Model\Routine::class, 'subjectID', 'subjectID');
    }

    public function syllabusTopics() {
        return $this->hasMany(\App\Model\SyllabusTopic::class, 'subject_id', 'subjectID');
    }

    public function sectionSubjectTeachers() {
        return $this->hasMany(\App\Model\SectionSubjectTeacher::class, 'subject_id', 'subjectID');
    }

    public function subjectMarks() {
        return $this->hasMany(\App\Model\SubjectMark::class, 'subjectID', 'subjectID');
    }

    public function subjectTopics() {
        return $this->hasMany(\App\Model\SubjectTopic::class, 'subjectID', 'subjectID');
    }

}
