<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubjectCount extends Model {

    protected $table = 'subject_count';

    public function student() {
        return $this->belongsTo('\App\Model\Student', 'student_id');
    }

    public function academic_year() {
        return $this->belongsTo(App\Model\AcademicYear::class, 'academic_year_id');
    }

    public function section() {
        return $this->belongsTo('\App\Model\Section', 'sectionID');
    }

    public function classes() {
        return $this->belongsTo('\App\Model\Classes', 'classesID');
    }

    public function teacher() {
        return $this->belongsTo('\App\Model\Teacher', 'teacher_id','teacherID')->withDefault(['name' =>\App\Model\Section::find($this->attributes['section_id'])->teacher->name]);
    }

}
