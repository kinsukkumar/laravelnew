<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubjectMark extends Model {

    /**
     * Generated
     */

    protected $table = 'subject_mark';
    protected $fillable = ['grade_mark', 'effort_mark', 'achievement_mark', 'student_id', 'subjectID', 'classesID', 'academic_year_id', 'examID', 'sectionID', 'year', 'exam', 'subject', 'id', 'status'];


    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'classesID', 'classesID');
    }

    public function exam() {
        return $this->belongsTo(\App\Model\Exam::class, 'examID', 'examID');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subjectID', 'subjectID');
    }


}
