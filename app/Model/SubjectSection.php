<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubjectSection extends Model {

    /**
     * Generated
     */

    protected $table = 'subject_section';
    protected $fillable = ['subject_section_id', 'subject_id', 'section_id'];


    public function section() {
        return $this->belongsTo(\App\Model\Section::class, 'section_id', 'sectionID');
    }

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subject_id', 'subjectID');
    }


}
