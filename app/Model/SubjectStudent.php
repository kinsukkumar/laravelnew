<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubjectStudent extends Model {

    /**
     * Generated
     */

    protected $table = 'subject_student';
    protected $primaryKey = 'subject_student_id';
    protected $fillable = ['subject_student_id', 'subject_id', 'student_id', 'academic_year_id'];


    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subject_id', 'subjectID');
    }


}
