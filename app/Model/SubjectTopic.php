<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubjectTopic extends Model {

    /**
     * Generated
     */

    protected $table = 'subject_topic';
    protected $fillable = ['id', 'subject_id', 'subjectID', 'topic_name', 'status', 'semester_id', 'academic_year_id', 'classesID'];


    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }

    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subjectID', 'subjectID');
    }


}
