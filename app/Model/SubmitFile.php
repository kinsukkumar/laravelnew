<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubmitFile extends Model {

    /**
     * Generated
     */

    protected $table = 'submit_files';
    protected $primaryKey = 'id';
    protected $fillable = ['id','assignment_submit_id', 'attach', 'attach_file_name','status'];

    public function submits() {
        return $this->belongsTo(\App\Model\AssignmentSubmit::class, 'assignment_submit_id', 'id');
    }

    
}
