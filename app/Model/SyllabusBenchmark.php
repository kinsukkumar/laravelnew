<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SyllabusBenchmark extends Model {

    /**
     * Generated
     */

    protected $table = 'syllabus_benchmarks';
    protected $fillable = ['id', 'grade_remark', 'grade', 'description', 'points'];


    public function syllabusStudentBenchmarkings() {
        return $this->hasMany(\App\Model\SyllabusStudentBenchmarking::class, 'syllabus_benchmark_id', 'id');
    }


}
