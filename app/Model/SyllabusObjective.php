<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SyllabusObjective extends Model {

    /**
     * Generated
     */

    protected $table = 'syllabus_objectives';
    protected $fillable = ['id', 'syllabus_subtopic_id', 'objective', 'activities', 'resources', 'assessment_criteria', 'remarks', 'periods'];


    public function syllabusSubtopic() {
        return $this->belongsTo(\App\Model\SyllabusSubtopic::class, 'syllabus_subtopic_id', 'id');
    }

    public function syllabusObjectiveReferences() {
        return $this->hasMany(\App\Model\SyllabusObjectiveReference::class, 'syllabus_objective_id', 'id');
    }

    public function syllabusStudentBenchmarkings() {
        return $this->hasMany(\App\Model\SyllabusStudentBenchmarking::class, 'syllabus_objective_id', 'id');
    }

 

}
