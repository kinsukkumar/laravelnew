<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SyllabusObjectiveReference extends Model {

    /**
     * Generated
     */

    protected $table = 'syllabus_objective_references';
    protected $fillable = ['id', 'syllabus_objective_id', 'book_id', 'page_description', 'reference_type', 'created_by_id', 'created_by_table'];


    public function syllabusObjective() {
        return $this->belongsTo(\App\Model\SyllabusObjective::class, 'syllabus_objective_id', 'id');
    }

 public function book() {
        return $this->belongsTo(\App\Model\Book::class, 'book_id', 'id');
    }

}
