<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SyllabusStudentBenchmarking extends Model {

    /**
     * Generated
     */

    protected $table = 'syllabus_student_benchmarking';
    protected $fillable = ['id', 'student_id', 'syllabus_benchmark_id', 'syllabus_objective_id', 'created_by_id', 'created_by_table', 'semester_id', 'syllabus_topic_id'];


    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function syllabusBenchmark() {
        return $this->belongsTo(\App\Model\SyllabusBenchmark::class, 'syllabus_benchmark_id', 'id');
    }

    public function syllabusObjective() {
        return $this->belongsTo(\App\Model\SyllabusObjective::class, 'syllabus_objective_id', 'id');
    }

    public function syllabusTopic() {
        return $this->belongsTo(\App\Model\SyllabusTopic::class, 'syllabus_benchmark_id', 'id');
    }


}
