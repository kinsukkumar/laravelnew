<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SyllabusSubtopic extends Model {

    /**
     * Generated
     */

    protected $table = 'syllabus_subtopics';
    protected $fillable = ['id', 'syllabus_topic_id', 'code', 'subtitle', 'start_date', 'end_date'];


    public function syllabusTopic() {
        return $this->belongsTo(\App\Model\SyllabusTopic::class, 'syllabus_topic_id', 'id');
    }

    public function syllabusObjectives() {
        return $this->hasMany(\App\Model\SyllabusObjective::class, 'syllabus_subtopic_id', 'id');
    }


}
