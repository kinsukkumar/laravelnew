<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SyllabusSubtopicsTeacher extends Model {

    /**
     * Generated
     */
    protected $table = 'syllabus_subtopics_teachers';
    protected $fillable = ['id', 'syllabus_subtopic_id', 'teacher_id', 'year', 'created_at', 'updated_at'];

    public function syllabusSubTopic() {
        return $this->belongsTo(\App\Model\SyllabusSubTopic::class, 'syllabus_subtopic_id', 'id');
    }

    public function teacher() {
        return $this->belongsTo(\App\Model\Teacher::class, 'teacher_id', 'teacherID');
    }

}
