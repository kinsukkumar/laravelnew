<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SyllabusTopic extends Model {

    /**
     * Generated
     */

    protected $table = 'syllabus_topics';
    protected $fillable = ['id', 'code', 'title', 'subject_id', 'start_date', 'end_date'];


    public function subject() {
        return $this->belongsTo(\App\Model\Subject::class, 'subject_id', 'subjectID');
    }

    public function syllabusStudentBenchmarkings() {
        return $this->hasMany(\App\Model\SyllabusStudentBenchmarking::class, 'syllabus_benchmark_id', 'id');
    }

    public function syllabusSubtopics() {
        return $this->hasMany(\App\Model\SyllabusSubtopic::class, 'syllabus_topic_id', 'id');
    }

    public function media() {
        return $this->hasMany(\App\Model\Media::class);
    }

}
