<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model {

    /**
     * Generated
     */

    protected $table = 'teacher';
      protected $primaryKey = 'teacherID';
    protected $fillable = ['teacherID', 'name', 'designation', 'dob', 'sex', 'religion', 'email', 'phone', 'address', 'jod', 'photo', 'username', 'password', 'usertype', 'employment_type', 'signature', 'id_number', 'library', 'signature_path', 'designation_id', 'religion_id', 'education_level_id', 'health_status_id', 'health_insurance_id', 'physical_condition_id', 'nationality', 'salary', 'default_password', 'status', 'status_id', 'bank_account_number', 'bank_name', 'remember', 'number','location','region','country_id','payroll_status','school_id','head_teacher_name','school_phone_number','role_id'];


    public function employmentType() {
        return $this->belongsTo(\App\Model\EmploymentType::class, 'employment_type_id', 'id');
    }

    public function healthInsurance() {
        return $this->belongsTo(\App\Model\HealthInsurance::class, 'health_insurance_id', 'id')->withDefault(['name'=>'Unknown']);
    }

    public function physicalCondition() {
        return $this->belongsTo(\App\Model\PhysicalCondition::class, 'physical_condition_id', 'id')->withDefault(['name'=>'Unknown']);
    }

    public function referReligion() {
        return $this->belongsTo(\App\Model\ReferReligion::class, 'religion_id', 'id');
    }

    public function students() {
        return $this->belongsToMany(\App\Model\Student::class, 'general_character_assessment', 'class_teacher_id', 'student_id');
    }

    public function teacherslessonPlans() {
        return $this->belongsToMany(\App\Model\LessonPlan::class, 'lesson_plan', 'teacher_id', 'syllabus_objective_id');
    }

    public function sections() {
        return $this->hasMany(\App\Model\Section::class, 'teacherID', 'teacherID');
    }

    public function classes() {
        return $this->hasMany(\App\Model\Classes::class, 'teacherID', 'teacherID');
    }

    public function generalCharacterAssessments() {
        return $this->hasMany(\App\Model\GeneralCharacterAssessment::class, 'class_teacher_id', 'teacherID');
    }

    public function subjects() {
        return $this->hasMany(\App\Model\Subject::class, 'teacherID', 'teacherID');
    }

    public function sectionSubjectTeachers() {
        return $this->hasMany(\App\Model\SectionSubjectTeacher::class, 'teacherID', 'teacherID');
    }

    public function uattendances() {
        return $this->hasMany(\App\Model\Uattendance::class, 'user_id', 'teacherID');
    }

    public function lessonPlans() {
        return $this->hasMany(\App\Model\LessonPlan::class, 'teacher_id', 'teacherID');
    }


}
