<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TeacherDesignation extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.teacher_designation';
    protected $fillable = ['id', 'name'];



}
