<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TeacherDuty extends Model
{
     protected $table = 'teacher_duties';
    protected $fillable = ['id', 'duty_id', 'teacher_id', 'created_at', 'updated_at'];

    public function duty() {
        return $this->belongsTo(\App\Model\Duty::class);
    }

     public function teacher() {
        return $this->belongsTo(\App\Model\Teacher::class,'teacher_id','teacherID');
    }
}
