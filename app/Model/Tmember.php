<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tmember extends Model {

    /**
     * Generated
     */

    protected $table = 'tmembers';
    protected $fillable = ['id', 'student_id', 'transport_route_id', 'tjoindate', 'vehicle_id', 'is_oneway'];


    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }

    public function transportRoute() {
        return $this->belongsTo(\App\Model\TransportRoute::class, 'transport_route_id', 'id');
    }
 
    public function installments() {
        return $this->belongsTo(\App\Model\Installment::class, 'installment_id', 'id');
    }
    public function vehicle() {
        return $this->belongsTo(\App\Model\Vehicle::class, 'vehicle_id', 'id');
    }


}
