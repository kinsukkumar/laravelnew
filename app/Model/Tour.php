<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model {

    /**
     * Generated
     */

    protected $table = 'tours';
    protected $fillable = ['id', 'location', 'name'];


    public function tourUsers() {
        return $this->hasMany(\App\Model\TourUser::class, 'tour_id', 'id');
    }


}
