<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TourUser extends Model {

    /**
     * Generated
     */
    protected $table = 'tour_users';
    protected $fillable = ['id', 'tour_id', 'user_id', 'table', 'tour_seen'];

    public function tours() {
        return $this->belongsTo(\App\Model\Tour::class, 'tour_id', 'id');
    }

    public function users() {
        return $this->belongsTo('\App\Model\User', 'user_id', 'id');
    }

}
