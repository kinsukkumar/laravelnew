<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Track extends Model {

    /**
     * Generated
     */

    protected $table = 'track';
    protected $fillable = ['id', 'user_id', 'user_type', 'table_name', 'column_name', 'column_value_from', 'column_final_value', 'status'];



}
