<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TrackPayment extends Model {
    /**
     * Generated
     */

    protected $table = 'track_payments';
     public $timestamps=false;
    protected $fillable = ['id', 'student_id', 'amount', 'payment_type_id', 'date', 'transaction_id', 'cheque_number', 'bank_account_id', 'payer_name', 'mobile_transaction_id', 'session_id', 'account_number', 'token', 'reconciled','created_at'];


    public function bankAccount() {
        return $this->belongsTo(\App\Model\BankAccount::class, 'bank_account_id', 'id')->withDefault(['name'=>'unknown']);
    }

    public function paymentType() {
        return $this->belongsTo(\App\Model\PaymentType::class, 'payment_type_id', 'id')->withDefault(['name'=>'cash']);
    }

    public function student() {
        return $this->belongsTo(\App\Model\Student::class, 'student_id', 'student_id');
    }



}
