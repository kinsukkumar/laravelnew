<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Training extends Model {

    /**
     * Generated
     */
    protected $table = 'trainings';
    protected $fillable = ['id', 'user_id', 'table', 'trainer_id', 'training_checklist_id', 'created_at', 'updated_at'];

    public function trainingChecklist() {
        return $this->belongsTo(\App\Model\TrainingChecklist::class);
    }

    public function user() {
        return \App\Model\User::where('id', $this->attributes['user_id'])->where('table', $this->attributes['table'])->first();
    }

}
