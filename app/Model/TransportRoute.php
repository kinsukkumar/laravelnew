<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TransportRoute extends Model {

    /**
     * Generated
     */

    protected $table = 'transport_routes';
    protected $fillable = ['id', 'name', 'note'];


    public function vehicles() {
        return $this->belongsToMany(\App\Model\Vehicle::class, 'route_vehicle', 'transport_id', 'vehicle_id');
    }

    public function feesInstallments() {
        return $this->belongsToMany(\App\Model\FeesInstallment::class, 'transport_routes_fees_installments', 'transport_route_id', 'fees_installment_id');
    }

    public function routeVehicles() {
        return $this->hasMany(\App\Model\RouteVehicle::class, 'transport_id', 'id');
    }

    public function transportRoutesFeesInstallments() {
        return $this->hasMany(\App\Model\TransportRoutesFeesInstallment::class, 'transport_route_id', 'id');
    }

    public function tmembers() {
        return $this->hasMany(\App\Model\Tmember::class, 'transport_route_id', 'id');
    }


}
