<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TransportRoutesFeesInstallment extends Model {

    /**
     * Generated
     */

    protected $table = 'transport_routes_fees_installments';
    protected $fillable = ['id', 'transport_route_id', 'fees_installment_id', 'amount'];


    public function feesInstallment() {
        return $this->belongsTo(\App\Model\FeesInstallment::class, 'fees_installment_id', 'id');
    }

    public function transportRoute() {
        return $this->belongsTo(\App\Model\TransportRoute::class, 'transport_route_id', 'id');
    }


}
