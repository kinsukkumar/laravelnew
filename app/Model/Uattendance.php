<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Uattendance extends Model {

    /**
     * Generated
     */
    protected $table = 'uattendances';
    protected $fillable = ['id', 'user_id', 'created_by', 'created_by_table', 'user_table', 'date', 'timein', 'timeout', 'present', 'absent_reason', 'absent_reason_id'];

    public function absentReason() {
        return $this->belongsTo(\App\Model\AbsentReason::class, 'absent_reason_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\Model\User::class);
    }

}
