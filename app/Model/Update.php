<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Update extends Model {

    /**
     * Generated
     */

    protected $table = 'admin.updates';
    protected $fillable = ['id', 'for', 'message', 'update_type', 'released_date', 'created_by', 'version'];


    public function userUpdates() {
        return $this->hasMany(\App\Model\UserUpdate::class, 'update_id', 'id');
    }


}
