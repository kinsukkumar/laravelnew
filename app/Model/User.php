<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable {

    use Notifiable;

    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'password', 'username', 'phone', 'table', 'id','payroll_status','fcm_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
    public $timestamps = false;

    public function userInfo($query) {
        $table=$this->attributes['table']=='student' ? 'student_id': $this->attributes['table'].'ID';
        return $query->where($table, $this->attributes['id'])->first();
    }

    public function pension() {
        return $this->hasMany('App\Model\User_pension');
    }

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function tourUser() {
        return $this->hasMany('App\Model\Tour_User');
    }

     public function uattendance() {
        return $this->hasMany(\App\Model\Uattendance::class, 'user_id', 'id');
    }
    
    public function role() {
        return $this->belongsTo(\App\Model\Role::class,'user_id', 'id');
    }
    
    public function user_role() {
        return $this->belongsTo(\App\Model\UserRole::class,'user_id', 'id');
    }
}
