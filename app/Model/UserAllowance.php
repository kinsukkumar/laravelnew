<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserAllowance extends Model {

    /**
     * Generated
     */

    protected $table = 'user_allowances';
    protected $fillable = ['id', 'user_id', 'table', 'allowance_id', 'created_by','deadline', 'type', 'amount'];


    public function allowance() {
        return $this->belongsTo(\App\Model\Allowance::class, 'allowance_id', 'id');
    }


}
