<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserDeduction extends Model {

    /**
     * Generated
     */

    protected $table = 'user_deductions';
    protected $fillable = ['id', 'user_id', 'table', 'deduction_id', 'created_by', 'deadline', 'type', 'amount','loan_application_id'];


    public function deduction() {
        return $this->belongsTo(\App\Model\Deduction::class, 'deduction_id', 'id');
    }


}
