<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserEmail extends Model {

    /**
     * Generated
     */

    protected $table = 'user_email';
    protected $fillable = ['id', 'user_id', 'user_types', 'email'];



}
