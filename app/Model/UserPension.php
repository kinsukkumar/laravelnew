<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPension extends Model {

    /**
     * Generated
     */

    protected $table = 'user_pensions';
    protected $fillable = ['id', 'user_id', 'table', 'pension_id', 'created_by', 'checknumber'];


    public function pension() {
        return $this->belongsTo(\App\Model\Pension::class, 'pension_id', 'id');
    }


}
