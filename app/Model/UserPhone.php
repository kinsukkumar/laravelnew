<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPhone extends Model {

    /**
     * Generated
     */

    protected $table = 'user_phones';
    protected $fillable = ['id', 'user_id', 'table', 'phone_number'];



}
