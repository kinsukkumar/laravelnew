<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model {

    /**
     * Generated
     */

    protected $table = 'user_role';
    protected $fillable = ['id', 'user_id', 'role_id', 'table'];



    public function role() {
        return $this->belongsTo(\App\Model\Role::class, 'role_id', 'id');
    }


}
