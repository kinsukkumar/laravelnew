<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserUpdate extends Model {

    /**
     * Generated
     */

    protected $table = 'user_updates';
    protected $fillable = ['id', 'update_id', 'is_opened', 'like', 'user_id', 'table', 'opened_date'];


}
