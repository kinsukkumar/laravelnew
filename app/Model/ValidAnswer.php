<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ValidAnswer extends Model {

    /**
     * Generated
     */

    protected $table = 'valid_answers';
      protected $primaryKey = 'id';
    protected $fillable = ['question_id', 'status','answer'];

    public function question() {
        return $this->belongsTo(\App\Model\Questions::class, 'question_id', 'id');
    }

}
