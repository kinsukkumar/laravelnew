<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model {

    /**
     * Generated
     */

    protected $table = 'vehicles';
    protected $fillable = ['id', 'plate_number', 'seats', 'description', 'created_by', 'name', 'driver_id', 'conductor_id'];


    public function transportRoutes() {
        return $this->belongsToMany(\App\Model\TransportRoute::class, 'route_vehicle', 'vehicle_id', 'transport_id');
    }

    public function routeVehicles() {
        return $this->hasMany(\App\Model\RouteVehicle::class, 'vehicle_id', 'id');
    }

    public function tmembers() {
        return $this->hasMany(\App\Model\Tmember::class, 'vehicle_id', 'id');
    }


}
