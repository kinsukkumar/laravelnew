<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model {

    /**
     * Generated
     */
    protected $table = 'admin.vendors';
    protected $primaryKey = 'id';
    protected $fillable = ['email', 'name', 'phone_number', 'telephone_number', 'country_id', 'city', 'location', 'bank_id', 'bank_branch', 'account_number', 'contact_person_name', 'contact_person_phone', 'contact_person_email', 'contact_person_jobtitle', 'status'.'created_by','created_by_table','schema_name','attachment'];

    public function vendorProductRegister() {
        return $this->hasMany(\App\Model\VendorProductRegister::class);
    }

    public function referBank() {
        return $this->belongsTo(\App\Model\ReferBank::class, 'bank_id')->withDefault(['name' => 'Not Defined']);
    }

    public function referCountry() {
        return $this->belongsTo(\App\Model\ReferCountry::class, 'country_id')->withDefault(['name' => 'Not Defined']);
    }

}
