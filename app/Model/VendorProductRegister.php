<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VendorProductRegister extends Model {

    /**
     * Generated
     */
    protected $table = 'admin.vendor_product_registers';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'vendor_id', 'product_register_id', 'created_at', 'updated_at'];

    public function vendor() {
        return $this->belongsTo(\App\Model\Vendor::class);
    }

    public function productRegister() {
        return $this->belongsTo(\App\Model\ProductRegister::class);
    }

}
