<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model {

    /**
     * Generated
     */

    protected $table = 'wallets';
     protected $primaryKey = 'id';
    protected $fillable = ['student_id', 'amount','date'];




    public function student() {
        return $this->hasMany(\App\Model\Student::class, 'student_id', 'id');
    }




}
