<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WalletCart extends Model {

    /**
     * Generated
     */
    protected $table = 'wallet_cart';
    protected $fillable = ['id', 'product_alert_id','quantity','amount', 'name','wallet_use_id_id'];
  
    public function walletuse() { 
        return $this->belongsTo(\App\Model\WalletUse::class, 'wallet_use_id_id', 'id');
    }

    public function productQuantity() { 
        return $this->belongsTo(\App\Model\ProductQuantity::class, 'product_alert_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

}
