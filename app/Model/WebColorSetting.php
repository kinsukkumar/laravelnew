<?php

/**
 * Created by Reliese Model.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebColorSetting
 * 
 * @property int $id
 * @property string $inner_header_color
 * @property string $upper_header_color
 * @property string $menu_link_color
 * @property string $main_content_color
 * @property string $footer_color
 * @property string $navigation_header_color
 * 
 *
 * @package App\Models
 */
class WebColorSetting extends Model
{
    protected $table = 'web_setting';
    public $timestamps = false;

    protected $casts = [
        'id' => 'int',
    
    ];

    protected $fillable = [
        'upper_header_color',
        'inner_header_color',
        'footer_color',
        'menu_link_color',
        'main_content_color',
        'navigation_header_color'
    ];


}
