<?php

/**
 * Created by Reliese Model.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebContent
 * 
 * @property int $id
 * @property character varying $title
 * @property character varying $description
 * @property timestamp without time zone|null $published_on
 * @property timestamp without time zone|null $updated_on
 * @property timestamp without time zone|null $schedule_date
 * @property int|null $published_by
 * @property int $menu_id
 * 
 * @property WebMenu $web_menu
 * @property WebMediaContent $web_media_content
 *
 * @package App\Models
 */
class WebContent extends Model
{
	protected $table = 'web_content';
	public $timestamps = false;

	protected $fillable = [
		'title',
		'description',
		'published_on',
		'updated_on',
		'display_style_id',
		'schedule_date',
		'published_by',
		'menu_id'
	];

	public function web_menu()
	{
		return $this->belongsTo(WebMenu::class, 'menu_id','id');
	}

	public function web_display_style()
	{
		return $this->belongsTo(WebDisplayStyle::class, 'display_style_id','id');
	}
}
