<?php

/**
 * Created by Reliese Model.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebDisplayStyle
 * 
 * @property int $id
 * @property character varying|null $name
 * @property character varying|null $position
 *
 * @package App\Models
 */
class WebDisplayStyle extends Model
{
	protected $table = 'web_display_style';
	public $timestamps = false;



	protected $fillable = [
		'name',
		'position'
	];
}
