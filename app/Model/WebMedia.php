<?php

/**
 * Created by Reliese Model.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebMedia
 * 
 * @property int $id
 * @property character varying $name
 * @property character varying|null $description
 * @property character varying|null $url
 * @property timestamp without time zone|null $created_on
 * @property timestamp without time zone|null $updated_on
 * @property int $media_categoy_id
 * 
 * @property WebMediaCategory $web_media_category
 *
 * @package App\Models
 */
class WebMedia extends Model
{
	protected $table = 'web_media';
	public $timestamps = false;

	protected $casts = [
		'name' => 'character varying',
		'description' => 'character varying',
		'url' => 'character varying',
		'created_on' => 'timestamp without time zone',
		'updated_on' => 'timestamp without time zone',
		'media_categoy_id' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'url',
		'created_on',
		'updated_on',
		'media_categoy_id'
	];

	public function web_media_category()
	{
		return $this->belongsTo(WebMediaCategory::class, 'id');
	}
}
