<?php

/**
 * Created by Reliese Model.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebMediaCategory
 * 
 * @property int $id
 * @property character varying|null $name
 * 
 * @property WebMedia $web_media
 *
 * @package App\Models
 */
class WebMediaCategory extends Model
{
	protected $table = 'web_media_category';
	public $timestamps = false;

	protected $casts = [
		'name' => 'character varying'
	];

	protected $fillable = [
		'name'
	];

	public function web_media()
	{
		return $this->hasOne(WebMedia::class, 'id');
	}
}
