<?php

/**
 * Created by Reliese Model.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebMediaContent
 * 
 * @property int $id
 * @property int $media_id
 * @property int $content_id
 * 
 * @property WebContent $web_content
 * @property WebMedia $web_media
 *
 * @package App\Models
 */
class WebMediaContent extends Model
{
	protected $table = 'web_media_content';
	public $timestamps = false;

	protected $casts = [
		'media_id' => 'int',
		'content_id' => 'int'
	];

	protected $fillable = [
		'media_id',
		'content_id'
	];

	public function web_content()
	{
		return $this->belongsTo(WebContent::class, 'id');
	}
	public function web_media()
	{
		return $this->belongsTo(WebMedia::class, 'id');
	}
}
