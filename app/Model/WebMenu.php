<?php

/**
 * Created by Reliese Model.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebMenu
 * 
 * @property int $id
 * @property string $name
 * @property int|null $sub_id
 * @property int|null $position
 * 
 * @property WebContent $web_content
 *
 * @package App\Models
 */
class WebMenu extends Model
{
    protected $table = 'web_menu';
    public $timestamps = false;

    protected $casts = [
        'sub_id' => 'int',
        'position' => 'int'
    ];

    protected $fillable = [
        'name',
        'sub_id',
        'position'
    ];

    public function web_content()
    {
        return $this->hasOne(WebContent::class, 'id');
    }
}
