<?php

/**
 * Created by Reliese Model.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebMenu
 * 
 * @property int $id
 * @property string $name
 * @property string $url
 * 
 * 
 * @package App\Models
 */
class WebSocialNetwork extends Model
{
    protected $table = 'social_network';
    public $timestamps = false;

    protected $casts = [
        'url' => 'string',
        
    ];

    protected $fillable = [
        'name',
        'url'
    ];

}
