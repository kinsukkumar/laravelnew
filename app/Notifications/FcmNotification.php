<?php


namespace App\Notifications;
use App\Library\Firebase;
use App\Library\Push;


class FcmNotification  {
    
	public function notify($receiver_id,$notification_title,$notification_message,$link_url) {

		

		try {


			$firebase = new Firebase();
			$push     = new Push();

			

			$title = $notification_title ?? '';

			// notification message
			$message = $notification_message ?? '';
                        
                        // notification message
			$link =  $link_url ??'';
			$push->setTitle( $title );
			$push->setMessage( $message );
			$push->setLink($link );


			
				$json     = $push->getPush();
                              
				$regId    = $receiver_id ;
				$response = $firebase->send( $regId, $json );

				return response()->json( [
					'response' => $response
				] );
			
			
		} catch ( \Exception $ex ) {
			return response()->json( [
				'error'   => true,
				'message' => $ex->getMessage()
			] );
		}

	}
}
