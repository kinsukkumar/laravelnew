<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;

use DB;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Validator::extend('iunique', function ($attribute, $value, $parameters, $validator) {
            $query = DB::table($parameters[0]);
            $column = $query->getGrammar()->wrap($parameters[1]);
            return !$query->whereRaw("lower({$column}) = lower(?)", [$value])->count();
        });
        Validator::extend('greater_than_field', function($attribute, $value, $parameters, $validator) {
            $min_field = $parameters[0];
            $data = $validator->getData();
            $min_value = $data[$min_field];
            return $value > $min_value;
        });

        Validator::replacer('greater_than_field', function($message, $attribute, $rule, $parameters) {
            return str_replace(':field', $parameters[0], $message);
        });
        Paginator::useBootstrap();
    }

    public function getInstalledStatus() {
        
        if (session('installed') == NULL && createRoute() != 'install@index') {

            $schema = str_replace('.', NULL, set_schema_name());
            $status = \collect(DB::select("SELECT distinct table_schema FROM INFORMATION_SCHEMA.TABLES WHERE lower(table_schema)='" . strtolower($schema) . "'"))->first();
            if (!empty($status)) {
                session(['installed' => 1]);
              
            } else {
                $is_valid_domain=DB::table('admin.clients')->where('username',$schema)->first();
                
                if(empty($is_valid_domain)){                
                     header('location: https://www.shulesoft.com/?source='.$schema.'&token='. sha1($schema).'&utm_=bad_url');
                    exit;
                }
                
                if(!empty($is_valid_domain) && !preg_match('/install/i', createRoute())) {
                  header('location: ' . base_url('install/index'));
                    exit;
                }
                
            }
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
        return $this->getInstalledStatus();
    }

}
