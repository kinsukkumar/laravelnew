<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Barryvdh\Elfinder\ElfinderServiceProvider;
use DB;
class FileServiceProvider extends ElfinderServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router) {
        $schema = str_replace('.', NULL, set_schema_name());
        $status = DB::select("SELECT distinct table_schema FROM INFORMATION_SCHEMA.TABLES WHERE lower(table_schema)='" . strtolower($schema) . "'");
     
        if (isset($_COOKIE['usertype']) && strlen($_COOKIE['usertype']) > 0 && !empty($status)) {
            $usertype = $_COOKIE['usertype'];
            //override root options,dir
            if (strtolower($usertype) == 'student') {

                $student = \App\Model\Student::find($_COOKIE['id']);
                if (!empty($student)) {
                    $levels = $student->classes->classlevel()->get();
                } else {
                    $levels = \App\Model\Classlevel::all();
                }
            } else {
                $levels = \App\Model\Classlevel::all();
            }
            $schema = str_replace('.', null, set_schema_name());
            $dirs = [
                array(
                    'driver' => 'LocalFileSystem',
                    'path' => 'storage/' . $schema,
                    'URL' => '',
                    'alias' => $schema,
                    //restrict edit, delete options especially for all folders
                    'defaults' => array('read' => true, 'write' => in_array(strtolower($usertype), ['student', 'parent']) ? false : true, 'locked' => !in_array(strtolower($usertype), ['admin']) ? true : false),
                )
            ];
            foreach ($levels as $level) {

                array_push($dirs, array(
                    'driver' => 'LocalFileSystem',
                    'path' => 'storage/' . $level->schoolLevel->name,
                    'URL' => '',
                    'alias' => 'Shared ' . $level->schoolLevel->name,
                    'defaults' => array('read' => true, 'write' => in_array(strtolower($usertype), ['student', 'parent']) ? false : true, 'locked' => !in_array(strtolower($usertype), ['admin']) ? true : false),
                ));
            }
            $this->app['config']->set('elfinder.roots', $dirs);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        
    }

}
