<?php

define('BASEPATH', '1');

function show_error($message) {
    echo $message;
}
function getDivisionBySort($arr, $format) {
    $sort = array();
    foreach ($arr as $k => $v) {
        $sort['subject_mark'][$k] = $v['subject_mark'];
    }
    array_multisort($sort['subject_mark'], SORT_DESC, $arr);
    $i = 1;
    $points = 0;
    foreach ($arr as $key => $value) {
        if ($value['is_counted_indivision'] == 1) {
            $points += $value['point'];
            if ($i == 3 && $format == 'ACSEE') {
                break;
            }
            if ($i == 7 && $format == 'CSEE') {
                break;
            }
            $i++;
        }
    }
    $penalty = 0;
    foreach ($arr as $key => $value) {
        if ($value['penalty'] == 1 && $value['subject_mark'] < $value['pass_mark']) {
            $penalty = 1;
        }
    }
if ($format == 'CSEE') {
        $div = $i < 7 ? 'INC' : cseeDivision($points, $penalty);
    } else {
        $div = $i < 3 ? 'INC' : acseeDivision($points, $penalty);
    }
    return [$div, $points];
}

function cambridgeDivision($point) {
    $remark = '';
    if ($point <= 18) {
        $remark = 'Distinction';
    } else if ($point > 18 && $point < 34) {
        $remark = 'Merit';
    } else if ($point > 34 && $point < 56) {
        $remark = 'Pass';
    } else {
        $remark = 'Certificate';
    }
    return $remark;
}

function createCode($last_code = 12345) {
        $number_part = substr($last_code, -3);
        return strtoupper(substr(set_schema_name(), 0, 2)) . '-' . ((int) $number_part + 1);
    }
function ExcelUploadKeyCheck($required_array,$uploadedArray) {
       $data = array_change_key_case(array_shift($uploadedArray), CASE_LOWER);
    $keys = str_replace( ' ', '_', array_keys($data ) );
   $results = array_combine( $keys, array_values( $data ) );  
        if (count(array_intersect_key(array_flip($required_array), $results)) === count($required_array)) {
            //All required keys exist!            
            $status = 1;
            
        } else {
            $missing = array_intersect_key(array_flip($required_array), $results);
            $data_miss = array_diff(array_flip($required_array), $missing);
            $status = ' Column with title <b>' . implode(', ', array_keys($data_miss)) . '</b>  miss from Excel file. Please make sure file is in the same format as a sample file';
        }
    
        return $status;
    } 
    
   function modify_keys_to_upper_and_underscore($value_array) {
        
  $new_array= array_change_key_case($value_array, CASE_LOWER);
$keys = str_replace( ' ', '_', array_keys($new_array ) );
$value = array_combine( $keys, array_values( $new_array));
return $value;
        
    }
function identical_values( $arrayA , $arrayB ) { 

    sort( $arrayA ); 
    sort( $arrayB ); 

    return $arrayA == $arrayB; 
}
Function accounting_money($amount){

    if($amount>0) {
        return money($amount);
    } else {
        return '('. money($amount*-1).')';
    }
}

function money($amount){
    $exchange=DB::table('exchange_rates')->orderBy('id','desc')->first();
    if(count($exchange)==1){
        $final_amount=$exchange->rate*$amount;
    }else{
        $final_amount=$amount;
    }
   return number_format($final_amount); 
}

function clean_htmlentities($id){
    return htmlentities($id, ENT_QUOTES, "UTF-8");
}

function  original_money($amount){ 
    $decimal_places=DB::table('setting')->first();
    
        $final_amount=$amount;
  
   return number_format((float)$final_amount,$decimal_places->currency_rounding,'.',',');
}

function remove_comma($string_number) {
     return (double)trim(str_replace(',', '', $string_number));
}
function device() {
    $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'unknown agent';

    $os_platform = "Unknown OS Platform";

    $os_array = array(
        '/windows nt 10/i' => 'Windows 10',
        '/windows nt 6.3/i' => 'Windows 8.1',
        '/windows nt 6.2/i' => 'Windows 8',
        '/windows nt 6.1/i' => 'Windows 7',
        '/windows nt 6.0/i' => 'Windows Vista',
        '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
        '/windows nt 5.1/i' => 'Windows XP',
        '/windows xp/i' => 'Windows XP',
        '/windows nt 5.0/i' => 'Windows 2000',
        '/windows me/i' => 'Windows ME',
        '/win98/i' => 'Windows 98',
        '/win95/i' => 'Windows 95',
        '/win16/i' => 'Windows 3.11',
        '/macintosh|mac os x/i' => 'Mac OS X',
        '/mac_powerpc/i' => 'Mac OS 9',
        '/linux/i' => 'Linux',
        '/ubuntu/i' => 'Ubuntu',
        '/iphone/i' => 'iPhone',
        '/ipod/i' => 'iPod',
        '/ipad/i' => 'iPad',
        '/android/i' => 'Android',
        '/blackberry/i' => 'BlackBerry',
        '/webos/i' => 'Mobile'
    );

    foreach ($os_array as $regex => $value) {

        if (preg_match($regex, $user_agent)) {
            $os_platform = $value;
        }
    }

    return $os_platform;
}

function createRoute(){
    $url=isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']:'';
    $url_param=explode('/', $url);

    $controller=isset($url_param[1]) && !empty($url_param[1]) ? $url_param[1] :'book';
    $method=isset($url_param[2]) && !empty($url_param[2]) ? $url_param[2]:'index';
    $view=$method=='view' ? 'show' : $method;

    return in_array($controller,array('public','storage')) ? NULL : $controller.'@'.$view;
}


function btn_add($uri, $name) {
    return anchor($uri, "<i class='fa fa-plus'></i> Add", "class='btn btn-primary add_student btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'");
}

function cseeDivision($total_point, $penalty) {
    if ($total_point >= 7 && $total_point <= 17 && $penalty == 0) {
        $division = 'I';
    } else if ($total_point >= 18 && $total_point <= 21 && $penalty == 0) {
        $division = 'II';
    } else if ($penalty == 1 && $total_point <= 25) {
        $division = 'III';
    } elseif ($total_point >= 18 && $total_point <= 25) {
        $division = 'III';
    } else if ($total_point >= 26 && $total_point <= 33) {
        $division = 'IV';
    } else if ($total_point >= 34 && $total_point <= 35) {
        $division = '0';
    } else {
        $division = '0';
    }
    return $division;
}

function acseeDivision($total_point, $penalty) {
    if ($total_point >= 3 && $total_point <= 9 && $penalty == 0) {
        $division = 'I';
    } else if ($total_point >= 10 && $total_point <= 12 && $penalty == 0) {
        $division = 'II';
    } else if ($penalty == 1 && $total_point <= 12) {
        $division = 'III';
    } elseif ($total_point >= 13 && $total_point <= 17) {
        $division = 'III';
    } else if ($total_point >= 18 && $total_point <= 19) {
        $division = 'IV';
    } else if ($total_point >= 20 && $total_point <= 21) {
        $division = '0';
    } else {
        $division = '0';
    }
    return $division;
}

function userAccessRole() {
    if (!empty(session('id'))) {
        $user = DB::table('user')->where(array("userID" => session("id"), "username" => session('username')))->first();

        if (strtolower(session('usertype')) == 'admin') {
            $sql = 'SELECT name FROM  constant.permission';
        } else {

            $usertype = strtolower(session('usertype'));

            if (in_array($usertype, array('student', 'parent'))) {
                $where = 'role_id=(select id FROM ' . set_schema_name() . 'role where lower(name)=\'' . strtolower($usertype) . '\'  ) ';
            } else {
                $where = 'role_id=' . $user->role_id . '';
            }
            $sql = 'SELECT * from constant.permission a where a.id IN (select permission_id FROM ' . set_schema_name() . 'role_permission WHERE ' . $where . ' )';
        }


        $permission = DB::select($sql);
        $objet = array();

        if (!empty($permission)) {
            foreach ($permission as $value) {
                array_push($objet, $value->name);
            }
        }
        return $objet;
    }
}

function can_access($permission, $access = null) {
    if (!empty(session('loggedin'))) {
        $global = userAccessRole();
        return in_array($permission, $global) ? 1 : 0;
    }
}

//reset password for any user
function reset_password($array) {
    return FALSE;
}

if (!function_exists('return_grade')) {

    function return_grade($grades, $avg, $level_format = NULL) {
        $data = '';

        foreach ($grades as $grade) {
            if ($grade->gradefrom <= $avg && $grade->gradeupto >= $avg) {
                $data .= "<td>" .
                        $grade->grade .
                        "</td>";
                $data .= $level_format == 'ACSEE' ? "<td>" .
                        $grade->point .
                        "</td>" : '';
                $data .= '<td>' . $grade->note . '</td>';
                return $data;
            }
        }
    }

}

    function return_grade_point($grades, $avg, $level_format = NULL) {
        $data = array();

        foreach ($grades as $grade) {
            if ($grade->gradefrom <= $avg && $grade->gradeupto >= $avg) {
                $data = array('grade' => $grade->grade, 'point' => $grade->point);
                return $data;
            }
        }
        return $data;
    }


function btn_view($uri, $name) {
    return anchor($uri, "<i class='fa fa-check-square-o'></i> View", "class='btn btn-success btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'");
}

function btn_edit($uri, $name) {
    return anchor($uri, "<i class='fa fa-edit'></i> Edit", "class='btn btn-warning btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'");
}

function btn_delete($uri, $name, $title = null) {
    $confirm = $title == null ? 'you are about to delete a record. This cannot be undone. are you sure?' : $title;
    array(
        'onclick' => "return confirm('" . $confirm . "')",
        'class' => 'btn btn-danger btn-xs mrg',
        'data-placement' => 'top',
        'data-toggle' => 'tooltip',
        'data-original-title' => $name
    );
    return anchor($uri, "<i class='fa fa-trash-o'></i> Delete", " class='btn btn-danger btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='{$name}' onclick='return confirm(\"$confirm\")' ");
}

function form_error($errors, $tag) {
    if ($errors != null && $errors->has($tag)) {
        return $errors->first($tag);
    }
}

/**
 * Text Input Field
 *
 * @access  public
 * @param   mixed
 * @param   string
 * @param   string
 * @return  string
 */
if (!function_exists('form_input')) {

    function form_input($data = '', $value = '', $extra = '') {
        $defaults = array('type' => 'text', 'name' => ((!is_array($data)) ? $data : ''), 'value' => $value);

        return "<input " . _parse_form_attributes($data, $defaults) . $extra . " />";
    }

}


/**
 * Form Prep
 *
 * Formats text so that it can be safely placed in a form field in the event it has HTML tags.
 *
 * @access  public
 * @param   string
 * @return  string
 */
if (!function_exists('form_prep')) {

    function form_prep($str = '', $field_name = '') {
        static $prepped_fields = array();

        // if the field name is an array we do this recursively
        if (is_array($str)) {
            foreach ($str as $key => $val) {
                $str[$key] = form_prep($val);
            }

            return $str;
        }

        if ($str === '') {
            return '';
        }

        // we've already prepped a field with this name
        // @todo need to figure out a way to namespace this so
        // that we know the *exact* field and not just one with
        // the same name
        if (isset($prepped_fields[$field_name])) {
            return $str;
        }

        $str = htmlspecialchars($str);

        // In case htmlspecialchars misses these.
        $str = str_replace(array("'", '"'), array("&#39;", "&quot;"), $str);

        if ($field_name != '') {
            $prepped_fields[$field_name] = $field_name;
        }

        return $str;
    }

}


/**
 * Image
 *
 * Generates an <img /> element
 *
 * @access  public
 * @param   mixed
 * @return  string
 */
if (!function_exists('img')) {

    function img($src = '', $index_page = FALSE) {
        if (!is_array($src)) {
            $src = array('src' => $src);
        }

        // If there is no alt attribute defined, set it to an empty string
        if (!isset($src['alt'])) {
            $src['alt'] = '';
        }

        $img = '<img';

        foreach ($src as $k => $v) {

            if ($k == 'src' AND strpos($v, '://') === FALSE) {
                $img_d = str_replace("https://" . set_schema_name() . "shulesoft.com/storage/uploads/images/", null, $v);
                $link = strlen(pathinfo($img_d, PATHINFO_EXTENSION)) > 0 ? $v : $img_d;
                
                    if (preg_match('/googleusercontent/', $link) || preg_match('/amazon/i', $v)) {
                    $url_link = str_replace(url('') . '/storage/uploads/images/', null, $link);
                } else {
                    $url_link = $link;
                }
                   $img .= ' src="' . url($v) . '"';
            } else {

                   if ($k == 'src') {
                    if (preg_match('/googleusercontent/i', $v) || preg_match('/amazon/i', $v)) {
                        $v = str_replace(url('') . '/storage/uploads/images/', null, $v);
                    }
                }
                $img .= " $k=\"$v\"";
            }
        }

        $img .= '/>';

        return $img;
    }

}
/**
 * Drop-down Menu
 *
 * @access  public
 * @param   string
 * @param   array
 * @param   string
 * @param   string
 * @return  string
 */
if (!function_exists('form_dropdown')) {

    function form_dropdown($name = '', $options = array(), $selected = array(), $extra = '') {
        if (!is_array($selected)) {
            $selected = array($selected);
        }

        // If no selected state was submitted we will attempt to set it automatically
        if (count($selected) === 0) {
            // If the form name appears in the $_POST array we have a winner!
            if (isset($_POST[$name])) {
                $selected = array($_POST[$name]);
            }
        }

        if ($extra != '')
            $extra = ' ' . $extra;

        $multiple = (count($selected) > 1 && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

        $form = '<select name="' . $name . '"' . $extra . $multiple . ">\n";

        foreach ($options as $key => $val) {
            $key = (string) $key;

            if (is_array($val) && !empty($val)) {
                $form .= '<optgroup label="' . $key . '">' . "\n";

                foreach ($val as $optgroup_key => $optgroup_val) {
                    $sel = (in_array($optgroup_key, $selected)) ? ' selected="selected"' : '';

                    $form .= '<option value="' . $optgroup_key . '"' . $sel . '>' . (string) $optgroup_val . "</option>\n";
                }

                $form .= '</optgroup>' . "\n";
            } else {
                $sel = (in_array($key, $selected)) ? ' selected="selected"' : '';

                $form .= '<option value="' . $key . '"' . $sel . '>' . (string) $val . "</option>\n";
            }
        }

        $form .= '</select>';

        return $form;
    }

}

function get_cookie($name) {
    return Cookie::get($name);
}

function uri_string() {
    return array();
}

function reset_keys($array) {
    $data = array();
    $i = 0;
    foreach ($array as $value) {
        $data[$i] = $value;
        $i++;
    }
    return $data;
}

function delete_file($uri, $id) {
    return anchor($uri, "<i class='fa fa-times '></i>", array(
        'onclick' => "return confirm('you are about to delete a record. This cannot be undone. are you sure?')",
        'id' => $id,
        'class' => "close pull-right"
            )
    );
}

function share_file($uri, $id) {
    return anchor($uri, "<i class='fa fa-globe'></i>", array(
        'onclick' => "return confirm('you are about to delete a record. This cannot be undone. are you sure?')",
        'id' => $id,
        'class' => "pull-right"
            )
    );
}

function btn_dash_view($uri, $name) {
    return anchor($uri, "<span class='fa fa-check-square-o'></span>", "class='btn btn-success btn-xs mrg' style='background-color:#1abc9c;color:#fff;border:1px solid #1abc9c' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'");
}

function btn_invoice($uri, $name) {
    return anchor($uri, "<i class='fa fa-credit-card'></i>", "class='btn btn-primary btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'");
}

function btn_return($uri, $name) {
    return anchor($uri, "<i class='fa fa-mail-forward'></i>", array(
        "onclick" => "return confirm('you are return the book . This cannot be undone. are you sure?')",
        "class" => 'btn btn-danger btn-xs',
        'data-placement' => 'top',
        'data-toggle' => 'tooltip',
        'data-original-title' => $name
            )
    );
}

function btn_attendance($id, $method, $class, $name) {
    return "<input type='checkbox' class='" . $class . "' $method id='" . $id . "' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "' >  ";
    // return anchor($uri, "<i class='fa fa-credit-card'></i>", "class='btn btn-primary btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");
}

function btn_promotion($id, $class, $name) {
    $warning = '';
    return "<input type='checkbox' class='" . $class . "' id='" . $id . "' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'  $warning >  ";
    // return anchor($uri, "<i class='fa fa-credit-card'></i>", "class='btn btn-primary btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");
}

if (!function_exists('dump')) {

    function dump($var, $label = 'Dump', $echo = TRUE) {
        // Store dump in variable 
        ob_start();
        var_dump($var);
        $output = ob_get_clean();

        // Add formatting
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';

        // Output
        if ($echo == TRUE) {
            echo $output;
        } else {
            return $output;
        }
    }

}

/**
 * Anchor Link
 *
 * Creates an anchor based on the local URL.
 *
 * @access  public
 * @param   string  the URL
 * @param   string  the link title
 * @param   mixed   any attributes
 * @return  string
 */
if (!function_exists('anchor')) {

    function anchor($uri = '', $title = '', $attributes = '') {
        $attr = '';
        if (is_array($attributes)) {
            foreach ($attributes as $key => $value) {
                $attr .= $key . '="' . $value . '" ';
            }
        } else {
            $attr = $attributes;
        }
        return '<a href="' . url($uri) . '"' . $attr . '>' . $title . '</a>';
    }

}

/**
 * Form Value --otilia prosper -form one B, needs results
 *
 * Grabs a value from the POST array for the specified field so you can
 * re-populate an input field or textarea.  
 *
 * @access  public
 * @param   string
 * @return  mixed
 */
if (!function_exists('set_value')) {

    function set_value($field = '', $default = '') {
        if (!empty($_POST)) {
            if (isset($_POST[$field])) {
                return request($field);
            }

            return $default;
        }
if($default=='' && $field !=''){
            return old($field);
        }

        return !empty($default) ? $default : '';
    }

}

function base_url($url = '/') {
    return url($url);
}

if (!function_exists('dump_exit')) {

    function dump_exit($var, $label = 'Dump', $echo = TRUE) {
        dump($var, $label, $echo);
    }

}

// infinite coding starts here..
function btn_add_pdf($uri, $name) {
    return anchor($uri, "" . $name, "class='btn-primary btn-xs btn-sm-cs' style='' role='button' target='_blank'");
}

function btn_sm_edit($uri, $name) {
    return anchor($uri, "<i class='fa fa-edit'></i> " . $name, "class='btn-cs btn-sm-cs' style='text-decoration: none;' role='button'");
}

function btn_sm_add($uri, $name) {
    return anchor($uri, "<i class='fa fa-plus'></i> " . $name, "class='btn-cs btn-sm-cs' style='text-decoration: none;' role='button'");
}

function btn_payment($uri, $name) {
    return anchor($uri, "<i class='fa fa-credit-card'></i> " . $name, "class='btn-cs btn-sm-cs'style='text-decoration: none;' role='button'");
}

/**
 * @uses: This function will be used to detect aa schema name
 *         which is the username of that school
 * @return string
 */
function set_schema_name() {
    $server_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'beta_testing';

   $domain = strtolower(str_replace('.shulesoft.com', '', $server_name));

 if ($domain == '158.69.112.216:8081'||$domain == '51.77.212.234:8081') {
  // if condition for local IP will be here. If not localhost, put your server name
            $schema = 'beta_testing.';
        } else if ($domain == 'demo') {
            $schema = 'public.';
        } else if ($domain == 'testing') {
            $schema = 'public.';
        } else {
            $schema = $domain . '.';
      }
        return $schema;
}


function checkInstaller() {
    $dbfile = 'app/config/development/db.txt';
    if (is_file($dbfile)) {
        $content = explode(',', file_get_contents($dbfile));
        if (!in_array(str_replace('.', '', set_schema_name()), $content) && !preg_match('/install/', uri_string())) {
            redirect(base_url('install/newschool'));
        }
    } else {
        fopen($dbfile, 'a');
    }
}

set_error_handler('errorHandler');

//this records errors that occurs in files or systems, its useful much

function errorHandler($errno, $errmsg, $filename, $linenum, $vars) {
    // timestamp for the error entry
    $dt = date("Y-M-d H:i:s");

    // define an assoc array of error string
    // in reality the only entries we should
    // consider are E_WARNING, E_NOTICE, E_USER_ERROR,
    // E_USER_WARNING and E_USER_NOTICE
    $errortype = array(
        E_ERROR => 'Error',
        E_WARNING => 'Warning',
        E_PARSE => 'Parsing Error',
        E_NOTICE => 'Notice',
        E_CORE_ERROR => 'Core Error',
        E_CORE_WARNING => 'Core Warning',
        E_COMPILE_ERROR => 'Compile Error',
        E_COMPILE_WARNING => 'Compile Warning',
        E_USER_ERROR => 'User Error',
        E_USER_WARNING => 'User Warning',
        E_USER_NOTICE => 'User Notice',
        E_STRICT => 'Runtime Notice',
        E_RECOVERABLE_ERROR => 'Catchable Fatal Error'
    );
    // set of errors for which a var trace will be saved
    $user_errors = array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);
    // $CI = & get_instance();  //get instance, access the CI superobject
    // $usertype = !empty($CI->session->userdata("usertype")) ? $CI->session->userdata("usertype") : 'Not loged usertype';
    //$username = !empty($CI->session->userdata("username")) ? $CI->session->userdata("username") : 'Not loged in user';
    //$object = new MY_Controller();

    $err = "<br/><hr/><ul>\n";
    $err .= "\t<li>date time " . $dt . "</li>\n";
    $err .= "\t<li>errornum " . $errno . "</li>\n";
    // $err .= "\t<li>Made By: " . $usertype . "</li>\n";
    //$err .= "\t<li>Usename login:  " . $username . "</li>\n";
    $err .= "\t<li>errortype " . $errortype[$errno] . "</li>\n";
    $err .= "\t<li>error msg: " . $errmsg . "</li>\n";
    // $err .= "\t<li>error sql: " . $CI->db->last_query() . "</li>\n";
    // $err .= "\t<li>error query: " . $CI->db->_error_message() . "</li>\n";
    //  $err .= "\t<li>Called Class: " . $CI->router->class . "</li>\n";
    // $err .= "\t<li>Called Method: " . $CI->router->method . "</li>\n"
    // $err .= "\t<li>request url: " . isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:'' . "</li>\n";
    $err .= "\t<li>File name: " . $filename . "</li>\n";
    $err .= "\t<li>Line no: " . $linenum . "</li>\n";
    // $err .= "\t<li>Error user agent: " . $object->user_agent()->agent . "</li>\n";
    //$err .= "\t<li>From which Computer Platform: " . $object->user_agent()->platform . "</li>\n";
    $err .= "\t<li>Error from which username: " . gethostname() . "</li>\n";

    if (in_array($errno, $user_errors)) {
        $err .= "\t<li>var trace: " . wddx_serialize_value($vars, "Variables") . "</li>\n";
    }
    $err .= "</ul>\n\n";


//    $CI->db->query("INSERT INTO constant.error_log(
//             type, madeby, username, logmessage, last_sql, 
//            called_class, called_method, request_url, filename, line_number, 
//            user_agent, user_platform, schema_name)
//    VALUES ( '$errortype[$errno]', '$usertype', '$username', '$errmsg', '" . $CI->db->last_query() . "', dophima molleli a
//            '" . $CI->router->class . "', '" . $CI->router->method . "', '" . $_SERVER['REQUEST_URI'] . "', '$filename', '$linenum', '" . $object->user_agent()->agent . "', '" . $object->user_agent()->platform . "', '" . set_schema_name() . "')");
    // save to the error log, and e-mail me if there is a critical user error
    // Don't change this constant file path as is independend of root folder name
    $filename = set_schema_name() . '_' . str_replace('-', '_', date('Y-M-d')) . '.html';

    error_log($err, 3, dirname(__FILE__) . "/../storage/logs/" . $filename);
}

/*
 * *  Function:   Convert number to string
 * *  Arguments:  int
 * *  Returns:    string
 * *  Description:
 * *      Converts a given integer (in range [0..1T-1], inclusive) into
 * *      alphabetical format ("one", "two", etc.).
 */

function number_to_words($number) {
    if (($number < 0) || ($number > 999999999)) {
        return "$number";
    }

    $Gn = floor($number / 1000000);  /* Millions (giga) */
    $number -= $Gn * 1000000;
    $kn = floor($number / 1000);     /* Thousands (kilo) */
    $number -= $kn * 1000;
    $Hn = floor($number / 100);      /* Hundreds (hecto) */
    $number -= $Hn * 100;
    $Dn = floor($number / 10);       /* Tens (deca) */
    $n = $number % 10; /* Ones */

    $res = "";

    if ($Gn) {
        $res .= number_to_words($Gn) . " Million";
    }

    if ($kn) {
        $res .= (empty($res) ? "" : " ") .
                number_to_words($kn) . " Thousand";
    }

    if ($Hn) {
        $res .= (empty($res) ? "" : " ") .
                number_to_words($Hn) . " Hundred";
    }

    $ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
        "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
        "Nineteen");
    $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
        "Seventy", "Eigthy", "Ninety");

    if ($Dn || $n) {
        if (!empty($res)) {
            $res .= " and ";
        }

        if ($Dn < 2) {
            $res .= $ones[$Dn * 10 + $n];
        } else {
            $res .= $tens[$Dn];

            if ($n) {
                $res .= "-" . $ones[$n];
            }
        }
    }

    if (empty($res)) {
        $res = "zero";
    }

    return $res;
}

/**
 * Create a Random String
 *
 * Useful for generating passwords or hashes.
 *
 * @access  public
 * @param   string  type of random string.  basic, alpha, alunum, numeric, nozero, unique, md5, encrypt and sha1
 * @param   integer number of characters
 * @return  string
 */
if (!function_exists('random_string')) {

    function random_string($type = 'alnum', $len = 8) {
        switch ($type) {
            case 'basic' : return mt_rand();
                break;
            case 'alnum' :
            case 'numeric' :
            case 'nozero' :
            case 'alpha' :

                switch ($type) {
                    case 'alpha' : $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        break;
                    case 'alnum' : $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        break;
                    case 'numeric' : $pool = '0123456789';
                        break;
                    case 'nozero' : $pool = '123456789';
                        break;
                }

                $str = '';
                for ($i = 0; $i < $len; $i++) {
                    $str .= substr($pool, mt_rand(0, strlen($pool) - 1), 1);
                }
                return $str;
                break;
            case 'unique' :
            case 'md5' :

                return md5(uniqid(mt_rand()));
                break;
        }
    }
}


/*--- --- --- --- Use the saved tour's id to save the relationship in tour_users --- --- --- --- ---*/
 function saveTour(){
return false;
if(session('id')!='') {
    $user_id = session('id');
    //Associate user with a tour and store the relationship in Tour_users table
    $user = \App\Model\User::find($user_id);
    $table = session('table');

    /*--- --- --- ---*/
    $tour_id = \App\Model\Tour::where('location', '=', request()->segment(1) . '/' . request()->segment(2))->value('id');

    if ($tour_id != null) {
        $tour = \App\Model\Tour::find($tour_id);
        $tour_user = \App\Model\Tour_user::firstOrNew(['table' => $table, 'user_id' => $user_id, 'tour_id' => $tour_id]);
        $tour_user->tours()->associate($tour);
        $tour_user->users()->associate($user);
        $tour_user->save();
    }
}
}

/* --- --- --- Has the current user seen the current page's tour yet?--- --- --- --- */
function checkTourStatus(){
return false;
    if(session('id')!='') {
        $param1 = request()->segment(1);
        $param2 = request()->segment(2);
        $location = $param1 . '/' . $param2;
        $tour_id = \App\Model\Tour::where(['location' => $location])->value('id');
        $tour = \App\Model\Tour_user::where(['tour_id' => $tour_id, 'user_id' => session('id'), 'table' => session('table')]);
        $tour_seen = $tour->where(['tour_seen' => 0])->exists();
        return $tour_seen;
    }
}

/* --- --- ---If they have seen it then change the seen status to hide it the next time they're in the same page --- --- --- --- */
function updateTourStatus(){
return false;
    if(session('id')!='') {
        $param1 = request()->segment(1);
        $param2 = request()->segment(2);
        $location = $param1 . '/' . $param2;
        $tour_id = \App\Model\Tour::where(['location' => $location])->value('id');
        $tour = \App\Model\Tour_user::where(['tour_id' => $tour_id, 'user_id' => session('id'), 'table' => session('table')]);
        App\Model\Tour_user::where(["id" => $tour->value('id')])->update(['tour_seen' => 1]);
    }
}

function timeAgo($datetime, $full = false) {
    $now = new \DateTime;
    $ago = new \DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}


/**
 * 
 * @param type $phone_number
 * @return array($country_name, $valid_number) or not array if wrong number
 */
function validate_phone_number($number) {
    $phone_number=preg_replace("/[^0-9]/", '', $number);;
    if (strlen(preg_replace('#[^0-9]#i', '', $phone_number)) < 7 || strlen(preg_replace('#[^0-9]#i', '', $phone_number)) > 14) {
	return FALSE;
    } else {

	$y = substr($phone_number, -9);
	$z = str_ireplace($y, '', $phone_number);
	$p = str_ireplace('+', '', $z);

	$x = array(
	    93 => " Afghanistan",
	    355 => " Albania", 213 => " Algeria",
	    1 => " American Samoa",
	    376 => "Andorra ",
	    244 => " Angola",
	    1 => " Anguilla",
	    1 => " Antigua and Barbuda",
	    54 => " Argentine Republic",
	    374 => " Armenia",
	    297 => " Aruba",
	    247 => " Ascension",
	    61 => " Australia",
	    672 => " Australian External Territories",
	    43 => " Austria ", 994 => " Azerbaijani Republic", 1 => " Bahamas ", 973 => " Bahrain", 880 => " Bangladesh ", 1 => " Barbados ", 375 => " Belarus ", 32 => " Belgium ", 501 => " Belize", 229 => " Benin ", 1 => " Bermuda ", 975 => " Bhutan", 591 => " Bolivia", 387 => " Bosnia and Herzegovina ", 267 => " Botswana", 55 => " Brazil (Federative Republic of)", 1 => " British Virgin Islands", 673 => " Brunei Darussalam ", 359 => " Bulgaria (Republic of)", 226 => " Burkina Faso", 257 => " Burundi (Republic of)", 855 => " Cambodia (Kingdom of)", 237 => " Cameroon (Republic of)", 1 => " Canada", 238 => " Cape Verde (Republic of)", 1 => " Cayman Islands ", 236 => " Central African Republic ", 235 => " Chad (Republic of)", 56 => " Chile ", 86 => " China ( Republic of)", 57 => " Colombia (Republic of)", 269 => " Comoros (Union of the)", 242 => " Congo (Republic of the)", 682 => " Cook Islands", 506 => " Costa Rica", 225 => " Côte d \"Ivoire (Republic of)", 385 => " Croatia (Republic of)", 53 => " Cuba", 357 => " Cyprus (Republic of)", 420 => " Czech Republic ", 850 => " Democratic People\"s Republic of Korea ", 243 => " Democratic Republic of the Congo", 670 => " Democratic Republic of Timor-Leste", 45 => " Denmark", 246 => " Diego Garcia ", 253 => " Djibouti (Republic of) ", 1 => " Dominica (Commonwealth of)", 1 => " Dominican Republic", 593 => " Ecuador", 20 => " Egypt (Arab Republic of)", 503 => " El Salvador (Republic of)", 240 => " Equatorial Guinea (Republic of)", 291 => " Eritrea", 372 => " Estonia (Republic of)", 251 => " Ethiopia (Federal Democratic Republic of) ", 500 => " Falkland Islands (Malvinas) ", 298 => " Faroe Islands", 679 => " Fiji (Republic of)", 358 => " Finland ", 33 => " France", 262 => " French Departments and Territories in the Indian Ocean ", 594 => " French Guiana (French Department of)", 689 => " French Polynesia (Territoire français \"outre-mer)", 241 => " Gabonese Republic", 220 => " Gambia (Republic of the)", 995 => " Georgia", 49 => " Germany (Federal Republic of)", 233 => " Ghana", 350 => " Gibraltar", 881 => " Global Mobile Satellite System (GMSS) shared code", 30 => " Greece ", 299 => " Greenland (Denmark)", 1 => " Grenada", 388 => " Group of countries shared code", 590 => " Guadeloupe (French Department of)", 1 => " Guam ", 502 => " Guatemala (Republic of)", 224 => " Guinea (Republic of)", 245 => " Guinea-Bissau (Republic of)", 592 => " Guyana", 509 => " Haiti (Republic of)", 504 => " Honduras (Republic of)", 852 => " Hong Kong China", 36 => " Hungary (Republic of)", 354 => " Iceland", 91 => " India (Republic of)", 62 => " Indonesia (Republic of)", 870 => " Inmarsat SNAC ", 98 => " Iran (Islamic Republic of)", 964 => " Iraq (Republic of)", 353 => " Ireland", 972 => " Israel (State of)", 39 => " Italy", 1 => " Jamaica", 81 => " Japan", 962 => " Jordan (Hashemite Kingdom of)", 7 => " Kazakhstan (Republic of)", 254 => " Kenya (Republic of)", 686 => " Kiribati (Republic of)", 82 => " Korea (Republic of)", 965 => " Kuwait (State of)", 996 => " Kyrgyz Republic ", 856 => " Lao People\"s Democratic Republic", 371 => " Latvia (Republic of)", 961 => " Lebanon ", 266 => " Lesotho (Kingdom of)", 231 => " Liberia (Republic of)", 218 => " Libya (Socialist People\"s Libyan Arab Jamahiriya)", 423 => " Liechtenstein (Principality of)", 370 => " Lithuania (Republic of) ", 352 => " Luxembourg", 853 => " Macao China", 261 => " Madagascar (Republic of)", 265 => " Malawi", 60 => " Malaysia", 960 => " Maldives (Republic of)", 223 => " Mali (Republic of)", 356 => " Malta", 692 => " Marshall Islands (Republic of the)", 596 => " Martinique (French Department of)", 222 => " Mauritania (Islamic Republic of)", 230 => " Mauritius (Republic of)", 269 => " Mayotte", 52 => " Mexico", 691 => " Micronesia (Federated States of)", 373 => " Moldova (Republic of) ", 377 => " Monaco (Principality of)", 976 => " Mongolia ", 382 => " Montenegro (Republic of)", 1 => " Montserrat", 212 => " Morocco (Kingdom of)", 258 => " Mozambique (Republic of) ", 95 => " Myanmar (Union of)", 264 => " Namibia (Republic of)", 674 => " Nauru (Republic of)", 977 => " Nepal (Federal Democratic Republic of)", 31 => " Netherlands (Kingdom of the)", 599 => " Netherlands Antilles", 687 => " New Caledonia (Territoire français d\"outre-mer)", 64 => " New Zealand", 505 => " Nicaragua", 227 => "Niger (Republic of the)", 234 => " Nigeria (Federal Republic of)", 683 => " Niue ", 1 => " Northern Mariana Islands (Commonwealth of the)", 47 => " Norway", 968 => " Oman (Sultanate of)", 92 => " Pakistan (Islamic Republic of)", 680 => " Palau (Republic of)", 507 => " Panama (Republic of)", 675 => " Papua New Guinea", 595 => " Paraguay (Republic of)", 51 => "Peru", 63 => "Philippines (Republic of the)", 48 => " Poland (Republic of)", 351 => " Portugal", 1 => " Puerto Rico", 974 => " Qatar (State of)", 40 => " Romania ", 7 => " Russian Federation", 250 => " Rwanda (Republic of)", 290 => " Saint Helena", 1 => " Saint Kitts and Nevis", 1 => " Saint Lucia", 508 => " Saint Pierre and Miquelon (Collectivité territoriale de la République française)", 1 => " Saint Vincent and the Grenadines", 685 => " Samoa (Independent State of)", 378 => " San Marino (Republic of) ", 239 => " Sao Tome and Principe (Democratic Republic of)", 966 => " Saudi Arabia (Kingdom of)", 221 => " Senegal (Republic of)", 381 => " Serbia (Republic of)", 248 => " Seychelles (Republic of)", 232 => " Sierra Leone", 65 => " Singapore (Republic of)", 421 => " Slovak Republic", 386 => " Slovenia (Republic of)", 677 => " Solomon Islands", 252 => " Somali Democratic Republic", 27 => " South Africa (Republic of)", 34 => " Spain", 94 => " Sri Lanka (Democratic Socialist Republic of)", 249 => " Sudan (Republic of the)", 597 => " Suriname (Republic of)", 268 => " Swaziland (Kingdom of)", 46 => " Sweden", 41 => " Switzerland (Confederation of)", 963 => " Syrian Arab Republic", 886 => " Taiwan China", 992 => " Tajikistan (Republic of)", 255 => " Tanzania (United Republic of)", 66 => " Thailand", 389 => " The Former Yugoslav Republic of Macedonia", 228 => " Togolese Republic", 690 => " Tokelau", 676 => " Tonga (Kingdom of)", 1 => " Trinidad and Tobago", 290 => " Tristan da Cunha", 216 => " Tunisia", 90 => " Turkey", 993 => " Turkmenistan", 1 => " Turks and Caicos Islands", 688 => " Tuvalu", 256 => " Uganda (Republic of)", 380 => " Ukraine", 971 => " United Arab Emirates", 44 => " United Kingdom of Great Britain and Northern Ireland ", 1 => " United States of America", 1 => " United States Virgin Islands", 598 => " Uruguay (Eastern Republic of)", 998 => " Uzbekistan (Republic of)", 678 => " Vanuatu (Republic of)", 379 => " Vatican City State", 39 => " Vatican City State", 58 => " Venezuela (Bolivarian Republic of)", 84 => " Viet Nam (Socialist Republic of)", 681 => " Wallis and Futuna (Territoire français d\"outre-mer)", 967 => " Yemen (Republic of)", 260 => "Zambia (Republic of)", 263 => " Zimbabwe");


	foreach ($x as $key => $value) {
	    if ($p == $key) {
		$country_name = $value;
		$code = $key;
	    } else {
		$country_name = ' Tanzania (United Republic of)';
		$code = '255';
	    }
	}

	$valid_number = '+'.$code . $y;

	$valid = array($country_name, $valid_number);
	return $valid;
    }
}
