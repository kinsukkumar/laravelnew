-- View: installment_reminders to get list of parents and contacts that have to be reminded foor payments of Ended Installments

-- view Payment deadline should now work with option to remind parents at the end of each isntallments

CREATE OR REPLACE VIEW public.installment_reminders AS
 SELECT a.student_id,
    b.name AS student,
    c.name AS parent,
    c.phone AS phone_number,
    a.total_amount,
    a.academic_year_id,
    a.balance,
    f.start_date,
    f.end_date,
        CASE
            WHEN f.end_date <= now() THEN 0
            ELSE 1
        END AS status
   FROM public.invoice_balances a
     JOIN public.student b ON b.student_id = a.student_id
     JOIN public.student_parents d ON b.student_id = d.student_id
     JOIN public.parent c ON c."parentID" = d.parent_id
     JOIN public.installments f ON f.id = a.installment_id
  WHERE a.balance > 0::numeric;


-- 	Add comment area for parents to to add their review on student progressive report



-- //Consider This -- All Class Name Should have have the same class_referer_ID
UPDATE public.classes SET refer_class_id=cp.id FROM constant.refer_classes cp WHERE public.classes.classes=cp.name;


--Statistical reports: Show no of student per year and transfer ration

CREATE OR REPLACE VIEW public.student_statisctical_report AS
 SELECT a.academic_year_id, b.name, c.classes, d.id AS status_id, d.reason, 
    count(a.student_id) AS total_student
   FROM public.student a
   JOIN constant.student_status d ON d.id = a.status_id
   JOIN public.academic_year b ON b.id = a.academic_year_id
   JOIN public.classes c ON c."classesID" = a."classesID"
  GROUP BY a.academic_year_id, b.name, d.reason, c.classes, d.id
  ORDER BY a.academic_year_id;

  -- Identify Database Constrains Type in All Schemas

SELECT tc.table_schema, tc.table_name, kc.column_name , kc.constraint_name , tc.constraint_type
FROM
    information_schema.table_constraints tc,
    information_schema.key_column_usage kc
WHERE
    tc.constraint_type = 'FOREIGN KEY'
    AND kc.table_name = tc.table_name AND kc.table_schema = tc.table_schema
    AND kc.constraint_name = tc.constraint_name
ORDER BY 1, 2;

-- Return Number of rows Per Tables

with tbl as (SELECT table_schema,table_name FROM information_schema.tables where table_name not like 'pg_%' and table_name in ('reminders')) select table_schema, table_name, (xpath('/row/c/text()', 
query_to_xml(format('select count(*) as c from %I.%I', table_schema, table_name), false, true, '')))[1]::text::int as rows_n from tbl ORDER BY 3 DESC;



Update muginiprimary.student set academic_year_id=c.academic_year_id,"sectionID"=c.section_id 
from (SELECT c.student_id, c.academic_year_id,c.section_id FROM muginiprimary.student_archive c,muginiprimary.student b 
	  WHERE c.academic_year_id=2) as c WHERE c.student_id=muginiprimary.student.student_i


--Find Columns With Duplicate Values on a Single Table
    SELECT a.*
FROM users a
JOIN (SELECT username, email, COUNT(*)
FROM users 
GROUP BY username, email
HAVING count(*) > 1 ) b
ON a.username = b.username
AND a.email = b.email
ORDER BY a.email


--update classes
Update shulesoft.student set "classesID"=c."classesID" 
from (SELECT "sectionID", "classesID" FROM inspire.section
	 ) as c WHERE c."sectionID"=shulesoft.student."sectionID"

--update year and section
Update shulesoft.student set academic_year_id=c.academic_year_id,"sectionID"=c.section_id 
from (SELECT student_id, academic_year_id,section_id FROM shulesoft.student_archive
	 ) as c WHERE c.student_id=shulesoft.student.student_id

-- Find tables_schema contains a specific column

select t.table_schema,
       t.table_name
from information_schema.tables t
inner join information_schema.columns c on c.table_name = t.table_name 
                                and c.table_schema = t.table_schema
where c.column_name = 'syllabus_topic_id'
      and t.table_schema in ('public', 'pg_catalog')
      and t.table_type = 'BASE TABLE'
order by t.table_schema;

