/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  swill
 * Created: Sep 27, 2020
 */

ALTER TABLE public.examschedule DROP CONSTRAINT "examschedule_sectionID_foreign";

ALTER TABLE public.examschedule
    ADD CONSTRAINT "examschedule_sectionID_foreign" FOREIGN KEY ("sectionID")
    REFERENCES public.section ("sectionID") MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;