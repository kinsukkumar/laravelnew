
/**
 * Author:  Ephraim Swilla
 * Created: 02 Jul 2019
    This file contains all important functions and sql that are useful and helpful in most cases
 */



/*
Get list of inner functions that use to manage database schema
*/
select schemaname,viewname from pg_views;

/*
option to autogenerate update sequences
*/
DO $$
DECLARE 
i TEXT;
BEGIN
    FOR i IN (select table_name from information_schema.tables where table_catalog='shulesoft_2017' and table_schema='joyland') LOOP
    EXECUTE 'Select setval(''joyland.'||i||'_id_seq'', 4050);';
    END LOOP;
END$$;
/*
--drop sequence statement (means to delete unused sequences)
*/
SELECT string_agg('DROP SEQUENCE ' || c.oid::regclass, '; ') || ';' AS ddl
FROM   pg_class       c
LEFT   JOIN pg_depend d ON d.refobjid = c.oid
                       AND d.deptype <> 'i'
WHERE  c.relkind = 'S'
AND    d.refobjid IS NULL;


--replace value in a column

UPDATE heritage.parent SET username=regexp_replace(username, '#VALUE!', '');

--deleting duplicates, key=what occurs more than once
DELETE FROM dupes a
WHERE a.ctid <> (SELECT min(b.ctid)
                 FROM   dupes b
                 WHERE  a.key = b.key);


--generate random integer
SELECT generate_series (1,16384),(random()*200000)::int AS id


--rename a certain column in db
CREATE OR REPLACE FUNCTION renamecolumn(from_ text, to_ text)
    RETURNS numeric
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

 DECLARE r record; _sql text;
 
 BEGIN 
 FOR r IN select "table_schema" as sch, table_name as tbl FROM information_schema.columns where column_name=from_
 LOOP
  _sql := format('ALTER TABLE  %I.%I RENAME %I TO %I', r.sch, r.tbl,from_,to_);
      EXECUTE _sql;	
 END LOOP;

 RETURN 0; 
 END; 

$BODY$;

select renamecolumn('studentID','student_id');


---function to create table SQL used in table/db synchronization

---table_name is the table which we want to show its create table sql
--slave_schema is the schema name which we want to synch (import) this table. i,e schema that miss table
--remember to change table_schema ='testing' to your default schema which is stable
CREATE OR REPLACE FUNCTION show_create_table(table_name text, slave_schema text, join_char text = E'\n' ) 
  RETURNS text AS 
$BODY$
SELECT 'CREATE TABLE ' || slave_schema||'.' || $1 || ' (' || $3 || '' || 
    string_agg(column_list.column_expr, ', ' || $3 || '') || 
    '' || $3 || ');'
FROM (
  SELECT '    "' || column_name || '" ' || case when ordinal_position=1 then 'serial' else data_type end || 
       coalesce('(' || character_maximum_length || ')', '') || 
       case when is_nullable = 'YES' then '' else ' NOT NULL' end || CASE WHEN (column_default is not NULL AND column_default NOT like '%nextval%') then '  DEFAULT ' || column_default else '' end as column_expr 
  FROM information_schema.columns
  WHERE table_schema = 'testing' AND table_name = $1
  ORDER BY ordinal_position) column_list;
$BODY$
  LANGUAGE SQL STABLE;

  alter table testing.mark add check (mark<=100)


  ---join tables and create view in public schema that combined all tables in all schema
  --core reference is here http://clarkdave.net/2015/06/aggregate-queries-across-postgresql-schemas/

CREATE OR REPLACE FUNCTION refresh_union_view(table_name text) RETURNS void AS $$
DECLARE
  schema RECORD;
  result RECORD;
  sql TEXT := '';
BEGIN
  FOR schema IN EXECUTE
    format(
      'SELECT distinct table_schema FROM INFORMATION_SCHEMA.TABLES WHERE table_name =%L',
      'email'
    )
  LOOP
    sql := sql || format('(SELECT a.email_id, a.body, a.subject, a.email,b.sname as sitename,b.name as sitedomain, b.photo, ''%I'' as schema_name FROM %I.email a, %I.setting b WHERE a.status is NULL OR a.status<>1 AND b."settingID"=1 AND b.email_enabled=1 ORDER BY a.created_at ASC)  UNION ALL ', schema.table_schema, schema.table_schema,schema.table_schema);
  END LOOP;

  EXECUTE
    format('CREATE OR REPLACE VIEW %I AS ', 'all_' || table_name) || left(sql, -11);
END
$$ LANGUAGE plpgsql;

select refresh_union_view('email');
select * from all_email;

--Add column if not exists
DO $$ 
    BEGIN
        BEGIN
           ALTER TABLE public.grade
    ADD COLUMN overall_academic_note character varying;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column <column_name> already exists in <table_name>.';
        END;
    END;
$$

--drop contrain if exists

ALTER TABLE IF EXISTS testing.general_character_assessment  DROP CONSTRAINT IF EXISTS fk_student_id ;
--JOIN ALL TABLES IN DATABASE SCHEMAS: condition:- all tables must have the same no of colum and data types


  CREATE OR REPLACE FUNCTION admin.join_all(table_ VARCHAR(70)) 
    RETURNS void AS $$
DECLARE
  schema RECORD;
  result RECORD;
  sql TEXT := '';
BEGIN
  FOR schema IN EXECUTE
    format(
      'SELECT distinct table_schema FROM INFORMATION_SCHEMA.TABLES WHERE table_schema NOT IN (''admin'',''beta_testing'') AND table_name =%L',
      table_
    )
  LOOP
    sql := sql || format('(SELECT *, ''%I'' as schema_name FROM %I.%I a)  UNION ALL ', schema.table_schema, schema.table_schema,table_);
  END LOOP;

  EXECUTE
    format('CREATE OR REPLACE VIEW admin.all_%I AS ',table_) || left(sql, -11);
END;
    $$ LANGUAGE plpgsql;

--SELECT a.* from ' . set_schema_name() . 'exam_report a where a.classes_id=' . $student->classesID . ' and a.combined_exams !=\'0\'  and a.academic_year_id=' . $student->academic_year_id . ' and (a.exam_id IN (SELECT "examID" from ' . set_schema_name() . 'exam where semester_id=' . $semester->id . ' ) OR a.exam_id IN (SELECT "unnest"::integer from (select unnest(combined_exam_array) from ' . set_schema_name() . 'exam_report) a where a."unnest"::integer IN (SELECT "examID" from ' . set_schema_name() . 'exam where semester_id=' . $semester->id . ')))

ALTER ROLE postgres SET TIMEZONE = '+3';

--user birtdays, we ewill send this to all users with today birthday
SELECT *
FROM canossa.student
WHERE 
    DATE_PART('day', dob) = date_part('day', CURRENT_DATE)
AND
    DATE_PART('month', dob) = date_part('month', CURRENT_DATE)



CREATE OR REPLACE FUNCTION clone_schema_table_only(source_schema text, dest_schema text) RETURNS void AS
$BODY$
DECLARE 
  objeto text;
  buffer text;
BEGIN
    EXECUTE 'CREATE SCHEMA ' || dest_schema ;


    FOR objeto IN
        SELECT table_name::text FROM information_schema.TABLES WHERE table_schema = source_schema
    LOOP        
        buffer := dest_schema || '.' || objeto;
        EXECUTE 'CREATE TABLE ' || buffer || ' (LIKE ' || source_schema || '.' || objeto || ' INCLUDING CONSTRAINTS INCLUDING INDEXES INCLUDING DEFAULTS)';
        EXECUTE 'INSERT INTO ' || buffer || '(SELECT * FROM ' || source_schema || '.' || objeto || ')';
    END LOOP;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;




-- Function: clone_schema(source text, dest text, include_records boolean default true, show_details boolean default false)

-- DROP FUNCTION clone_schema(text, text, boolean, boolean);

CREATE OR REPLACE FUNCTION clone_schema(
  source_schema text,
  dest_schema text,
  include_recs boolean DEFAULT true,
  show_details boolean DEFAULT false)
  RETURNS void AS
$BODY$

--  This function will clone all sequences, tables, data, views & functions from any existing schema to a new one
-- SAMPLE CALL:
-- SELECT clone_schema('public', 'new_schema');
-- SELECT clone_schema('public', 'new_schema', TRUE);
-- SELECT clone_schema('public', 'new_schema', TRUE, TRUE);

DECLARE
  src_oid          oid;
  tbl_oid          oid;
  func_oid         oid;
  object           text;
  buffer           text;
  srctbl           text;
  default_         text;
  column_          text;
  qry              text;
  xrec             record;
  dest_qry         text;
  v_def            text;
  seqval           bigint;
  sq_last_value    bigint;
  sq_max_value     bigint;
  sq_start_value   bigint;
  sq_increment_by  bigint;
  sq_min_value     bigint;
  sq_cache_value   bigint;
  sq_log_cnt       bigint;
  sq_is_called     boolean;
  sq_is_cycled     boolean;
  sq_cycled        char(10);
  rec              record;
  source_schema_dot text = source_schema || '.';
  dest_schema_dot text = dest_schema || '.';

BEGIN

  -- Check that source_schema exists
  SELECT oid INTO src_oid
  FROM pg_namespace
  WHERE nspname = quote_ident(source_schema);
  IF NOT FOUND
  THEN
    RAISE NOTICE 'source schema % does not exist!', source_schema;
    RETURN ;
  END IF;

  -- Check that dest_schema does not yet exist
  PERFORM nspname
  FROM pg_namespace
  WHERE nspname = quote_ident(dest_schema);
  IF FOUND
  THEN
    RAISE NOTICE 'dest schema % already exists!', dest_schema;
    RETURN ;
  END IF;

  EXECUTE 'CREATE SCHEMA ' || quote_ident(dest_schema) ;

  -- Defaults search_path to destination schema
  PERFORM set_config('search_path', dest_schema, true);

  -- Create sequences
  -- TODO: Find a way to make this sequence's owner is the correct table.
  FOR object IN
  SELECT sequence_name::text
  FROM information_schema.sequences
  WHERE sequence_schema = quote_ident(source_schema)
  LOOP
    EXECUTE 'CREATE SEQUENCE ' || quote_ident(dest_schema) || '.' || quote_ident(object);
    srctbl := quote_ident(source_schema) || '.' || quote_ident(object);

    EXECUTE 'SELECT last_value, max_value, start_value, increment_by, min_value, cache_value, log_cnt, is_cycled, is_called
              FROM ' || quote_ident(source_schema) || '.' || quote_ident(object) || ';'
    INTO sq_last_value, sq_max_value, sq_start_value, sq_increment_by, sq_min_value, sq_cache_value, sq_log_cnt, sq_is_cycled, sq_is_called ;

    IF sq_is_cycled
    THEN
      sq_cycled := 'CYCLE';
    ELSE
      sq_cycled := 'NO CYCLE';
    END IF;

    EXECUTE 'ALTER SEQUENCE '   || quote_ident(dest_schema) || '.' || quote_ident(object)
            || ' INCREMENT BY ' || sq_increment_by
            || ' MINVALUE '     || sq_min_value
            || ' MAXVALUE '     || sq_max_value
            || ' START WITH '   || sq_start_value
            || ' RESTART '      || sq_min_value
            || ' CACHE '        || sq_cache_value
            || sq_cycled || ' ;' ;

    buffer := quote_ident(dest_schema) || '.' || quote_ident(object);
    IF include_recs
    THEN
      EXECUTE 'SELECT setval( ''' || buffer || ''', ' || sq_last_value || ', ' || sq_is_called || ');' ;
    ELSE
      EXECUTE 'SELECT setval( ''' || buffer || ''', ' || sq_start_value || ', ' || sq_is_called || ');' ;
    END IF;
    IF show_details THEN RAISE NOTICE 'Sequence created: %', object; END IF;
  END LOOP;

  -- Create tables
  FOR object IN
  SELECT TABLE_NAME::text
  FROM information_schema.tables
  WHERE table_schema = quote_ident(source_schema)
        AND table_type = 'BASE TABLE'

  LOOP
    buffer := dest_schema || '.' || quote_ident(object);
    EXECUTE 'CREATE TABLE ' || buffer || ' (LIKE ' || quote_ident(source_schema) || '.' || quote_ident(object)
            || ' INCLUDING ALL)';

    IF include_recs
    THEN
      -- Insert records from source table
      EXECUTE 'INSERT INTO ' || buffer || ' SELECT * FROM ' || quote_ident(source_schema) || '.' || quote_ident(object) || ';';
    END IF;

    FOR column_, default_ IN
    SELECT "column_name"::text,
      REPLACE("column_default"::text, source_schema, dest_schema)
    FROM information_schema.COLUMNS
    WHERE table_schema = dest_schema
          AND TABLE_NAME = object
          AND "column_default" LIKE 'nextval(%' || quote_ident(source_schema) || '%::regclass)'
    LOOP
      EXECUTE 'ALTER TABLE ' || buffer || ' ALTER COLUMN "' || column_ || '" SET DEFAULT ' || default_;
    END LOOP;

    IF show_details THEN RAISE NOTICE 'base table created: %', object; END IF;

  END LOOP;

  --  add FK constraint
  FOR xrec IN
  SELECT ct.conname as fk_name, rn.relname as tb_name,  'ALTER TABLE ' || quote_ident(dest_schema) || '.' || quote_ident(rn.relname)
         || ' ADD CONSTRAINT ' || quote_ident(ct.conname) || ' ' || replace(pg_get_constraintdef(ct.oid), source_schema_dot, '') || ';' as qry
  FROM pg_constraint ct
    JOIN pg_class rn ON rn.oid = ct.conrelid
  WHERE connamespace = src_oid
        AND rn.relkind = 'r'
        AND ct.contype = 'f'

  LOOP
    IF show_details THEN RAISE NOTICE 'Creating FK constraint %.%...', xrec.tb_name, xrec.fk_name; END IF;
    --RAISE NOTICE 'DEF: %', xrec.qry;
    EXECUTE xrec.qry;
  END LOOP;

  -- Create functions
  FOR xrec IN
  SELECT proname as func_name, oid as func_oid
  FROM pg_proc
  WHERE pronamespace = src_oid

  LOOP
    IF show_details THEN RAISE NOTICE 'Creating function %...', xrec.func_name; END IF;
    SELECT pg_get_functiondef(xrec.func_oid) INTO qry;
    SELECT replace(qry, source_schema_dot, '') INTO dest_qry;
    EXECUTE dest_qry;
  END LOOP;

  -- add Table Triggers
  FOR rec IN
  SELECT
    trg.tgname AS trigger_name,
    tbl.relname AS trigger_table,

    CASE
    WHEN trg.tgenabled='O' THEN 'ENABLED'
    ELSE 'DISABLED'
    END AS status,
    CASE trg.tgtype::integer & 1
    WHEN 1 THEN 'ROW'::text
    ELSE 'STATEMENT'::text
    END AS trigger_level,
    CASE trg.tgtype::integer & 66
    WHEN 2 THEN 'BEFORE'
    WHEN 64 THEN 'INSTEAD OF'
    ELSE 'AFTER'
    END AS action_timing,
    CASE trg.tgtype::integer & cast(60 AS int2)
    WHEN 16 THEN 'UPDATE'
    WHEN 8 THEN 'DELETE'
    WHEN 4 THEN 'INSERT'
    WHEN 20 THEN 'INSERT OR UPDATE'
    WHEN 28 THEN 'INSERT OR UPDATE OR DELETE'
    WHEN 24 THEN 'UPDATE OR DELETE'
    WHEN 12 THEN 'INSERT OR DELETE'
    WHEN 32 THEN 'TRUNCATE'
    END AS trigger_event,
    'EXECUTE PROCEDURE ' ||  (SELECT nspname FROM pg_namespace where oid = pc.pronamespace )
    || '.' || proname || '('
    || regexp_replace(replace(trim(trailing '\000' from encode(tgargs,'escape')), '\000',','),'{(.+)}','''{\1}''','g')
    || ')' as action_statement

  FROM pg_trigger trg
    JOIN pg_class tbl on trg.tgrelid = tbl.oid
    JOIN pg_proc pc ON pc.oid = trg.tgfoid
  WHERE trg.tgname not like 'RI_ConstraintTrigger%'
        AND trg.tgname not like 'pg_sync_pg%'
        AND tbl.relnamespace = (SELECT oid FROM pg_namespace where nspname = quote_ident(source_schema) )

  LOOP
    buffer := dest_schema || '.' || quote_ident(rec.trigger_table);
    IF show_details THEN RAISE NOTICE 'Creating trigger % % % ON %...', rec.trigger_name, rec.action_timing, rec.trigger_event, rec.trigger_table; END IF;
    EXECUTE 'CREATE TRIGGER ' || rec.trigger_name || ' ' || rec.action_timing
            || ' ' || rec.trigger_event || ' ON ' || buffer || ' FOR EACH '
            || rec.trigger_level || ' ' || replace(rec.action_statement, source_schema_dot, '');

  END LOOP;

  -- Create views
  FOR object IN
  SELECT table_name::text,
    view_definition
  FROM information_schema.views
  WHERE table_schema = quote_ident(source_schema)

  LOOP
    buffer := dest_schema || '.' || quote_ident(object);
    SELECT replace(view_definition, source_schema_dot, '') INTO v_def
    FROM information_schema.views
    WHERE table_schema = quote_ident(source_schema)
          AND table_name = quote_ident(object);
    IF show_details THEN RAISE NOTICE 'Creating view % AS %', object, regexp_replace(v_def, '[\n\r]+', ' ', 'g'); END IF;
    EXECUTE 'CREATE OR REPLACE VIEW ' || buffer || ' AS ' || v_def || ';' ;

  END LOOP;

  RETURN;

END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


UPDATE gracenursery.student set "sectionID"=a.section_id, "section"=a.section, academic_year_id=a.academic_year_id, "classesID"=a."classesID" FROM  (select a.student_id,a.section_id,b.section,b."classesID",a.academic_year_id from gracenursery.student_archive a join gracenursery.section b on b."sectionID"=a.section_id where a.academic_year_id=1) a WHERE "student_id"=a.student_id




--delete check if exists
DO $$ 
    BEGIN
        BEGIN
            ALTER TABLE <table_name> ADD COLUMN <column_name> <column_type>;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column <column_name> already exists in <table_name>.';
        END;
    END;
$$

insert into greenbird.fee_installment (fee_id,amount,installment_id,class_level_id,academic_year_id, total_amount, status,class_id )
select fee_id,amount,installment_id,class_level_id,academic_year_id, total_amount, status,"classesID" from greenbird.fee_installment_classes WHERE NOT EXISTS (SELECT 1
    FROM greenbird.fee_installment
    WHERE 
    class_id = greenbird.fee_installment_classes."classesID" AND fee_id = greenbird.fee_installment_classes.fee_id  AND installment_id = greenbird.fee_installment_classes.installment_id
    );

--check foreign key, primary, check it by sql
SELECT conrelid::regclass AS table_from
      ,conname
      ,pg_get_constraintdef(c.oid)
FROM   pg_constraint c
JOIN   pg_namespace n ON n.oid = c.connamespace
WHERE  contype IN ('f', 'p ')
AND    n.nspname = 'testing' -- your schema here
ORDER  BY conrelid::regclass::text, contype DESC;





CREATE OR REPLACE FUNCTION testing.shift_to_installment(
	academic_id integer,
	installment_id integer)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

 DECLARE r record; j record; h record; f record; d record; s record; n record ;l record ;previous_academic_id integer; current_id integer; resultant text; 
 
 BEGIN 
 resultant='No change Made'; 
 
 For j IN select * from testing.installments where id=installment_id   
 LOOP  
 IF exists(select 1 FROM testing.installments where academic_year_id=j.academic_year_id and start_date>j.start_date) THEN 
 For r IN select a.id,a.start_date,b.id as fees_installment_id FROM testing.installments a join fees_installments b on b.installment_id=a.id and b.fee_id=1000 where academic_year_id=j.academic_year_id and start_date>j.start_date order by start_date asc limit 1

 LOOP
if exists(select 1  from testing.fees_installments a join testing.installments b on a.installment_id=b.id join testing.transport_routes_fees_installments c on c.fees_installment_id=a.id  join testing.tmembers e on e.transport_route_id=c.transport_route_id AND e.installment_id=j.id where b.id=j.id and a.fee_id=1000 group by e.student_id,a.id,a.fee_id,b.start_date,c.amount,c.transport_route_id order by e.student_id) 
THEN
FOR d in select e.student_id,a.id,a.fee_id,b.start_date,c.amount,c.transport_route_id,e.is_oneway,e.vehicle_id from testing.fees_installments a join testing.installments b on a.installment_id=b.id join testing.transport_routes_fees_installments c on c.fees_installment_id=a.id  join testing.tmembers e on e.transport_route_id=c.transport_route_id AND e.installment_id=j.id where b.id=j.id and a.fee_id=1000 group by e.student_id,a.id,a.fee_id,b.start_date,c.amount,c.transport_route_id,e.is_oneway,e.vehicle_id order by e.student_id
LOOP
IF exists(select 1 FROM testing.tmembers where tmembers.installment_id=r.id and student_id=d.student_id) THEN

ELSE

INSERT INTO testing.tmembers (student_id,vehicle_id,is_oneway,installment_id,transport_route_id) VALUES (d.student_id,d.vehicle_id,d.is_oneway,r.id,d.transport_route_id);
END IF;
IF EXISTS(select 1 from testing.discount_fees_installments where fees_installment_id=d.id and student_id=d.student_id) THEN 
For h IN select amount,student_id from testing. discount_fees_installments  where fees_installment_id=d.id and student_id=d.student_id
LOOP
IF NOT EXISTS(select 1 from testing.discount_fees_installments where fees_installment_id=r.fees_installment_id and student_id=h.student_id) THEN 
INSERT INTO testing.discount_fees_installments (fees_installment_id,amount,student_id) VALUES (r.fees_installment_id,h.amount,h.student_id);
END IF;
END LOOP;
 
END IF;

END LOOP;
END IF;

END LOOP;
resultant='Transfer made successfully';

ELSE

END IF;
 
END LOOP; 
 
 RETURN resultant;
 END; 

$BODY$;

ALTER FUNCTION testing.shift_to_installment(integer, integer)
    OWNER TO postgres;

ALTER TABLE the_table ALTER COLUMN col_name TYPE integer USING (trim(col_name)::integer);



--upgrades library module
CREATE OR REPLACE RULE payment_del_protect AS
    ON DELETE TO geniuskings.payments
    DO INSTEAD
NOTHING;

--delete duplicates
--WITH single AS (SELECT DISTINCT ON (user_id, "table", payment_date) * FROM public.salaries) DELETE FROM  public.salaries WHERE  salaries.id NOT IN (SELECT id FROM single);

