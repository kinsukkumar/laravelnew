

CREATE OR REPLACE FUNCTION testing.automatic_promotion(
	)
RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$ 
DECLARE r record; j record; h record; f record; d record; s record; n record ; l record ; previous_academic_id integer; current_id integer ; resultant text; new_section_id integer; p record;
BEGIN
resultant='0'; 

for r IN select distinct start_date,class_level_id from testing.academic_year where date_part ('year', start_date) =  date_part('year', current_date - interval '1 year')
LOOP


IF EXISTS(select 1 from testing.academic_year where  start_date=r.start_date and class_level_id=r.class_level_id order by class_level_id desc) THEN
FOR j IN select * from testing.academic_year where  start_date=r.start_date and class_level_id=r.class_level_id
LOOP


IF NOT EXISTS(select 1 from testing.academic_year where   class_level_id=j.class_level_id and start_date=j.start_date + interval '1 year' and end_date=j.end_date + interval '1 year') THEN
insert into testing.academic_year (name,class_level_id,start_date,end_date) values(date_part ('year', j.end_date + interval '1 year'),j.class_level_id,j.start_date + interval '1 year',j.end_date + interval '1 year') returning id into current_id ;
ELSE
select id into current_id from testing.academic_year where   class_level_id=j.class_level_id and start_date=j.start_date + interval '1 year' and end_date=j.end_date + interval '1 year';

END IF;

IF EXISTS(select 1 from testing.semester where academic_year_id=j.id) THEN
FOR h IN select * from testing.semester where academic_year_id=j.id
LOOP

IF NOT EXISTS(select 1 from testing.semester where academic_year_id=current_id and class_level_id=r.class_level_id and start_date=h.start_date + interval '1 year' and end_date = h.end_date + interval '1 year') THEN
insert into testing.semester (name,academic_year_id,class_level_id,start_date,end_date) values(h.name, current_id,r.class_level_id,h.start_date + interval '1 year', h.end_date + interval '1 year');
END IF;
END LOOP;
END IF;
END LOOP;
ELSE

resultant='3';
RETURN  resultant;
END IF;


if exists(select 1 from testing.classes where classlevel_id=r.class_level_id) THEN
FOR f IN select * from testing.classes where classlevel_id=r.class_level_id  order by classes_numeric desc
LOOP


if exists(select 1 from testing.classes where classes_numeric > f.classes_numeric and classlevel_id=f.classlevel_id order by classes_numeric asc limit 1) THEN

FOR d IN select * from testing.classes where classes_numeric > f.classes_numeric and classlevel_id=r.class_level_id  order by classes_numeric asc limit 1 
LOOP

IF EXISTS(select 1 from testing.student_archive a join section b on a.section_id=b."sectionID" WHERE b."classesID"=f."classesID") THEN

FOR n IN select a.student_id,b."sectionID",b.section from testing.student_archive a join section b on a.section_id=b."sectionID" WHERE b."classesID"=f."classesID"
LOOP

IF EXISTS(SELECT 1 FROM testing.section  WHERE  lower(section)=lower(n.section)  and "classesID"=d."classesID")THEN

FOR s IN SELECT  * FROM testing.section where section=n.section  and  "classesID"=d."classesID" LIMIT 1
LOOP


update testing.student set "classesID"=d."classesID","sectionID"=s."sectionID",academic_year_id=current_id where student_id=n.student_id;
if NOT exists(select 1 from testing.student_archive where  student_id=n.student_id  and academic_year_id=current_id) THEN
insert into testing.student_archive (section_id,academic_year_id,student_id) VALUES(s."sectionID",current_id,n.student_id);
resultant='1';
END IF;
END LOOP;
ELSE

IF EXISTS(SELECT  1 FROM testing.section where lower(section)=lower('none') AND "classesID"=d."classesID") THEN
FOR p IN SELECT  * FROM testing.section where "classesID"=d."classesID" and lower(section)='none' limit 1
LOOP
update testing.student set "classesID"=d."classesID","sectionID"=p."sectionID",academic_year_id=current_id where student_id=n.student_id;
if NOT exists(select 1 from testing.student_archive where  student_id=n.student_id  and academic_year_id=current_id) THEN
insert into testing.student_archive (section_id,academic_year_id,student_id) VALUES(p."sectionID",current_id,n.student_id);
resultant='1';
END IF;
END LOOP;

ELSE
insert into testing.section (category,"teacherID",section,"classesID") values('none',1,'none', d."classesID") returning "sectionID" into new_section_id ;
 
update testing.student set "classesID"=d."classesID","sectionID"=new_section_id,academic_year_id=current_id;
if NOT exists(select 1 from testing.student_archive where  student_id=n.student_id  and academic_year_id=current_id) THEN
insert into testing.student_archive (section_id,academic_year_id,student_id) VALUES(new_section_id,current_id,n.student_id);
resultant='1';
END IF;

END IF;

END IF;

END LOOP;
END IF;
END LOOP;
ELSE
resultant='2';
update testing.student set status=2 where "classesID"=f."classesID";

END IF;

END LOOP;
END IF;

END LOOP;
RETURN resultant;
END ;
$BODY$;

ALTER FUNCTION testing.automatic_promotion()
    OWNER TO postgres;
