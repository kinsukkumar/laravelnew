/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  hp
 * Created: 25 Jul 2020
 */

alter table testing.deductions add column predefined smallint default 0;
CREATE TABLE constant.loan_sources
(
    id serial,
    name character varying COLLATE pg_catalog."default",
	description text COLLATE pg_catalog."default",
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone,   
    CONSTRAINT loan_sources_id_primary PRIMARY KEY (id)
);

alter table testing.revenues add column loan_application_id integer;
alter table testing.user_deductions add column loan_application_id integer;
CREATE TABLE testing.loan_payments
(
    id serial,
    loan_application_id integer,
    amount numeric(10,2) NOT NULL,
    payment_type_id integer,
    date date NOT NULL,
    transaction_id character varying COLLATE pg_catalog."default",
    created_at timestamp without time zone DEFAULT now(),
    cheque_number character varying COLLATE pg_catalog."default",
    bank_account_id integer,
    payer_name character varying COLLATE pg_catalog."default",
    mobile_transaction_id character varying COLLATE pg_catalog."default",
    transaction_time character varying COLLATE pg_catalog."default",
    account_number character varying COLLATE pg_catalog."default",
    token character varying COLLATE pg_catalog."default",
    reconciled smallint DEFAULT 0,
    receipt_code character varying COLLATE pg_catalog."default" NOT NULL DEFAULT nextval('testing.payments_receipt_seq'::regclass),
    updated_at timestamp without time zone,
    channel character varying COLLATE pg_catalog."default",
    created_by integer,
    created_by_table character varying COLLATE pg_catalog."default",
    status smallint DEFAULT 0,
    CONSTRAINT loan_payments_id_primary PRIMARY KEY (id),
    CONSTRAINT loan_payments_bank_account_id_foreign FOREIGN KEY (bank_account_id)
        REFERENCES testing.bank_accounts (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT loan_payments_payment_type_foreign FOREIGN KEY (payment_type_id)
        REFERENCES constant.payment_types (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

COMMENT ON COLUMN testing.loan_payments.transaction_id
    IS 'from bank, paygate, cheque no, etc';

COMMENT ON COLUMN testing.loan_payments.transaction_time
    IS 'from third party, bank, mobile etc';

COMMENT ON COLUMN testing.loan_payments.token
    IS 'If token is not empty, then this transaction is done with e-payments so we need to also disable option to delete this transaction';


CREATE TABLE testing.loan_types
(
    id serial,
    name character varying COLLATE pg_catalog."default",
    source smallint DEFAULT 1,
    minimum_amount numeric,
    maximum_amount numeric,
    maximum_tenor numeric,
    minimum_tenor numeric,
    interest_rate numeric,
    credit_ratio numeric,
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone,
    created_by integer,
    created_by_table character varying COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    CONSTRAINT loan_type_id_primary PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE testing.loan_types
    OWNER to postgres;



CREATE TABLE testing.loan_applications
(
    id serial,
    user_id integer,
    "table" character varying COLLATE pg_catalog."default",
    created_by integer,
    created_by_table character varying COLLATE pg_catalog."default",
    approved_by integer,
    approved_by_table character varying COLLATE pg_catalog."default",
    approval_status smallint DEFAULT 0,
    amount numeric(10,2),
    payment_start_date date,
    loan_type_id integer,
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone,
    quality character varying COLLATE pg_catalog."default",
    months integer,
    description text COLLATE pg_catalog."default",
    monthly_repayment_amount numeric,
    loan_source_id integer,
    CONSTRAINT loan_applications_id_primary PRIMARY KEY (id),
    CONSTRAINT loan_applications_loan_type_id FOREIGN KEY (loan_type_id)
        REFERENCES testing.loan_types (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE testing.loan_applications
    OWNER to postgres;

CREATE TABLE admin.training_modules
(
    id serial,
    name character varying,
    description text,
    created_by integer,
    specialist_id integer,
	  created_at timestamp without time zone default now(),
    updated_at timestamp without time zone,
    CONSTRAINT training_modules_id_primary PRIMARY KEY (id),
    CONSTRAINT training_modules_created_by_id_foreign FOREIGN KEY (created_by)
        REFERENCES admin.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);
CREATE TABLE admin.training_sections
(
    id serial,
    title character varying,
    position integer,
    created_by integer,
	training_module_id integer,
	created_at timestamp without time zone default now(),
    updated_at timestamp without time zone,
    CONSTRAINT training_sections_id_primary PRIMARY KEY (id),
    CONSTRAINT training_sections_created_by_id_foreign FOREIGN KEY (created_by)
        REFERENCES admin.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
	 CONSTRAINT training_sections_training_module_id_foreign FOREIGN KEY (training_module_id)
        REFERENCES admin.training_modules (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);
ALTER TABLE beta_testing.user_deductions DROP CONSTRAINT user_deductions_user_id_table_deduction_id_unique;