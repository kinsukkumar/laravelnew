set search_path='beta_testing';

--classesID, ACADEMIC_YEAR_ID,SECTIONID MUST BE UPDATED TO RELATE WITH STUDENT_ARCHIVE

select distinct start_date,class_level_id from academic_year where date_part ('year', start_date) =  date_part('year', current_date - interval '1 year');
--select here to get previous academic_year_id
select * from academic_year where  start_date='' and class_level_id='';

--loop here to insert a new academic_year returning id to insert in installment
insert into academic_year (start_date,end_date) values(start_date+ interval '1 year', end_date + interval '1 year');
--select from semester for the previous year
select * from semester where academic_year_id='previous_id';
---loop through to insert the semester for a new year
insert into semester (academic_year_id,class_level_id,start_date,end_date) values(new_academic_year_id,class_level_id,start_date + interval '1 year', end_date + interval '1 year');

---select classes
select * from classes1 where class_level_id='class_level_id';
--check whether final  class or not
select * from classes2 where class_numeric > 'class_numeric' order by class_numeric asc limit 1
--if this is the final then
update student set status=2 where classesID='classesID';
--IF THIS IS NOT FINAL THEN
SELECT  * FROM section1 where classesID='classes2ID';
select * from students where classesID='classes1ID';
--UPDATE STUDENT AND enter student to student_archive
update student set (classesID,sectionID,academic_year_id) values(classes2ID, section1ID,current_academic_year_id);
--check if this  student exists first in student_archive
select * from student_archive where section_id='' and student_id=''  and academic_year_id='';
--if student_does not exists then insert
insert into student_archive (section_id,academic_year_id,student_id)














-- FUNCTION: beta_testing.automatic_promotion()

-- DROP FUNCTION beta_testing.automatic_promotion();

CREATE OR REPLACE FUNCTION beta_testing.automatic_promotion(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$ 
DECLARE r record; j record; h record; f record; d record; s record; n record ; l record ; previous_academic_id integer; current_id integer ; resultant text; new_section_id integer; p record;
BEGIN
resultant='No change Made'; 


for r IN select distinct start_date,class_level_id from beta_testing.academic_year where date_part ('year', start_date) =  date_part('year', current_date - interval '1 year')
LOOP

--select here to get previous academic_year_id
IF EXISTS(select 1 from beta_testing.academic_year where  start_date=r.start_date and class_level_id=r.class_level_id) THEN
FOR j IN select * from beta_testing.academic_year where  start_date=r.start_date and class_level_id=r.class_level_id
LOOP

--loop here to insert a new academic_year returning id to insert in installment
insert into beta_testing.academic_year (start_date,end_date) values(j.start_date + interval '1 year', j.end_date + interval '1 year') returning id into current_id ;
--select from semester for the previous year
IF EXISTS(select 1 from beta_testing.semester where academic_year_id=j.id) THEN
FOR h IN select * from beta_testing.semester where academic_year_id=j.id
LOOP
---loop through to insert the semester for a new year
insert into beta_testing.semester (academic_year_id,class_level_id,start_date,end_date) values(current_id,r.class_level_id,h.start_date + interval '1 year', h.end_date + interval '1 year');
END LOOP;
END IF;
END LOOP;
ELSE
resultant='CAN NOT PROMOTE TILL NEXT YEAR OR DEFINE NEW ACADEMIC YEAR BEFORE PROMOTE';
RETURN  resultant;
END IF;



---select classes
if exists(select 1 from beta_testing.classes where class_level_id=r.class_level_id) THEN
FOR f IN select * from beta_testing.classes where class_level_id=r.class_level_id
LOOP

--check whether class exists above this one in this class level
if exists(select 1 from beta_testing.classes where class_numeric > f.class_numeric order by class_numeric asc limit 1) THEN
FOR d IN select * from beta_testing.classes where class_numeric > f.class_numeric order by class_numeric asc limit 1 
LOOP

IF EXISTS(select 1 from beta_testing.student_archive a join section b on a.section_id=b."sectionID" WHERE b."classesID"=f."classesID") THEN

---SELECT STUDENTS FROM STUDENT_ARCHIVE TO GET THE PREVIOUS SECTION
FOR n IN select a.student_id,b.section_id,b.name from beta_testing.student_archive a join section b on a.section_id=b."sectionID" WHERE b."classesID"=f."classesID"
LOOP
IF EXISTS(SELECT 1 FROM beta_testing.section  WHERE  lower(section)=lower(n.name)  and "classesID"=d."classesID")THEN

FOR s IN SELECT  * FROM beta_testing.section where section=n.name  and  "classesID"=d."classesID" LIMIT 1
LOOP
--UPDATE STUDENT AND enter student to student_archive
update beta_testing.student set classesID=d."classesID",sectionID=s."sectionID",academic_year_id=current_id;
--check if this  student exists first in student_archive
if NOT exists(select 1 from beta_testing.student_archive where section_id=s."sectionID" and student_id=n.student_id  and academic_year_id=current_id) THEN
--if student_does not exists then insert
insert into beta_testing.student_archive (section_id,academic_year_id,student_id) VALUES(s."sectionID",n.student_id,current_id);
resultant='Promoted successfully';
END IF;
END LOOP;
ELSE

IF EXISTS(SELECT  1 FROM beta_testing.section where lower(section)=lower('none') AND "classesID"=d."classesID") THEN
--DEFAULT SECTION HAS BEEN CREATED SO INSERT STUDENT
FOR p IN SELECT  * FROM beta_testing.section where 'classesID'=d."classesID" and lower(section)='none' limit 1
LOOP
update beta_testing.student set classesID=d."classesID",sectionID=p."sectionID",academic_year_id=current_id;
--check if this  student exists first in student_archive
if NOT exists(select 1 from beta_testing.student_archive where section_id=p."sectionID" and student_id=n.student_id  and academic_year_id=current_id) THEN
--if student_does not exists then insert
insert into beta_testing.student_archive (section_id,academic_year_id,student_id) VALUES(p."sectionID",n.student_id,current_id);
resultant='Promoted successfully';
END IF;
END LOOP;

ELSE
---INSERT NEW SECTION AND
insert into beta_testing.section (section,"classesID") values('none', d."classesID") returning "sectionID" into new_section_id ;
 
update beta_testing.student set classesID=d."classesID",sectionID=new_section_id,academic_year_id=current_id;
--check if this  student exists first in student_archive
if NOT exists(select 1 from beta_testing.student_archive where section_id=new_section_id and student_id=n.student_id  and academic_year_id=current_id) THEN
--if student_does not exists then insert
insert into beta_testing.student_archive (section_id,academic_year_id,student_id) VALUES(new_section_id,n.student_id,current_id);
resultant='Promoted successfully';
END IF;
END IF;
END IF;
END LOOP;
ELSE
--if this is the final then
update beta_testing.student set status=2 where 'classesID'=d."classesID";

END IF;
--END LOOP;
--END IF;
--END LOOP;
--END IF;
END LOOP;
END IF;
END LOOP;



RETURN resultant;

END; $BODY$;

ALTER FUNCTION beta_testing.automatic_promotion()
    OWNER TO postgres;
