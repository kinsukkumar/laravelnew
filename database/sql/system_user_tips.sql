
CREATE sequence page_tips_viewers_id_seq;

CREATE TABLE public.page_tips_viewers
(
    id integer NOT NULL DEFAULT nextval('page_tips_viewers_id_seq'::regclass),
    user_id integer,
    "table" character varying COLLATE pg_catalog."default",
    page_tip_id integer,
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone,
    is_helpful integer,
    not_helpful_comment text COLLATE pg_catalog."default",
    CONSTRAINT page_tips_viewer_primary PRIMARY KEY (id),
    CONSTRAINT page_tips_viewer_tip_id_foreign FOREIGN KEY (page_tip_id)
        REFERENCES admin.page_tips (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)

CREATE sequence admin.page_tips_id_seq;
CREATE TABLE admin.page_tips
(
    id integer NOT NULL DEFAULT nextval('admin.page_tips_id_seq'::regclass),
    page character varying COLLATE pg_catalog."default",
    content text COLLATE pg_catalog."default",
    language character varying COLLATE pg_catalog."default",
    controller_page character varying COLLATE pg_catalog."default",
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone,
    CONSTRAINT page_tips_pkey PRIMARY KEY (id)
)
