

CREATE OR REPLACE FUNCTION testing.redistribute_student_payments(
	studentid integer)
RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$ DECLARE r record; resultant Text; BEGIN resultant := 'The student had No Payments'; IF exists(select 1 from testing.payments where student_id=studentid) THEN FOR r IN select id from testing.payments where student_id=studentid LOOP 
DELETE FROM testing.advance_payments where student_id=student_id and payment_id is not null;
DELETE FROM testing.payments_invoices_fees_installments where payment_id=r.id; 
delete from testing.advance_payments_invoices_fees_installments where advance_payment_id in (select id from testing.advance_payments where student_id=studentid); resultant='Success'; END LOOP; END IF; PERFORM testing.payments_distributions(); PERFORM testing.due_amounts_distributions(); PERFORM testing.advance_payments_distribution_by_student_id(studentid); RETURN resultant; END; $BODY$;

ALTER FUNCTION testing.redistribute_student_payments(integer)
    OWNER TO postgres;
