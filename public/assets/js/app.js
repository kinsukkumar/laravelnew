var wrapper = document.getElementById("signature-pad"),
    clearButton = wrapper.querySelector("[data-action=clear]"),
    saveButton = wrapper.querySelector("[data-action=save]"),
    canvas = wrapper.querySelector("canvas"),
    signaturePad;

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

signaturePad = new SignaturePad(canvas);
signaturePad.penColor = "rgb(66, 133, 244)";
//signaturePad.minWidth = 5;
//signaturePad.maxWidth = 10;

clearButton.addEventListener("click", function (event) {
    signaturePad.clear();
});

saveButton.addEventListener("click", function (event) {


    if (signaturePad.isEmpty()) {
        swal('No signature',"Please provide signature first.");
    } else {
        signaturePad.removeBlanks();

        //Save the signature string on the server

var img = signaturePad.toDataURL("image/png");
        $.ajax({
            url:'save_signature',
            data:'signature='+img,
            type:'POST',
            success:function(data){

                console.log('SUCCESS: '+data);
                if(data=='SUCCESS'){
                swal({
                    type:'success',
                    title:'Captured',
                    text:"Signature Successfully captured!."},function(){
                    window.location.replace('signature');
                });

            }else
                swal({
                    type:'warning',
                    title:'Could not Save',
                    text:"Signature not captured!."});
            },
            error:function(data){

                console.log('ERROR '+ data);
                swal({
                    type:'warning',
                    title:'No signature',
                    text:"Signature not captured!."});
            }
        })
       // window.open(signaturePad.toDataURL());
    }
});

SignaturePad.prototype.removeBlanks = function () {
    var imgWidth = this._ctx.canvas.width;
    var imgHeight = this._ctx.canvas.height;
    var imageData = this._ctx.getImageData(0, 0, imgWidth, imgHeight),
        data = imageData.data,
        getAlpha = function(x, y) {
            return data[(imgWidth*y + x) * 4 + 3]
        },
        scanY = function (fromTop) {
            var offset = fromTop ? 1 : -1;

            // loop through each row
            for(var y = fromTop ? 0 : imgHeight - 1; fromTop ? (y < imgHeight) : (y > -1); y += offset) {

                // loop through each column
                for(var x = 0; x < imgWidth; x++) {
                    if (getAlpha(x, y)) {
                        return y;
                    }
                }
            }
            return null; // all image is white
        },
        scanX = function (fromLeft) {
            var offset = fromLeft? 1 : -1;

            // loop through each column
            for(var x = fromLeft ? 0 : imgWidth - 1; fromLeft ? (x < imgWidth) : (x > -1); x += offset) {

                // loop through each row
                for(var y = 0; y < imgHeight; y++) {
                    if (getAlpha(x, y)) {
                        return x;
                    }
                }
            }
            return null; // all image is white
        };

    var cropTop = scanY(true),
        cropBottom = scanY(false),
        cropLeft = scanX(true),
        cropRight = scanX(false);

    var relevantData = this._ctx.getImageData(cropLeft, cropTop, cropRight-cropLeft, cropBottom-cropTop);
    this._canvas.width = cropRight-cropLeft;
    this._canvas.height = cropBottom-cropTop;
    this._ctx.clearRect(0, 0, cropRight-cropLeft, cropBottom-cropTop);
    this._ctx.putImageData(relevantData, 0, 0);
};
