var FormWizard = function () {
    var wizardContent = $('#wizard');
    var wizardForm = $('#form');
    $.validator.addMethod("loginRegex", function (value, element) {
        return this.optional(element) || /\(?([0-9]{8})\)?([-]?)([0-9]{5})\2([0-9]{5})\2([0-9]{2})/i.test(value);
    }, "National ID must contain numbers and dashes.");
    $.validator.addMethod("textOnly",
            function (value, element) {
                return this.optional(element) || /^[a-z]+$/i.test($.trim(value));
            }, "Letters only please"
            );

    var initWizard = function () {
        // function to initiate Wizard Form
        wizardContent.smartWizard({
            selected: 0,
            keyNavigation: false,
            onLeaveStep: leaveAStepCallback,
            onShowStep: onShowStep,
        });
        var numberOfSteps = 0;
        animateBar();
        initValidator();
    };
    var animateBar = function (val) {
        if ((typeof val == 'undefined') || val == "") {
            val = 1;
        }
        ;
        numberOfSteps = $('.swMain > ul > li').length;
        var valueNow = Math.floor(100 / numberOfSteps * val);
        $('.step-bar').css('width', valueNow + '%');
    };
    var initValidator = function () {
        $.validator.addMethod("cardExpiry", function () {
            //if all values are selected
            if ($("#card_expiry_mm").val() != "" && $("#card_expiry_yyyy").val() != "") {
                return true;
            } else {
                return false;
            }
        }, 'Please select a month and year');
        $.validator.addMethod("textCheck", function (value, element) {
            var filter = /^[a-zA-Z']+$/;
            if (!filter.test(value)) {
                return false;
            } else {
                return true;
            }
        }, 'Only characters are allowed');
        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) {// render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") {// for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: ':hidden',
            rules: {
                firstname: {
                    minlength: 3,
                    required: true,
                    textCheck : true
                },
                companyname: {
                    minlength: 3,
                    required: true
                },
                lastname: {
                    minlength: 3,
                    required: true,
                    textCheck: true
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    minlength: 6,
                    required: true
                },
                password_again: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                middlename: {
                    minlength: 3,
                    required: false,
                    textOnly: true
                },
                mobilephone: {
                    required: true,
                    minlength: 10,
                    maxlength: 15,
                    number: true
                },
                gender: {
                    required: true
                },
                address: {
                    required: true
                },
                city: {
                    required: true
                },
                country: {
                    required: true
                },
                card_name: {
                    required: true
                },
                card_number: {
                    minlength: 16,
                    maxlength: 16,
                    required: true
                },
                /*identitynumber1 : {
                 number:true,
                 minlength : 8,
                 maxlength : 8,
                 required : true
                 },
                 identitynumber : {
                 //number:true,
                 //minlength : 1,
                 //maxlength : 6,
                 required : true
                 },
                 identitynumber2 : {
                 //loginRegex: true,
                 //minlength : 23,
                 //maxlength : 23,
                 required : true
                 },*/
                /*identitynumber3 : {
                 number:true,
                 minlength : 10,
                 maxlength : 10,
                 required : true
                 },*/
                /*identitynumber4 : {
                 minlength : 16,
                 maxlength : 16,
                 required : true
                 },*/
                card_cvc: {
                    digits: true,
                    required: true,
                    minlength: 3,
                    maxlength: 4
                },
                card_expiry_yyyy: "cardExpiry",
                payment: {
                    required: true,
                    minlength: 1
                },
                street: {
                    required: true,
                    minlength: 3
                },
                road: {
                    required: true,
                    minlength: 3
                },
            },
            messages: {
                mobilephone: "Please enter a valid phone number",
                street: "Please specify a valid street",
                road: "Please specify a valid road",
                companyname: "Please specify a valid Name",
                identitynumber: "Please specify a valid Registration Number.",
                identitynumber1: "Please specify a valid Voting ID.",
                identitynumber2: "Please specify a valid National ID.",
                identitynumber3: "Please specify a valid Driving License.",
                identitynumber4: "Please specify a valid Passport ID."
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) {// revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    };
    var displayConfirm = function () {
        $('.display-value', form).each(function () {
            var input = $('[name="' + $(this).attr("data-display") + '"]', form);
            if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                $(this).html(input.val());
            } else if (input.is("select")) {
                $(this).html(input.find('option:selected').text());
            } else if (input.is(":radio") || input.is(":checkbox")) {

                $(this).html(input.filter(":checked").closest('label').text());
            } else if ($(this).attr("data-display") == 'card_expiry') {
                $(this).html($('[name="card_expiry_mm"]', form).val() + '/' + $('[name="card_expiry_yyyy"]', form).val());
            }
        });
    };
    var onShowStep = function (obj, context) {
        $(".next-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goForward");
        });
        $(".back-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goBackward");
        });
        $(".finish-step").unbind("click").click(function (e) {
            e.preventDefault();
            onFinish(obj, context);
        });
    };
    var leaveAStepCallback = function (obj, context) {
        return validateSteps(context.fromStep, context.toStep);
        // return false to stay on step and true to continue navigation
    };
    var onFinish = function (obj, context) {
        if (validateAllSteps()) {

            $('.anchor').children("li").last().children("a").removeClass('wait').removeClass('selected').addClass('done');
            //wizardForm.submit();
        }
    };
    var validateSteps = function (stepnumber, nextstep) {
        var isStepValid = false;
        if (numberOfSteps > nextstep && nextstep > stepnumber) {
            // cache the form element selector
            if (wizardForm.valid()) {// validate the form
                wizardForm.validate().focusInvalid();
                $('.anchor').children("li:nth-child(" + stepnumber + ")").children("a").removeClass('wait');
                //focus the invalid fields
                animateBar(nextstep);
                isStepValid = true;
                return true;
            }
            ;
        } else if (nextstep < stepnumber) {
            $('.anchor').children("li:nth-child(" + stepnumber + ")").children("a").addClass('wait');
            animateBar(nextstep);
            return true;
        } else {
            if (wizardForm.valid()) {
                $('.anchor').children("li:nth-child(" + stepnumber + ")").children("a").removeClass('wait');
                displayConfirm();
                animateBar(nextstep);
                return true;
            }
        }
        ;
    };
    var validateAllSteps = function () {
        var isStepValid = true;
        // all step validation logic
        return isStepValid;
    };
    return {
        init: function () {
            initWizard();
        }
    };
}();
