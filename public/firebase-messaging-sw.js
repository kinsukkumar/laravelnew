/*
Give the service worker access to Firebase Messaging.
Note that you can only use Firebase Messaging here, other Firebase libraries are not available in the service worker.
*/
importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase-messaging.js');
//importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase-analytics.js')

/*
Initialize the Firebase app in the service worker by passing in the messagingSenderId.
* New configuration for app@pulseservice.com
*/
firebase.initializeApp({
   apiKey: "AIzaSyCrSP0vteP6gBoq7zK63ZsLczt0Z7aEmhk",
    authDomain: "shulesoft-1507541174851.firebaseapp.com",
    databaseURL: "https://shulesoft-1507541174851.firebaseio.com",
    projectId: "shulesoft-1507541174851",
    storageBucket: "shulesoft-1507541174851.appspot.com",
    messagingSenderId: "678078088465",
    appId: "1:678078088465:web:61614ed8bae019f55978b0",
    measurementId: "G-41DVG6JXC0" 
   
   
});

/*
Retrieve an instance of Firebase Messaging so that it can handle background messages.
*/
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Shulesoft';
  const notificationOptions = {
    body: 'We have New Message for you.',
    icon: 'default-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});

