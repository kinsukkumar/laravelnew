#ShuleSoft

ShuleSoft is a cloud based school management system. This system connect all school stakeholders in one platform and facilitate of different department within the school under one platform. ShuleSoft support Nursery Schools, Primary Schools, Secondary Schools and Collages. ShuleSoft provides different modules interconnected together.

cfe6972a7b4fe416a44f16211f265ced7d19c382 commit ID with forum

## Important Notices

This project has been done by incorporating Laravel and Codeigniter 2.1 features. Developer is highly recommended to use Laravel standards and syntax but Codeigniter 2.1 functions (especially database functions) can be used and are supported by on this application. 

##Routing
This application follow Codeigniter 2.1 routing process, ie controller/method . This is the default for most requests but a developer is also allowed to define Laravel routes or resource routes

#models
This project recommend the use of Laravel default models. All models are placed in /app/model/... folder. Codeigniter models are also supported and are placed in app/models/... folder(though we are not recommending these models anymore and we will remove them slowly). To use Codeigniter models, just follow the normal CI methods or autoload them in Controller.php class

--Model Generators: Due to some unknown errors occurred in auto model generator, the file located in Vendor/Xethron/laravel-4-generators/src/Way/Generators/Filesystem/Filesystem.php , line 20 has been modified to detect if user.php file has been created or not. In this way, User.php Model will not be generated as its skipped here      

return preg_match('/user/', $file) ? '': file_put_contents($file, $content);

#Language Files
This project use separate language folders to defining languages and each module can use one or more language files which are defined in module controller.


##Git commands
How to merge local new branch with a master branch

git checkout master
git pull origin master
git merge test
git push origin master



## License

ShuleSoft is commercially owned by Inets Company Limited and is distributed under subscription model

## ShuleSoft basic configurations

How to copy public key 
cat ~/.ssh/id_rsa.pub 

- ** If you want to setup shulesoft in your pc, follow these steps
1. pull from remote repository
2. composer update/install (based on your configurations)
3. Add these file manually and ignore it in git commands gitignore 
   a) App/Http/Middleware/RedirectIfNotAunthenticated.php
   b) App/Helper.php
4. In the above two files, find a method called CreateRoute(). Change parameters based on folder location
   where you put your project. 
  for example, in live server, use route 1, and route 2 (as default from server)
  in local computer, user route 2 and 3 instead if you place your project in certain folder eg. htdocs/shulesoft


#remove folder from git commit
git rm -r --cached node_modules
git commit -m 'Remove the now ignored directory node_modules'
git push origin master

#attendance device configuration
in school, choose one computer, with good specifications

