<?php

/* List Language  */
$lang['panel_title'] = "Balance";
$lang['select_class_level']='Select class level';
$lang['select_fee']='Select fee';
$lang['fee']='Fee';
$lang['class_level']='Class level';
$lang['slno'] = "#";
$lang['balance_classesID'] = "Class";
$lang['balance_select_classes'] = "Select Class";
$lang['balance_all_students'] = 'All Student';
$lang['balance_photo'] = "Photo";
$lang['balance_name'] = "Name";
$lang['balance_roll'] = "Roll";
$lang['balance_phone'] = "Phone";
$lang['select_year']='Select Year';
$lang['select_class_level']='Select Class Level';
$lang['balance_totalbalance'] = "Total Balance";
$lang['class_numeric']='Class Number';
$lang['academic_year'] = 'Academic Year';
$lang['fee_name'] = 'Fee Name';
$lang['number_student'] = 'Number of Student';
$lang['class_name'] = 'Class Name';
$lang['invoice_notpaid'] = 'Not Paid';
$lang['invoice_pending_approved'] = 'Invoice Pending Approved';
$lang['invoice_partially_paid'] = 'Partially Paid';
$lang['invoice_fully_paid'] = 'Fully Paid';
$lang['invoice_status'] = 'Invoice Status';
$lang['paid_amount'] = 'Paid Amount';
$lang['remained_amount'] = 'Remained Amount';
$lang['over_amount'] = 'Current Advance Amount';
$lang['to_bepaid'] = 'To be Paid';
$lang['action'] = 'Action';
return $lang;