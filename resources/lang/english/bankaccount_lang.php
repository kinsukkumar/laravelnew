<?php

/**
 * Description of bankaccount_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
/* List Language  */
$lang['panel_title'] = "Bank Account Details";
$lang['add_title'] = "Add New Bank Account";
$lang['edit_title'] = "Edit  Bank Account";
$lang['slno'] = "#";
$lang['branch']='Branch of your bank';
$lang['bank_account']='Bank account name';
$lang['e.g NMB']='e.g NMB';
$lang['account_number']='e.g 011238857653';
$lang['optional']='Optional Swift Code for bank to bank transfer';
$lang['menu_bankaccount']='Bank Account';
$lang['bankaccount_name'] = "Bank";
$lang['bankaccount_branch'] = "Branch";
$lang['bankaccount_account'] = "Account Number";
$lang['bankaccount_amount_name'] = "Account Name";
$lang['bankaccount_amount'] = "Account Number";
$lang['bankaccount_currency'] = "Currency";
$lang['bankaccount_swiftcode'] = "Swiftcode";
$lang['bankaccount_note'] = "Note";
$lang['action'] = "Action";
$lang['settings'] = "Settings";


$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['submit_bankaccount'] = 'Save';
$lang['add_bankaccount'] = 'Add Bank Account';
$lang['update_bankaccount'] = 'Update Bank Account';
$lang['bankaccount_select_currency'] = 'Select Currency';

$lang['opening_balance']='Opening Balance';

/*New Accounts Menu*/
$lang['banking'] = 'Banking';
$lang['opening_balance'] = 'Opening Balance';
$lang['opening_balance_placeholder'] = 'Opening Balance';
$lang['account_name_eg'] = 'e.g ShuleSoft School Management';
$lang['select_bank'] = 'Select Bank';
$lang['select_account_number']='Select Bank Account';

return $lang;