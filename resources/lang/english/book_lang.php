<?php

/* List Language  */
$lang['panel_title'] = "Books";
$lang['add_title'] = "Add New book";
$lang['slno'] = "#";
$lang['from'] = "From";
$lang['to'] = "To";
$lang['category'] = "Category";
$lang['subject'] = "Subject";
$lang['subject_bID'] = "Book code";
$lang['book_name'] = "Name";

$lang['book_serialno'] = "Serial No.";
$lang['book_subject_code'] = "Subject Code";
$lang['no_books']='Number of books distribution in your school';
$lang['book_author'] = "Author";
$lang['book_label'] = "Book Label";
$lang['Normal Book']='Normal Book';
$lang['Repairable Books']='Repairable Book';
$lang['due_date']='Due date';
$lang['Discard Books']='Discard Books';
$lang['library_books']='Library Books';
$lang['fields_marked']='Fields_marked';
$lang['mandatory']='are mandatory';
$lang['borrowed_books']='Borrowed books';
$lang['book_serial']='Book serial No as appears on the book';
$lang['book_title']='Book Title';
$lang['borrowed_books']='Borrowed books';
$lang['total_number']='Total Number';

$lang['available_books']='Available books';
$lang['repairable']='Repairable';
$lang['discarded_books']='Discarded Books';
$lang['subjects']='Subjects';
$lang['upload_book']='Upload Books From Excel file';
$land['sample_book']=' sample file here';
$lang['download']='Download';
$lang['submit']='Submit';
$lang['book_class'] =$lang['class'] = "Class";
$lang['book_subject'] = "Subject";
$lang['book_price'] = "Price";
$lang['book_edition'] = "Edition";
$lang['book_for'] = "Book For";
$lang['book_quantity'] = "Quantity";
$lang['book_rack_no'] = "Rack No";
$lang['book_status'] = "Status";
$lang['book_select_class'] = "Select Class";
$lang['book_available'] = "Available";
$lang['book_unavailable'] = "Unavailable";

$lang['action'] = "Action";
$lang['return_date']="Return date";
$lang['due_date']="Due Date";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['general_report'] = 'General Report';
/* Add Language */

$lang['add_book'] = 'Add Book';
$lang['update_book'] = 'Update Book';
$lang['book_condition'] = 'Book status';
$lang['book_edit'] = 'change status';
return $lang;