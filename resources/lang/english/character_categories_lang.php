<?php

/* List Language  */
$lang['panel_title'] = "Character Category";
$lang['add_title'] = "Add a Category";
$lang['action'] = "Action";
$lang['edit_character_categories'] = 'Edit Character Category';
$lang['menu_character_categories'] = 'Character categories';
$lang['subject_based'] = 'Assessment Mode';
$lang['menu_edit'] = "Edit";
$lang['slno'] = '#';
$lang['character_category'] = 'Character Category';
$lang['both_effort_achievement'] = 'Both Achievement and Effort';
$lang['achievement_only'] = 'Achievement Only';
$lang['category_message'] = 'Achievement Only means characters of this category will be assessed per the Achievement (level) while for Both Achievement and Effort means the characters will be assessed as per student effort and achievement';
$lang['message_heading'] = 'How to define Categories';
$lang['position']='Position';

$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_character_categories'] = 'Add Character Category';
$lang['update_character_categories'] = 'Update Character Category';
return $lang;