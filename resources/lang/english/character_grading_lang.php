<?php

/* List Language  */
$lang['panel_title'] = "Character Grade";
$lang['add_title'] = "Add a Character Grade";
$lang['action'] = "Action";
$lang['slno'] = '#';
$lang['grade'] = 'Grade';
$lang['grade_remark'] = 'Remark';
$lang['description'] = 'Description';

$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['edit_character_grade'] = 'Edit Character Grade';
$lang['add_grade'] = 'Add Character Grade';


/* Add Language */

$lang['add_character'] = 'Add Character';
$lang['update_character'] = 'Update Character';
return $lang;