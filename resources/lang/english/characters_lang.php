<?php

/* List Language  */
$lang['panel_title'] = "Character";
$lang['add_title'] = "Add a Character";
$lang['action'] = "Action";
$lang['slno'] = '#';
$lang['character_category'] ='Category';
$lang['select_category'] ='Select category';
$lang['auto_generate'] ='Auto generate';
$lang['character_code'] = 'Code';
$lang['character'] = 'Character';
$lang['list_character'] = 'List of Characters';
$lang['position']='Position';


$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['edit_character'] = 'Edit Character';


/* Add Language */

$lang['add_character'] = 'Add Character';
$lang['update_character'] = 'Update Character';
return $lang;