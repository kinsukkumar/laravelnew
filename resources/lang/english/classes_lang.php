<?php

/* List Language  */
$lang['panel_title'] = "Class";
$lang['add_title'] = "Add a class";
$lang['slno'] = "#";
$lang['classes_name'] = "Class";
$lang['classes_numeric'] = "Class Numeric";
$lang['teacher_name'] = "Teacher Name";
$lang['classes_note'] = "Note";
$lang['action'] = "Action";
$lang['add_exam'] = "Add Exam";

$lang['classes_select_teacher'] = "Select Teacher";

$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['fields_marked']='Fields marked';
$lang['are_mandatory']='are mandatory';
$lang['class_info']='Class Information';
$lang['class_eg']='Number for this class eg 1';
$lang['classes_eg']='Class eg class one';

/* Add Language */

$lang['add_class'] = 'Add Class';
$lang['update_class'] = 'Update Class';
$lang['classlevel'] = 'Class Level';
$lang['academic_year']='Academic Year';
$lang['exam_group']='Exam Group';

$lang['add_exam_group']='Add Exam Group';
$lang['weight']='Weight';
$lang['exam_group_name']='Exam Group Name';
$lang['exam_group_weight']='Exam Group Weight';
$lang['add_new_exam_group']='Add New Exam Group';
return $lang;