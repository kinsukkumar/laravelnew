<?php
$lang['panel_title'] = "Dashboard";
$lang['dashboard_notice'] = "Notice";
$lang['no_notice'] = "There are no notices!";
$lang['dashboard_username'] = "Username";
$lang['dashboard_email'] = "Email";
$lang['dashboard_phone'] = "Phone";
$lang['dashboard_address'] = "Address";
$lang['dashboard_libraryfee'] = "Library Fee";
$lang['dashboard_transportfee'] = 'Transport Fee';
$lang['dashboard_hostelfee'] = 'Hostel Fee';
$lang['total_teachers']='Total teachers';
$lang['total_parents']='Total parents';
$lang['total_students']='Total students';
$lang['books']='Books';
$lang['invoices']='Invoices';
$lang['total_payment']='Total Payments';
$lang['add_payment']='Add Payment';
$lang['nonteaching_staff']='Non teaching staff';
$lang['welcome']='Welcome';
$lang['dashboard_subject_avg']='Subjects Performance';

$lang['dashboard_earning_graph'] = "Earning Graph";
$lang['dashboard_notpaid'] = "Not Paid";
$lang['dashboard_partially_paid'] = "Partially Paid";
$lang['dashboard_fully_paid'] = "Fully Paid";
$lang['dashboard_cash'] = "Cash";
$lang['dashboard_cheque'] = "Cheque";
$lang['dashboard_paypal'] = "PayPal";
$lang['dashboard_stripe'] = "Stripe";
$lang['dashboard_sample'] = "Sample";
$lang['view'] = "View";
$lang['calendar_description'] = "School Events, Exams and Meetings";
$lang['calendar'] = "Calendar";
$lang['select_subject'] = "Select Subject";
$lang['parent_subject_avg_graph']='Subject Average Performance for ';
$lang['parent_subject_avg_graph_subtitle']='Each Subject Average';
$lang['class_student'] = "Class Teacher Student";


$lang['getting_started']='Getting Started';
$lang['how_title'] ='How to View Your Child Reports';
$lang['how_description'] ='With Shulesoft you can view different kind of reports to measure and evaluate your child performance';
 $lang['how_available_reports'] ='Available Reports';
$lang['how_overall'] ='Overall average of each subject done by your child';
$lang['how_single'] ='Single Exam Reports (Quantitative)';
$lang['how_per_all'] ='Overall Performance per Class, Per Section/Stream';
$lang['how_per_subject'] ='Subject Performance per Class, Per Section/Stream';
$lang['how_single_graphical'] ='Single Exam Reports (Graphical results)';
$lang['how_accumulative'] ='Accumulative reports (quantitative combine more than one exam)';
$lang['how_accumulative_graphical'] ='Accumulative reports (graphically combine more than one exam)';
$lang['view_all_reports']='View all exams here';


$lang['performance'] = "Performance";
$lang['best_student'] = "Best top  Students By Average";
$lang['student_name'] = "Student Name";
$lang['overall'] = "Overall Average";
$lang['student_class'] = "Student Class";
$lang['student_avg'] = "Average";
$lang['student_last'] = "Last  Students By Average";
$lang['class_level'] = "Select Student Class Level";
$lang['level'] = "Select Class Level";

$lang['teacher_name'] = "Teacher Name";

$lang['reference'] = "Reference";
$lang['action'] = "Action";
$lang['teacher_top'] = "Best top 5 Teachers By Average";



return $lang;