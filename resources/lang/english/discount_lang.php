<?php

/* List Language  */
$lang['panel_title'] = "Fees Discount";
$lang['panelcat_title'] = "Fee Category Section";
$lang['discount']='Discount for fees/contributions';
$lang['add_discount']='Add discount';
$lang['class_level']='Class level';
$lang['select_class_level']='Select class level';
$lang['select_fee']='Select Fee';
$lang['register_discount']='Register discount';
$lang['submit_discount']='Submit discount';
$lang['fee_name']='Fee name';
$lang['date']='Date';
$lang['description_note']='Description note';
$lang['class']='Class';
$lang['add_title'] = "Add a Fee Type";
$lang['GUIDANCE']='GUIDANCE';
$lang['select_class']='Select class';
$lang['select_student']='Select student';
$lang['fee']='Fee';
$lang['slno'] = "#";
$lang['feetype_name'] = "Fee Name";
$lang['ensure']='Ensure you have defined fees and fee details for a defined installment, academic year, installment and account number.';
$lang['discount_amount'] = "Discount Amount";
$lang['add_discount']='Add discount';
$lang['feetype_amount'] = "Amount";
$lang['feetype_is_repeative'] = "Is Repeative";
$lang['feetype_is_repeative_yes'] = "Yes";
$lang['exam_select_year']='Select Year';
$lang['feetype_is_repeative_no'] = "No";
$lang['feetype_startdate'] = "Start Date";
$lang['feetype_enddate'] = "End Date";
$lang['discount_amount'] = "Discount Amount";
$lang['feetype_note'] = "Note";
$lang['action'] = "Action";
$lang['feetypecategory']="Fee Category";
$lang['feetpype_select_category']="feetype select category";
$lang['category_title']="Add feetype Category";
$lang['category_fee']="feetype Category";
$lang['category_note']="Category Note";
$lang['feetype_category']="Feetype Category";
$lang['add_category']="Add Category";
$lang['select_feetype_category']="select feetype category";
$lang['menu_feediscount']="Fee Discount";


$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['submit'] = 'Save';
$lang['upload_inst_by_excel'] = 'Upload Installment by Excel';

/* Add Language */
$lang['add_feetype'] = 'Add Fee';
$lang['update_feetype'] = 'Update Fee';
$lang['student_name'] = 'Student Name';
$lang['feetype_account'] = 'Fee Type Account';
$lang['feetpype_select_account'] = 'Select account number';
$lang['invoice_classesID'] = "Class";
$lang['invoice_date'] = "Date";

$lang['discount_note'] = "Note";
return $lang;