<?php

/* List Language  */
$lang['panel_title'] = "e-Diary";
$lang['add_title'] = "Add";
$lang['slno'] = "#";
$lang['category_hname'] = "Hostel Name";
$lang['school_hostel_category']='School hostel category';
$lang['fields']='Fields marked';
$lang['mandatory']='are mandatory';
$lang['hostel_type'] = "Hostel Type";
$lang['category_hbalance'] = "Hostel Fee";
$lang['category_note'] = "Note";

$lang['category_select_hostel'] = "Select Hostel";

$lang['action'] = "Action";
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_category'] = 'Add Hostel Category';
$lang['update_category'] = 'Update Hostel Category';
return $lang;