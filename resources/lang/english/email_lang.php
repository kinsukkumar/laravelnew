<?php

/**
 * Description of email_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Joshua John
 */

$schema=set_schema_name()=='public.' ? 'demo.' : set_schema_name();
/*
 * Student information
 */
//$lang['parent_registered_student']="Hello %s,Your child "
//        . "%s has been registered at %s . Now you can Login at ".$schema."shulesoft.com at anytime to access your child academic information.";
//$lang['parent_edit_student']="Hello %s, the details of your child %s"
//        . " has been modified in ".$schema."shulesoft.com. You can login at anytime to verify if they are correct" ;
//$lang['parent_deleted_student']="Hello %s, Your child %student_name has been removed from %s .  ";
//
///*
// * Student information
// */
//$lang['parent_registered_student_sw']="Habari %s,Mwanao "
//        . "%s amesajiliwa %s . Sasa unaweza kuingia ".$schema."shulesoft.com muda wowote kufuatilia taarifa za kitaaluma za mwanao.";
//$lang['parent_edit_student_sw']="Habari %s, Taarifa za mwanao %s"
//        . " zimebadilishwa katika ".$schema."shulesoft.com. Unaweza kuingia muda wowote katika ShuleSoft kuhakiki" ;
//$lang['parent_deleted_student_sw']="Habari %s, mwanao %student_name ameondolewa katika mfumo %s .  ";
//
//
//
///*
// * Parent information
// */
//$lang['add_new_parent']="Hello %s, You have been added at %s ShuleSoft system (".$schema."shulesoft.com) with default username: %s and default password: %s . Please remember to change this password once you are logged in"
//        . ". Now you can log into the system and monitor your child academics";
//$lang['add_new_parent_sw']="Habari %s, Umeunganishwa katika mfumo wa ShuleSoft wa %s (".$schema."shulesoft.com), ingia kwa nenotumizi: %s na nenosiri: %s . Tafadhali kumbuka kubadili nenosiri pindi utakapoingia."
//        . ". Sasa unaweza kuingia na kufuatilia taarifa za kitaaluma za mwanao";
//
//$lang['delete_parent']="Hello %s, You have been removed from %s (".$schema."shulesoft.com) system, Now you do not have any access at %s";
//
///*
// * Teacher information
// */
//$lang['add_new_teacher']="Hello %s, you have been added as a Teacher in %s  ShuleSoft system (".$schema."shulesoft.com) with default username: %s and default password: %s "
//    . "for you to log into the system and add students marks and more. Please remember to change this password once you are logged in";
//$lang['edit_teacher']="Hello %s, your account details in ShuleSoft (".$schema."shulesoft.com) have been modified, Login at anytime to verify their correctness";
//
//
//$lang['edit_parent']="Hello %s, your account details in ShuleSoft (".$schema."shulesoft.com) have been modified, Login at anytime to verify their correctness";
//
//$lang['delete_teacher']="Hello %s, you have been removed from %s (".$schema."shulesoft.com) system, Now you do not have any access at %s";
//
///*
// * User information
// */
//$lang['add_new_user']="Hello %s, you have been added as a(n) %s at %s ShuleSoft System(".$schema."shulesoft.com) with default username: %s and default password: %s "
//    . "for you to log into the system and manage your activities. Please remember to change this password once you are logged in";
//$lang['edit_user']="Hello %s, your details have been modified, Login at ".$schema."shulesoft.com anytime to verify changes";
//$lang['delete_user']="Hello %s, you have been removed from %s (".$schema."shulesoft.com) system, Now you do not have any access at %s";
//
///*
// * Class information
// */
//$lang['add_new_class']="Hello %s, you have been added as %s Class Teacher. Your signature and general remarks for students will appear in this class students' academic reports. For more information,  visit ".$schema."shulesoft.com";
//$lang['edit_class']="Hello %s, your class details have been modified as %s of %s . For more information visit ".$schema."shulesoft.com";
//$lang['delete_class']="Hello %s, you are no longer a class teacher of %s because it has been deleted. For more information visit ".$schema."shulesoft.com";
//
//
//
///*
// * Section information
// */
//$lang['add_new_section']="Hello %s ,you have been added as %s of section %s in %s and class teacher is %s. For more information visit ".$schema."shulesoft.com";
//$lang['edit_section']="Hello %s ,section %s in class: %s has been modified as %s of class %s and class teacher is %s. For more information visit ".$schema."shulesoft.com";
//$lang['delete_section']="Hello %s,this section: %s has been deleted,So now you are no longer a teacher of section  %s. For more information visit ".$schema."shulesoft.com";
//$lang['class_teacher_notify_edit']="Hello %s ,section %s in your class %s has been modified as %s and section teacher is %s. For more information visit ".$schema."shulesoft.com";
//$lang['class_teacher_section_deletion']="Hello %s ,section %s has been deleted in your class %s. For more information visit ".$schema."shulesoft.com";
//
///*
// * Subject information
// */
//$lang['add_new_subject']="Hello %s, you have been assigned as a teacher of %s. For more information visit ".$schema."shulesoft.com";
//
//
//$lang['edit_subject']="Hello %s ,subject %s has been modified. For more information visit ".$schema."shulesoft.com";
//$lang['delete_subject']="Hello %s ,subject %s has been deleted. For more information visit ".$schema."shulesoft.com";
//
///*
// * Grade information
// */
//
//$lang['add_new_grade']="Hello %s ,new grade has been added ,Please login to %s to view all grades";
//$lang['edit_grade']="Hello %s ,grade has been modified, please login to %s to view modifications";
//$lang['delete_grade']="Hello %s ,Grade has been deleted ";
//
//
///*
// * Exam information
// */
//$lang['add_new_exam']="Hello %s ,%s new exam has been added";
//$lang['edit_exam']="Hello %s ,%s exam has been modified, so you can login to see changes";
//$lang['delete_exam']="Hello %s, %s has been deleted";
//
//
///*
// * Exam Schedule information
// */
//$lang['add_new_exam_schedule']="Hello %s ,new exam schedule has been added";
//$lang['edit_exam_schedule']="Hello %s ,exam schedule has been modified, so you can login to see changes";
//$lang['delete_exam_schedule']="Hello %s, exam schedule has been deleted";
//
//
///*
// * Routine information
// */
//$lang['add_new_routine']="Hello %s ,new class routine has been added";
//$lang['edit_routine']="Hello %s ,class routine has been modified, so you can login to see changes";
//$lang['delete_routine']="Hello %s, class routine has been deleted";
//
///*
// * character assessment
// */
//$lang['assign_character_to_teacher']="Hello %s, you have been assigned to assess character under name %s. For more information visit ".$schema."shulesoft.com";
//
///**
// * Register Install system
// */
//$lang['install_success']='';
//
//// Revenue
//$lang['add_revenue']='Hello %s, your payment amount of %s has been accepted by %s. Your receipt number is %s. ';
//return $lang;

/**
 * Description of email_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Joshua John
 */

/*
 * Student information
 */
$lang['new_user_registration']='Habari %s, tunapenda kukutaarifu kuwa %s tumejiunga na mfumo wa ShuleSoft ambao utatoa urahisi katika uendeshaji wa shule na kuunganisha shule na wazazi kupata taarifa za watoto wao kwa urahisi. Kwa mawasiliano yoyote na Shule yetu piga %s, kuwasiliana na wataalam wa ShuleSoft tumia 0655406004.

Ahsante';


$lang['parent_new_registration']='Habari %s, tunapenda kukutaarifu kuwa %s tumejiunga na mfumo wa ShuleSoft ambao utakuwezesha mzazi kupata taarifa mbalimbali za mwanao kupitia ShuleSoft kama matokeo, michango, nk . Kwa mawasiliano yoyote na Shule yetu piga %s, kuwasiliana na wataalam wa ShuleSoft tumia 0655406004.

Ahsante';
$lang['parent_admission_accept_student']="Habari %s, Tunafurahi kukutaarifu kuwa maombi yako ya mwanafunzi %s kujiunga na shule ya   "
        . "%s (".$schema."shulesoft.com) yamekubaliwa. Kumbuka kufanya maandalizi kwa ajili ya mtoto wako kuja kujiunga shuleni. Kwa mawasiliano zaidi tupigie kupitia %s.";

$lang['parent_admission_reject_student']="Habari %s, Tunasikitika kukutaarifu kuwa maombi yako ya mwanafunzi %s kujiunga na shule ya   "
        . "%s (".$schema."shulesoft.com) hayajakubalika kutokana na vigezo mbalimbali tulivyoweka kwenye shule yetu.  Kwa mawasiliano zaidi tupigie kupitia %s.";

$lang['parent_registered_student']="Habari %s,Mwanao "
        . "%s amesajiliwa katika program ya ShuleSoft %s . Sasa unaweza kuingia ".$schema."shulesoft.com muda wowote kufuatilia taarifa za kitaaluma za mwanao.";
$lang['parent_edit_student']="Habari %s, Taarifa za mwanao %s"
        . " zimebadilishwa ".$schema."shulesoft.com. Unaweza kuingia muda wowote kuhakiki" ;
$lang['parent_deleted_student']="Habari %s, mwanao %student_name ameondolewa katika mfumo %s .  ";


/*
 * Parent information
 */
$lang['admission_new_parent']="Habari %s, Maombi yako kujiunga na shule yetu yametufikia na tunayafanyia kazi. Pia Umeunganishwa katika mfumo wetu wa ShuleSoft wa %s (".$schema."shulesoft.com), ingia kwa nenotumizi: %s na nenosiri: %s . Sasa utaweza kuingia kufuatilia maombi yako. Tafadhali kumbuka kubadili nenosiri pindi utakapoingia.";

$lang['add_new_parent']="Habari %s, Umeunganishwa katika mfumo wa ShuleSoft wa %s (".$schema."shulesoft.com), ingia kwa nenotumizi: %s na nenosiri: %s . Tafadhali kumbuka kubadili nenosiri pindi utakapoingia."
        . ". Sasa utaweza kuingia na kufuatilia taarifa za kitaaluma za mwanao punde zitakapowekwa";

$lang['login_credentials']="Habari #name, Ili uweze kuingia katika system ya ShuleSoft, fungua link hii (".$schema."shulesoft.com) , nenotumizi lako ni: #username na nenosiri la kuanzia ni : #default_password . Tafadhali kumbuka kubadili nenosiri pindi utakapoingia. Asante";

$lang['delete_parent']="Habari %s, Umeondolewa katika mfumo wa ShuleSoft wa %s (".$schema."shulesoft.com), Hivyo hutaweza tena kuingia katika mfumo huu.";

/*
 * Teacher information
 */
$lang['add_new_teacher']="Habari %s, Umewekwa kama mwalimu katika mfumo wa ShuleSoft wa %s (".$schema."shulesoft.com) na nenotumizi: %s na nenosiri la kuanzia: %s "
    . "na utaweza kuingia na kurekodi maksi za wanafunzi nakadhalika. Tafadhali kumbuka kubadili nenosiri pindi utakapoingia katika mfumo.";
$lang['edit_teacher']="Habari %s, taarifa zako katika ShuleSoft (".$schema."shulesoft.com) zimebadilika, tafadhali ingia kuhakiki.";


$lang['edit_parent']="Habari %s, taarifa zako katika mfumo wa ShuleSoft (".$schema."shulesoft.com) zimebadilika, tafadhali ingia kuhakiki.";

$lang['delete_teacher']="Habari %s, Umeondolewa katika mfumo wa ShuleSoft wa %s (".$schema."shulesoft.com), Hivyo hutaweza tena kuingia katika mfumo huu.";

/*
 * User information
 */
$lang['add_new_user']="Habari %s, umewekwa kama %s katika mfumo wa ShuleSoft wa %s (".$schema."shulesoft.com) na nenotumizi: %s na nenosiri: %s . Tafadhali kumbuka kubadili nenosiri pindi utakapoingia.";
$lang['edit_user']="Habari %s, taarifa zako katika ShuleSoft (".$schema."shulesoft.com) zimebadilika, tafadhali ingia kuhakiki.";
$lang['delete_user']="Habari %s, Umeondolewa katika mfumo wa ShuleSoft wa %s (".$schema."shulesoft.com), Hivyo hutaweza tena kuingia katika mfumo huu.";

/*
 * Class information
 */
$lang['add_new_class']="Habari %s, umewekwa kama mwalimu wa darasa la %s. Sahihi yako itaonekana katika ripoti za wanafunzi pindi mfumo utakapotengeneza.";
$lang['edit_class']="Habari %s, sasa umewekwa kama %s wa %s . Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['delete_class']="Habari %s,darasa: %s limeondolewa katika Mfumo,hivyo wewe siyo tena mwalimu wa %s. Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";



/*
 * Section information
 */
$lang['add_new_section']="Habari %s ,umewekwa kama %s wa mkondo %s katika darasa:- %s na mwalimu wa darasa ni %s. Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['edit_section']="Habari %s ,mkondo %s wa: %s umekua %s wa: %s na mwalimu wa darasa ni %s. Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['delete_section']="Habari %s,mkondo: %s umeondolewa katika Mfumo, hivyo wewe sio mwalimu wa %s. Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['class_teacher_notify_edit']="Habari %s ,mkaondo: %s wa %s umebadilishwa na kuwa %s na mwalimu wa mkondo ni %s. Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['class_teacher_section_deletion']="Habari %s ,section %s has been deleted in your class %s. Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";

/*
 * Subject information
 */
$lang['add_new_subject']="Habari %s, umewekwa kuwa mwalimu wa %s. Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['edit_subject']="Habari %s , taarifa za somo la %s zimebadilishwa. Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi na kuhakiki";
$lang['delete_subject']="Habari %s ,somo la %s limeondolewa katika mfumo wa ShuleSoft. Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";

/*
 * Grade information
 */

$lang['add_new_grade']="Habari %s ,daraja jipya la mitihani limeongezwa ,Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kuhakiki";
$lang['edit_grade']="Habari %s ,grade has been modified, please login to %s to view modifications";
$lang['delete_grade']="Habari %s ,Grade has been deleted ";


/*
 * Exam information
 */
$lang['add_new_exam']="Habari %s ,mtihani  wa %s  umeongezwa katika mfumo wa ShuleSoft";
$lang['edit_exam']="Habari %s , taarifa za mtihani  wa %s zimebadilishwa, tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['delete_exam']="Habari %s,mtihani wa %s umeondolewa katika mfumo wa ShuleSoft";


/*
 * Exam Schedule information
 */
$lang['add_new_exam_schedule']="Habari %s ,ratiba mpya ya mitihani imewekwa katika mfumo wa ShuleSoft";
$lang['edit_exam_schedule']="Habari %s ,ratiba ya mitihani imerekebishwa, tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['delete_exam_schedule']="Habari %s, ratiba ya mitihani imefutwa katika mfumo wa ShuleSoft";


/*
 * Routine information
 */
$lang['add_new_routine']="Habari %s ,ratiba mpya ya darasa imeongezwa katika mfumo wa ShuleSoft";
$lang['edit_routine']="Habari %s ,ratiba ya darasa imebadilishwa katika mfumo wa ShuleSoft";
$lang['delete_routine']="Habari %s, ratiba ya darasa imefutwa katika mfumo wa ShuleSoft";
/*
 * character assessment
 */
$lang['assign_character_to_teacher']="Habari %s, umewekwa kushughulikia tabia na mienendo eneo la %s. Tafadhali tembelea ".$schema."shulesoft.com kisha ingia ndani kufahamu zaidi";

// Revenue
$lang['add_revenue']='Habari %s. Malipo yako ya %s yamepokelewa  %s. Namba yako ya risiti ni %s. ';


$lang['parent_registered_student_jifunze']="Habari %s,Mwanao "
        . "%s amesajiliwa katika program ya ShuleSoft %s . Unaweza kuingia ".$schema."shulesoft.com muda wowote kufuatilia mafunzo yote ya mwanao.";

$lang['parent_registered_student_jifunze']="Habari %s, Hongera kwa kujiunga na Mfumo wa Kujifunza kwa njia ya Mtandao kupitia ShuleSoft. Kwa Mwanafunzi %s kuingia tumia username %s na password %s.  Kwa mzazi kuingia tumia username %s na password %s, kwa msaada wasiliana nasi. Karibu";

$lang['school_registered_student_jifunze']="Habari %s, mwanafunzi %s wa shule yako %s amejiunga na mfumo wa ShuleSoft ili kuanza kujifunza kwa wa njia ya Mtandao. Kama ungependa kuunganisha wanafunzi wote shuleni, usisite kuwasiliana nasi. Karibu";

$lang['add_new_teacher_jifunze']="Habari %s, Umewekwa kama mwalimu katika mfumo wa ShuleSoft wa %s (".$schema."shulesoft.com) na nenotumizi: %s na nenosiri la kuanzia: %s "
    . "na utaweza kuingia na kuweka notes, vitabu, past papers, na pia kufundisha wanafunzi live. Utapigiwa simu pia kupewa ufafanuzi zaidi  na kuandaa ratiba ya vipindi. Tafadhali kumbuka kubadili nenosiri pindi utakapoingia katika mfumo.";
return $lang;
