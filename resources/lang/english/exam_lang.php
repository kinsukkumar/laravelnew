<?php

/* List Language  */
$lang['panel_title'] = "Exam";
$lang['add_title'] = "Add a exam";
$lang['slno'] = "#";
$lang['exam_name'] = "Exam Name";
$lang['exam_date'] = "Date";
$lang['exam_class']="Class";
$lang['exam_section']="Section";
$lang['exam_select_section']="Select Section";
$lang['fields']='Fields marked';
$lang['mandatory']='are mandatory';
$lang['exam_info']='Exam information';
$lang['exam_note'] = "Note";
$lang['add_minor_exam'] = 'Add Minor School Exam';
$lang['edit_minor_exam'] = 'Edit Minor School Exam';
$lang['exam_date']='Select date of exam';
$lang['exam_select'] = "Select exam";
$lang['abbreviation']='Abbreviation';
$lang['abbreviation_placeholder']='Abbreviation of Characters e.g CT1';
$lang['exam_all'] = "All";
$lang['exam_academic_year']='Academic Year';
$lang['exam_select_semester']='Select Semester';
$lang['semester'] = "Semester";
$lang['exam_select_year']='Select Year';
$lang['set_year']='Set accademic year first';


$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_exam'] = 'Add Exam';
$lang['define_exam'] = 'Define Exam';
$lang['update_exam'] = 'Update Exam';
/* Add Language */

$lang['class_level'] = 'Class Level';
$lang['class_allocation'] = 'Class Allocation';
$lang['academic_year'] = 'Academic Year';

/* List Language  */
$lang['exam'] = "Exams";
$lang['add_exam'] = "Add Exam";
$lang['add_title'] = 'Add a mark';
$lang['slno'] = "#";
$lang['mark_exam'] = "Exam";
$lang['mark_classes'] =$lang['classes']= "Class";
$lang['view_report']='View Report';
$lang['mark_student'] = "Student";
$lang['mark_subject'] = "Subject";
$lang['mark_photo'] = "Photo";
$lang['mark_name'] = "Name";
$lang['mark_roll'] = "Roll";
$lang['mark_phone'] = "Phone";
$lang['mark_dob'] = "Date of Birth";
$lang['mark_sex'] = "Gender";
$lang['mark_religion'] = "Religion";
$lang['mark_email'] = "Email";
$lang['mark_address'] = "Address";
$lang['mark_username'] = "Username";

$lang['mark_subject'] = "Subject";
$lang['mark_mark'] = "Mark";
$lang['mark_point'] = "Point";
$lang['mark_grade'] = "Grade";


$lang['mark_select_classes'] = "Select Class";
$lang['mark_select_level'] = "Select Level";
$lang['mark_select_exam'] = "Select Exam";
$lang['mark_select_subject'] = "Select Subject";
$lang['mark_select_student'] = "Select Student";
$lang['mark_success'] = "Success";
$lang['personal_information'] = "Personal Information";
$lang['mark_information'] = "Mark Information";
$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

// /* Add Language */
$lang['add_mark'] = 'Mark';
$lang['add_sub_mark'] = 'Add Mark';

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email sent successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!';
$lang['show_division']='Show Division in a Report';
$lang['reporting_date']='Reporting Date';
$lang['division'] = "Division";

$lang['single_report_title']='STUDENT PROGRESSIVE REPORT';
$lang['child_report_title']='CHILD PROGRESSIVE REPORT';
$lang['pupil_report_title']='PUPIL PROGRESSIVE REPORT';
$lang['grade_status']='GRADE STATUS';
$lang['pos_in_class']='Position in Class';
$lang['pos_in_section']='Position in Stream';
$lang['total']='TOTAL';
$lang['avg_mark']='AVERAGE MARK';
$lang['out_of']='Out of ';

$lang['cell']='Cell';
$lang['subject_name']='Subject';
$lang['points']='POINTS';
$lang['remarks']='Remarks';
$lang['sign']='Sign';
$lang['teacher_name']='Teacher';
$lang['exam_avg']='Exam Average';
$lang['total_of']='TOTAL OF ';
$lang['grading']='GRADING';
$lang['key']='KEY';
$lang['formula']='FORMULA';
$lang['academic_remark']='Academic Remark';
$lang['character_assessment']='Character Assessment';
$lang['class_teacher']='Class Teacher';
$lang['class_teacher_signature']='Class Teacher\'s Signature';
$lang['date_of_reporting']='Date of Reporting';

/**
 * Description of exam_report_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$lang['panel_title'] = "Exam Report";
$lang['add_title'] = "Add Exam Attendance";
$lang['slno'] = "#";
$lang['class_report']='Select class to view its reports';
$lang['eattendance_photo'] = "Photo";
$lang['eattendance_name'] = "Name";
$lang['eattendance_email'] = "Email";
$lang['eattendance_roll'] = "Roll";
$lang['eattendance_phone'] = "Phone";
$lang['eattendance_attendance'] = "Attendance";
$lang['eattendance_section'] = "Section/Stream";
$lang['eattendance_exam'] = "Exam";
$lang['eattendance_classes'] = "Class";
$lang['eattendance_subject'] = "Subject";
$lang['eattendance_all_students'] = 'All Students';

$lang['eattendance_select_exam'] = "Select Exam";
$lang['eattendance_select_classes'] = "Select Class";
$lang['eattendance_select_subject'] = "Select Subject";
//Validate and Create Report
$lang['validate_report'] = "Validate & Create Report";
$lang['report_name'] = "Report Name";
$lang['close'] = "Close";
$lang['name'] = "NAME:";
$lang['admission_no'] = "Admission No:";
$lang['stream'] = "STREAM:";

$lang['action'] = "Action";
$lang['class'] = "Class";
$lang['teacher'] = "Teacher";
$lang['subject'] = "Subject";
$lang['class_teacher'] = "Class Teacher";
$lang['attendance'] = "Attendance: ";
$lang['sign'] = "Sign ";


/* Add Language */

$lang['add_attendance'] = 'Attendance';
$lang['add_all_attendance'] = 'Add All Attendance';
$lang['view_report'] = "View Report";
$lang['division']='Division';
$lang['total_point']='Total Points';
$lang['total']='Total';
$lang['view_combined_report']='View Combined Report';
$lang['add_exam_defn']='Add Exam Definition';
$lang['sample_exam_file'] = "Download Sample of Exam Definition Excel Here...";
$lang['add_exam_school'] = "Add New School Exam";
$lang['abbreviation_placeholder'] = "Abbreviation";
$lang['edit_exam_group']='Edit Exam Group';
$lang['add_minor_exam']='Add Minor School Exam';
$lang['minor_exam']='Minor School Exams';
$lang['school_exam']='School Exam Groups';
$lang['school_exams']='School Exams';
$lang['exam_abbreviation']='Exam Abbreviation';

$lang['add_exam_name']='Add an exam name to help you to identify exams.';
$lang['add_exam_initial']='Add an Initial that represent this exam. Eg, ABC ';
$lang['select_level']='Select the Levels that will take this Exam Eg. Primary.';
$lang['select_group']='Select the group where this Exam will belong. eg, Quiz';
$lang['add_text_appear']='Add the text you want to appear in the description field of exam';
$lag['school_exam_add']='To add a School Exam, Make sure you fill the Inputs as Required';
$lang['about_exam_school']='About the School Exam fields';
$lang['add_online_exam']='Add Online Exam';
$lang['number_question']='Number of Questions';
$lang['main_topic']='Main Topic';
$lang['create_topic']='Or Create a New Topic';
$lang['time_minutes']='Time in Minutes';
$lang['minor']='Minor Exam';

return $lang;
