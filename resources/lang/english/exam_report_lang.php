<?php

/**
 * Description of exam_report_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$lang['panel_title'] = "Exam Report";
$lang['add_title'] = "Add Exam Attendance";
$lang['slno'] = "#";
$lang['class_report']='Select class to view its reports';
$lang['eattendance_photo'] = "Photo";
$lang['eattendance_name'] = "Name";
$lang['eattendance_email'] = "Email";
$lang['eattendance_roll'] = "Roll";
$lang['eattendance_phone'] = "Phone";
$lang['eattendance_attendance'] = "Attendance";
$lang['eattendance_section'] = "Section/Stream";
$lang['eattendance_exam'] = "Exam";
$lang['eattendance_classes'] = "Class";
$lang['eattendance_subject'] = "Subject";
$lang['eattendance_all_students'] = 'All Students';
$lang['share'] = 'Share';


$lang['eattendance_select_exam'] = "Select Exam";
$lang['eattendance_select_classes'] = "Select Class";
$lang['eattendance_select_subject'] = "Select Subject";
//Validate and Create Report
$lang['validate_report'] = "Validate & Create Report";
$lang['report_name'] = "Report Name";
$lang['close'] = "Close";
$lang['name'] = "NAME:";
$lang['admission_no'] = "Admission No:";
$lang['stream'] = "STREAM:";

$lang['action'] = "Action";
$lang['class'] = "Class";
$lang['teacher'] = "Teacher";
$lang['subject'] = "Subject";
$lang['class_teacher'] = "Class Teacher";
$lang['attendance'] = "Attendance: ";
$lang['sign'] = "Sign ";


/* Add Language */

$lang['add_attendance'] = 'Attendance';
$lang['add_all_attendance'] = 'Add All Attendance';
$lang['view_report'] = "View Report";
$lang['division']='Division';
$lang['total_point']='Total Points';
$lang['total']='Total';
$lang['view_combined_report']='View Combined Report';
$lang['signature']='Signature';
$lang['gender']='Gender';
return $lang;