<?php

/**
 * Description of examreport_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */


/* List Language  */
$lang['panel_title'] = "Exams Official Report";
$lang['add_title'] = "Add a exam";
$lang['slno'] = "#";
$lang['examreport_name'] = "Report Name";
$lang['examreport_date'] = "Created Date";
$lang['exam_class']="Class";
$lang['examreport_section']="Exam Official Report";
$lang['examreport_select_classes']="Select Class";
$lang['examreport_exams_combined'] = "Exams Combined";
$lang['exam_all'] = "All";
$lang['share'] = 'Share';

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_exam'] = 'Add Exam';
$lang['update_exam'] = 'Update Exam';
$lang['subject_classes']='Select Class';
return $lang;