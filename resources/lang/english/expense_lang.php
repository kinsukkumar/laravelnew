<?php

/* List Language  */
$lang['panel_title'] = "School Expenses";
$lang['panel_category_title'] = "Charts of Accounts";
$lang['add_title'] = "Add Transaction";
$lang['add_category'] = "Add Expense Category";
$lang['slno'] = "#";
$lang['expense_expense'] = "Name";
$lang['expense_date'] = "Date";
$lang['depreciation'] = "Depreciation";
$lang['expense_amount'] = "Amount";
$lang['expense_code'] = "Code";
$lang['expense_note'] = "Descriptions";
$lang['expense_uname'] = "User";
$lang['expense_total'] = "Total";
$lang['action'] = "Action";
$lang['balance_sheet_report'] = "Balance sheet";
$lang['profit_loss_report'] = "Income Statement";
$lang['account_report'] = "Account Report";
$lang['category'] = "Category";
$lang['phone'] = "Phone";
$lang['email'] = "Email";
$lang['ref_no'] = "Reference No.";
$lang['payment_method'] = "Payment Method";
$lang['account_code']='Account Code';
$lang['add_account']='Add Account';
$lang['submit_account']='Save';
$lang['update_account']='Save';
$lang['account_type']='Account Type';
$lang['account_name']='Account Name';
$lang['upload_inst_by_excel']='Upload by Excel';
$lang['sample_installment_file']='Sample Installment File';

// $lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_expense'] = 'Transactions';
$lang['add_expenses'] = 'Add Expense';
$lang['category'] = 'Category';
$lang['expense'] = 'Name';
$lang['add_category'] = 'Add Category';
$lang['edit_category'] = 'Edit Category';
$lang['update_category'] = 'Update Category';
$lang['subcategory'] = 'Name';
$lang['update_expense'] = 'Update Expense';
$lang['select_expense'] = 'Select Name';
$lang['open_balance'] = 'Enter Open Balance';
$lang['account_group'] = 'Account Group';
$lang['select_group'] = 'Select Group';
$lang['add_group'] = 'Add Group';
$lang['action'] = 'Action';
$lang['group_name'] = 'Group Name';
$lang['group_note'] = 'Group Note';
$lang['group'] = 'Custom Groups';
$lang['sum'] = 'Sum';
$lang['code']='Code';


$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['submit'] = 'Submit';
$lang['open_balance'] = 'Open Balance';
$lang['payment_method']='Payment Method';
$lang['ref_no']='Reference No.';
$lang['save']='Save';
$lang['userform']='User Form';
$lang['userin']='User in ShuleSoft';
$lang['usernot']='User Not in ShuleSoft';
$lang['usertype']='Select User Type';
$lang['sample_expense_file']='Sample Expense File';

$lang['requested_by'] = 'Requested By';
$lang['checked_by']='Checked By';
$lang['approved_by'] = 'Approved By';
$lang['received_by']='Received By';
return $lang;