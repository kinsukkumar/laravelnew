<?php
/* List Language  */
$lang['panel_title'] = "General Character Assessment";
$lang['assess_title'] = "General Comment";
$lang['select_academic_year']='Select Academic year';
$lang['select_class']='Select class';
$lang['select_semester']='Select semester';
$lang['action'] = "Action";
$lang['slno'] = '#';
$lang['class'] = 'Class';
$lang['roll'] = 'Roll/RegNo';
$lang['student_name'] = 'Student Name';
$lang['section'] = 'Section/Stream';
$lang['classlevel'] ='Class Level';
$lang['academic_year'] ='Academic Year';
$lang['semester'] ='Semester';
$lang['general_comment'] ='Comment';
$lang['send_comment'] = 'Send';
$lang['character_code'] = 'Code';
$lang['character'] = 'Character';
$lang['code'] = 'Code';
$lang['view'] = 'View';
$lang['remark1'] = 'Achievement';
$lang['remark2'] = 'Effort';
$lang['menu_success'] = 'Successfully Assessed';
$lang['menu_error'] =  'Failed';
$lang['student_assessed'] = 'Remark to: ';

$lang['menu_characters'] ='Character';
$lang['menu_character_categories'] = 'C Category';
$lang['menu_assigned'] = 'Assigned';
$lang['menu_add_characters'] = 'Character';
$lang['menu_characters'] = 'Character';
$lang['menu_character_grades'] = 'Character Grades';

$lang['menu_character_category'] = 'Category';
$lang['report_student_refresh'] = 'Refresh';
$lang['number_character_assessed'] = 'Character Assessed';
$lang['message'] = 'out of ';

/* Add Language */
$lang['report_student_name'] = 'STUDENT NAME: ';
$lang['report_character_category'] = 'Category: ';
$lang['report_grading_system'] = "Character Grades' System";
$lang['report_class_teacter_comment'] ='Class Teacher Comment';
$lang['report_head_teacter_comment'] = 'Head Teacher Comment';
$lang['report_class_teacter_signature'] = 'Signature';
$lang['report_head_teacter_signature'] = 'Signature';
$lang['character_report_header'] = 'PROGRESS REPORT FORM  ';
$lang['character_report_box'] = 'P.O BOX ';
$lang['character_report_email'] = 'Email: ';
$lang['character_report_website'] = 'Website: ';
$lang['character_report_cell'] = 'Cell: ';
$lang['character_report_select_a_grade'] = 'Achievement';
$lang['character_report_select_e_grade'] = 'Effort';
$lang['character_report_assess'] = 'Asses';
$lang['character_report_selectall_message'] = 'Select All';
$lang['character_report_remark'] = 'Remark';
$lang['character_report_grade'] = 'Grade';
$lang['class_teacher_comment'] = 'Class Teacher Comment';
$lang['head_teacher_comment'] = 'Head Teacher Comment';
return $lang;