<?php

/* List Language  */
$lang['panel_title'] = "Grade";
$lang['add_title'] = "Add a grade";
$lang['slno'] = "#";
$lang['grade_name'] = "Grade Name";
$lang['grade_name_eg']='Grade name eg A';
$lang['weight']='Weight of this grade e.g 5';
$lang['minimum']='Minimum marks for this grade';
$lang['max']='Maximum marks for this grade';
$lang['excellent']='Eg excellent';
$lang['remarks']='This is used as remarks of a subject that will appear on a report according to subject marks of a student';
$lang['grade_remarks']='Grade remarks';
$lang['overall remarks']='Overall remarks';
$lang['comment']='This is used as remarks of a head teacher that will appear at the bottom of a report according to the overall average of a student';
$lang['grade_point'] = "Grade Point";
$lang['grade_gradefrom'] = "Mark From";
$lang['grade_gradeupto'] = "Mark Up to";
$lang['grade_note'] = "Remark";
$lang['overall_note'] = "Overall Remark";

$lang['action'] = "Action";
$lang['select_level']='Select a specific class level to view grades assigned to it.';
$lang['select_classlevel']='Select Class level';
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['grade_info']='Grade Information';
$lang['fields_marked']='Fields marked';
$lang['are_mandatory']='are mandatory';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_class'] = 'Add Grade';
$lang['update_class'] = 'Update Grade';
$lang['classlevel'] = 'Class Level';
$lang['overall_academic_note']='Overall Academic Note';
$lang['overall_academic_note_comment']='This is used as remarks of a academic/class teacher that will appear at the bottom of a report according to the overall average of a student';
return $lang;