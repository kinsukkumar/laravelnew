<?php

/* List Language  */
$lang['panel_title'] = "School Expenses";
$lang['panel_group_title'] = "Custom Groups";
$lang['slno'] = "#";
$lang['add_group'] = 'Add Group';
$lang['action'] = 'Action';
$lang['group_name'] = 'Group Name';
$lang['group_note'] = 'Group Note';
$lang['group'] = 'Custom Groups';
$lang['sum'] = 'Sum';
return $lang;