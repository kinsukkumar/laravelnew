<?php

/* List Language  */
$lang['panel_title'] = "Hostel";
$lang['add_title'] = "Add a hostel";
$lang['slno'] = "#";
$lang['hostel_name'] = "Name";
$lang['hostel_htype'] = "Type";
$lang['hostel_address'] = "Address";
$lang['school_hostel'] = "School Hostel";
$lang['hostel_note'] = "Note";
$lang['fields'] = "Fields marked";
$lang['mandatory'] = "are mandatory";
$lang['select_hostel_type'] = 'Select Type';
$lang['hostel_boys'] = "Boys";
$lang['hostel_girls'] = "Girls";
$lang['hostel_combine'] = "Combine";
$lang['hmember_name']='Hostel Name';
$lang['hostel_type']='Hostel Type';
$lang['hostel_category']='Hostel Category';
$lang['select_class']='Select Class';
$lang['select_year']='Year';
$lang['patron'] = "Patron/Matron";
$lang['Hostel']="Hostel Name";
$lang['beds_no'] = "Number of Beds";

$lang['action'] = "Action";
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['hostel_route'] = 'Hostel Name';

/* Add Language */

$lang['add_hostel'] = 'Add Hostel';
$lang['update_hostel'] = 'Update Hostel';
return $lang;