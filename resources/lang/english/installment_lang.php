<?php

/**
 * Description of classlevel_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$lang['menu_installement']='Installment';
$lang['panel_title']='Fee Installment Details';
$lang['add_title']='Add Installment';
$lang['submit']='Save';
$lang['name']='Installment Name';
$lang['start_date']='Start Date';
$lang['end_date']='End Date';
$lang['note']='Notes';
$lang['action']='Action';
$lang['slno']='#';

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['edit_installment'] = 'Save';
$lang['delete'] = 'Delete';
$lang['select_year']='Select Year';
$lang['select_installment']='Select Installment';

$lang['academic_year'] = 'Academic Year';
/* Add Language */

$lang['class_level'] = 'Class Level';
$lang['academic_year'] = 'Academic Year';
$lang['exam_select_year'] = 'Select year';
$lang['update_level'] = 'Update Installment';
$lang['check_due_amount'] = 'Click to view/Add';

$lang['upload_inst_by_excel']='Upload Installments from Excel';
$lang['sample_installment_file']='Sample File for Installments';
return $lang;
