<?php

/**
 * Description of inventory_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
/* List Language  */
$lang['panel_title'] = "List of Items";
$lang['add_title'] = "Inventory ";
$lang['add_register'] = "Add New Item";
$lang['add_purchase'] = "Purchase ";
$lang['add_sale'] = "Add Usage ";

$lang['slno'] = "#";
$lang['item_name'] = "Item Name";
$lang['batch_number'] = "Batch Number";
$lang['product_code'] = "Product Code";
$lang['item_status'] = "Item Status";
$lang['vendor'] = " Vendor";
$lang['quantity'] = " Quantity";
$lang['detail'] = " Detail";
$lang['total_quantity'] = " Total Quantity";
$lang['used_quantity'] = " Used Quantity";
$lang['amount'] = " Amount";
$lang['select_payer'] = " Select payer";
$lang['alert_quantity']='Alert Quantity';
$lang['remain_quantity']='Remained Quantity';
$lang['vendor_name'] = "Vendor Name";
$lang['product_code']='Product code';
$lang['purchase_inventory']='Purchases';
$lang['Usage_inventory']='Usage';
$lang['payer_phone']='Payer Phone';
$lang['payment_method']='Payment Method';
$lang['price'] = "Price";
$lang['contact_name']="Contact Person";
$lang['account_group']="Account Group";
$lang['edit_item']="Edit Item";
$lang['add_item']="Add Item";
$lang['set_alert']="Set Remained Quantity Reminder";


$lang['add'] = 'Add';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */
$lang['inventory_title']='Add new Inventory';
$lang['name'] = 'Name';
$lang['email'] = 'Email';
$lang['phone_number'] = 'Phone';
$lang['batch_number']='Batch Number';
$lang['status'] = 'Item Status';
$lang['contact_person_number'] = 'Contact Person Phone';
$lang['contact_person_name'] = 'Contact Person Name';
$lang['depreciation'] = 'Depreciation';
$lang['add_item'] = 'Add Item';
$lang['quantity']='Quantity';
$lang['alert_quantity']='Alert Quantity';
$lang['comments']='Descriptions';
$lang['category']='Category';
$lang['group']='Item Group';
$lang['unit']='Unit';
$lang['date_purchased']='Date';
$lang['created_at']='Date Recorded';
$lang['view_vendor_indo']='Click to View Vendor Information';
$lang['update_item']='Update Item';
$lang['add_new_item'] = "Add New Item";
$lang['add_from_item'] = "Add From Existing Items";
$lang['item_category'] = "Select Category";
$lang['open_blance'] = "Current balance";
return $lang;