<?php

/* List Language  */
$lang['panel_title'] = "Invoice";
$lang['panel_receipt_title'] = "Receipts";
$lang['add_title'] = "Add Invoice(s)";
$lang['slno'] = "#";
$lang['invoice_classesID'] = "Class";
$lang['all']='All';
$lang['create']='Create';
$lang['panel_receipt_title']='Receipt';
$lang['select_installment']='Select installment';
$lang['payment_receipts']='Payment receipts';
$lang['PAYMENT BALANCE REMINDER']='PAYMENT BALANCE REMINDER';
$lang['pending_approval']='Pending approval';
$lang['approved']='Approved payments';
$lang['rejected']='Rejected payments';
$lang['select_class'] = "Select class";
$lang['select_student'] = "Select student";
$lang['select_installment'] = "Select installment";
$lang['select_class_level'] = "Select class level";
$lang['payment_title']='Payment title';
$lang['select_payment_type']='Select payment type';
$lang['invoice_studentID'] = "Student";
$lang['invoice_select_classes'] = "Select Class";
$lang['invoice_select_student'] = "Select Student";
$lang['search_invoice']='Search invoice by parent';
$lang['invoice_select_paymentmethod'] = "Select Payment Method";
$lang['invoice_select_feetype'] = "Select Fee Type";
$lang['all_class']='All class student invoice';
$lang['invoice_amount'] = "Amount";
$lang['all_levels']='All levels';
$lang['class_level']='Class level';
$lang['back_to_invoice']='Back to invoice';
$lang['invoice_number']='Invoice number';
$lang['total_amount_unpaid']='Total Amount Unpaid';
$lang['total_invoice_amount']='Total Invoice Amount';
$lang['student_name']='NAME:';
$lang['student_class']='CLASS:';

$lang['unpaid_amount']='Remained Amount';
$lang['remained_amount']='Remained Amount';
$lang['total_amount_paid']='Total Amount Paid';
$lang['select_parent']='Select parent';
$lang['select_class_level']='Select class level';
$lang['select_academic_year']='Select academic year';
$lang['fee_priority']='Fee Priority';
$lang['invoice_student'] = "Student";
$lang['invoice_date'] = "Date";
$lang['invoice_feetype'] = "Fee Type";
$lang['invoice_invoice'] = "Invoice #";
$lang['invoice_pdate'] = "Payment Date";
$lang['invoice_total'] = "Total to pay";
$lang['paid_invoice_amount'] = "Invoice Paid Amount";
$lang['invoice_from'] = "From";
$lang['invoice_to'] = "To";
$lang['invoice_create_date'] = "Date";
$lang['invoice_subtotal'] = "Sub Total";
$lang['invoice_sremove'] = "Student Remove";
$lang['invoice_roll'] = "Roll";
$lang['invoice_phone'] = "Phone";
$lang['invoice_email'] = "Email";
$lang['invoice_status'] = "Payment Status";
$lang['invoice_notpaid'] = "Not Paid";
$lang['discount_note'] = "Note";
$lang['discount_amount']='Discount Amount';
$lang['feetype_name'] = "Fee Name";
$lang['invoice_partially_paid'] = "Partially Paid";
$lang['invoice_fully_paid'] = "Fully Paid";
$lang['invoice_paymentmethod'] = 'Payment Method';
$lang['invoice_pending_approved']='Pending Approval';
$lang['feetype_category']="feetype Category";
$lang['add_category']="Add Category";
$lang['select_fee']="select fee";
$lang['invoice_cash'] = "Cash";
$lang['invoice_cheque'] = "Cheque";
$lang['invoice_mobile'] = "Mobile";
$lang['invoice_deposit'] = "Bank Deposit";
$lang['invoice_paypal'] = "Paypal";
$lang['invoice_cheque_number']='Enter Cheque number';


$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email sent successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!';


$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";
$lang['payment'] = 'Payment';
$lang['invoice_due'] = "Balance to pay";
$lang['invoice_made'] = "Total Received";
$lang['view'] = "View";
$lang['previous_due_amount']='Previous Due Amount';

$lang['add_invoice'] = 'Create Invoice(s)';

$lang['update_invoice'] = 'Update Invoice';
$lang['add_payment'] = 'Add Payment';
$lang['action'] = 'Action';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

$lang['bankslip_image']='Bank Slip Image';
$lang['transaction_id']='Transaction ID';
$lang['bank_name']='Bank Name';
$lang['select_bank']='Select Bank';
$lang['bankslip_title']='Submit Bank Paying Slip';

$lang['payment_title']='Payment Records';
$lang['select_payment_type']='Select Payment Type';
$lang['parent_name']='Parent Name';
$lang['student_name']='Student Name';
$lang['payment_date']='Payment Date';
$lang['menu_success']='Success';
$lang['invoice_exist']='Invoice exist, you cannot duplicate invoices';
$lang['optional_name']='Other Receipt Name';
$lang['invoice_title']='Invoice title';
$lang['exam_select_year']='Select Year';

$lang['student_section']='Section';
$lang['all_students']='All Students';
$lang['semester']='Semester';
$lang['pay_amount']='Pay Amount';
$lang['select_student']='Select Student';
$lang['total_invoices']='Total Invoices';
$lang['payment_summary']='Payment Summary';
$lang['save_guide']='Save Guide';
$lang['invoice_guide']='Invoice Guide';
$lang['invoice_payment_guide']='Invoice Payment Guide';
$lang['exchange_rate']='Exchange Rate';

$lang['fee_name']='Fee Name';
$lang['amount_from_wallet']='Amount Taken From Wallet';
$lang['guidance']='Guidance';
$lang['guidance_details']=' You need to enter correct details and ensure all transaction information are correct. Transaction ID is used to identify unique transaction done, and is available in bank paying slip as transaction ID, Transaction number.';

$lang['transactions']='Transactions';
$lang['type']='Type';
$lang['amount_reconciled']='Amount Reconciled';
$lang['amount_not_reconciled']='Amount not Reconciled';
$lang['account_name']='Account Name';
$lang['account_number']='Account Number';
$lang['dates']='Dates';
$lang['payment_method']='Payment Method';
$lang['received']='Received';
$lang['receipt']='Receipt';
$lang['invoices']='Invoices';
$lang['deleted_amount']='Deleted Amount';
$lang['deleted_date_time']='Deleted Date time';
$lang['recorded_date']='Recorded Date';
$lang['deleted_by']='Recorded Date';

$lang['due_date']='Due Date';
$lang['total_invoiced_amount']='Total Invoiced Amount';
$lang['advance_payment']='Advance Payment';
$lang['previous_due_amount']='Previous Due Amount';


$lang['invoices']='Invoice:';
$lang['section']='Section:';
$lang['summary']='Summary:';
$lang['advance_amount'] = "Advanced Amount";
$lang['previous_amount'] = 'Previous Amount';

$lang['total_invoiceA'] = 'Total Invoiced Amount';
$lang['total_paid'] = 'Total Paid Amount';
$lang['total_upaid'] = 'Total Unpaid Amount';
$lang['total_invoices'] = 'Total Invoices';
$lang['partially'] = 'Partially Paid';
$lang['full_paid'] = 'Full Paid Invoices';
$lang['unpaid'] = 'Unpaid Invoices';
$lang['notify_parents']="Notify parents By SMS & Emails";
return $lang;