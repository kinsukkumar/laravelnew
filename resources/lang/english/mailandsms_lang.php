<?php

/* List Language  */
$lang['panel_title'] = "Email / SMS ";
$lang['add_title'] = "Add Email / SMS";
$lang['slno'] = "#";
$lang['mailandsms_users'] = "Users";
$lang['chars/SMS count']='chars/SMS count';
$lang['select_criteria']='Select criteria';
$lang['message']='Write a message';
$lang['write']='NB: If you write';
$lang['#name']='#name';
$lang['it_will']='it will be replaced with Parent/Teacher\'s Name';
$lang['#username']='#username';
$lang['it_will_username']='will be replaced with Parent/Teacher\'s username';
$lang['all_parents']='All parents';
$lang['based_on_class']='Based on class';
$lang['based_on_hostel']='Based on Hostel';
$lang['based_on_studenttype']='Based on student characteristics';
$lang['based_on_accounts']='Based on accounts';
$lang['custom_selection']='Custom selection';
$lang['mailandsms_select_user'] = "Select Users";
$lang['based_on_phone_number'] = "Based on Parent's Phone Number";
$lang['based_on_names'] = "Based on Parent's Names";
$lang['based_on_teachers_phone_number'] = "Based on Teachers's Phone Number";
$lang['based_on_teachers_names'] = "Based on Teachers's Names";
$lang['based_on_transport_routes'] = "Based on Transport Routes";
$lang['based_on_feetype']='Based on Fees subscription';
$lang['mailandsms_select_template'] = "Select Template";
$lang['mailandsms_select_send_by'] = "Select Send By";
$lang['mailandsms_students'] = "Students";
$lang['mailandsms_parents'] = "Parents";
$lang['mailandsms_teachers'] = "Teachers";
$lang['other_users'] = "Staff and Other Users";
$lang['custom_to_parents_and_teachers'] = "SMS to Parents and Teachers";
$lang['mailandsms_compose'] = "Compose";
$lang['mailandsms_librarians'] = "Librarians";
$lang['mailandsms_accountants'] = "Accountants";
$lang['mailandsms_template'] = "Template";
$lang['mailandsms_type'] = "Type";
$lang['mailandsms_email'] = "Email";
$lang['mailandsms_sms'] = "SMS to other Users";
$lang['sent_and_summary'] = "Sent Items / Summary";
$lang['mailandsms_getway'] = 'Send By';
$lang['mailandsms_subject'] = 'Subject';
$lang['mailandsms_message'] = "Message";
$lang['mailandsms_dateandtime'] = "Date and Time";
$lang['send_to'] = "Send SMS to";
$lang['all_teachers'] = "All Teachers";
$lang['teacher_type'] = "Teacher Type";
$lang['no_teachers_error'] = "There are no active teachers in your school";

$lang['mailandsms_clickatell'] = "Clickatell";
$lang['mailandsms_twilio'] = "Twilio";
$lang['mailandsms_bulk'] = "Bulk";


$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['send'] = "Send";
$lang['from'] = "From";
$lang['mail_success'] = 'Email sent successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!';
$lang['mail_error_user'] = 'oops! User list is empty!';
$lang['no_student_error'] = 'No student in this class/category yet';
$lang['sms_sent_successfully'] = 'Your message has been successfully sent to the parents';

$lang['mailandsms_numbers']='Input Numbers';
$lang['write_Phone_numbers']='Write phone numbers (or paste from excel file)';
$lang['title']='Title';
$lang['message']='Message';
$lang['time']='Time';
$lang['sent_to']='Sent To';
$lang['users']='Users';
$lang['is_repeated']='Is Reapeted';
$lang['select_student']='Select';
$lang['to_date']='To Date';

$lang['schedule']='Schedule';
$lang['active']='Active';
$lang['inactive']='Inactive';
$lang['status']='Student status';
$lang['all']='All';
$lang['others_users']='Other Users (Groups)';

return $lang;



