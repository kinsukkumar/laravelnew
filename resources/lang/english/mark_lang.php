<?php

/* List Language  */
$lang['panel_title'] = "Mark";
$lang['add_title'] = 'Add Mark(s)';
$lang['file_download']='Download file here';
$lang['upload_marks']='Upload marks from excel file';
$lang['upload_file']='Upload file';
$lang['slno'] = "#";
$lang['published']='This report is already published. You cannot edit marks for reports that have been published';
$lang['optional']='This is Optional subject. Marks added to a certain student, will make that student automatically subscribed/enrolled to students who take this subject. Remember, when you remove mark to any student into 0 or empty, that student will not automatically removed from a list of subject subscribers unless you remove him/her by';
$lang['mark_exam'] = "Exam";
$lang['clicking here..']='Clicking here..';
$lang['no_sections']='No section(s) have been added in this class. Please ';
$lang['add_section']='add section';
$lang['action']='Action';
$lang['efforts']='Efforts';
$lang['import_from_excel']='Import from Excel';
$lang['for_class']='for this class first';
$lang['mark_instructions']='Marking instructions';
$lang['marking']='On adding mark, system will only save marks for students that appear in the page (default 10 students per one page). After inserting mark, click MARK button or press Enter. Each time you go to the next page, you need to insert mark in that page and click MARK to save. System will not save mark for  hidden student';
$lang['Subject'] = "Subject";
$lang['Topic'] = "Topic";
$lang['Geography'] = "Geography";
$lang['for this class first']='for this class first';
$lang['no_semester']='No semester(s) have been added. Please';
$lang['add_semester']='add semester';
$lang['semester']='Semester';
$lang['mark_classes'] = "Class";
$lang['mark_student'] = "Student";
$lang['mark_subject'] = "Subject";
$lang['upload_mark']='Upload mark';
$lang['mark_photo'] = "Photo";
$lang['mark_name'] = "Name";
$lang['marks']='Marks';
$lang['exam']='Exam';
$lang['exam_date']='Exam date';
$lang['exam_subject']='Exam subject';
$lang['subject']='Subject';
$lang['file_upload']='The file you upload MUST have the same format with the sample file for marks to be uploaded (subject names should be in lowercase)';
$lang['mark_roll'] = "Roll";
$lang['mark_phone'] = "Phone";
$lang['mark_dob'] = "Date of Birth";
$lang['mark_sex'] = "Gender";
$lang['mark_religion'] = "Religion";
$lang['mark_email'] = "Email";
$lang['mark_address'] = "Address";
$lang['mark_username'] = "Username";
$lang['student_classes'] = "Select Class";

$lang['mark_subject'] = "Subject";
$lang['mark_section'] = "Section";
$lang['mark_mark'] = "Mark";
$lang['mark_point'] = "Point";
$lang['mark_grade'] = "Grade";
$lang['mark_semester'] = "Semester";


$lang['mark_select_classes'] = "Select Class";
$lang['mark_select_exam'] = "Select Exam";
$lang['mark_select_subject'] = "Select Subject";
$lang['mark_select_section'] = "Select Section";
$lang['mark_select_student'] = "Select Student";
$lang['mark_select_semester'] = "Select Semester";
$lang['exam_academic_year']='Academic Year';
$lang['exam_select_year']='Select Year';

$lang['mark_success'] = "Success"; //kamayugi
$lang['personal_information'] = "Personal Information";
$lang['mark_information'] = "Mark Information";
$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

// /* Add Language */
$lang['add_mark'] = 'Mark';
$lang['add_sub_mark'] = 'Add Mark(s)';

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email sent successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!';
$lang['report_information']='Exams Reports'; 
$lang['marking_status']='Marking Status';
$lang['abbreviation']='Abbreviation';
return $lang;