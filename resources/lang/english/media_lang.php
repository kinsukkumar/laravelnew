<?php

/* List Language  */
$lang['panel_title'] = "Media";
$lang['add_title'] = "Create Folder";
$lang['slno'] = "#";
$lang['media_title'] = "Title";
$lang['media_date'] = "Date";
$lang['action'] = "Action";

$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_class'] = 'Add media';
$lang['update_class'] = 'Update media';
$lang['file'] = 'Media';
$lang['upload_file'] = 'Upload media';
$lang['folder_name'] = 'Folder name';
$lang['share'] = 'Share';
$lang['share_with'] = 'Share with';
$lang['select_class'] = 'Select class';
$lang['all_class'] = 'All class';
$lang['public'] = 'Public';
$lang['class'] = 'Class'; 
$lang['access'] = 'Access'; 
$lang['live_class'] = 'of all live classes'; 
$lang['live_recorded'] = 'of recorded live classes'; 
$lang['live_discussion'] = 'live discussions'; 
$lang['unlimited'] = 'Unlimited'; 
$lang['download_notes'] = 'download of notes, books etc'; 
$lang['self_evaluate'] = 'Self Evaluation with'; 
$lang['live_access'] = 'Live Access'; 
$lang['qn_quiz'] = 'questions and quiz'; 
$lang['best_teacher'] = 'of best teachers'; 
$lang['pay_now'] = 'Pay and Proceed'; 
$lang['now'] = 'Now'; 
$lang['off'] = 'Off'; 
$lang['only'] = 'Only'; 
$lang['days'] = 'days'; 
$lang['week_plan'] = 'Weekly Plan'; 
$lang['day_plan'] = 'Day Plan'; 
$lang['month_plan'] = 'Monthly Plan'; 
$lang['day'] = 'day'; 
$lang['reference'] = 'Reference';
$lang['save_continue'] = 'Save & Continue';
return $lang;
