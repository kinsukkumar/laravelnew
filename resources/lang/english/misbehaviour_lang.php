<?php

/* List Language  */
$lang['panel_title'] = "Student Misbehaviour Section";
$lang['panelcat_title'] = "Misbehaviour ";
$lang['add_title'] = "Add a Misbehaviour Type";
$lang['slno'] = "#";
$lang['misbehaviour_name'] = "Misbehaviour Name";
$lang['discount_amount'] = "Discount Amount";
$lang['misbehaviour_amount'] = "Amount";
$lang['misbehaviour_is_repeative'] = "Is Repeative";
$lang['misbehaviour_is_repeative_yes'] = "Yes";
$lang['exam_select_year']='Select Year';
$lang['misbehaviour_is_repeative_no'] = "No";
$lang['misbehaviour_startdate'] = "Start Date";
$lang['misbehaviour_enddate'] = "End Date";
$lang['discount_amount'] = "Discount Amount";
$lang['misbehaviour_note'] = "Note";
$lang['action'] = "Action";
$lang['misbehaviourcategory']="Misbehaviour Category";
$lang['feetpype_select_category']="misbehaviour select category";
$lang['category_title']="Add misbehaviour Category";
$lang['category_fee']="misbehaviour Category";
$lang['category_note']="Category Note";
$lang['misbehaviour_category']="Misbehaviourtype Category";
$lang['add_category']="Add Category";
$lang['select_misbehaviour_category']="select misbehaviour category";

$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */
$lang['add_misbehaviour'] = 'Add Misbehaviour';
$lang['update_misbehaviour'] = 'Update Misbehaviour';
$lang['student_name'] = 'Student Name';
$lang['fee_percent'] = 'Misbehaviour percent';
$lang['misbehaviour_account'] = 'Misbehaviour Type Account';
$lang['feetpype_select_account'] = 'Select account number';
 $lang['menu_misbehaviour'] ='Student status'; return $lang;