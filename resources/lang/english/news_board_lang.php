<?php

/* List Language  */
$lang['panel_title'] = "News Board";
$lang['add_title'] = "Add Event";
$lang['slno'] = "#";
$lang['news_board_title'] = "Title";
$lang['news_board_news_board'] = "News Description";
$lang['news_board_date'] = "Date";
$lang['action'] = "Action";
$lang['upload_calendar']='Upload School Calender From Excel';
$lang['download']='Download';
$lang['sample_file']='Sample file here';
$lang['submit']='Submit News';
$lang['menu_news_board']='News & Announcement';
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['print'] = 'Print';
$lang['pdf_preview'] = 'PDF Preview';
$lang["mail"] = "Send Pdf to Mail";

/* Add Language */

$lang['add_class'] = 'Add Event';
$lang['update_class'] = 'Update Event';
$lang['news_board_for']='News For';

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['select_all']='Select All';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email sent successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!';
$lang['welcome_board']='Welcome to school Board';
$lang['school_board']='School Board';
$lang['news_board_made']='Made for';
return $lang;