<?php

/* List Language  */
$lang['panel_title'] = "Calendar";
$lang['add_title'] = "Add Event";
$lang['slno'] = "#";
$lang['notice_title'] = "Title";
$lang['notice_notice'] = "Description";
$lang['notice_date'] = "Date";
$lang['action'] = "Action";
$lang['upload_calendar']='Upload School Calender From Excel';
$lang['download']='Download';
$lang['sample_file']='Sample file here';
$lang['submit']='Submit';

$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['print'] = 'Print';
$lang['pdf_preview'] = 'PDF Preview';
$lang["mail"] = "Send Pdf to Mail";

/* Add Language */

$lang['add_class'] = 'Add Event';
$lang['update_class'] = 'Update Event';
$lang['notice_for']='Calendar For';

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['select_all']='Select All';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email sent successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!'; return $lang;