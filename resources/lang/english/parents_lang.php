<?php
/* List Language  */
$lang['panel_title'] = "Parents";
$lang['add_title'] = "Add parent(s)";
$lang['slno'] = "#";
$lang['parents_photo'] = "Photo";
$lang['parents_name'] = "Name";
$lang['parents_email'] = "Email";
$lang['parents_dob'] = "Date of Birth";
$lang['occupation']="Occupation";
$lang['occupation_eg']="Job eg Farmer";
$lang['employer_eg']="Employer eg Inets company Ltd";
$lang['names_eg']='Both names eg John Richard Samson';
$lang['menu_parents']="Parents";
$lang['email'] = "Valid email address samson@yahoo.com";
$lang['profession']='Profession';
$lang['guardian_employer']='Parent/Guardian employer';
$lang['add']='Add a parent/guardian';
$lang['parent_info']='Parent Details';
$lang['fields_marked']='Fields marked';
$lang['are_mandatory']='are manadatory';
$lang['file']='Download sample file here';
$lang['physical_condition']='Physical condition';
$lang['upload_excel']='Upload excel';
$lang['guardian_employer']='Parent/Guardian employer';
$lang['relation'] = "Relation";
$lang['parents_sex'] = "Sex";
$lang['parents_religion'] = "Religion";
$lang['parents_phone'] = "Phone";
$lang['parents_other_phone'] = "Other Phone";
$lang['relation_s']='Relation with student eg Uncle';
$lang['phone_eg'] = "Primary phone number";
$lang['address']='Parents address eg P.O BOX 33287,Dar';
$lang['parents_address'] = "Address";
$lang['parents_classes'] = "Class";
$lang['parents_roll'] = "Roll";
$lang['parents_photo'] = "Photo";
$lang['parents_username'] = "Username";
$lang['parents_password'] = "Password";
$lang['parents_select_class'] = "Select Class";
$lang['parents_name'] = "Parent Name";

/* parents */
$lang['parents_guargian_name'] = "Guardian Name";
$lang['parents_father_name'] = "Father's Name";
$lang['parents_mother_name'] = "Mother's Name";
$lang['parents_father_profession'] = "Father's Profession";
$lang['parents_mother_profession'] = "Mother's Profession";


$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email sent successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!';

/* Add Language */

$lang['add_parents'] = 'Add Parent';

$lang['submit_parent'] = 'Upload Parent(s)';
$lang['update_parents'] = 'Update Parents';
$lang['preferred_language'] = 'Preferred Language';
$lang['kiswahili'] = 'Kiswahili';
$lang['english'] = 'English';



/* ini code starts here*/
$lang['personal_information'] = "Personal Information";
$lang['parentss_information'] = "Parents Information";


$lang['student_photo'] = "Photo";
$lang['student_name'] = "Name";
$lang['student_email'] = "Email";
$lang['student_dob'] = "Date of Birth";
$lang['student_sex'] = "Gender";
$lang['student_sex_male'] =$lang['sex_male']= "Male";
$lang['student_sex_female'] =$lang['sex_female']= "Female";
$lang['student_religion'] = "Religion";
$lang['student_hostel'] = "Hostel";
$lang['student_address'] = "Address";
$lang['student_classes'] = "Class";
$lang['student_roll'] = "Roll";
$lang['student_username'] = "Username";
$lang['student_library'] = "Library";
$lang['student_select_class'] = "Select Class";
$lang['student_guargian'] = "Guardian";
$lang['guardian_employer'] = "Employer Name";
$lang['parent_information']='Parent Information';
$lang['admission_status']='Admission Status';
$lang['locate_student']='Locate Student';
$lang['student_to_link']='Select Student to Link';

//INVOICE

$lang['invoice_notpaid'] = "Not Paid";
$lang['invoice_partially_paid'] = "Partially Paid";
$lang['invoice_fully_paid'] = "Fully Paid";
return $lang;