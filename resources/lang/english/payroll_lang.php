<?php

/* List Language  */
$lang['panel_title'] = "Payroll";
$lang['add_title'] = "Add a payroll";
$lang['slno'] = "#";
$lang['payroll_name'] = "Name";
$lang['payroll_from']='From Amount';
$lang['payroll_to']='To Amount';
$lang['tax_rate']='Tax rate';
$lang['excess_amount']='TAX Amount Excess';
$lang['description']='Description';
$lang['name']='Name';
$lang['employer_percentage']='Employer Percentage Contribution';
$lang['employee_percentage']='Employee Percentage Contribution';

$lang['type']='Type';
$lang['percentage']='Percentage';
$lang['percent']='Percent';
$lang['fixed_amount'] = "Fixed Amount";
$lang['amount'] = "Amount";
$lang['allowance_title'] = "Allowance";
$lang['add_allowance']="Add New Allowance";
$lang['pension'] = "Pension";
$lang['tax'] = "Tax";

$lang['action'] = "Action";
$lang['deduction']='Deductions';
$lang['employee_name']='Name';
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['employee_number']='ID Number';
$lang['fields_marked']='Fields marked';
$lang['are_mandatory']='are mandatory';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['address'] = 'Address';
$lang['basic_pay'] = 'Basic Pay';
$lang['allowance'] = 'Allowance';
$lang['gross_pay']='Gross Pay';
$lang['paye']='PAYE';
$lang['net_pay']='Net Payment';
$lang['taxable_amount']='Taxable Amount';
$lang['payment_date']='Payment Date';
$lang['total_users']='Total Users';
$lang['save']='Save';

$lang['category']='Category';
$lang['select']='Select';
$lang['members']='Members';
$lang['employer_amount']='Employer Amount';
$lang['employer_percent']='Employer Percentage';
$lang['select_pension']='Select Pension Fund';
$lang['pension_name']='Pension Name';
$lang['enter_pension'] = 'Enter Pension Fund Name';

$lang['loan_application']='Loan Application';
$lang['source']='Source';
$lang['loan_type']='Loan Type';
$lang['months']='Months';
$lang['borrowers']='Borrowers';
$lang['date_requested']='Date Requested';
$lang['date_approved']='Date Approved';
$lang['remain_amount']='Remain Amount';
$lang['profit']='Profit';
$lang['loan_types']='Loan Types';
$lang['minimum_amount']='Minimum Amount';
$lang['maximum_amount']='Maximum Amount';
$lang['minimum_tenor']='Payment Months (minimum)';
$lang['maximum_tenor']='Payment months (maximum)';
$lang['interest_rate']='Interest Rate';
$lang['credit_ratio']='Credit Ratio';
return $lang;