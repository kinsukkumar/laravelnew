<?php

$lang['panel_title'] = "Promotion";
$lang['add_title'] = "Add promotion";
$lang['slno'] = "#";
$lang['promotion_photo'] = "Photo";
$lang['promotion_name'] = "Name";
$lang['promotion_section'] = "section";
$lang['promotion_from_section'] = "From Section";
$lang['promotion_to_section'] = "To Section";
$lang['promotion_result'] = "Result";
$lang['promotion_pass'] = "Pass";
$lang['promotion_fail'] = "Fail";
$lang['promotion_modarate'] = "Not Yet";
$lang['promotion_phone'] = "Phone";
$lang['promotion_classes'] = "Class";
$lang['promotion_from_classes'] = "From Class";
$lang['promotion_to_classes'] = "To Class";
$lang['promotion_roll'] = "Roll";
$lang['promotion_create_class'] = "Please create a new class";
$lang['promotion_select_class'] = "Select Class";
$lang['promotion_select_student'] = "Select Student";
$lang['add_all_promotion'] = "All Promotion";
$lang['promotion_alert'] = "Alert";
$lang['promotion_ok'] = "Ok";
$lang['promotion_success'] = "Promotion Success";
$lang['promotion_emstudent'] = "Empty Student List";
$lang['student_promotion'] = "Promote Student to Next Class";
$lang['promote_per_class'] = "Promote Student Per Class";
$lang['promote_per_class2'] = " Eg. Form One to Form Two";
$lang['per_class'] = " Promote Per Class";
$lang['promote_all'] = " Promote All Student";

$lang['promotion_students'] = "This part allows you to promote all student From one class to another.";
$lang['promote_note'] = "Note: Before you made any promotion, Please make sure you already defined new academic year for all <b>Class level</b> ";
$lang['action'] = "Action";

$lang['add_mark_setting'] = 'Promote';
$lang['add_promotion'] = 'Promote To Next Class';
$lang['promoted_student'] = "Students Who Are Not Promoted to next class";
return $lang;