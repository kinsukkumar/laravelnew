<?php

/* List Language  */
$lang['panel_title'] = "School Revenue";
$lang['panel_category_title'] = "Charts of Accounts";
$lang['add_title'] = "Add Revenue";
$lang['add_category'] = "Add Revenue Category";
$lang['slno'] = "#";
$lang['payer_name'] = "Payer Name";
$lang['revenue_name'] = "Name";
$lang['amount'] = "Amount";
$lang['payer_phone'] = "Payer Phone";
$lang['transaction_id'] = "Transaction ID";
$lang['date'] = "Date Received";
$lang['total'] = "Total";
$lang['action'] = "Action";
$lang['note'] = "Note";
$lang['receipt'] = "Receipt";
$lang['payer_email'] = "Payer Email";
$lang['fee_type'] = "Revenue Type";
$lang['phone'] = "Phone";
$lang['email'] = "Email";
$lang['ref_no'] = "Reference No.";
$lang['payment_method'] = "Payment Method";
$lang['account_code']='Account Code';
$lang['add_account']='Add Account';
$lang['account_type']='Account Type';
$lang['account_name']='Account Name';

// $lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_expense'] = 'Transactions';
$lang['category'] = 'Category';
$lang['expense'] = 'Name';
$lang['expense_amount'] = 'Amount';
$lang['expense_note'] = 'Note';
$lang['expense_date'] = 'Date';
$lang['revenue_name'] = 'Name';
$lang['add_category'] = 'Add Category';
$lang['edit_category'] = 'Edit Category';
$lang['update_category'] = 'Update Category';
$lang['subcategory'] = 'Name';
$lang['update_expense'] = 'Update Expense';
$lang['select_expense'] = 'Select Name';
$lang['upload_inst_by_excel'] = 'Upload Revenues By Excel';
$lang['sample_installment_file']='Sample  Revenue Excel File';

return $lang;