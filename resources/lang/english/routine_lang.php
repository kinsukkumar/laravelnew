<?php

/* List Language  */
$lang['panel_title'] = "Routine";
$lang['add_title'] = "Add a routine";
$lang['slno'] = "#";
$lang['routine_classes'] = "Class";
$lang['routine_select_classes'] = "Select Class";
$lang['routine_select_section'] = "Select Section";
$lang['routine_subject_select'] = "Select Subject";
$lang['routine_select_student'] = "Select Student";
$lang['routine_all_routine'] = "All Routines";
$lang['routine_section'] = "Section";
$lang['class_routine']='Class routine';
$lang['fields_marked']='Fields marked';
$lang['are_mandatory']='are mandatory';
$lang['routine_student'] = "Student";
$lang['routine_subject'] = "Subject";
$lang['routine_day'] = "Day";
$lang['routine_s_to'] = "To";
$lang['routine_start_time'] = "Starting Time";
$lang['routine_end_time'] = "Ending Time";
$lang['routine_room'] = "Room";
$lang['routine_note'] = "Note";


$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */
$lang['add_routine'] = 'Add Routine';
$lang['update_routine'] = 'Update Routine';

$lang['sunday'] = "SUNDAY";
$lang['monday'] = "MONDAY";
$lang['tuesday'] = "TUESDAY";
$lang['wednesday'] = "WEDNESDAY";
$lang['thursday'] = "THURSDAY";
$lang['friday'] = "FRIDAY";
$lang['saturday'] = "SATURDAY";
$lang['sample_routine_file'] = "Download Sample Routine File Here..";
$lang['please_download']='Please download excel template to know which fields to register. Remember to put all information as correct as possible.';
return $lang;