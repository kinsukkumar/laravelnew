<?php

/* List Language  */
$lang['panel_title'] = "Section";
$lang['add_title'] = "Add section(s)";
$lang['slno'] = "#";
$lang['section_name'] = "Section";
$lang['section_category'] = "Category";
$lang['section_info'] = "Section Information";
$lang['fields_marked'] = "Fields marked";
$lang['are_mandatory'] = "are mandatory";
$lang['class_stream']='Class stream eg A';
$lang['category']='eg Science class';
$lang['section_classes'] = "Class";
$lang['section_teacher_name'] = "Teacher Name";
$lang['section_note'] = "Note";
$lang['action'] = "Action";

$lang['section_select_class'] = "Select Class";
$lang['section_select_teacher'] = "Select Teacher";

$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['classlevel'] = 'Select Class Level';
$lang['class'] = 'Class';
$lang['academic_year'] = 'Academic Year';

/* Add Language */

$lang['add_class'] = 'Add Section';
$lang['update_class'] = 'Update Section';
$lang['menu_error_section_used'] = 'Section can not be deleted because is used in students data';
return $lang;