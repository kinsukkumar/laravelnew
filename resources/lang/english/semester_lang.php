<?php

/**
 * Description of classlevel_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$lang['panel_title']='Term';
$lang['add_title']='Add Term';
$lang['name']='Term';
$lang['start_date']='Start Date';
$lang['end_date']='End Date';
$lang['semster/term']='Semester/Term eg First Term';
$lang['semester_info']='School Term information';
$lang['fields']='Fields marked';
$lang['mandatory']='are mandatory';
$lang['note']='Notes';
$lang['action']='Action';
$lang['slno']='#';

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['exam_select_year']='Select Year';
$lang['select_semester']='Select Term';

/* Add Language */

$lang['class_level'] = 'Class Level';
$lang['academic_year'] = 'Academic Year';
$lang['update_level'] = 'Update Term';
return $lang;