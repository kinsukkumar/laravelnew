<?php

/* List Language  */
$lang['panel_title'] = "SMS Summary";
$lang['smssettings_username'] = "Api Secret";
$lang['smssettings_type'] = "Type";
$lang['smssettings_api_key'] = "Api Key";
$lang['pending_email']='Pending email';
$lang['pending_SMS']='Pending SMS';
$lang['sent_email']='Sent email';
$lang['sent_SMS']='Sent SMS';
$lang['summary']='Summary';
$lang['information']='If you want to enable/disable email & SMS to be sent, go to "settings tab". You have been registered to use karibuSMS application to easily send SMS. For more information, visit ';
$lang['smssettings_select_type'] = "Select Type";
$lang['smssettings_smartphone'] = "Smart Phone";
$lang['smssettings_internetsms'] = "Internet SMS";

$lang['save'] = "Save";
return $lang;




