<?php

/* List Language  */
$lang['panel_title'] = "Student Characters";
$lang['action'] = "Action";
$lang['assign_title'] = 'Assign Character';
$lang['subject_achievement'] ='Achievement';
$lang['subject_effort'] = 'Effort';
$lang['class'] = 'Class';
$lang['character'] ='Character';
$lang['add_remark'] ='Remark';
$lang['classlevel'] = 'Class Level';
$lang['academic_year'] = 'Academic Year';
$lang['semester'] = 'Semester';
$lang['character_category'] = 'Category';
$lang['character_id'] = 'Character';
$lang['teacherID'] = 'Teacher';
$lang['teacherID_select'] = 'Select Teacher';
$lang['edit_teacher_character'] ='Change Teacher';
$lang['remark_effort'] = 'Effort';
$lang['remark_achievement'] = 'Achievement';
$lang['remark_non_subject'] = 'Remark';
$lang['student_character_remark'] = 'Remark to: ';
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['edit_character'] = 'Edit Character';
$lang['slno'] = '#';
$lang['character_code'] = 'Character Code';
$lang['description'] = 'Character';
$lang['total'] = 'Total';
$lang['classes'] = 'Class';

$lang['alert_student_msg'] = 'Student Not Assigned';
$lang['alert_assess_msg'] = 'Assign Now';
$lang['alert_success'] = 'Success';
$lang['alert_error'] = 'Failed';

$lang['teacher_name'] = 'Teacher Name';

/* Add Language */
$lang['assign_student_characters'] = 'Assign ';
$lang['update_character'] = 'Update Character';
return $lang;