<?php

/* List Language  */
$lang['panel_title'] = "Student";
$lang['add_title'] = "Add student(s)";
$lang['slno'] = "#";
$lang['location'] = 'Location';
$lang['student_photo'] = "Photo";
$lang['student_name'] = "Name";
$lang['student_email'] = "Email";
$lang['student_dob'] = "Date of Birth";
$lang['student_jod'] = "Date of joining";
$lang['student_jod'] = "Joining Date";
$lang['student_sex'] = "Gender";
$lang['student_sex_male'] = "Male";
$lang['student_sex_female'] = "Female";
$lang['student_religion'] = "Religion";
$lang['close']='Close';
$lang['class name']='Class name';
$lang['academic_year']='Academic year';
$lang['number of students']='Number of students';
$lang['number of sections']='Number of sections';
$lang['download']='Download';
$lang['select_city']='Choose city';
$lang['Religion']='Religion';
$lang['reg_no']='Enter registration number';
$lang['health_condition']='Health condition';
$lang['status']='Health status';
$lang['view_profile']='View profile';
$lang['name']='Both names eg Jane Samson';
$lang['nationality']='Choose nationality';
$lang['reg_status']='Registration status';
$lang['student_status']='Student Status';
$lang['new_entry']='New entry';
$lang['joining']='Joining';
$lang['parent/guardian_info']='Parent/Guardian information';
$lang['parent_ype']='Choose parent type';
$lang['parent_name']='Name';
$lang['parent_profession']='Profession';
$lang['other_phone']='Other Phone';

$lang['previous_records']= 'Student\'s previous academic records';
$lang['submit']='Submit';
$lang['file']='Download sample file here';
$lang['upload_records']='Upload students from excel file';
$lang['auto_generate']='Auto Generate';
$lang['phy_cond']='Select physical condition';
$lang['health_status']='Select health status';
$lang['student_insurance']='Health Insurance';
$lang['health_insurance']='Choose what is applicable';
$lang['student_phone'] = "Phone";
$lang['student_address'] = "Address";
$lang['student_classes'] = "Class";
$lang['student_roll'] = "Roll/RegNo";
$lang['student_username'] = "Username";
$lang['physical_condition']='Physical condition';
$lang['student_registration']='Student Registration';
$lang['sregistration']='Student';
$lang['registration']='admission';
$lang['iregistration']='Student registration information';
$lang['eregistration']='Enter basic student information';
$lang['student_password'] = "Password";
$lang['student_select_class'] = "Select Class";
$lang['student_guardian'] = "Guardian";
$lang['student_city'] = 'City';
$lang['classlevel'] = 'Class Level';
$lang['academic_year'] = 'Academic Year';

/* Parent */
$lang['parent_guargian_name'] = "Guardian";
$lang['parent_father_name'] = "Father's Name";
$lang['parent_mother_name'] = "Mother's Name";
$lang['parent_father_profession'] = "Father's Profession";
$lang['parent_mother_profession'] = "Mother's Profession";

$lang['guardian_employer'] = "Employer Name";
$lang['parent_email'] = "Email";
$lang['parent_phone'] = "Phone";
$lang['parent_address'] = "Address";
$lang['parent_username'] = "Username";
$lang['parent_error'] = "Parents have not been added yet! Please add parents information.";


$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['parent'] = 'Add a parents';
$lang['pdf_preview'] = 'PDF Preview';
$lang['idcard'] = 'ID Card';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email sent successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!';

/* Add Language */
$lang['student_information'] = "Student Information";
$lang['parents_information'] = "Parents Information";
$lang['add_student'] = 'Add Student';
$lang['add_parent'] = 'Add Parents';
$lang['update_student'] = 'Update Student';
$lang['register_student'] = 'Registration';
$lang['excel_student'] = 'Excel Registration';
$lang['relation']='Relation';
$lang['date_registered']='Date Registered';

$lang['student_section'] = 'Section/Stream';
$lang['student_select_section'] = 'Select Section/stream';
$lang['student_distance_from_school']='Distance From School';
$lang['student_other_info']='More Info';
$lang['student_all_students'] = 'All Students';
$lang['parent_type']='Parent';
$lang['student_health']='Health Status';
$lang['other_health_problem']='Health note';
$lang['student_nationality']='Nationality';
$lang['student_admitted_from']='Previous School Name';
$lang['student_region']='Region';
$lang['student_reg_number']='Index Number';
$lang['student_year_finished']='Year Finished';
$lang['student_division']='Division';$lang['student_point']='Points';
$lang['student_math']='Maths';$lang['student_physics']='Physics';
$lang['student_chemistry']='Chemistry';$lang['student_biology']='Biology';
$lang['student_geograph']='Geograph';$lang['student_history']='History';
$lang['student_kiswahili']='Kiswahili';$lang['student_english']='English';
$lang['student_civics']='Civics';$lang['student_bk']='Book Keping';
$lang['student_commerce']='Commerce';$lang['student_other']='List of subjects with grades';
$lang['report_information']='Exams Reports'; 

$lang['interview_title']='Send Interview Notice';
$lang['time']='Time';
$lang['date']='Date';
$lang['final_student']='Final Students';

$lang['total_students']='Total Students';
$lang['boys']='Boys';
$lang['girls']='Girls';
$lang['subject_type']='Subject Type';
$lang['teacher']='Teacher Name';
//INVOICE

$lang['invoice_notpaid'] = "Not Paid";
$lang['invoice_partially_paid'] = "Partially Paid";
$lang['invoice_fully_paid'] = "Fully Paid";

//export excel
$lang['first_name']=$lang['firstname']='First Name';
$lang['middle_name']=$lang['middlename']='Middle Name';
$lang['last_name']=$lang['lastname']='Last Name';
$lang['sex']='sex';
$lang['dob']='Date of Birth';
$lang['dor']='Date of Registration';
$lang['year']='Year';
$lang['religion']='Religion';
$lang['physical']='Phyisical Condition';

$lang['employer']="Employer";
$lang['upload_graduate']="Upload Graduate";
$lang['graduate_registration']="Graduate Registration section";
$lang['occupation']="Occupation";
$lang['profession']='Profession';
$lang['advance_payment']='Advance Amount';

//Exam

//Unsubscribe
$lang['unsubscribe']='Unsubscribe';
$lang['submit_particulars']='Parent has submitted all transfer particulars ?';

//Student Prem View
$lang['physical']="Physical Impairment";
$lang['hearing']="Hearing Impairment";
$lang['disorder']="Disorder";
$lang['blind']="Blind";
$lang['intellectual']="Intellectual Impairment";
$lang['lowvision']="Low Vision";
$lang['sex']="Sex";
$lang['school']="School";
$lang['region']="Region";
$lang['district']="District";
$lang['number']="Number";
$lang['name']="Name";
$lang['male']="Male";
$lang['female']="Female";

$lang['student_info']="Student Information";
$lang['parent_info']="Parent Information";


$lang['specify']="Please specify correctly class and bank name associated with this fee";
$lang['bank_name'] = 'Bank Name';
$lang['classes'] = 'Classes';
$lang['feename'] = 'Fee Name';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['expense_revenue'] = 'Expense Vs Revenue';
$lang['no_invoice'] = 'Student without Invoices';
$lang['no_payments'] = 'Students with No Payments';
$lang['payments_received'] = 'Invoice Payments Received';
$lang['revenue_received'] = 'Revenue Received';
$lang['summary'] = 'Summary';
$lang['today_collect'] = 'Amount Collected Today';
$lang['week_collect'] = 'Amount Collected Week';
$lang['month_collect'] = 'Amount Collected Month';
$lang['expense'] = 'Expense';
$lang['revenue'] = 'Revenue';
$lang['project_collect'] = 'Collection Projection';
$lang['expense_project'] = 'Expense Projection';
$lang['expected_amount'] = 'Expected Amount';

$lang['exam_report'] = 'Exam Reports';
$lang['payment_report'] = 'Payment Reports';
$lang['parent_info'] = 'Parent Informations';
$lang['student_info'] = 'Student Information';
$lang['allocations'] = 'Allocations';
$lang['select'] = 'Select Class to view exam results';
$lang['schoolreport'] = 'School Accumulative Reports';
$lang['singlereport'] = 'Single Exam Report';
$lang['payment_record'] = 'Payment Records';
$lang[''] = '';

$lang['number']='Number';
$lang['name']='Name';
$lang['print_all_front_portrait']='Print all front portrait';
$lang['print_all_front_landscape']='Print all front landscape';
$lang['print_all_back_landscape']='Print all Back landscape';

$lang['students_upload'] = "This part allows you to upload both student and parent information in one place with only basic records";
$lang['students_uploads'] = "This part allows you to upload all student information ";
return $lang;



