<?php

/* List Language  */
$lang['panel_title'] = "Subject";
$lang['add_title'] = "Add subject(s)";
$lang['specify']='Choose subject teacher';
$lang['GUIDANCE']='GUIDANCE';
$lang['add_teacher']='Ad teacher';
$lang['class_name']='Class name';
$lang['this']='This part Helps to define Teachers of the subject to a class sections,';
$lang['subject']='Subject';
$lang['sections']='Section';
$lang['teachers']='Teacher(s)';
$lang['add_class_subject']="Add class subject";
$lang['slno'] = "#";
$lang['subject_class_name'] = "Class Name";
$lang['subject_teacher_name'] = "Teacher Name";
$lang['subject_student'] = "Student";
$lang['subject'] = "Subject";
$lang['fields_marked'] = "Fields marked";
$lang['subject_name'] = "Subject name eg Mathematics";
$lang['are mandatory'] = "are mandatory";
$lang['core']='Core';
$lang['option']='Option';
$lang['advanced']='Advanced';
$lang['advanced_subject_settings']='Advanced subject settings';
$lang['subject_settings1']='Choose whether this subject is counted in the final student average or not.';
$lang['subject_settings2']='Choose whether this subject has penalty or not (eg. GS  & Advanced Math in A-Level).';
$lang['is_counted']='Is Counted in Average ?';
$lang['YES']='YES';
$lang['NO']='NO';
$lang['included']='Included in Division (CSEE & ACSEE only)?';
$lang['penalty']='Has penalty ?';
$lang['subject_register']='If you dont see your subject here go to subject menu and register subject first';
$lang['register']='Subject register';
$lang['subject_info'] = "Subject Information";
$lang['subject_name'] = "Subject Name";
$lang['subject_type'] = "Subject Type";
$lang['subject_code'] = "Subject Code";
$lang['subject_teacher'] = "Teachers";
$lang['subject_classes'] = "Class";
$lang['select_all'] = "All";
$lang['subject_select_class'] = "Select Class";
$lang['class_level'] = "Select Class Level";
$lang['subject_select_section'] = "Select Section";
$lang['subject_select'] = "Select Subject";
$lang['subject_select_classes'] = "Select Class";
$lang['subject_select_teacher'] = "Select Teacher";
$lang['subject_select_student'] = "Select Student";
$lang['exam_select_year'] = "Select Year";

$lang['penaltity'] = "Has Penalt";
$lang['is_counted_ave'] = 'Is Counted in Average';
$lang['is_counted_div'] = 'Is Counted in Division';
$lang['pass_mark'] = 'Pass Mark';

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['close'] = 'Close';

/* Add Language */

$lang['add_subject'] = 'Add subject';
$lang['update_subject'] = 'Update subject';
$lang['add_section_subject'] = 'Add Section Subject Teacher';
$lang['section_subject'] = 'Section Subject Teacher';
$lang['academic_year']='Academic Year';
$lang['photo']='Photo';
$lang['employment_type']='Employment Type';

$lang['arrangement']='Arrangement';

$lang['register_subject']='Register Subject';
$lang['register_excel']='Register Subjects by Excel';
$lang['register_necta']='Register Subjects From NECTA';
$lang['please_download'] = 'Please download excel template to know which fields to register. Remember to put all information as correct as possible.';
$lang['download_sample'] = 'Download Sample File here..';
$lang['use_necta'] = 'Use Standard Default Subjects From NECTA';
$lang['click_necta'] = 'Click here to import from Default NECTA Subjects';
return $lang;