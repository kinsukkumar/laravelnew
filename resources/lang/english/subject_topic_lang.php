<?php

/* List Language  */
$lang['panel_title'] = "Subject_topic";
$lang['add_title'] = "Add subject_topic(s)";
$lang['add_class_subject_topic']="Add class subject_topic";
$lang['slno'] = "#";
$lang['subject_class_name'] = "Class Name";
$lang['subject_topic_teacher_name'] = "Teacher Name";
$lang['subject_topic_student'] = "Student";
$lang['subject_name'] = "Subject Name";
$lang['subject_topic_type'] = "Subject_topic Type";
$lang['edit_subject']='Edit subject';
$lang['class_level *']='Class Level *';
$lang['academic_year *']='Academic year *';
$lang['Name of Topic']='Name of Topic';
$lang['Students']='Students';
$lang['subject_topic_code'] = "Subject_topic Code";
$lang['academic_select_year'] = "Select Academic year";
$lang['class'] = "Classes";
$lang['subject_help']='This part Helps to define topics for the subject to a class,';
$lang['GUIDANCE']='GUIDANCE';
$lang['click_to_add']='Click to add';
$lang['enter_topics']='Enter topics';
$lang['no_subjects']='No subject(s) have been defined for this class. Please ';
$lang['no_semester']='No semester(s) been added in this level. Please';
$lang['add_semester(s)']='add_semester(s)';
$lang['subject_add']='add subject for this class';
$lang['subject_topic_classes'] = "Class";
$lang['semester'] = "Term/semester";
$lang['select_all'] = "All";
$lang['subject_topic_select_class'] = "Select Class";
$lang['subject_topic_select_section'] = "Select Section";
$lang['subject_topic_select'] = "Select Subject_topic";
$lang['subject_select_classes'] = "Select Class";
$lang['subject_topic_select_teacher'] = "Select Teacher";
$lang['subject_topic_select_student'] = "Select Student";
$lang['topic_subject'] = "Subject";
$lang['select_topic_subject'] = "Subject";

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['close'] = 'Close';

/* Add Language */

$lang['add_subject_topic'] = 'Add subject_topic';
$lang['update_subject_topic'] = 'Update subject_topic'; return $lang;