<?php

$lang['syllabus'] =$lang['panel_title']= "Syllabus";
$lang['add_syllabus'] = 'Add Syllabus';
$lang['select_classes'] = 'Select Classes';
$lang['slno'] = '#';
$lang['topic'] = strtoupper('Topic');
$lang['topics'] = 'Topics';
$lang['academic_year'] = 'Academic Year';
$lang['add_topic'] = 'Add Topic';
$lang['subtopic'] = strtoupper('Sub Topic');
$lang['sub_topics'] = 'Sub Topics';
$lang['add_sub_topic'] = 'Add Sub Topic';
$lang['specific_objective'] = strtoupper('Specific Objective');
$lang['objectives'] = 'Objectives';
$lang['add_obj_group'] = 'Add Objective';
$lang['code'] = 'Code';
$lang['objective'] = 'Objective';
$lang['title'] = 'Title';
$lang['remark'] = 'Remark';
$lang['activity'] = 'Activity';
$lang['resource'] = 'Resource';
$lang['time'] = 'Time';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['syllabus_report'] = 'Syllabus reports';


$lang['teaching_and_learning_activity'] = 'TEACHING AND LEARNING STRATEGIES';
$lang['teaching_and_learning_resources'] = 'TEACHING AND LEARNING RESOURCES';
$lang['reference'] = strtoupper('References');
$lang['assessment'] = 'ASSESSMENT';
$lang['no_of_periods'] = 'NUMBER OF PERIODS';
$lang['action']='action';
$lang['mark_select_subject']='Select Subject Here';

$lang['grade']='Grade';
$lang['points']='Points';
$lang['grade_remark']='Remark';
$lang['description']='Description';
$lang['edit_benchmark']='Edit Benchmark';
$lang['add_benchmark']='Add Benchmark';
$lang['add_title'] = "Add new Benchmark";
return $lang;
