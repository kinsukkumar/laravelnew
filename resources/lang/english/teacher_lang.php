<?php

/* List Language  */
$lang['panel_title'] = "Teacher";
$lang['add_title'] = "Add teacher(s)";
$lang['slno'] = "#";
$lang['teacher_photo'] = "Photo";
$lang['teacher_name'] = "Name"; 
$lang['teacher_designation'] = 'Designation';
$lang['teacher_email'] = "Email";
$lang['teacher_dob'] = "Date of Birth";
$lang['student_employment']='Choose employment type';
$lang['email_eg']='Valid email eg samson@yahoo.com';
$lang['no_eg']='eg 655406004';
$lang['mail_eg']='eg P.O BOX 33287, Dar';
$lang['upload_teacher']='Upload teachers from excel file';
$lang['file_download']='Download sample file here';
$lang['teacher_sex'] = "Gender";
$lang['teacher_sex'] = "Sex";
$lang['designation'] = "Designation";
$lang['teacher_sex_male'] = "Male";
$lang['submit']='Submit';
$lang['teacher_sex_female'] = "Female";
$lang['both_names']='Both names';
$lang['teacher_religion'] = "Religion";
$lang['student_nationality'] = "Nationality";
$lang['teacher_info'] ='Teacher information';
$lang['fields']='Fields marked';
$lang['marked']='are manadatory';
$lang['teacher_id'] = "ID number";
$lang['physical_condition'] ='Physical condition';
$lang['phy_cond'] ='Choose physical condition';
$lang['teacher_phone'] = "Phone";
$lang['teacher_address'] = "Address";
$lang['teacher_jod'] = "Joining Date";
$lang['teacher_username'] = "Username";
$lang['employment_type'] = "Employment type";
$lang['teacher_password'] = "Password";
$lang['employment_type'] = "Employment Type";
$lang['education']='Education';
$lang['health_status']='Health status';
$lang['health_insurance']='Health Insurance';


$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['print'] = "Print";
$lang['pdf_preview'] = "PDF Preview";
$lang['idcard'] = "ID Card";
$lang['mail'] = "Send Pdf to Mail";

/* Add Language */
$lang['personal_information'] = "Personal Information";
$lang['add_teacher'] = 'Add Teacher';
$lang['update_teacher'] = 'Update Teacher';

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email sent successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!';

$lang['exam']='Exam Name';
$lang['max_score']='Max Score';
$lang['min_score']='Min Score';
$lang['average']='Average';
$lang['class']='Class';
$lang['user_contract']=' Contract End Date';
$lang['payroll_status'] = " Payroll Status";

 return $lang;