<?php

/* List Language  */
$lang['panel_title'] = "Member";
$lang['panel_title_profile'] = "Profile";
$lang['slno'] = "#";
$lang['tmember_photo'] = "Photo";
$lang['tmember_name'] = "Name";
$lang['tmember_section'] = "Section";
$lang['tmember_roll'] = "Roll";
$lang['tmember_email'] = "Email";
$lang['tmember_phone'] = "Phone";
$lang['tmember_tfee'] = "Transport Fee";
$lang['tmember_route_name'] = "Route Name";
$lang['tmember_classes'] = "Class";
$lang['tmember_select_class'] = "Select Class";
$lang['classes_select_route_name'] = "Select Route";
$lang['tmember_message'] = "You are not added.";

$lang['tmember_joindate'] = "Join Date";
$lang['tmember_dob'] = "Date of Birth";
$lang['tmember_sex'] = "Gender";
$lang['tmember_religion'] = "Religion";
$lang['tmember_address'] = "Address";
$lang['tmember_select_route'] = "Select Route";
$lang['tmember_route'] = "Route";
$lang['tmemmber_transport_routes']='Transport Routes Imports';
$lang['tmember_import_routes']='Import Transport Routes';
$lang['tmember_transport_sample']='Download sample Excel File here, fill all information as specified in this Excel file, then Import it here';
$lang['tmember_file']='Import Files';
$lang['tmember_information']='Information:';
$lang['tmember_choose']='Choose the Excel File and upload it here';
$lang['tmember_download']='Download Sample File Here:';
$lang['tmember_download_excel']='Download Excel';
$lang['upload_member_excel']='Upload Members From Excel';
$lang['tmember_id']='ID';
$lang['tmember_name']='Name';
$lang['tmember_way']='Ways';
$lang['tmember_route_name']='Route Name';
$lang['tmember_amount']='Total Amount';
$lang['tmember_discount']='Discount';
$lang['tmember_discount_amount']='Discounted Amount';
$lang['tmember_vehicle']='Vehicles';
$lang['tmember_action']='Action';



$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['tmember'] = 'Transport';
$lang['delete'] = 'Delete';
$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

$lang['personal_information'] = "Personal Information";

$lang["add_tmember"] = "Add Member";
$lang["update_tmember"] = "Update Member";

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email sent successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!'; return $lang;