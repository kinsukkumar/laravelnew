<?php

/* List Language  */
//$lang['language'] = "Language";
$lang['profile'] = "Profile";
$lang['change_password'] = "Change Password";
$lang['definitions'] = 'Definitions';
$lang['religions'] = "List of religions";
$lang['countries'] = 'Countries';
$lang['cities'] = "Cities";
$lang['messages'] = 'Messages';
$lang['health_conditions'] = "Health conditions";
$lang['physical_disabilities'] = "Physical disabilities";
$lang['teacher_education_level'] = 'Teacher education level';
$lang['payment_reminder_template'] = 'Payment reminder template';
$lang['health_insurances'] = "Health insurances";
$lang['religions'] = "List of religions";
$lang['occupations'] = "Occupations";
$lang['paid_amount']='Paid Amount';
$lang['employment_types'] = "Employment types";
$lang['select_role'] = "Select role";
$lang['role_specified'] = "Role specified";
$lang['user_role'] = "User role";
$lang['available_roles'] = "Available roles";
$lang['advance_amount']='Advance Amount';
$lang['school_address']='School Address';
$lang['make_sure']='Make sure you are using your registered username or reset password above, otherwise contact the school at';
$lang['login_desc1']='Log in to your account to continue.';
$lang['login_desc2']='If you have forgotten or you don\'t have password, please click on forgot password or key symbol above to get it';
$lang['login_social']='Or Login with information provided to you';
$lang['enter_characters']='Please enter the characters you see above';
$lang['cannot_login']='I cannot login';
$lang['forget_password']='I forgot my password';
$lang['role'] = "Role";
$lang['option'] = "Option";
$lang['add_new_role'] = "Add new role";
$lang['no_role'] = 'No role specified. Specified it to load permission';
$lang['panel_title'] = "Password";
$lang['password'] = "Password";
$lang['old_password'] = "Enter Old Password";
$lang['new_password'] = "Enter New Password";
$lang['panel'] = "Character";
$lang['Select Academic year'] =$lang['select_year']= 'Select Academic year';
$lang['academic_year'] =$lang['select_academic_year']= 'Academic year';
$lang['attendance_report'] =$lang['select_attendance_period']= 'Period';
$lang['setting'] = 'System Definitions and Constants Used ';
$lang['Select_character'] = 'Select character';
$lang['Select class'] =$lang['select_class']= 'Select class';
$lang['Select Semester'] = 'Select Term';
$lang['re_password'] = "Retype New Password";
$lang['welcome'] = 'Welcome';
$lang['password'] = 'Password';
$lang['lost_password'] = "Click here to get a new password";
$lang['logout'] = "Log out";
$lang['language'] = "Change Language";
$lang['account_setting'] = 'Setting';
$lang['exams_settings']='Setting';
$lang['menu_exam_groups']='Exam Groups';
$lang['exams_class_allocation']='Class Allocation';
$lang['menu_exam_report']='Reports';
$lang['menu_accumulative_report']='Combined Reports';
$lang['fees'] = 'Fees';
$lang['official_report'] = 'Official report';
$lang['academy'] = 'Academy';
$lang['subject_topics'] = 'Subject topics';
$lang['topic_mark'] = 'Topic mark';
$lang['exam_mark'] = 'Exam mark';
$lang['assesment'] = 'Assesment';
$lang['transport_members'] = 'Transport members';
$lang['transport_member_details'] = 'Member Details';
$lang['transport_assign_member'] = 'Members';
$lang['upload_member_excel']='Upload Members From Excel';
$lang['menu_vehicle'] = 'Vehicle';
$lang['installment'] =$lang['select_installment']= 'Installments';
$lang['register_fee_details'] = 'Register fee structure';
$lang['fee_details'] = 'Fee Structure';
$lang['unsubscribe_student'] = 'Unsubscribe student';
$lang['student_reports'] = 'Student reports';
$lang['payment'] = 'Payment';
$lang['parent_invoice'] = 'Parent invoice';
$lang['due_amount'] = 'Due amount';
$lang['print_reminder'] = 'Schedule';
$lang['other_accounts'] = 'Other accounts';
$lang['fixed_assets'] = 'Fixed assets';
$lang['current_assets'] = 'Current assets';
$lang['liabilities'] = 'Liabilities';
$lang['capital'] = 'Capital';
$lang['account_reports'] = 'Account reports';
$lang['income_statement'] = 'Income statement';
$lang['cash_flow'] = 'Cash Flow Statement';
$lang['balance_sheet'] = 'Balance sheet';

$lang['finacial_statement'] = 'Finacial statement';
$lang['balance_report'] = 'Balance Report';
$lang['payment_report'] = 'Payment Reports';
$lang['class_student'] = "Class Teacher Student";


$lang['sms_parents'] = 'SMS parents';
$lang['add_fees'] = 'Definition';
$lang[''] = 'Balance sheet';
$lang['view_more'] = "See All Notifications";
$lang['la_fs'] = "You have";
$lang['la_ls'] = "notifications";
$lang['close'] = "Close";
$lang['username'] = "Username";
$lang['name']='Name';
$lang['roll']='Admission Number';
$lang['or'] = "or";
$lang['phone'] = "Phone Number";
$lang['login_hint'] = "Login";
$lang['login_btn'] = "LOG IN";
$lang['forgot_password']='Forgot password';
$lang['questions']='Questions & Answers';
$lang['bulk_invoice'] = "Bulk Invoice";
$lang['bank_account'] = "Bank Account";
//reset password//
$lang['reset_password'] = 'Reset Password';
$lang['reset_calculate'] = 'Calculate';
$lang['reset_answer'] = 'Answer';
$lang['reset_phone'] = 'Your Phone Number or Email';
$lang['email']='Email';
$lang['website']='Website';

$lang['reset_email'] = 'Your Email';
$lang['reset_btn'] = 'SEND';
$lang['reset_backtologin'] = 'Back to Login';
$lang['reset_entercodes'] = 'Enter Verification Codes';
$lang['reset_complete'] = 'Complete';
$lang['reset_send'] = 'Email and SMS  have been sent! . Wait for a few seconds to receive SMS or check your email';

$lang['Admin'] = "Admin";
$lang['Student'] = "Student";
$lang['Parent'] = "Parent";
$lang['Teacher'] = "Teacher";
$lang['Accountant'] = "Accountant";
$lang['Librarian'] = "Librarian";

$lang['Laboratorian'] = "Laboratory Technician";
$lang['Driver'] = "Driver";
$lang['Guards'] = "Guards";
$lang['Matron'] = "Matron";
$lang['Patron'] = "Patron";
$lang['Cook'] = "Cook";
$lang['Nurse'] = "Nurse";
$lang['Secretary'] = 'Secretary';
$lang['Other'] = "Other";

$lang['success'] = "Success!";
$lang['cmessage'] = "Your password is changed.";

/* Menu List */
$lang['menu_add'] = 'Add';
$lang['menu_edit'] =$lang['edit'] = 'Edit';
$lang['menu_view'] = 'View';
$lang['menu_success'] = 'Success';
$lang['menu_dashboard'] = 'Dashboard';
$lang['menu_student'] = 'Students';
$lang['menu_parent'] = $lang['menu_parents'] = 'Parents';
$lang['menu_teacher'] = 'Teachers';
$lang['menu_user'] = 'Users';
$lang['menu_staff'] = 'Supporting Staff';
$lang['menu_classes'] = 'Class';
$lang['menu_group'] = 'Account Groups';
$lang['menu_section'] =$lang['single_section']= 'Section';
$lang['student_section'] = "Section";
$lang['menu_subject'] = 'Subjects';
$lang['menu_subject_list'] = 'List of Subjects';
$lang['class_subject'] = 'Class Subject';
$lang['section_subject_teacher'] = 'Section Subject Teacher';

$lang['menu_grade'] = 'Grading';
$lang['special_grade_names'] = 'Special Grade Name';
$lang['special_grade'] = 'Special Grading';
$lang['add_special_grade_name'] = 'Add Special Grade Name';
$lang['add_special_grade'] = 'Add Special Grade';
$lang['select_special_grades'] = 'Select Special Grade Name';
$lang['default_grade']='Default Grading';



$lang['menu_classlevel'] =$lang['class_level'] =$lang['select_class_level']= 'Class Levels';
$lang['menu_exam'] = 'Exam';
$lang['menu_exam_definition']='Exam Definition';
$lang['menu_school_exam'] = 'School Exams';
$lang['menu_examschedule'] = 'Exam Schedule';
$lang['menu_library'] = 'Library';
$lang['menu_mark'] = 'Mark';
$lang['menu_routine'] = 'Routine';
$lang['menu_attendance'] = 'Attendance';
$lang['menu_parent_invoice'] = "Parent invoice";
$lang['menu_member'] = 'Members';
$lang['menu_books'] = 'Books';
$lang['menu_issue'] = 'Issue';
$lang['menu_fine'] = 'Fine';
$lang['menu_profile'] = 'Profile';
$lang['view_profile'] = 'View Profile';
$lang['edit_profile'] = 'Edit Profile';
$lang['menu_transport'] = 'Transport';
$lang['menu_transport_routes']='Transport Routes';
$lang['menu_hostel'] = 'Hostel';
$lang['menu_category'] = 'Category';
$lang['menu_account'] = 'Account';
$lang['menu_account_new'] = 'Account';
$lang['menu_feetype'] = 'Fee Type';
$lang['menu_feeclass'] = 'Fee Class';
$lang['menu_setfee'] = 'Set Fee';
$lang['menu_balance'] = 'General Balance';

$lang['menu_detailed_balance'] = 'Detailed Balance';
$lang['menu_paymentsettings'] = 'Payment Settings';
$lang['menu_expense'] = 'Expense';
$lang['menu_revenue']='Revenue';
$lang['menu_transactions']='Transactions';
$lang['menu_notice'] = 'School Calendar';
$lang['character_report'] = 'Reports';
$lang['menu_report'] = 'Single Reports';
$lang['menu_other_report']='Other Reports';
$lang['menu_setting'] = 'Settings';
$lang['menu_setting_general'] = 'System Settings';
$lang['menu_setting_definition'] = 'Definition';
$lang['menu_setting_guide'] = 'User Guide';
$lang['menu_signature'] = 'Signature';
$lang['menu_semester'] = 'Terms';
$lang['menu_mark_report'] = 'Exam Report';

$lang['accountant'] = 'Accountant';
$lang['librarian'] = 'Librarian';
$lang['help'] = 'Help';

$lang['upload'] = "Upload";


/* Start Update Menu */
$lang['menu_sattendance'] = 'Student Attendance';
$lang['menu_sattendance_report'] = 'Attendance Report';
$lang['menu_attendance_report'] = 'Attendance Reports';
$lang['menu_tattendance'] = 'Employee Attendance';
$lang['menu_eattendance'] = 'Exam Attendance';
$lang['menu_promotion'] = 'Promotion';
$lang['menu_media'] = 'e-Resources';
$lang['menu_message'] = 'Message';
$lang['menu_smssettings'] = 'SMS Summary';
$lang['accounting_setting'] = 'Account Setting';
$lang['payment_reminder_template'] = 'Reminder Template';
$lang['menu_mailandsms'] = 'Email / SMS';
$lang['menu_mailandsmstemplate'] = 'Template';
$lang['menu_invoice'] = 'Invoices';
$lang['menu_payment_history'] = 'Payments';
$lang['menu_combined_report'] = 'Official Report';
$lang['menu_create_report'] = 'Create Report';
$lang['menu_view_report'] = 'Reports Created';
$lang['payment_type']='Payment Type';

$lang['user_permission'] = 'User Permission';
$lang['user_roles'] = 'User Roles';
/**
 * Inventory navigation
 */
$lang['menu_inventory'] = 'Inventory';
$lang['menu_vendor'] = 'Vendors';
$lang['menu_stockregister'] = 'Stock Register';
$lang['menu_message'] = 'Message';
$lang['menu_library_report'] = 'Library Report';
$lang['general_report'] = 'General Report';
$lang['other_report'] = 'Other Report';
$lang['menu_discount'] = 'Discount';
$lang['menu_amount_due'] = 'Amount Due';
$lang['menu_parent_invoice'] = 'Parent Invoice';
$lang['menu_student_invoice'] = 'Student Invoice';
$lang['menu_payment_reminder'] = 'Payment Reminder';
$lang['menu_payment'] = 'Payment';
$lang['menu_global_exchange']='Global Exchange';

/**
  character
 */
$lang['menu_characters'] = 'Character';
$lang['menu_character_list'] = 'List of Characters';
$lang['menu_characters_sub'] = 'Characters';
$lang['menu_character_grades'] = 'Character Grades';
$lang['menu_character_categories'] = 'Categories';
$lang['menu_character_category'] = 'Category';
$lang['menu_assign'] = 'Assign';
$lang['menu_assess'] = 'Assess Student Character';
$lang['menu_general'] = 'General Assessment';
$lang['menu_general_head_teacher'] = "Head Teacher Assessment";
$lang['menu_receipt'] = 'Payment Receipt';
$lang['menu_misbehaviour'] = 'Misbehaviours';
$lang['administration'] = 'School Administration';
$lang['technical_support'] = 'Technical Support';
$lang['reach_support'] = 'Contact Our Support';
$lang['menu_chart_account'] = 'Chart of Accounts';
$lang['menu_vehicle']='Vehicle';
$lang['fee_priority'] = 'Fee Priority ';
$lang['id_number']='ID number';
$lang['salary']='Salary';
$lang['print']='Print';
$lang['menu_collection']='Fee Collection';
$lang['menu_graduate']='Graduates';

$lang['terms_for_admission']='Terms for Admission';
$lang['admission_status']= 'Admission Status';

$lang['menu_submit']='Submit';
$lang['menu_diary']=$lang['ediary']='Diary';
$lang['total']='Total';
$lang['total_amount']='Total Amount';
$lang['priority']='Priority';

$lang['exchange_rate']='Exchange Rate';
$lang['from_currency']='From Currency';
$lang['to_currency']='To Currency';
$lang['exchange_date']='Exchange Date';
$lang['global_exchange_rate']='Global Exchange Rate';
$lang['add_exchange_rate']='Add Exchange Rate';
$lang['select_subject']='Select Subject';

$lang['exam_name']='Exam Name';
$lang['exam_date']='Exam Date';
$lang['marking_status']='Marking Status';
$lang['view_report']='View Report';
$lang['view_graph']='View Graph';

//invoice
$lang['invoice_number']='Invoice number';
$lang['invoice_date']='Invoice Date';
$lang['total_paid']='Total Paid';
$lang['total_unpaid']='Total Unpaid';
$lang['payment_status']='Status';
$lang['reset_success']='Email & SMS have been sent to you successfully. Please wait within 5 minutes';

$lang['menu_reconciliation']='Reconciliation';
$lang['transaction_date']='Transaction Date';
$lang['user_upload']='Upload Users By Excel';
$lang['download']='Download';
$lang['upload_from_excel']='Upload From Excel File';
$lang['menu_syllabus']='Syllabus';
$lang['menu_syllabus_report']='Syllabus Reports';
$lang['menu_scheme_of_work']='Scheme of Work';
$lang['menu_lesson_plan']='Lesson Plan';
$lang['menu_benchmark']='Benchmark';
$lang['menu_inactive_user']='Inactive Users';
$lang['download']='Download';
$lang['upload_from_excel']='Upload From Excel File';

$lang['bankaccount_account_name']='Account Name';
$lang['menu_comment_per_student']='Students Characters';
$lang['menu_minor_exams']='Minor School Exams';

$lang['salaries']='Salaries';
$lang['menu_payroll'] = 'Payroll';
$lang['or']='or';
$lang['comment_per_student']='Comments per Student';
$lang['menu_invoice_guide']='Invoice Guide';
$lang['assessment_criteria']='Assessment Criteria';
$lang['remarks']='Remarks';
$lang['period']='Number of Periods';
$lang['parent_payment_submitted']= 'Payment Submitted successfully. You will be notified after the approval from school.';
$lang['unapproved_payments']='Parents Payments';
//$lang['no_single_report']='No exam marks haves been entered. Please call %s for more information ';
//$lang['no_official_report']='No official report has been published for this term. Please call %s for more information';
$lang['no_single_report']='';
$lang['no_official_report']='';

$lang['save']='Save';
$lang['summary']='Summary';
$lang['note']='Note';
$lang['status']='Status';

$lang['import_export']='Import/Export';
$lang['financial_category']='Financial Category';

$lang['opening_balance']='Opening Balance';
$lang['class']='Class';
$lang['select_classes']='Select Class';
$lang['menu_insight']='ShuleSoft Insight';
$lang['menu_default_passwords']='Default Passwords';
$lang['print_all_front']='Print All Front';
$lang['print_all_back']='Print All Back';

$lang['start_date']='Start Date';
$lang['end_date']='End Date';
$lang['amount']='Amount';
$lang['transaction_id']='Transaction ID';
$lang['recorded_by']='Recorded By';
$lang['date']='Date';
$lang['date_recorded']='Date Recorded';
$lang['bank']='Bank';
$lang['menu_parents_feedbacks']='Parents Feedback';

$lang['national_id']='National ID No';
$lang['nationality']='Nationality';
$lang['reminder_to']='Reminder To';
$lang['select_user']='Select User';

$lang['menu_inbox']='Inbox';

$lang['start_date']='Start Date';
$lang['level_info']='Class level information';
$lang['fields']='Fields marked';
$lang['mandatory']='are mandatory';
$lang['education_level']='Education level eg Secondary';
$lang['edit_level']='Update level';
$lang['notes']='Any notes about this level';
$lang['end_date']='End Date';
$lang['span_number']='Total No of Classes';
$lang['note']='Notes';
$lang['action']='Action';
$lang['slno']='#';
$lang['select name']='Select Name';

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['head_teacher_title']='Head Teacher Title';

/* Add Language */

$lang['add_level'] = 'Add Level';
$lang['result_format'] = 'Result Format';
$lang['update_level'] = 'Update Level';
$lang['menu_tod']='Teacher on Duty';

$lang['account_number']='Account Number';
$lang['leaving_certificates']='Leaving Certificates';

$lang['show_rank'] = "Show Rank Per Each Subject";
$lang['show_grade'] = "Show Grade Per Each Subject";
$lang['show_footer'] = "Show Footer with Total, AVG and Rank Per Each Subject";

$lang['subject_name']='Subject Name';
$lang['total_mark']='Total Marks';
$lang['subject_avg']='Subject Average';
$lang['gd']='GD';
$lang['rank']='Rank';
$lang['exam_name']='Exam Name';
$lang['class_name']='CLass Name';
$lang['exam_dates']='Exam Date';
$lang['summary']='Summary';
$lang['section']='SECTION';


$lang['ondate']='On Date';
$lang['page_visit']='Page Visit Action';
$lang['browser']='Browser';
$lang['device']='Device';
$lang['location']='Location';
$lang['activity'] ='Activity';

$lang['invoice_n']='Invoice Number';
$lang['total_amount']='Total Amount';
$lang['total_paid']='Total Paid';
$lang['unpaid']='Total Unpaid';
$lang['payment_status']='Payment Status';
$lang['payment_due']='Payment Due';
$lang['action']='Action';
$lang['parent_infos']='Parent Information';
$lang['sms']='SMS';
$lang['payments']='Payments';
$lang['students']='Students';
$lang['profile'] = 'User Profile';
$lang['study_days'] = 'Study Days';
$lang['section_info']='Section Information';

$lang['total_student']='Total Students';
$lang['created_at']='Created At';
$lang['created_by']='Created By';
$lang['last_update']='Last Update';
$lang['add_exam']='Add Exam';
$lang['signature']='Signature';
$lang['mark']='Mark';
$lang['student_name']='Student Name';
$lang['number_subject']='Number of Subject';
$lang['reply']='Reply';
$lang['employer type']='Employer Type';
$lang['basic_info']='Basic Information';
$lang['sent_sms']='Sent SMS';
$lang['sms_sent']='SMS';
$lang['class_level_id']='Class Level';
$lang['sex']='Sex';
$lang['dob']='Date of Birth';
$lang['sex']='Gender';
$lang['menu_inventory']='Inventory';
$lang['class_sign'] = "Class Teacher's Signature";
$lang['search_menu'] = "Search name,Invoice,Phone,Email,Reference number";
$lang['option']='Option';
$lang['select_date']='Click to Select Date';
$lang['subject_teachers']='Register Subject Teacher';
$lang['subject_teachers_by']='Register Subject Teacher by Excel';
$lang['submit']='Submit';
$lang['download_sample']='Download Sample File here.. ';
$lang['please_download']='Please download excel template to know which fields to register. Remember to put all information as correct as possible.';
$lang['Expense']='Expenses';
$lang['Summary']='Summary';
$lang['type']='Transaction Type';
$lang['expected_amount']='Expected Amount';
$lang['expense_project']='Expense Projection';
$lang['revenue_received']='Revenue Received';
$lang['payments_received']='Payments Received';
$lang['no_payments']='Number of Payments';
$lang['no_invoice']='Number of Invoices';
$lang['today_collect']='Today Collections';
$lang['week_collect']='Week Collections';
$lang['month_collect']='Mounthly Collections';
$lang['project_collect']='Projection Collections';
$lang['revenue']='Revenue';
$lang['expense_revenue']='Revenue and Expenses';
$lang['tra_month']='Transaction By Month';
$lang['total_amount']="Total Amount";
$lang['start_date']="Start Date";
$lang['end_date']="End Date";
$lang['comment-area']="Parent Feedback and Teacher Review on Student Progressive Report";
$lang['step_two'] = "Step 2 Parent Information";
$lang['step_three'] = "Step 3 Previous School records";
$lang['step_four'] = "Step 4 previous school subject results";

// ShuleSoft Insight Translation
$lang['age_st']="Student Age Distribution Name";
$lang['age_st']="Age";
$lang['age_number']="Student AgeNumber";
$lang['gender_dst'] = "Gender Distribution";
$lang['gender_over'] = "Gender Overview By Class Level and Classes";
$lang['gender_cls'] = "Gender Distribution By Classes";
$lang['total_student'] = " Total Students";
$lang['total_male'] = " Total Males";
$lang['total_female'] = " Total Females";
$lang['total'] = " Total";
$lang['male'] = "Male";
$lang['female'] = "Female";
$lang['gender_view'] = "Gender Overview";
$lang['physical'] = "Physical Conditions";
$lang['student_hel'] = "Student Health Insights";
$lang['no_student'] = "Number of students";
$lang['student_diff'] = "Number of Students with Different Physical Condition";
$lang['health_cond'] = "Health Condition";
$lang['health_diff'] = "Number of Students with Different Health Condition";
$lang['health_number'] = "Number of Students with Health Insurance";
$lang['health_ins'] = "Health Insurance";
$lang['health_status'] = " Current Health Status ";
$lang['status_no'] = "Number of Students with Health Status";
$lang['h_status'] = "Health Status";
$lang['previous_sch'] = "Previous School Insights";
$lang['previous_name'] = "Student Previous School Names Distribution";
$lang['schools'] = "Schools";
$lang['student_previous'] = "Student Previous School Types Distribution";
$lang['ownership'] = "Ownership";
$lang['previous_location'] = "Student Previous School Locations Distribution";
$lang['location'] = "Locations";
$lang['previous_district'] = "Student Previous School District Distribution";
$lang['district'] = "District";
$lang['student_religion'] = "Student Religion Insights";
$lang['no_religion'] = "Number of Students with Religion";
$lang['religion'] = "Religion";
$lang['g_religion'] = "General Religion";
$lang['local_ins'] = "Location Insights";
$lang['student_local'] = "Students Location Distribution";
$lang['local_dst'] = "Location Distribution";
$lang['local_detail'] = "Detailed Student Location";
$lang['student_other'] = "Student Other Insights";
$lang['no_parent'] = "Number of Students with Parent Types";
$lang['parent_type'] = "Parent Type";
$lang['student_active'] = "Student Active/Inactive Condition";
$lang['no_status'] = "Number of Students with Different Status";
$lang['student_status'] = " Student Active Status";
$lang['student_distance'] = "Student Distance From School";
$lang['no_distance'] = "Number of Students with Distance From School";
$lang['distance'] = "Distance From School";
$lang['reg_type'] = "Registration Type";
$lang['reg_date'] = "Registration Date";
$lang['no_joining'] = "Number of Students with Joining Status";
$lang['joining'] = "Joining Status";
$lang['no_date'] = "Number of Students with Date of Registration";
$lang['dates'] = "Date of Registration";
$lang['insight_title'] = "Insight module is available to help schools make informed decision. If you need other analytical reports based on your needs, write your request to";
$lang['month_report'] = "Month Report";
$lang['select_module'] = "Select Module";
$lang['classlevel'] = "Select Class Level";
$lang['exam_comment'] = "Parent Feedback and Teacher Review on Student Progressive Report";
$lang['all_exam_comment'] = "Parent and Teacher Feedback on Students Progressive Reports";
$lang['exam_message'] = "Write Your Comment on This Student Progressive Report Here..";
$lang['comment'] = "Comment";
$lang['benchmark'] = "Benchmark";
$lang['add_syylabus'] = "Add Syllabus";
$lang['sample_benchmark_file'] = "Download Sapmle of Benchmark Excel File Here...";
$lang['fields_marked']='Fields_marked';
$lang['mandatory']='are mandatory';
$lang['are_mandatory']='are mandatory';
$lang['add_exam_school'] = "Add New School Exam";
$lang['sample_exam_file'] = "Download Sample of Exam Definition Excel Here...";
$lang['abbreviation'] = "Abbreviation of Exam";
$lang['abbreviation_placeholder'] = "Abbreviation of Exam eg. ENG1";
$lang['user_contract']=' Contract End Date';
$lang['tabular']="Table of Examination Performance";
$lang['subjects_allocated'] = "Subject Allocation";
$lang['contract'] = "Contract";
$lang['contract_notify'] = "Reminder Date";
$lang['contract_status'] = "Contract Status";
$lang['contract_start'] = "Start Date";

$lang['employee_percent'] = "Employee Percent";
$lang['employee_amount'] = "Employee Amount";

$lang['vendor'] = "Vendor";
$lang['vendor_name'] = "Vendor Name";

//inventory

$lang['purchase_inventory']='Purchases';
$lang['sale_inventory']='Usage';
$lang['register_inventory']='Items';
$lang['view_member'] = 'View Member';

$lang['no_of_students']='Number of Students';
$lang['cash_request']='Cash Request';
$lang['menu_vendors']='Vendors';
$lang['menu_character_grading']= 'Character Grading';
$lang['menu_payroll'] = "Payroll";
$lang['add'] = "Add";
$lang['remove'] = "Remove";

$lang['exam_pass_mark']='Exam Pass Mark';
$lang['menu_live_studies']='Live Studies';
$lang['menu_files']='Files';
$lang['menu_notes']='Class Notes';


$lang['online_exam'] = "Online Exams";
$lang['view_result'] = "View Result";
$lang['score'] = "Marks Score";
$lang['answer'] = "Answered Question";
$lang['online_exam_results'] = "Online Exam Results";
$lang['total_time'] = 'Duration';
$lang['topic'] = 'Topic';
$lang['questions'] = "Questions";
$lang['release_result'] = "Release Exam Results";
$lang['unrelease_result'] = "Unrelease Exam Results";
$lang['deadline'] = "Deadline";
$lang['assignment'] = "Home Packages";
$lang['attach'] = "Attachment";
$lang['add_assignment'] = "Add Home Packages";
$lang['title'] = "Title";
$lang['subject'] = "Subject";
$lang['total_submit'] = "Submitted";
$lang['total_unsubmitted'] = "Unsubmitted";
$lang['total_marked'] = "Marked";
$lang['total_unmarked'] = "Unmarked";
$lang['attachment'] = "Attachment";
$lang['attachment2'] = "Second Attachment";
$lang['teacher'] = "Teacher";
$lang['go_back'] = "Go Back";
$lang['online_studies'] = "Home Packages";
$lang['a_type'] = "Type";
$lang['created_at'] = "Added On";
$lang['submited_on'] = "Submitted On";
$lang['no_attach'] = 'No Attachment';
$lang['select_student'] = "Select Student";
$lang['news_board'] = "News & Announcements";

// Applications
$lang['menu_applicatiions'] = "Applications";
$lang['health_application'] = "Health Insuarance Application";
$lang['add_application'] = "Add Application";
$lang['date_application'] = "Application Date";
$lang['guidance'] = "Guidance";
$lang['rapplication']="You can add one or more student in a class and submit for application";
$lang['total_payment']="Total Payment";
$lang['support']="Support";
$lang['join_school']="Join Our School";
$lang['mark_select_subject']='Select Subject Here';
$lang['parent_status']="Select Parent Status";

$lang['bank_name']="Bank Account";
$lang['classes']="Fee Classes";
$lang['tax_status']="Tax Status";
$lang['pension_fund']="Pension Funds";
$lang['menu_allowance']="Allowance";
$lang['menu_deduction']="Deduction";
$lang['menu_salary']="Salaries";
$lang['pocket_money']="Pocket Money";
$lang['send_sms'] = 'Send Sms';
$lang['wallet'] = 'Student Wallet';
$lang['used_amount'] = 'Used Amount';
$lang['remained_amount'] = 'Remained Amount';
$lang['reference']="Reference";
$lang['used_for']="Used For";
$lang['amount']="Amount";
$lang['subject_semester'] = 'Semester';
$lang['subject_credit'] = 'Credits';
$lang['sponsor']="Sponsors";
$lang['sponsor_info']="Add Sponsor Profile";
$lang['photo']="Photo";
$lang['sponsor']="Sponsors";
$lang['this_address']="Address";
$lang['this_email']="Email";
$lang['add']="ADD";
$lang['menu_sponsor']="Sponsor";
$lang['view_invoice']="View Invoices";
$lang['shulesoft_id']='ShuleSoft ID';
$lang['chart_account']="Select Category";
$lang['address']="Address";
$lang['tin_number']="EMPLOYEES TIN";
$lang['employment_type']="TYPE OF EMPLOYMENT";
$lang['resident_status']="RESIDENTAL STATUS";
$lang['employee_nam']="NAME OF EMPLOYEE";
$lang['social_number'] = "SOCIAL SECURITY NUMBER";
$lang['staff_report'] = "Staff Report";
$lang['college_exam_report'] = "College Report";
$lang['semester_report'] = "Semester Report";
$lang['year_report'] = "Yearly Report";
$lang['grade_report'] = "Grade A Report";


return $lang;
