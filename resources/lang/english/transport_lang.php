<?php

/* List Language  */
$lang['panel_title'] = "Transport";
$lang['add_title'] = "Add a transport";
$lang['add_route_title'] = "Add transport Route";
$lang['slno'] = "#";
$lang['transport_route'] = "Route Name";
$lang['transport_vehicle'] = "Route vehicle";
$lang['transport_fare'] = "Route Fare";
$lang['transport_note'] = "Note";
$lang['transport_vehicle'] = "Route Vehicle(s)";
$lang['transport_members'] = "Transport Members";
$lang['panel_main'] = "Transport Routes";
$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['class_level_id'] = 'Class Level';

// Vehicle registration
$lang['plate_number']='Plate Number';
$lang['seats']='Number of Seats';
$lang['driver']='Driver(s)';
$lang['description']='Description';
$lang['add_vehicle']='Add Vehicle';
$lang['update_vehicle']='Update Vehicle';
$lang['select_driver']='Select Driver';
$lang['select_conductor']='Select Conductor';
$lang['conductor']='Conductor';
$lang['action']='Action';
$lang['no_vehicles'] = 'Number of Vehicles';

/* Add Language */

$lang['add_transport'] = 'Add transport';
$lang['update_transport'] = 'Update transport'; return $lang;