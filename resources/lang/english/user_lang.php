<?php

/* List Language  */
$lang['panel_title'] = "Staff";
$lang['add_title'] = "Add a staff";
$lang['slno'] = "#";
$lang['user_photo'] = "Photo";
$lang['user_name'] = "Name";
$lang['user_email'] = "Email";
$lang['user_dob'] = "Date of Birth";
$lang['user_info'] = "User's Information";
$lang['fields_marked'] = "Fields marked";
$lang['are_mandatory']='are mandatory';
$lang['name_eg']='Full name eg John Samson';
$lang['religion_option']='Choose religion';
$lang['mail_eg']='Valid email eg samson@yahoo.com';
$lang['no_eg']='eg 655406004';
$lang['name_eg']='Full name eg John Samson';
$lang['users_upload']='Upload users from excel file';
$lang['download']='Download';
$lang['sample']='sample file here';
$lang['submit']='Submit';
$lang['mail_eg']='P.O BOX eg 33287,Dar';
$lang['user_sex'] = "Gender";
$lang['user_sex_male'] = "Male";
$lang['user_sex_female'] = "Female";
$lang['user_religion'] = "Religion";
$lang['user_phone'] = "Phone";
$lang['user_address'] =$lang['address'] = "Address";
$lang['user_jod'] = "Joining Date";
$lang['user_type'] = "Type";
$lang['user_role'] = "Role";
$lang['user_username'] = "Username";
$lang['user_password'] = "Password";
$lang['user_roles'] = "User Role/Group";
$lang['user_select_role'] = "Select Role/Group";



$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['pdf_preview'] = 'PDF Preview';
$lang['idcard'] = 'ID Card';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

/* Add Language */
$lang['personal_information'] = "Personal Information";
$lang['add_user'] = 'Add User';
$lang['update_user'] = 'Update User';

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email sent successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!';
$lang['payroll_status'] = "Payroll Status";

return $lang;