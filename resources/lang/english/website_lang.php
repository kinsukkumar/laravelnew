<?php

/**
 * Description of classlevel_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ismaili Mohamedi
 */
$lang['panel_title'] = 'Website Contents';
$lang['add_title'] = 'Add Content';
$lang['add_menu'] = 'Add Menu';
$long['add_menu_title'] = 'Add website Menu';
$lang['menu_dashboard'] = 'Dashboard';
$lang['menu_website'] = 'Contents';
$lang['select_menu'] = 'Select Menu';
$lang['select_media_type'] = 'Select Media Type';
$lang['select_display_style'] = 'Select Display Style';
$lang['select_parent_menu'] = 'Select Parent Menu';
$lang['slno'] = '#';
$lang['posted_on'] = 'Published On';
$lang['posted_by'] = 'Published by';
$lang['content_title'] = 'Title';
$lang['content_publication'] = 'Content Publication';
$lang['media'] = 'Media';
$lang['media_type'] = 'Media Type';;
$lang['description'] = 'Description';
$lang['add_menu_title'] = 'Add Website Menu';
$lang['menu_name'] = 'Menu Name';
$lang['display_style'] = 'Display Style';
$lang['action'] = 'Action';
$lang['color_setting'] = 'Color Setting';
$lang['setting_title'] = 'Website Color Setting';
$lang['upper_header_color'] = 'Upper header Color';
$lang['inner_header_color'] = 'Inner Header Color';
$lang['menu_button_color'] = 'Menu Button Color';
$lang['navigation_header_color'] = 'Navigation Header Color';
$lang['footer_color'] = 'Footer Color';
$lang['main_content_color'] = 'Main Content Color';
$lang['add_social_network'] = ' Add Social Network';
$lang['web_socail_network'] = 'Social Network';
$lang['web_social_networks'] = 'Social networks';
$lang['social_network_name'] = 'Name';
$lang['url'] = 'Url';
$lang['close'] = 'Close';
$lang['action'] = 'Action';
$lang['slno'] = '#';

$lang['publication_button'] = "Publish";
$lang['Sub_menu_of'] = 'Sub Menu Of ( Option )';
$lang['add'] = 'Add';
$lang['delete'] = 'Delete';
$lang['position'] = 'Position';
$lang['add_menu'] = 'Add Menu';
$lang['sub_menu'] = 'Sub Menu of';
$lang['delete_warning'] = 'Warning';
$lang['website_settings'] = 'Website Setting';
$lang['warning_message'] = 'Do you real want to delete ?';
$lang['website_color'] = 'Website Color';
$lang['warning_yes'] = 'Yes';
$lang['warning_no'] = 'No';
$lang['menu'] = 'Menu';
$lang['website_social_network_title'] = 'Website social Network';
$lang['social_type'] = 'Social Network Name';
$lang['url_link'] = 'Url/Link';
$lang['select_social_type'] = 'Select Social Network Type';
$lang['contents'] = 'Contents';
$lang['website_content'] = 'Website Contents';
$lang['website_menu'] = 'Website Menu';

/* Add Language */

$lang['view'] = 'View';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';
$lang['general'] = 'General';
$lang['site_content'] = 'Website Content';
$lang['Websitemenu'] = 'Website Menu';
$lang['Websitereport'] = 'Website Reports';
return $lang;
