<?php

/* List Language  */
$lang['panel_title'] = "Utaratibu";
$lang['add_title'] = "Ongeza utaratibu";
$lang['slno'] = "#";
$lang['routine_classes'] = "Darasa";
$lang['select_class'] = "Chagua Darasa";
$lang['routine_select_section'] = "Chagua Kipengele";
$lang['routine_subject_select'] = "Chagua Somo";
$lang['routine_select_student'] = "Chagua Mwanafunzi";
$lang['routine_all_routine'] = "Taratibu zote";
$lang['routine_section'] = "Kipengele";
$lang['routine_student'] = "Mwanafunzi";
$lang['routine_subject'] = "Somo";
$lang['routine_day'] = "Siku";
$lang['routine_s_to'] = "Kwenda";
$lang['routine_start_time'] = "Muda wa Kuanza";
$lang['routine_end_time'] = "Muda wa Kumaliza";
$lang['routine_room'] = "Chumba";
$lang['routine_note'] = "Noti";

$lang['action'] = "Hatua";
$lang['view'] = 'Angalia';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */
$lang['add_routine'] = 'Ongeza utaratibu';
$lang['update_routine'] = 'Sahisha utaratibu';

$lang['sunday'] = "JUMAPILI";
$lang['monday'] = "JUMATATU";
$lang['tuesday'] = "JUMANNE";
$lang['wednesday'] = "JUMATANO";
$lang['thursday'] = "ALHAMIS";
$lang['friday'] = "IJUMAA";
$lang['saturday'] = "JUMAMOSI";
return $lang;