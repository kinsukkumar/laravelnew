<?php

/* List Language  */
$lang['panel_title'] = "Salio";
$lang['select_class_level']='Chagua ngazi ya darasa';
$lang['select_class_level']='Chagua ngazi ya darasa';
$lang['select_fee']='Chagua ada';
$lang['class_level']='Ngazi ya darasa';
$lang['fee']='Ada';
$lang['slno'] = "#";
$lang['balance_classesID'] = "Darasa";
$lang['balance_select_classes'] = "Chagua darasa";
$lang['balance_all_students'] = 'Wanafunzi wote';
$lang['balance_photo'] = "Picha";
$lang['balance_name'] = "Jina";
$lang['balance_roll'] = "Namba ya udahili";
$lang['balance_phone'] = "Picha";
$lang['balance_totalbalance'] = "Jumla ya Salio";
return $lang;