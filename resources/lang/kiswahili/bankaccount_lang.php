<?php

/**
 * Description of bankaccount_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
/* List Language  */
$lang['panel_title'] = "Akaunti ya Benki";
$lang['add_title'] = "Ongeza akaunti mpya ya Benki";
$lang['branch']='Tawi la benki yako';
$lang['bank_account']='Jina la akaunti ya benki';
$lang['e.g CRDB']='mfano CRDB';
$lang['account_number']='Namba ya akaunti';
$lang['optional']='Msimbo wa swift wa hiari kwa ajili ya uhamisho wa benki na benki';
$lang['slno'] = "#";
$lang['menu_bankaccount']='Akaunti ya Benki';
$lang['bankaccount_name'] = "Jina la Benki";
$lang['bankaccount_branch'] = "Tawi";
$lang['bankaccount_account'] = "Number ya Akaunti";
$lang['bankaccount_account_name'] = "Jina la Akaunti";
$lang['bankaccount_currency'] = "Sarafu";
$lang['bankaccount_swiftcode'] = "Msimbo wa haraka";
$lang['bankaccount_note'] = "Noti";
$lang['action'] = "Hatua";

$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */
$lang['add_bankaccount'] = 'Ongeza akaunti ya benki';
$lang['update_bankaccount'] = 'Sasisha akaunti ya benki';
$lang['bankaccount_select_currency'] = 'Chagua Sarafu';
return $lang;