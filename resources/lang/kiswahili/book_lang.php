<?php


/* List Language  */
$lang['panel_title'] = "Vitabu";
$lang['add_title'] = "Ongeza Kitabu";
$lang['slno'] = "#";
$lang['borrowed_books']='Vitabu vilivyoazimwa';
$lang['book_name'] = "Jina";
$lang['book_subject_code'] = "Namba ya somo";
$lang['from'] = "Kutoka";
$lang['to'] = "Mpaka";
$lang['category'] = "Kikundi";
$lang['subject'] = "Somo";
$lang['no_books']='Idadi ya usambazaji wa vitabu shuleni kwako';
$lang['book_author'] = "Mwandishi";
$lang['available_books']='Vitabu vilivyopo';
$lang['repairable']='Vinavyotengenezeka';
$lang['discarded_books']='Vitabu vilivyotupwa';
$lang['subjects']='Masomo';
$lang['book_price'] = "Bei";
$lang['book_class'] =$lang['class'] = "Darasa";
$lang['library_books']='Vitabu vya maktaba';
$lang['due_date']='Tarehe inayohitajika';
$lang['Normal Book']='Vitabu vya kawaida';
$lang['Repairable Books']='Vitabu vyenye matengenezo';
$lang['Discard Books']='Vitabu vya kuacha';
$lang['fields_marked']='Sehemu zenye alama';
$lang['book_serial']='Namba ya serial ya kitabu kama inavyoonekana kwenye kitabu';
$lang['book_title']='Jina la kitabu';
$lang['mandatory']='ni za lazima';
$lang['upload_book']='Pakia vitabu kutoka kwenye faili la excel';
$lang['sample_book']=' faili la sampuli hapa';
$lang['download']='Pakua';
$lang['submit']='Tuma';
$lang['book_edition'] = "Toleo";
$lang['book_for'] = "Kitabu kwa ajili ya";
$lang['book_quantity'] = "Kiasi";
$lang['book_serialno']='Namba ya serial';
$lang['book_select_class']='Chagua darasa';
$lang['book_rack_no'] = "Namba ya rafu";
$lang['book_status'] = "Hali";
$lang['book_label']='Alama ya kitabu';
$lang['book_edit']='Hariri kitabu';
$lang['book_condition']='Hali ya kitabu';
$lang['book_subject']='Somo la kitabu';
$lang['book_available'] = "Kinapatikana";
$lang['book_unavailable'] = "Haipatikani";

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_book'] = 'Ongeza Kitabu';
$lang['update_book'] = 'Sasisha Kitabu';
return $lang;
