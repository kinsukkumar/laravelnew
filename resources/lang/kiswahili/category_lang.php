<?php

/* List Language  */
$lang['panel_title'] = "Kipengere";
$lang['add_title'] = "Ongeza  Kipengere";
$lang['slno'] = "#";
$lang['category_hname'] = "Jina la darasa";
$lang['category_class_type'] = "Aina ya darasa";
$lang['category_hbalance'] = "Ada ya bweni";
$lang['category_note'] = "Notisi";
$lang['school_hostel_category']='Kipengee cha bweni la shule';
$lang['fields']='Sehemu zenye alama';
$lang['mandatory']='ni za lazima';
$lang['category_select_hostel'] = "Chagua Bweni";

$lang['action'] = "Hatua";
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_category'] = 'ongeza Kipengere';
$lang['update_category'] = 'Sasisha Kipengere';
return $lang;