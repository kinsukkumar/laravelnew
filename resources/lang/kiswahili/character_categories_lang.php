<?php

/* List Language  */
$lang['panel_title'] = "Kikundi cha Tabia";
$lang['add_title'] = "Ongeza Kikundi";
$lang['action'] = "Hatua";
$lang['edit_character_categories'] = 'Hariri Kikundi cha Tabia';
$lang['menu_character_categories'] = 'Kikundi cha tabia';
$lang['subject_based'] = 'Njia ya Tathmini';
$lang['menu_edit'] = "Badilisha";
$lang['slno'] = '#';
$lang['character_category'] = 'Kikundi cha Tabia';
$lang['both_effort_achievement'] = 'Vyote Mafanikio na juhudi';
$lang['achievement_only'] = 'Mafanikio Tu';
$lang['category_message'] = 'Mafanikio tu inamaanisha characters wa kikundi hichi watatathminiwa kwa kutumia mafanikio(Ngazi) wakati kwa vyote Mafanikio na Juhudi inamaanisha character atatathminiwa kwa kutumia juhudi na mafanikio ya mwanafunzi';
$lang['message_heading'] = 'Jinsi ya kufafanua vikundi';

$lang['view'] = 'Angalia';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['menu_characters'] ='Tabia';
$lang['slno']='#';

$lang['menu_add_characters'] = 'Tabia';
$lang['menu_characters'] = 'Tabia';
$lang['menu_characters_sub'] = 'Tabia';
$lang['menu_character_grades'] = 'Daraja';

$lang['menu_character_category'] = 'Kikundi';
$lang['menu_assign'] = 'Toa';
$lang['menu_assess'] ='Tathmini';
$lang['menu_general'] = 'Tathmini ya Jumla';
$lang['menu_report'] ='Ripoti';


/* Add Language */

$lang['add_character_categories'] = 'Ongeza Kikundi cha Tabia';
$lang['update_character_categories'] = 'Sasisha kikundi cha Tabia';
return $lang;