<?php

/* List Language  */
$lang['panel_title'] = "Daraja la Tabia";
$lang['add_title'] = "Ongeza daraja la tabia";
$lang['action'] = "Hatua";
$lang['slno'] = '#';
$lang['grade'] = 'Daraja';
$lang['grade_remark'] = 'Sahihisha tena';
$lang['description'] = 'Maelezo';

$lang['view'] = 'Angalia';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['edit_character_grade'] = 'Badilisha daraja la tabia';
$lang['menu_character_grading']='Daraja la tabia';
$lang['add_grade'] = 'Ongeza daraja la tabia';
$lang['menu_characters'] ='Tabia';

$lang['menu_add_characters'] = 'Tabia';
$lang['menu_characters'] = 'Tabia';
$lang['menu_characters_sub'] = 'Tabia';
$lang['menu_character_grades'] = 'Daraja';

$lang['menu_character_category'] = 'Kikundi';
$lang['menu_assign'] = 'Toa';
$lang['menu_assess'] ='Tathmini';
$lang['menu_general'] = 'Tathmini ya jumla';
$lang['menu_report'] ='Ripoti';

/* Add Language */

$lang['add_character'] = 'Ongeza tabia';
$lang['update_character'] = 'Sahihisha tabia';
return $lang;