<?php

/* List Language  */
$lang['panel_title'] = "Tabia";
$lang['add_title'] = "Ongeza Tabia";
$lang['action'] = "Hatua";
$lang['slno'] = '#';
$lang['character_category'] ='Kikundi';
$lang['select_category'] ='Chagua kikundi';
$lang['auto_generate'] ='Zalisha moja kwa moja';
$lang['character_code'] = 'Msimbo';
$lang['character'] = 'Tabia';

$lang['view'] = 'Angalia';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['edit_character'] = 'Hariri Tabia';
$lang['menu_characters'] ='Tabia';
$lang['menu_character_categories'] = 'C Kikundi';
$lang['menu_index'] = 'Zilizotoka';
$lang['menu_add_characters'] = 'Tabia';
 $lang['menu_characters'] = 'Tabia';
 $lang['menu_characters_sub'] = 'Tabia';
 $lang['menu_character_grades'] = 'Madaraja';

 $lang['menu_character_category'] = 'Kikundi';
 $lang['menu_assign'] = 'Toa';
 $lang['menu_assess'] ='Tathmini';
 $lang['menu_general'] ='Tathmini ya jumla';
 $lang['menu_report'] ='Ripoti';
 $lang['menu_character_categories']='Kikundi cha tabia';

/* Add Language */

$lang['add_character'] = 'Ongeza tabia';
$lang['update_character'] = 'Sasisha tabia';
return $lang;