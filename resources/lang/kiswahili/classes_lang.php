<?php

/* List Language  */
$lang['panel_title'] = "Darasa";
$lang['add_title'] = "Ongeza darasa";
$lang['slno'] = "#";
$lang['classes_name'] = "Darasa";
$lang['classes_numeric'] = "Namba ya darasa";
$lang['teacher_name'] = "Jina la mwalimu";
$lang['classes_note'] = "Notisi";
$lang['action'] = "Hatua";

$lang['classes_select_teacher'] = "Chagua mwalimu";

$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['fields_marked']='Sehemu zenye alama';
$lang['are_mandatory']='ni za lazima';
$lang['class_info']='Maelezo ya madarasa';
$lang['class_eg']='Namba ya darasa mfano 1';
$lang['classes_eg']='Darasa mfano darasa la kwanza';


/* Add Language */

$lang['add_class'] = 'Ongeza darasa';
$lang['update_class'] = 'Sasisha Darasa';
$lang['classlevel'] = 'Daraja la Darasa';
return $lang;