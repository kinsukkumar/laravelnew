<?php

/**
 * Description of classlevel_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$lang['panel_title']='Daraja la Darasa';
$lang['add_title']='Ongeza Daraja';
$lang['name']='Jina la Daraja';
$lang['start_date']='Tarehe ya kuanza';
$lang['level_info']='Maelezo ya daraja la darasa';
$lang['fields']='Sehemu zenye alama';
$lang['education_level']='Ngazi ya elimu mfano Sekondari';
$lang['edit_level']='Hariri ngazi';
$lang['notes']='Maelezo yoyote kuhusu ngazi hii';
$lang['number']='Jumla ya madarasa mfano 4';
$lang['mandatory']='ni za lazima';
$lang['end_date']='Tarehe ya kumaliza';
$lang['span_number']='Namba ya Spani';
$lang['result_format']="Muundo wa matokeo";
$lang['note']='Notisi';
$lang['action']='Hatua';
$lang['slno']='#';

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_level'] = 'Ongeza Daraja';
$lang['update_level'] = 'Sasisha Daraja';
return $lang;