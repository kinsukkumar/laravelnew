<?php

/* List Language  */
$lang['panel_title'] = "Kipengele cha Ada";
$lang['discount']='Punguzo';
$lang['add_discount']='Ongeza punguzo';
$lang['class_level']='Ngazi ya darasa';
$lang['select_class_level']='Chagua ngazi ya darasa';
$lang['fee_name']='Jina la ada';
$lang['select_fee']='Chagua ada';
$lang['date']='Tarehe';
$lang['register_discount']='Andikisha punguzo';
$lang['description_note']='Noti ya maelezo';
$lang['class']='Darasa';
$lang['ensure']=' Hakikisha umefafanua ada na vielelezo vya ada kwa kipengee kilichofafanuliwa, mwaka wa masomo, kipengee na akaunti namba.';
$lang['GUIDANCE']='MWONGOZO';
$lang['select_class']='Chagua darasa';
$lang['select_student']='Chagua mwanafunzi';
$lang['fee']='Ada';
$lang['panelcat_title'] = "Kipengele cha kikundi cha ada";
$lang['add_title'] = "Ongeza aina ya ada";
$lang['slno'] = "#";
$lang['feetype_name'] = "Jina la ada";
$lang['discount_amount'] = "Kiasi cha punguzo";
$lang['feetype_amount'] = "Kiasi";
$lang['feetype_is_repeative'] = "Inajirudia";
$lang['feetype_is_repeative_yes'] = "Ndio";
$lang['exam_select_year']='Chagua Mwaka';
$lang['feetype_is_repeative_no'] = "Hapana";
$lang['feetype_startdate'] = "Tarehe ya kuanza";
$lang['feetype_enddate'] = "Tarehe ya Kumaliza";
$lang['discount_amount'] = "Kiasi cha punguzo";
$lang['feetype_note'] = "Note";
$lang['action'] = "Hatua";
$lang['feetypecategory']="Kikundi cha ada";
$lang['feetpype_select_category']="Chagua kikundi cha aina ada";
$lang['category_title']="Ongeza kikundi cha aina ya ada";
$lang['category_fee']="Kikundi cha aina ya ada";
$lang['category_note']="Note ya kikundi";
$lang['feetype_category']="Kikundi cha aina ya ada";
$lang['add_category']="Ongeza kikundi";
$lang['select_feetype_category']="Chagua kikundi cha aina ya ada";

$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */
$lang['add_feetype'] = 'Ongeza ada';
$lang['update_feetype'] = 'Sasisha ada';
$lang['student_name'] = 'Jina la mwanafunzi';
$lang['feetype_account'] = 'Akaunti ya aina ya ada';
$lang['feetpype_select_account'] = 'Chagua namba ya akaunti';
$lang['invoice_classesID'] = "Darasa";
$lang['invoice_date'] = "Tarehe";

$lang['discount_note'] = "Note";
return $lang;