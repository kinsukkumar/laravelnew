<?php

/* List Language  */
$lang['panel_title'] = "Mahudhurio ya mtihani";
$lang['add_title'] = "Ongeza Mahudhurio ya mtihani";
$lang['slno'] = "#";
$lang['eattendance_photo'] = "Picha";
$lang['eattendance_name'] = "Jina";
$lang['eattendance_email'] = "Barua pepe";
$lang['eattendance_roll'] = "Roll";
$lang['eattendance_phone'] = "Namba ya Simu ya Mkononi";
$lang['eattendance_attendance'] = "Mahudhurio";
$lang['eattendance_section'] = "Mkondo";
$lang['eattendance_exam'] = "Mtihani";
$lang['eattendance_classes'] = "Darasa";
$lang['eattendance_subject'] = "Somo";
$lang['eattendance_all_students'] = 'Wanafunzi wote';

$lang['eattendance_select_exam'] = "Chagua mtihani";
$lang['eattendance_select_classes'] = "Chagua Darasa";
$lang['eattendance_select_subject'] = "Chagua somo";


$lang['action'] = "Hatua";

/* Add Language */

$lang['add_attendance'] = 'Mahudhurio';
$lang['add_all_attendance'] = 'Ongeza Mahudhurio';
$lang['view_attendance'] = "Tazama Mahudhurio";
return $lang;