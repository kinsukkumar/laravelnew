<?php

/**
 * Description of email_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Joshua John
 */

/*
 * Student information
 */
$lang['parent_registered_student']="Habari %s,Mwanao "
        . "%s amesajiliwa katika program ya ShuleSoft %s . Sasa utaweza kuingia ".set_schema_name()."shulesoft.com mda wowote kufuatilia taarifa za kitaaluma za mwanao.";
$lang['parent_edit_student']="Habari %s, Taarifa za mwanao %s"
        . " zimebadilishwa ".set_schema_name()."shulesoft.com. Unaweza kuingia muda wowote kuhakiki" ;
$lang['parent_deleted_student']="Habari %s, mwanao %student_name ameondolewa katika mfumo %s .  ";


/*
 * Parent information
 */
$lang['add_new_parent']="Habari %s, Umeunganishwa katika mfumo wa ShuleSoft wa %s (".set_schema_name()."shulesoft.com), ingia kwa nenotumizi: %s na nenosiri: %s . Tafadhali kumbuka kubadili nenosiri pindi utakapoingia."
        . ". Sasa utaweza kuingia na kufuatilia taarifa za kitaaluma za mwanao";

$lang['login_credentials']="Habari #name, Ili uweze kuingia katika system ya ShuleSoft, fungua link hii (".set_schema_name()."shulesoft.com) , nenotumizi lako ni: #username na nenosiri la kuanzia ni : #default_password . Tafadhali kumbuka kubadili nenosiri pindi utakapoingia. Asante";

$lang['delete_parent']="Habari %s, Umeondolewa katika mfumo wa ShuleSoft wa %s (".set_schema_name()."shulesoft.com), Hivyo hutaweza tena kuingia katika mfumo huu.";

/*
 * Teacher information
 */
$lang['add_new_teacher']="Habari %s, Umewekwa kama mwalimu katika mfumo wa ShuleSoft wa %s (".set_schema_name()."shulesoft.com) kwa nenotumizi: %s na nenosiri la kuanzia: %s "
    . "na utaweza kuingia na kurekodi maksi za wanafunzi nakadhalika. Tafadhali kumbuka kubadili nenosiri pindi utakapoingia katika mfumo.";
$lang['edit_teacher']="Habari %s, taarifa zako katika ShuleSoft (".set_schema_name()."shulesoft.com) zimebadilika, tafadhali ingia kuhakiki.";


$lang['edit_parent']="Habari %s, taarifa zako katika mfumo wa ShuleSoft (".set_schema_name()."shulesoft.com) zimebadilika, tafadhali ingia kuhakiki.";

$lang['delete_teacher']="Habari %s, Umeondolewa katika mfumo wa ShuleSoft wa %s (".set_schema_name()."shulesoft.com), Hivyo hutaweza tena kuingia katika mfumo huu.";

/*
 * User information
 */
$lang['add_new_user']="Habari %s, umewekwa kama %s mfumo wa ShuleSoft wa %s (".set_schema_name()."shulesoft.com) kwa nenotumizi: %s na nenosiri: %s . Tafadhali kumbuka kubadili nenosiri pindi utakapoingia.";
$lang['edit_user']="Habari %s, taarifa zako katika ShuleSoft (".set_schema_name()."shulesoft.com) zimebadilika, tafadhali ingia kuhakiki.";
$lang['delete_user']="Habari %s, Umeondolewa katika mfumo wa ShuleSoft wa %s (".set_schema_name()."shulesoft.com), Hivyo hutaweza tena kuingia katika mfumo huu.";

/*
 * Class information
 */
$lang['add_new_class']="Habari %s, umewekwa kama mwalimu wa darasa la %s. Sahihi yako itaonekana katika ripoti za wanafunzi pindi mfumo utakapotengeneza.";
$lang['edit_class']="Habari %s, sasa umewekwa kama %s wa %s . Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['delete_class']="Habari %s,darasa: %s limeondolewa katika Mfumo,hivyo wewe siyo tena mwalimu wa %s. Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";



/*
 * Section information
 */
$lang['add_new_section']="Habari %s ,umewekwa kama %s wa mkondo %s katika darasa:- %s na mwalimu wa darasa ni %s. Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['edit_section']="Habari %s ,mkondo %s wa: %s umekua %s wa: %s na mwalimu wa darasa ni %s. Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['delete_section']="Habari %s,mkondo: %s umeondolewa katika Mfumo, hivyo wewe sio mwalimu wa %s. Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['class_teacher_notify_edit']="Habari %s ,mkaondo: %s wa %s umebadilishwa na kuwa %s na mwalimu wa mkondo ni %s. Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['class_teacher_section_deletion']="Habari %s ,section %s has been deleted in your class %s. Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";

/*
 * Subject information
 */
$lang['add_new_subject']="Habari %s, umewekwa kuwa mwalimu wa %s. Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['edit_subject']="Habari %s , taarifa za somo la %s zimebadilishwa. Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi na kuhakiki";
$lang['delete_subject']="Habari %s ,somo la %s limeondolewa katika mfumo wa ShuleSoft. Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";

/*
 * Grade information
 */

$lang['add_new_grade']="Habari %s ,daraja jipya la mitihani limeongezwa ,Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kuhakiki";
$lang['edit_grade']="Habari %s ,grade has been modified, please login to %s to view modifications";
$lang['delete_grade']="Habari %s ,Grade has been deleted ";


/*
 * Exam information
 */
$lang['add_new_exam']="Habari %s ,mtihani  wa %s  umeongezwa katika mfumo wa ShuleSoft";
$lang['edit_exam']="Habari %s , taarifa za mtihani  wa %s zimebadilishwa, tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['delete_exam']="Habari %s,mtihani wa %s umeondolewa katika mfumo wa ShuleSoft";


/*
 * Exam Schedule information
 */
$lang['add_new_exam_schedule']="Habari %s ,ratiba mpya ya mitihani imewekwa katika mfumo wa ShuleSoft";
$lang['edit_exam_schedule']="Habari %s ,ratiba ya mitihani imerekebishwa, tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";
$lang['delete_exam_schedule']="Habari %s, ratiba ya mitihani imefutwa katika mfumo wa ShuleSoft";


/*
 * Routine information
 */
$lang['add_new_routine']="Habari %s ,ratiba mpya ya darasa imeongezwa katika mfumo wa ShuleSoft";
$lang['edit_routine']="Habari %s ,ratiba ya darasa imebadilishwa katika mfumo wa ShuleSoft";
$lang['delete_routine']="Habari %s, ratiba ya darasa imefutwa katika mfumo wa ShuleSoft";
/*
 * character assessment
 */
$lang['assign_character_to_teacher']="Habari %s, umewekwa kushughulikia tabia na mienendo eneo la %s. Tafadhali tembelea ".set_schema_name()."shulesoft.com kisha ingia ndani kufahamu zaidi";

// Revenue
$lang['add_revenue']='Habari %s. Malipo yako ya %s yamepokelewa  %s. Namba yako ya risiti ni %s. ';
return $lang;
