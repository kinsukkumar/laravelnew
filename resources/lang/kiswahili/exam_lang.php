<?php

/* List Language  */
$lang['panel_title'] = "Mtihani";
$lang['add_title'] = "Ongeza Mtihani";
$lang['slno'] = "#";
$lang['exam_name'] = "Jina la mtihani";
$lang['exam_date'] = "Tarehe";
$lang['exam_class']="Darasa";
$lang['exam_section']="Mkondo";
$lang['exam_select_section']="Chagua Mkondo";
$lang['fields']='Sehemu zenye alama';
$lang['mandatory']='ni za lazima';
$lang['exam_info']='Maelezo ya mitihani';
$lang['add_minor_exam'] = 'Ongeza Mtihani wa Shule';
$lang['edit_minor_exam'] = 'Hariri Mtihani wa Shule';
$lang['exam_select'] = "Chagua mtihani";
$lang['exam_note'] = "Kumbuka";
$lang['exam_date']='Chagua tarehe ya mtihani';
$lang['exam_all'] = "Yote";
$lang['exam_academic_year']='Mwaka wa masomo';
$lang['exam_select_year']='Chagua mwaka';
$lang['abbreviation']='Kifupi';
$lang['semester']='Muhula';
$lang['class_level']='Ngazi ya darasa';
$lang['mark_select_level']='Chagua ngazi';
$lang['academic_year']='Mwaka wa masomo';
$lang['exam_select_semester']='Chagua mwaka wa masomo';
$lang['abbreviation_placeholder']='Kifupi Mfano CT1';

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_exam'] = 'Ongeza Mtihani';
$lang['update_exam'] = 'Sasisha Mtihani';

/* List Language  */
$lang['exam'] = "Mitihani";
$lang['add_exam'] = "Ongeza mtihani";
$lang['add_title'] = 'Ongeza Maksi';
$lang['slno'] = "#";
$lang['mark_exam'] = "Mtihani";
$lang['mark_classes'] = "Darasa";
$lang['mark_student'] = "Mwanafunzi";
$lang['mark_subject'] = "Somo";
$lang['mark_photo'] = "Picha";
$lang['mark_name'] = "Jina";
$lang['mark_roll'] = "Namba ya udahili";
$lang['mark_phone'] = "Namba ya Simu ya mkononi";
$lang['mark_dob'] = "Tarehe ya kuzaliwa";
$lang['mark_sex'] = "Jinsia";
$lang['mark_religion'] = "Dini";
$lang['mark_email'] = "Barua pepe";
$lang['mark_address'] = "Anuani";
$lang['mark_username'] = "Jina la kutumia";

$lang['mark_subject'] = "Somo";
$lang['mark_mark'] = "Alama";
$lang['mark_point'] = "Pointi";
$lang['mark_grade'] = "Daraja";


$lang['mark_select_classes'] = "Chagua Darasa";
$lang['mark_select_exam'] = "Chagua Mtihani";
$lang['mark_select_subject'] = "Chagua Somo";
$lang['mark_select_student'] = "Chagua Mwanafunzi";
$lang['mark_success'] = "Fanikiwa";
$lang['personal_information'] = "Taarifa za Mtu";
$lang['mark_information'] = "Taarifa za Alama";
$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['pdf_preview'] = 'Onesha PDF ya awali';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma Pdf kwa barua pepe";

// /* Add Language */
$lang['add_mark'] = 'Alama';
$lang['add_sub_mark'] = 'Ongeza Alama';

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Eneo hili linahitajika.";
$lang['mail_valid'] = "Eneo hili linahitaji barua pepe iliyo kweli";
$lang['mail_subject'] = "Eneo la somo linahitajika.";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe haijatumwa';
$lang['show_division']='Onesha Division kwenye Ripoti';
$lang['reporting_date']='Tarehe ya Kufungua Shule';

$lang['single_report_title']='RIPOTI YA MAENDELEO YA MWANAFUNZI';
$lang['pupil_report_title']='RIPOTI YA MAENDELEO YA MWANAFUNZI';
$lang['child_report_title']='RIPOTI YA MAENDELEO YA MTOTO';
$lang['grade_status']='MAELEZO';
$lang['pos_in_class']='Nafasi Kidarasa';
$lang['pos_in_section']='Nafasi Kimkondo';
$lang['total']='JUMLA';
$lang['avg_mark']='WASTANI WA MAKSI';
$lang['out_of']='Kati ya ';
$lang['subject_name']='Somo';
$lang['points']='POINTI';
$lang['remarks']='Maoni';
$lang['teacher_name']='Mwalimu';
$lang['sign']='Sahihi';
$lang['exam_avg']='Wastani wa Mtihani';
$lang['total_of']='JUMLA YA ';
$lang['grading']='MADARAJA';
$lang['key']='VIFUPISHO';
$lang['formula']='FOMULA';
$lang['academic_remark']='Maoni ya Kitaaluma';
$lang['character_assessment']='Tabia';
$lang['class_teacher']='Mwalimu wa Darasa';
$lang['class_teacher_signature']='Sahihi ya Mwalimu wa Darasa';
$lang['date_of_reporting']='Tarehe ya Kufungua Shule';
$lang['panel_title'] = "Ripoti ya Mtihani";
$lang['add_title'] = "Ongeza mahudhurio ya Mtihani";
$lang['slno'] = "#";
$lang['menu_report']="Ripoti ya Mtihani";
$lang['class_report']='Chagua darasa kutazama ripoti yake';
$lang['eattendance_photo'] = "Picha";
$lang['eattendance_name'] = "Jina";
$lang['eattendance_email'] = "Barua Pepe";
$lang['eattendance_roll'] = "Namba ya udahili";
$lang['eattendance_phone'] = "Namba ya Simu";
$lang['eattendance_attendance'] = "Mahudhurio";
$lang['eattendance_section'] = "Mkondo";
$lang['eattendance_exam'] = "Mtihani";
$lang['eattendance_classes'] = "Darasa";
$lang['eattendance_subject'] = "Somo";
$lang['eattendance_all_students'] = 'Wanafunzi wote';

$lang['eattendance_select_exam'] = "Chagua Mtihani";
$lang['eattendance_select_classes'] = "Chagua Darasa";
$lang['eattendance_select_subject'] = "Chagua Somo";
$lang['validate_report'] = "Hakiki & Tengeneza Ripoti";
$lang['report_name'] = "Jina la Ripoti";
$lang['close'] = "Funga";
$lang['name'] = "Jina:";
$lang['admission_no'] = "Namba ya Usajili:";
$lang['stream'] = "MKONDO:";
$lang['class'] = "Darasa";
$lang['class_teacher'] = "Mwalimu wa Darasa";
$lang['teacher'] = "Mwalimu";
$lang['division'] = "Divisheni";
$lang['subject'] = "Somo";
$lang['attendance'] = "Mahudhurio";
$lang['sign'] = "Sahihi";

$lang['action'] = "Hatua";

/* Add Language */

$lang['add_attendance'] = 'Mahudhurio';
$lang['add_all_attendance'] = 'Ongeza wote kwenye mahudhurio';
$lang['view_report'] = "Tazama Ripoti";
$lang['total_point']='Jumla ya Pointi';
$lang['total']='Jumla';
$lang['view_combined_report']='Tazama Ripoti iliyounganishwa';
$lang['add_exam_group']='Weka kundi jipya la mtihani';
$lang['exam_group']='Kundi la Mitihani';
$lang['school_exam']='Kundi la Mitihani ya Shule';
$lang['add_new_exam_group']='Weka kundi jipya la mtihani';
$lang['exam_group_name']='Jina la kundi la mtihani';
$lang['exam_group_weight']='Uzito wa kundi  la mtihani';
$lang['add_exam']='Ongeza Mtihani';
$lang['menu_add']='Ongeza';
$lang['add_exam_school']='Ongeza Mtihani Wa Shule';
$lang['exam_abbreviation']='Muhutasari wa mitihani';
$lang['sample_exam_file']='Unaweza kupakua faili ya mfano hapa';
$lang['add_exam_name']='Ongeza jina la mtihani ili kukusaidia kutambua mtihani';
$lang['add_exam_initial']='Ongeza awali ambayo inawakilisha mtihani huu mfano:ABC';
$lang['select_level']='Chagua kiwango ambacho kitachukua mtihani huu mfano:primary';
$lang['select_group']='Chagua kikundi ambacho mitihani hii ni ya ';
$lang['add_text_appear']='Ongeza Maandishi unayotaka kuonekana katika uwanja wa maelezo';
$lang['about_exam_school']='Kuhusu nafasi za mitihani ya shule';
$lang['add_exam_defn']='Ongeza ufafanuzi wa mtihani';
$lang['define_exam']='Fafanua mtihani';
$lang['add_online_exam']='Ongeza Mtihani kwenye mkondoni';
$lang['Date']='Tarehe';
$lang['number_question']='Idadi ya maswali';
$lang['main_topic']='Mada kuu';
$lang['create_topic']='Au unda mada';
$lang['time_minutes']='Wakati katika dakika';
$lang['note']='Kumbuka';
return $lang;