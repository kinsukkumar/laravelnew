<?php

/**
 * Description of exam_report_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$lang['panel_title'] = "Ripoti ya Mtihani";
$lang['add_title'] = "Ongeza mahudhurio ya Mtihani";
$lang['slno'] = "#";
$lang['menu_report']="Ripoti ya Mtihani";
$lang['class_report']='Chagua darasa kutazama ripoti yake';
$lang['eattendance_photo'] = "Picha";
$lang['eattendance_name'] = "Jina";
$lang['eattendance_email'] = "Barua Pepe";
$lang['eattendance_roll'] = "Namba ya udahili";
$lang['eattendance_phone'] = "Namba ya Simu";
$lang['eattendance_attendance'] = "Mahudhurio";
$lang['eattendance_section'] = "Mkondo";
$lang['eattendance_exam'] = "Mtihani";
$lang['eattendance_classes'] = "Darasa";
$lang['eattendance_subject'] = "Somo";
$lang['eattendance_all_students'] = 'Wanafunzi wote';

$lang['eattendance_select_exam'] = "Chagua Mtihani";
$lang['eattendance_select_classes'] = "Chagua Darasa";
$lang['eattendance_select_subject'] = "Chagua Somo";
$lang['validate_report'] = "Hakiki & Tengeneza Ripoti";
$lang['report_name'] = "Jina la Ripoti";
$lang['close'] = "Funga";
$lang['name'] = "JINA:";
$lang['admission_no'] = "Namba ya Usajili:";
$lang['stream'] = "MKONDO:";
$lang['class'] = "Darasa";
$lang['class_teacher'] = "Mwalimu wa Darasa";
$lang['teacher'] = "Mwalimu";
$lang['division'] = "Divisheni";
$lang['subject'] = "Somo";
$lang['attendance'] = "Mahudhurio";
$lang['sign'] = "Sahihi";

$lang['action'] = "Hatua";

/* Add Language */

$lang['add_attendance'] = 'Mahudhurio';
$lang['add_all_attendance'] = 'Ongeza wote kwenye mahudhurio';
$lang['view_report'] = "Tazama Ripoti";
$lang['total_point']='Jumla ya Pointi';
$lang['total']='Jumla';
$lang['view_combined_report']='Tazama Ripoti iliyounganishwa';

$lang['signature']='Sahihi';

return $lang;