<?php

/**
 * Description of examreport_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */


/* List Language  */
$lang['panel_title'] = "Ripoti rasmi ya mitihani";
$lang['add_title'] = "Ongeza mtihani";
$lang['slno'] = "#";
$lang['examreport_name'] = "Jina la ripoti";
$lang['examreport_date'] = "Tarehe iliyotengenezwa";
$lang['exam_class']="Darasa";
$lang['examreport_section']="Ripoti rasmi ya mitihani";
$lang['examreport_select_classes']="Chagua darasa";
$lang['examreport_exams_combined'] = "Mitihani pamoja";
$lang['exam_all'] = "WOTE";


$lang['action'] = "Hatua";
$lang['view'] = 'Angalia';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_exam'] = 'Ongeza mtihani';
$lang['update_exam'] = 'Sasisha mtihani';
$lang['subject_classes']='Chagua Darasa';
return $lang;