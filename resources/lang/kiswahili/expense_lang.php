<?php

/* List Language  */
$lang['panel_title'] = "Gharama";
$lang['add_title'] = "Ongeza Gharama";
$lang['slno'] = "#";
$lang['expense_expense'] = "Jina";
$lang['expense_date'] = "Tarehe";
$lang['expense_amount'] = "Kiasi";
$lang['expense_note'] = "Kumbuka";
$lang['expense_uname'] = "Mtumiaji";
$lang['expense']='Gharama';
$lang['select_expense']='Chagua gharama';
$lang['expense_total'] = "Jumla";
$lang['action'] = "Hatua";
$lang['add_category']='Ongeza kikundi cha gharama';
$lang['panel_category_title']='Akaunti Jedwali';
$lang['subcategory']='Kikundi';
$lang['add_category']='Ongeza kikundi';
// $lang['view'] = 'View';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['account_code']='Msimbo wa Akaunti';
$lang['add_account']='Ongeza Akaunti';
$lang['account_type']='Aina ya Akaunti';
$lang['account_name']='Jina la Akaunti';
/* Add Language */

$lang['start_date'] = 'Tarehe ya Kuanza';
$lang['end_date'] = 'Tarehe ya Mwisho';
$lang['submit'] = 'Angalia';
$lang['open_balance'] = 'Kiwango Kilichopo';
$lang['category']='Kikundi';
$lang['group_name']='Jina la Kundi';
$lang['sum']='Jumla';
$lang['payment_method']='Njia ya Malipo';
$lang['ref_no']='Kumb. Numba';
$lang['save']='Hifadhi';
$lang['userform']='Aina ya Mtumiaji';

$lang['userin']='Mtumiaji wa ShuleSoft';
$lang['usernot']='Sio Mtumiaji wa ShuleSoft';
$lang['usertype']='Chagua Aina ya Mtumiaji';

$lang['add_expense'] = 'Ongeza Gharama';
$lang['update_expense'] = 'Sasisha Gharama';
return $lang;