<?php


/* List Language  */
$lang['panel_title'] = "Aina ya Ada";
$lang['select_class']='Chagua darasa';
$lang['select_academic_year']='Mwaka wa masomo';
$lang['select_class']='Chagua darasa';
$lang['class_level']='Ngazi ya darasa';
$lang['fee']='Ada';
$lang['select']='Chagua';
$lang['select_a_state']='Chagua mji';
$lang['installment']='Kipengee';
$lang['subject_select_class']='Chagua darasa';
$lang['classes']="Madarasa ya Ada";

$lang['add_new_fee']='Ongeza ada mpya';
$lang['fees_subscription']=', uandikishaji wa ada';
$lang['sports']='Michezo';
$lang['select_template_type']='Chagua aina ya template';
$lang['payment_balance']='Kiasi cha malipo';
$lang['template_type']='Aina ya template';
$lang['select_fees']='Chagua ada';
$lang['all']='Zote';
$lang['add_due_amount']='Ongeza kiasi kinachodaiwa';
$lang['select_class_level']='Chagua ngazi ya darasa';
$lang['view_fees']='Angalia ada kwenye darasa';
$lang['add_all']='Ongeza ada zote zikitengwa na mkato';
$lang['here']='Hapa unafafanua ada zote zilizopo shuleni';
$lang['fees_section']='Kipengele cha ada';
$lang['all_fees']='Ada zote';
$lang['select_fee']='Chagua ada';
$lang['selcet_add']='Chagua darasa ili kuongeza';
$lang['total_installment_fee']='Ada ya jumla ya kipengee';
$lang['define_install']='Hakuna vipengee vilivyofafanuliwa. Tafadhali fafanua vipengee kwanza.';
$lang['click']='Bonyeza hapa kufafanua';
$lang['total_fee']='Jumla ya ada';
$lang['no_classes']='Hamna madarasa yamewekwa kwenye ngazi hii. Tafadhali,';
$lang['add']='ongeza madarasa';
$lang['first']='kwanza kwenye ngazi hii';
$lang['select_account']='Chagua nambari ya akaunti';
$lang['Enable/disable Edit']='Wezesha/Usiwezeshe Kusasisha';
$lang['Installments']='Vipengee';
$lang['Description']='Maelezo';
$lang['amount']='Kiasi';
$lang['percent']='Asilimia';
$lang['Subscription']='Usajili';
$lang['select_class']='Chagua darasa';
$lang['enter_total_amount']='Ingiza kiasi chote';
$lang['account_number']='Namba ya akaunti';
$lang['select']='Chagua';
$lang['add_details']='Ongeza maelezo';
$lang['exam_select_year']='Chagua mwaka';
$lang['class_level']='Ngazi ya darasa';
$lang['add_title'] = "Ongeza Aina ya Ada";
$lang['slno'] = "#";
$lang['feetype_name'] = "Aina ya Ada";
$lang['feetype_amount'] = "Kiasi";
$lang['feetype_is_repeative'] = "Ni Marudio";
$lang['feetype_is_repeative_yes'] = "Ndio";
$lang['feetype_is_repeative_no'] = "Hapana";
$lang['feetype_startdate'] = "Tarehe ya Kuanza";
$lang['feetype_enddate'] = "Tarehe ya Kumaliza";
$lang['feetype_note'] = "Kumbuka";
$lang['action'] = "Hatua";

$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */
$lang['add_feetype'] = 'Ongeza Ada';
$lang['update_feetype'] = 'Sasisha Aina ya Ada';

$lang['installments'] = "Taarifa za Ada Zote";
$lang['summary'] = "Taarifa kwa Kifupi";

return $lang;