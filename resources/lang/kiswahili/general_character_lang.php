<?php
/* List Language  */
$lang['panel_title'] = "Tathmini ya jumla ya Tabia";
$lang['character_select_year']='Chagua mwaka';
$lang['assess_title'] = "Maoni ya jumla";
$lang['select_academic_year']='Chagua mwaka wa masomo';
$lang['select_class']='Chagua darasa';
$lang['select_semester']='Chagua muhula';
$lang['action'] = "Hatua";
$lang['slno'] = '#';
$lang['class'] = 'Darasa';
$lang['roll'] = 'Roll/Namba ya usajili';
$lang['student_name'] = 'Jina la mwanafunzi';
$lang['section'] = 'Sehemu/Mkondo';
$lang['classlevel'] ='Ngazi ya darasa';
$lang['academic_year'] ='Mwaka wa masomo';
$lang['semester'] ='Muhula';
$lang['general_comment'] ='Maoni';
$lang['send_comment'] = 'Tuma';
$lang['character_code'] = 'Msimbo';
$lang['character'] = 'Tabia';
$lang['code'] = 'Msimbo';
$lang['view'] = 'Angalia';
$lang['remark1'] = 'Mafanikio';
$lang['remark2'] = 'Juhudi';
$lang['menu_success'] = 'Imefanikiwa kutathminiwa';
$lang['menu_error'] =  'Imefeli';
$lang['student_assessed'] = 'Sema kwa: ';

$lang['menu_characters'] ='Tabia';
$lang['menu_character_categories'] = 'C Tabia';
$lang['menu_assigned'] = 'Iliyotoka';
$lang['menu_add_characters'] = 'Tabia';
$lang['menu_characters'] = 'Tabia';
$lang['menu_character_grades'] = 'Daraja la tabia';

$lang['menu_character_category'] = 'Kikundi';
$lang['menu_report'] ='Report';
$lang['report_student_refresh'] = 'Reja tena';
$lang['number_character_assessed'] = 'Tabia zilizotathminiwa';
$lang['message'] = 'chini ya ';

/* Add Language */
$lang['report_student_name'] = 'JINA LA MWANAFUNZI: ';
$lang['report_character_category'] = 'Kikundi: ';
$lang['report_grading_system'] = "Daraja la Tabia' Mfumo";
$lang['report_class_teacter_comment'] ='Maoni ya mwalimu wa darasa';
$lang['report_head_teacter_comment'] = 'Maoni ya mwalimu mkuu';
$lang['report_class_teacter_signature'] = 'Sahihi';
$lang['report_head_teacter_signature'] = 'Sahihi';
$lang['character_report_header'] = 'RIPOTI YA TATHMINI YA CHARACTER YA MWANAFUNZI ';
$lang['character_report_box'] = 'S.L.P ';
$lang['character_report_email'] = 'Barua pepe: ';
$lang['character_report_website'] = 'Tovuti: ';
$lang['character_report_cell'] = 'Cell: ';
$lang['character_report_select_a_grade'] = 'Mafanikio';
$lang['character_report_select_e_grade'] = 'Juhudi';
$lang['character_report_assess'] = 'Tathmini';
$lang['character_report_selectall_message'] = 'Chagua vyote';
$lang['character_report_remark'] = 'Maoni';
$lang['character_report_grade'] = 'Daraja';
$lang['class_teacher_comment'] = 'Maoni ya mwalimu wa darasa';
$lang['head_teacher_comment'] = 'Maoni ya mwalimu mkuu';
return $lang;