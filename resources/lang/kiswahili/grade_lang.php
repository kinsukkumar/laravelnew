<?php


/* List Language  */
$lang['panel_title'] = "Daraja";
$lang['add_title'] = "Ongeza Daraja";
$lang['slno'] = "#";
$lang['grade_name'] = "Jina la Daraja";
$lang['grade_name_eg']='Jina la daraja mfano A';
$lang['weight']='Uzito wa daraja hili mfano 5';
$lang['minimum']='Maksi za chini zaidi za daraja hili';
$lang['max']='Maksi za juu zaidi za daraja hili';
$lang['excellent']='Mfano Bora zaidi';
$lang['grade_point'] = "Pointi za Daraja";
$lang['grade_gradefrom'] = "Alama kutoka";
$lang['remarks']='Hii inatumika kama maoni ya somo yatakayoonekana kwenye ripoti kulingana na alama za somo za mwanafunzi';
$lang['grade_remarks']='Maoni ya daraja';
$lang['overall remarks']='Maoni ya jumla';
$lang['comment']='Hii inatumika kama maoni ya mwalimu wa darasa itakayotumika kwenye mwisho wa ripoti kulingana na wastani wa jumla wa mwanafunzi';
$lang['grade_gradeupto'] = "Alama Mpaka";
$lang['grade_note'] = "Maelezo";
$lang['select_level']='Chagua ngazi ya darasa ili kutazama daraja iliyopewa.';
$lang['select_classlevel']='Chagua ngazi ya darasa';
$lang['overall_note'] = "Maelezo Kwa ujumla";


$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['grade_info']='Maelezo ya madaraja';
$lang['fields_marked']='Sehemu zenye alama';
$lang['are_mandatory']='ni za lazima';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_class'] = 'Ongeza Daraja';
$lang['update_class'] = 'Sasisha Daraja';
$lang['classlevel'] = 'Kiwango cha Darasa';
$lang['overall_academic_note']='Notisi ya Mtaaluma';
$lang['overall_academic_note_comment']='Hii notisi ya mtaaluma itaonekana chini ya ripoti ya mwanafunzi sehemu ya notisi ya mtaaluma';
return $lang;