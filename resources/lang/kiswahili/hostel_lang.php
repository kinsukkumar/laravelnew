<?php

/* List Language  */
$lang['panel_title'] = "Bweni";
$lang['add_title'] = "Ongeza Bweni";
$lang['slno'] = "#";
$lang['hostel_name'] = "Jina";
$lang['hostel_htype'] = "Aina";
$lang['hostel_address'] = "Anuani";
$lang['hostel_note'] = "Notisi";
$lang['fields'] = "Sehemu zenye alama";
$lang['mandatory'] = "ni ya lazima";
$lang['school_hostel'] = "Bweni ya shule";
$lang['select_hostel_type'] = 'Chagua Aina';
$lang['hostel_boys'] = "Kiume";
$lang['hostel_girls'] = "Kike";
$lang['hostel_combine'] = "Wote";
$lang['patron'] = "Msimamizi";
$lang['beds_no'] = "Jumla ya Vitanda";



$lang['action'] = "Hatua";
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_hostel'] = 'Ongeza Bweni';
$lang['update_hostel'] = 'Sasisha Bweni';
return $lang;