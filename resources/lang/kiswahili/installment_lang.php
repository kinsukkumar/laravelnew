<?php

/**
 * Description of classlevel_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$lang['menu_installement']='Kipengee';
$lang['panel_title']='Kipengee cha ada';
$lang['add_title']='Ongeza Kipengee';
$lang['name']='Jina la Kipengee';
$lang['start_date']='Tarehe ya Kuanzia';
$lang['end_date']='Tarehe ya kumaliza';
$lang['note']='Notsi';
$lang['action']='Hatua';
$lang['slno']='#';

$lang['action'] = "Hatua";
$lang['view'] = 'Angalia';
$lang['edit'] = 'Hariri';
$lang['edit_installment'] = 'Hariri Kipengee';
$lang['delete'] = 'Futa';
$lang['select_year']='Chagua mwaka';
$lang['select_installment']='Chagua Kipengee';

/* Add Language */

$lang['class_level'] = 'Ngazi ya darasa';
$lang['academic_year'] = 'Mwaka wa masomo';
$lang['exam_select_year'] = 'Chagua mwaka';
$lang['update_level'] = 'Sasisha Kipengee';

$lang['upload_inst_by_excel']='Weka  Kipengee cha ada kwa ekseli';
$lang['sample_installment_file']='Sampuli ya faili la ekseli';
return $lang;
