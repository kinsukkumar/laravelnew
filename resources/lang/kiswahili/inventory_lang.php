<?php

/**
 * Description of inventory_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
/* List Language  */
$lang['panel_title'] = "Orodha ya Vitu";
$lang['add_title'] = "Ongeza Kitu";
$lang['slno'] = "#";
$lang['item_name'] = "Jina";
$lang['batch_number'] = "Namba ya Kundi";
$lang['item_status'] = "Hali ya Kitu";
$lang['available_quantity'] = "Kiasi kilichopo";
$lang['vendor_name'] = "Jina la Muuzaji";

$lang['price'] = "Bei";
$lang['contact_name']="Mawasiliano binafsi";
$lang['edit_item']="Hariri";
$lang['add_item']="Ongeza";
$lang['set_alert']="Weka kiwango cha Mwisho ili Kukumbushwa";


$lang['add'] = 'Add';

$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */
$lang['inventory_title']='Ongeza Kitu Kipya';
$lang['name'] = 'Jina';
$lang['email'] = 'Barua Pepe';
$lang['phone_number'] = 'Namba ya Simu';
$lang['batch_number']='Namba ya kundi';
$lang['status'] = 'Hali ya kitu';
$lang['contact_person_number'] = 'Namba ya simu ya mtu binafsi';
$lang['contact_person_name'] = 'Jina la Mtu';
$lang['depreciation'] = 'Pungua thamani';
$lang['add_item'] = 'Ongeza kitu';
$lang['quantity']='Kiasi';
$lang['comments']='Maoni';
$lang['date_purchased']='Tarehe ya kununua';
$lang['created_at']='Tarehe ya Kurekodi';
$lang['view_vendor_indo']='Bofya kuona taarifa za muuzaji';
$lang['update_item']='Sasisha kitu';
$lang['open_blance'] = "Kiasi Kilichopo";

return $lang;