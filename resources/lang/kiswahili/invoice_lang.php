<?php

/* List Language  */
$lang['panel_title'] = "Ankara";
$lang['payment_title']='Malipo';
$lang['select_payment_type']='Chagua aina ya malipo';
$lang['all']='Yote';
$lang['pending_approval']='Inasubiri kupitishwa';
$lang['select_installment']='Chagua kipengee';
$lang['approved']='Malipo yaliyopitishwa';
$lang['rejected']='Malipo yaliyokataliwa';
$lang['PAYMENT BALANCE REMINDER']='KUMBUKUMBU YA KIASI CHA MALIPO';
$lang['panel_receipt_title']='Risiti';   
$lang['payment_receipts']='Risiti za malipo';
$lang['add_title'] = "Ongeza Ankara";
$lang['slno'] = "#";
$lang['invoice_classesID'] = "Darasa";
$lang['invoice_studentID'] = "Mwanafunzi";
$lang['class_level']='Ngazi ya darasa';
$lang['back_to_invoice']='Rudi kwenye ankara';
$lang['select_class'] = "Chagua darasa";
$lang['select_class_level'] = "Chagua ngazi ya darasa";
$lang['select_student'] = "Chagua mwanafunzi";
$lang['select_installment'] = "Chagua kipengee";
$lang['invoice_select_classes'] = "Chagua Darasa";
$lang['invoice_select_student'] = "Chagua Mwanafunzi";
$lang['invoice_select_paymentmethod'] = "Chagua njia ya malipo";
$lang['invoice_amount'] = "Kiasi";
$lang['invoice_student'] = "Mwanafunzi";
$lang['all_class']='Ankara ya wanafunzi wote darasani';
$lang['search_invoice']='Tafuta ankara kwa kutumia mzazi';
$lang['select_parent']='Chagua mzazi';
$lang['select_class_level']='Chagua ngazi ya darasa';
$lang['select_academic_year']='Chagua mwaka wa masomo';
$lang['all_levels']='Ngazi zote';
$lang['invoice_number']='Namba ya ankara';
$lang['total_amount_unpaid']='Jumla ya kiasi ambacho hakijalipwa';
$lang['unpaid_amount']='Deni';
$lang['total_amount_paid']='Jumla ya kiasi kilicholipwa';
$lang['total_invoice_amount']='Gharama Jumla ya Ankara';

$lang['invoice_date'] = "Tarehe";
$lang['invoice_feetype'] = "Aina ya Ada";
$lang['invoice_invoice'] = "Ankara #";
$lang['invoice_pdate'] = "Tarehe ya malipo";
$lang['invoice_total'] = "Jumla";
$lang['paid_invoice_amount'] = "Malipo Yaliyofanyika";
$lang['student_name']='JINA:';
$lang['student_class']='DARASA:';

$lang['invoice_from'] = "Kutoka";
$lang['invoice_to'] = "Kwenda";
$lang['invoice_create_date'] = "Tarehe ya Kutengeneza";
$lang['invoice_subtotal'] = "Jumla Ndogo";
$lang['invoice_sremove'] = "Ondoa Mwanafunzi";
$lang['invoice_roll'] = "Namba ya udahili";
$lang['invoice_phone'] = "Namba ya Simu ya mkononi";
$lang['invoice_email'] = "Barua pepe";
$lang['optional_name']='Jina la hiari';
$lang['invoice_status'] = "Hali ya malipo";
$lang['invoice_number'] = "Namba ya malipo";
$lang['invoice_title']='JIna la ankara';
$lang['invoice_notpaid'] = "Haijalipwa";
$lang['invoice_partially_paid'] = "Imelipwa kiasi";
$lang['invoice_fully_paid'] = "Imelipwa Kamili";
$lang['invoice_paymentmethod'] = 'Njia ya malipo';

$lang['invoice_cash'] = "Fedha";
$lang['invoice_cheque'] = "Hundi";
$lang['invoice_paypal'] = "Paypal";
$lang['invoice_stripe'] = "Stripe";


$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "sehemu ya kutuma inahitajika ";
$lang['mail_valid'] = "Sehemu ya kutuma lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajka";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutumwa!';


$lang['pdf_preview'] = 'Onesha PDF kwa awali';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa barua pepe";
$lang['payment'] = 'Malipo';
$lang['invoice_due'] = "Deni ";
$lang['previous_due_amount']='Deni Lililopita';
$lang['invoice_made'] = "Malipo yamefanyika";

$lang['view'] = "Tazama";
$lang['add_invoice'] = 'Ongeza Ankara ';
$lang['update_invoice'] = 'Sasisha Ankara';
$lang['add_payment'] = 'Ongeza Malipo';
$lang['action'] = 'Hatua';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['total_invoices']='Ankara Zote';

$lang['fee_name']='Jina la Ada';
$lang['pay_amount']='Hela Iliyolipwa';
$lang['amount']='Kiasi';

$lang['invoice_cheque_number']='Ingiza Namba ya Hundi';
$lang['bank_name']='Jina la Benki';
$lang['transaction_id']='Namba ya Muamala';
$lang['payment_date']='Tarehe ya Malipo';
$lang['bankslip_title']='Ambatanisha Hati ya Malipo kutoka Benki';
$lang['guidance']='Maelekezo';
$lang['guidance_details']='Unahitajika kuingiza taarifa ambazo ni sahihi na taarifa zote za malipo ni sawa na sahihi. Namba ya muamala hutumika kutambulisha malipo yaliyofanya. Namba hii inapatikana katika nakala ya malipo kutoka benk au katika muamala wa njia ya simu.';

$lang['due_date']='Tarehe ya Mwisho';

$lang['invoices']='Ankara:';
$lang['section']='Mkondo:';
$lang['summary']='Taarifa Zaidi:';
$lang['advance_amount'] = "Kiwango cha Juu";
$lang['previous_amount'] = 'Kiwango Cha Chini';


$lang['total_invoiceA'] = 'Jumla ya kiwango cha Ankara';
$lang['total_paid'] = 'Jumla Iliyolipwa';
$lang['total_upaid'] = 'Jumla Ambayo Haijalipwa';
$lang['total_invoices'] = 'Jumla ya Ankara';
$lang['partially'] = 'Malipo Kiasi';
$lang['full_paid'] = 'Ankara Zilizolipwa';
$lang['unpaid'] = 'Ankara Ambazo Hazijalipwa';
$lang['Create Date'] = 'Tarehe iliyotengenezwa';
$lang['create'] = 'Tengeneza';
$lang['notify_parents']='Wajulishe wazazi kwa ujumbe mfupi/email';
return $lang;