<?php

/* List Language  */
$lang['panel_title'] = "Posta / ujumbe Mfupi ";
$lang['add_title'] = "Ongeza Barua pepe / Ujumbe mfupi";
$lang['slno'] = "#";
$lang['mailandsms_users'] = "Watumiaji";
$lang['mailandsms_select_user'] = "Chagua watumiaji";
$lang['chars/SMS count']='Hesabu ya char/SMS';
$lang['select_criteria']='Chagua kigezo';
$lang['message']='Andika ujumbe';
$lang['write']='NB: Ukiandika';
$lang['#name']='#jina';
$lang['it_will']='itachagua jina la mzazi';
$lang['#username']='#jina tumizi';
$lang['it_will_username']='itachagua jina tumizi la mzazi';
$lang['all_parents']='Wazazi wote';
$lang['based_on_class']='Kutegemea na darasa';
$lang['based_on_studenttype']='Kutegemea na aina ya mwanafunzi';
$lang['based_on_accounts']='Kutegemea na akaunti';
$lang['based_on_transport_routes'] = "Kutegemea na Njia za Usafiri";
$lang['custom_selection']='Uchaguzi wa kujitegemea';
$lang['mailandsms_select_template'] = "Chagua Template";
$lang['mailandsms_select_send_by'] = "Chagua imetumwa kwa";
$lang['mailandsms_students'] = "Wanafunzi";
$lang['mailandsms_parents'] = "Wazazi";
$lang['mailandsms_teachers'] = "Walimu";
$lang['mailandsms_librarians'] = "Wakutubi";
$lang['mailandsms_accountants'] = "Wahasibu";
$lang['mailandsms_template'] = "Template";
$lang['mailandsms_type'] = "Aina";
$lang['mailandsms_email'] = "Barua pepe";
$lang['mailandsms_sms'] = "Ujumbe Mfupi";
$lang['mailandsms_getway'] = 'Imetumwa kwa';
$lang['mailandsms_subject'] = 'Kichwa cha Ujumbe';
$lang['mailandsms_message'] = "Ujumbe";
$lang['mailandsms_dateandtime'] = "Tarehe na Muda";
$lang['sms_sent_successfully'] = "Ujumbe umetumwa kwa wazazi";



$lang['mailandsms_clickatell'] = "Clickatell";
$lang['mailandsms_twilio'] = "Twilio";
$lang['mailandsms_bulk'] = "Bulk";


$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['send'] = "Tuma";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa!';
$lang['mail_error'] = 'Barua pepe imeshindwa kutumwa!';
$lang['mail_error_user'] = 'Orodha ya watumia haipo!';return $lang;





