<?php

/* List Language  */
$lang['panel_title'] = "Posta / Ujumbe Mfupi Template";
$lang['add_title'] = "Ongeza template";
$lang['slno'] = "#";
$lang['mailandsmstemplate_name'] = "Jina";
$lang['mailandsmstemplate_user'] = "Mtumiaji";
$lang['mailandsmstemplate_tags'] = "Kiambatanisho";
$lang['mailandsmstemplate_template'] = "Template";
$lang['mailandsmstemplate_select_user'] = " Chagua Mtumiaji ";
$lang['mailandsmstemplate_student'] = "Mwanafunzi";
$lang['mailandsmstemplate_parents'] = "Wazazi";
$lang['mailandsmstemplate_teacher'] = "Mwalimu";
$lang['mailandsmstemplate_accountant'] = "Mhasibu";
$lang['mailandsmstemplate_librarian'] = "Mkutubi";
$lang['mailandsmstemplate_type'] = "Aina";
$lang['mailandsmstemplate_select_type'] = "Chagua Aina";
$lang['mailandsmstemplate_email'] = "Barua pepe";
$lang['mailandsmstemplate_sms'] = "Ujumbe Mfupi";
$lang['mailandsmstemplate_select_tag'] = "Chagua Kiambanisho";
$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['add_template'] = 'Ongeza Template';
$lang['update_template'] = 'Sasisha Template';return $lang;