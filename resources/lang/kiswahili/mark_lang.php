<?php

/* List Language  */
$lang['panel_title'] = "Alama";
$lang['add_title'] = 'Ongeza Alama';
$lang['slno'] = "#";
$lang['mark_exam'] = "Mtihani";
$lang['mark_classes'] = "Darasa";
$lang['upload_file']='Pakia faili';
$lang['published']='Hii ripoti ishachapishwa. Huwezi kusasiha maksi kwenye ripoti ambazo zimeshachapishwa';
$lang['file_download']='Pakua faili hapa';
$lang['action']='Hatua';
$lang['efforts']='Juhudi';
$lang['import_from_excel']='Ingiza kutoka kwenye excel';
$lang['upload_marks']='Pakia maksi kutoka faili la excel';
$lang['mark_student'] = "Mwanafunzi";
$lang['upload_mark']='Pakia alama';
$lang['optional_subject']='Hili ni somo la hiari. Maksi zitakazoongezwa kwa mwanafunzi zitamfanya asajiliwe moja kwa moja kwenye kundi la wanafunzi wanaosoma hili somo. Kumbuka , ukitoa maksi kwa mwanafunzi yoyote kuwa 0 au tupu, huyo mwanafunzi hatatolewa moja kwa moja kwenye kundi la waliosajiliwa kwenye somo mpaka umtoe kwa';
$lang['marks']='Alama';
$lang['clicking here..']='Bonyeza hapa..';
$lang['marking']='Kwenye kuongeza alama, mfumo utahifadhi maksi za wanafunzi watakaonekana kwenye ukurasa (Kwa chaguo-msingi wanafunzi 10 kwenye ukurasa moja).Baada ya kuingiza maksi, bonyeza kitufe cha MAKSI au kibonyezo ENTER . Kila mara ukienda kwenye ukurasa unaofwata, unahitaji kuingiza maksi kwenye ukurasa huo na kisha kubonyeza MAKSI ili kuhifadhi. Mfumo hauto hifadhi maksi za wanafunzi waliojificha ';
$lang['Subject'] = "Somo";
$lang['Topic'] = "Mada";
$lang['Geography'] = "Geographia";
$lang['no_semester']='Hamna muhula umeongezwa, Tafadhali';
$lang['add_semester']='ongeza muhula';
$lang['for this class first']='kwenye darasa hili kwanza';
$lang['exam']='Mtihani';
$lang['exam_date']='Tarehe ya mtihani';
$lang['no_sections']='Hamna vipengele vilivyowekwa kwenye hili darasa. Tafadhali ';
$lang['mark_instructions']='Maelezo ya kusahisha';
$lang['add_section']='ongeza kipengele';
$lang['for_class']='kwenye darasa hili kwanza';
$lang['exam_subject']='Somo la mtihani';
$lang['file_upload']='Faili unalo pakia LAZIMA liwe na muundo sawa na faili la sampuli ili alama ziweze kupakiwa(majina ya somo yawe kwenye herufi ndogo)';
$lang['subject']='Somo';
$lang['mark_semester'] = "Muhula";
$lang['student_classes']='Darasa la mwanafunzi';
$lang['exam_academic_year']='Mwaka wa mtihani';
$lang['exam_select_year']='Chagua mwaka';
$lang['mark_select_semester']='Chagua muhula';
$lang['mark_subject'] = "Somo";
$lang['mark_photo'] = "Picha";
$lang['mark_name'] = "Jina";
$lang['mark_roll'] = "Namba ya Udahili";
$lang['mark_phone'] = "Namba ya Simu";
$lang['mark_dob'] = "Tarehe ya Kuzaliwa";
$lang['mark_sex'] = "Jinsia";
$lang['mark_religion'] = "Dhehebu";
$lang['mark_email'] = "Barua pepe";
$lang['mark_address'] = "Anuani";
$lang['mark_username'] = "Jina la Mtumiaji";

$lang['mark_subject'] = "Somo";
$lang['mark_section'] = "Mkondo";
$lang['mark_mark'] = "Alama";
$lang['mark_point'] = "Pointi";
$lang['mark_grade'] = "Daraja";


$lang['mark_select_classes'] = "Chagua Darasa";
$lang['mark_select_exam'] = "Chagua Mtihani";
$lang['mark_select_subject'] = "Chagua Somo";
$lang['mark_select_section'] = "Chagua Mkondo";
$lang['mark_select_student'] = "Chagua Mwanafunzi";
$lang['mark_success'] = "Imefanikiwa";
$lang['personal_information'] = "Taarifa Binafsi";
$lang['mark_information'] = "Taarifa za Alama";
$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['pdf_preview'] = 'Onesha PDF ya awali';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa barua pepe";

// /* Add Language */
$lang['add_mark'] = 'Alama';
$lang['add_sub_mark'] = 'Ongeza Alama';

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';
$lang['report_information']='Ripoti ya Mitihani';
$lang['marking_status']='Hali ya usahihishaji';
$lang['abbreviation']='Kifupisho';

return $lang;