<?php

/* List Language  */
$lang['panel_title'] = "Media";
$lang['add_title'] = "Tengeneza Folder";
$lang['slno'] = "#";
$lang['media_title'] = "Kichwa";
$lang['media_date'] = "Tarehe";
$lang['action'] = "Hatua";

$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_class'] = 'Ongeza media';
$lang['update_class'] = 'Sasisha media';
$lang['file'] = 'Media';
$lang['upload_file'] = 'Pakia media';
$lang['folder_name'] = 'Jina la folder';
$lang['share'] = 'Shirikisha';
$lang['share_with'] = 'Shirikisha na';
$lang['select_class'] = 'Chagua Darasa';
$lang['all_class'] = 'Darasa lote';
$lang['public'] = 'Wazi';
$lang['class'] = 'Darasa';

$lang['access'] = 'Kusoma'; 
$lang['live_class'] = 'Vipindi Vyote Vinavyofundishwa Live'; 
$lang['live_recorded'] = 'Vipindi Vyote vilivyorekodiwa tayari '; 
$lang['live_discussion'] = 'Kushiriki majadiliano ya moja kwa moja'; 
$lang['unlimited'] = 'Pakua'; 
$lang['download_notes'] = 'Notisi, vitabu, Mitihani bila Kikomo'; 
$lang['self_evaluate'] = 'Kujipima mwenyewe'; 
$lang['live_access'] = 'Shiriki vipindi'; 
$lang['qn_quiz'] = 'kwa Maswali na Mazoezi'; 
$lang['best_teacher'] = 'vya waalimu bora  moja kwa moja(live)'; 
$lang['pay_now'] = 'Lipia'; 
$lang['now'] = 'Sasa '; 
$lang['off'] = 'Ofa'; 
$lang['only'] = 'Tu'; 
$lang['days'] = 'Siku'; 
$lang['week_plan'] = 'Malipo kwa Wiki'; 
$lang['day_plan'] = 'Malipo kwa Siku'; 
$lang['month_plan'] = 'Malipo kwa Mwezi'; 
$lang['day'] = 'Siku'; 

return $lang;