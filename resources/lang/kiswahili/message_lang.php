<?php

/* List Language  */
$lang['panel_title'] = "Ujumbe";
$lang['add_title'] = "Andika";
$lang['folder'] = "Folda";
$lang['subject2']='Kichwa cha ujumbe';

$lang['choose_file']='Chagua faili';
$lang['message']='Ujumbe';
$lang['inbox'] = "Kisanduku pokezi";
$lang['to'] = "Kwenda";
$lang['status'] = "Hali";
$lang['name'] = "Jina";
$lang['subject'] = "Somo";
$lang['attach'] = "Ambatanisha";
$lang['time'] = "Muda";
$lang['sent'] = "Imetumwa";
$lang['trash'] = "Takataka";
$lang['favorite'] = "Ninayoipenda";
$lang['slno'] = "#";
$lang['message_title'] = "Kichwa";
$lang['message_message'] = "Ujumbe";
$lang['message_date'] = "Tarehe";
$lang['action'] = "Hatua";
$lang['read_message'] = "Soma Ujumbe";
$lang['reply'] = "Jibu";
$lang['compose_new'] = "Andika ujumbe mpya";
$lang['admin_select_label'] = "Orodha ya Wasimamizi";
$lang['student_select_label'] = "Orodha ya wanafunzi";
$lang['parent_select_label'] = "Orodha ya wazazi";
$lang['teacher_select_label'] = "Orodha ya walimu";
$lang['librarian_select_label'] = "Orodha ya wakutubi";
$lang['accountant_select_label'] = "Orodha ya wahasibu";
$lang['attachment'] = "Kiambanisho";
$lang['send'] = "Tuma";
$lang['close'] = "Funga";
$lang['sender'] = "Mtumaji";
$lang['discard'] = "Tupa";
$lang['error'] = "Majibu hayajatumwa!";
return $lang;