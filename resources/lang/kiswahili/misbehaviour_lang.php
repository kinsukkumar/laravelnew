<?php

/* List Language  */
$lang['panel_title'] = "Kipengele cha tabia mbaya ya mwanafunzi";
$lang['panelcat_title'] = "Tabia Mbaya ";
$lang['add_title'] = "Ongeza aina ya tabia mbaya";
$lang['slno'] = "#";
$lang['misbehaviour_name'] = "Jina la tabia mbaya";
$lang['discount_amount'] = "Kiasi cha punguzo";
$lang['misbehaviour_amount'] = "Kiasi";
$lang['misbehaviour_is_repeative'] = "Inajirudia";
$lang['misbehaviour_is_repeative_yes'] = "Ndio";
$lang['exam_select_year']='Chagua mwaka';
$lang['misbehaviour_is_repeative_no'] = "Hapana";
$lang['misbehaviour_startdate'] = "Tarehe ya kuanza";
$lang['misbehaviour_enddate'] = "Tarehe ya kumaliza";
$lang['discount_amount'] = "Kiasi cha punguzo";
$lang['misbehaviour_note'] = "Note";
$lang['action'] = "Hatua";
$lang['misbehaviourcategory']="Kikundi cha tabia mbaya";
$lang['feetpype_select_category']="Chagua kikundi cha tabia mbaya";
$lang['category_title']="Ongeza kikundi ha tabia mbaya";
$lang['category_fee']="Kikundi cha tabia mbaya";
$lang['category_note']="Category Note";
$lang['misbehaviour_category']="Kikundi cha aina ya tabia mbaya";
$lang['add_category']="Ongeza kikundi";
$lang['select_misbehaviour_category']="Chagua kikundi cha tabia mbaya";

$lang['edit'] = 'Harir';
$lang['delete'] = 'Futa';

/* Add Language */
$lang['add_misbehaviour'] = 'Ongeza tabia mbaya';
$lang['update_misbehaviour'] = 'Sasisha tabia mbaya';
$lang['student_name'] = 'Jina la mwanafunzi';
$lang['fee_percent'] = 'Asilimia ya tabia mbaya';
$lang['misbehaviour_account'] = 'Akaunti ya aina ya tabia mbaya';
$lang['feetpype_select_account'] = 'Chagua namba ya akaunti';
 $lang['menu_misbehaviour'] ='Hali ya mwanafunzi'; return $lang;