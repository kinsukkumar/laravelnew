<?php

/* List Language  */
$lang['panel_title'] = "Bodi ya habari";
$lang['add_title'] = "Ongeza tukio";
$lang['slno'] = "#";
$lang['news_board_title'] = "Kichwa";
$lang['news_board_news_board'] = "maelezo ya habari";
$lang['news_board_date'] = "Tarehe";
$lang['action'] = "Hatua";
$lang['upload_calendar']='Pakia kalenda ya Shule';
$lang['download']='Pakua';
$lang['sample_file']='Mfano wa faili hapa';
$lang['submit']='Wasilisha habari';

$lang['view'] = 'Tazama';
$lang['edit'] = 'hariri';
$lang['delete'] = 'Futa';
$lang['print'] = 'Chapisha';
$lang['pdf_preview'] = 'hakiki PDF';
$lang["mail"] = "Tuma Pdf kwa barua";

/* Add Language */

$lang['add_class'] = 'Ongeza tukio';
$lang['update_class'] = 'sahisha tukio';
$lang['news_board_for']='Habari za';

$lang['to'] = 'kwa';
$lang['subject'] = 'Mada';
$lang['select_all']='Chagua wote';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Uwanja huu unahitajika kujazwa";
$lang['mail_valid'] = "Jaza barua pepe ya halali";
$lang['mail_subject'] = "Mada ni lazima kujazwa";
$lang['mail_success'] = 'Ujumbe umefanikiwa kutumwa';
$lang['mail_error'] = 'oops! Ujumbe Haujafanikiwa kutumwa'; 
$lang['welcome_board']='Karibu Bodi ya shule';
$lang['school_board']='Bodi ya shule';
$lang['news_board_made']='imetengenezwa kwa';
$lang['read_more']='soma zaidi';
return $lang;