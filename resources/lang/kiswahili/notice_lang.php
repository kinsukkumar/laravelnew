<?php

/* List Language  */
$lang['panel_title'] = "Notisi";
$lang['add_title'] = "Ongeza Notisi";
$lang['slno'] = "#";
$lang['notice_title'] = "Kichwa";
$lang['notice_notice'] = "Notisi";
$lang['notice_date'] = "Tarehe";
$lang['action'] = "Hatua";
$lang['upload_calendar']='Pakia kalenda ya shule kutoka kwenye faili la excel';
$lang['download']='Pakua';
$lang['sample_file']='faili la sampuli hapa';
$lang['submit']='Tuma';

$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['print'] = 'Chapa';
$lang['pdf_preview'] = 'Onesha PDF ya awali';
$lang["mail"] = "Tuma PDF kwa Barua pepe";
$lang['notice_for']='Notsi ni kwa ajili ya:';
$lang['select_all']='Chagua yote';

/* Add Language */

$lang['add_class'] = 'Ongeza Notisi';
$lang['update_class'] = 'Sasisha Notisi';

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';return $lang;