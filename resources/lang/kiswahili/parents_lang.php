<?php
/* List Language  */
$lang['panel_title'] = "Wazazi";
$lang['add_title'] = "Ongeza mzazi";
$lang['slno'] = "#";
$lang['parents_photo'] = "Picha";
$lang['parents_name'] =$lang['parents_name']= "Jina la mzazi";
$lang['name'] = "Jina la mzazi au mlezi";
$lang['student_classes']="Darasa la mwanafunzi";
$lang['employer']="Mwajiri";
$lang['occupation']="Kazi";
$lang['menu_parents']="Wazazi";
$lang['occupation_eg']="Kazi ya mzazi mfano Mkulima";
$lang['employer_eg']="Mwajiri wa mzazi mfano Inets company Ltd";
$lang['submit_parent']="Tuma";
$lang['add']='Ongeza mzazi au mlezi';
$lang['names_eg']='Majina yote mfano Robert Samson';
$lang['profession']='Taaluma';
$lang['guardian_employer']='Mwajiri wa mzazi/mlezi';
$lang['parent_info']='Maelezo ya mzazi';
$lang['fields_marked']='Sehemu zenye alama';
$lang['are_mandatory']='ni za lazima';
$lang['file']='Pakua faili ya sampuli hapa';
$lang['phy_cond']='Chagua hali ya kimwili';
$lang['upload_excel']='Pakia wazazi kutoka kwenye excel';
$lang['parents_email'] = "Barua pepe";
$lang['email'] = "Barua pepe halali mfano samson@yahoo.com";
$lang['relation_s']='Uhusiano na mwanafunzi mfano Mjomba';
$lang['parents_dob'] = "Tarehe ya Kuzaliwa";
$lang['parent_sex'] = "Jinsia";
$lang['parent_sex_male'] = "Kiume";
$lang['parent_sex_female'] = "Kike";
$lang['parent_dob']='Tarehe ya kuzaliwa';
$lang['parents_religion'] = "Dini";
$lang['parents_phone'] = "Namba ya Simu";
$lang['student_select_class']='Chagua darasa la mwanafunzi';
$lang['phone_eg'] = "Namba ya simu kuu";
$lang['address']='Anuani ya mzazi mfano S.L.P 33287,Dar';
$lang['parents_address'] = "Anuani";
$lang['parents_classes'] = "Darasa";
$lang['parents_roll'] = "Namba ya Udahili";
$lang['physical_condition']='Hali ya kimwili';
$lang['view_profile']='Angalia wasifu';
$lang['parents_photo'] = "Picha";
$lang['relation'] = "Uhusiano";
$lang['parents_username'] = "Jina la Kutumia";
$lang['parents_password'] = "Neno Siri";
$lang['parents_select_class'] = "Chagua Darasa";

/* parents */
$lang['parents_guargian_name'] = "Jina la Mlezi";
$lang['parents_father_name'] = "Jina la Baba";
$lang['parents_mother_name'] = "Jina la Mama";
$lang['parents_father_profession'] = "Kazi ya Baba";
$lang['parents_mother_profession'] = "Kazi ya Mama";


$lang['action'] = "Hatua";
$lang['view'] = 'Tazma';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['pdf_preview'] = 'Onesha PDF ya Awali';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa Barua pepe";

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';

/* Add Language */

$lang['add_parents'] = 'Ongeza Mzazi';
$lang['update_parents'] = 'Sasisha Mzazi';
$lang['preferred_language'] = 'Lugha Pendekezwa';
$lang['kiswahili'] = 'Kiswahili';
$lang['english'] = 'English';

/* ini code starts here*/
$lang['personal_information'] = "Taarifa Binafsi";
$lang['parentss_information'] = "Taarifa za Wazazi";
$lang['parent_information']='Taarifa za Wazazi';
$lang['admission_status']='Hali ya Usajili';

//INVOICE
$lang['invoice_notpaid'] = "Haijalipwa";
$lang['invoice_partially_paid'] = "Imelipwa kiasi";
$lang['invoice_fully_paid'] = "Imelipwa Kamili";

return $lang;
