<?php 

$lang['panel_title'] = 'Mpangilio wa Malipo';
$lang['paypal_email'] = 'Barua pepe ya paypal';
$lang['paypal_api_username'] = 'Jina la Paypal';
$lang['paypal_api_password'] = 'Neno siri la Paypal';
$lang['paypal_api_signature'] = 'Sahihi ya Paypal';
$lang['paypal_demo'] = 'Paypal Sandbox';
$lang['save'] = 'Tunza';

$lang['tab_paypal'] = 'Paypal';
$lang['tab_stripe'] = 'Stripe';
$lang['stripe_private_key'] = 'Stripe private key';
$lang['stripe_public_key'] = 'Stripe public key';return $lang;