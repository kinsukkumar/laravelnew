<?php

/* List Language  */
$lang['panel_title'] = "Daraja";
$lang['add_title'] = "Ongeza daraja";
$lang['slno'] = "#";
$lang['grade_name'] = "Jina la daraja";
$lang['grade_name_eg']='Jina la daraja eg A';
$lang['weight']='Uzito wa alama e.g 5';
$lang['minimum']='Kiwango cha chini cha hizi alama';
$lang['max']='Kiwango cha juu cha hizi alama';
$lang['excellent']='Eg Bora';
$lang['']='Hutumika kama maoni yatatokea chini ya ripoti kutokana na wastani wa mwanafunzi';
$lang['grade_remarks']='Maoini ya Alama';
$lang['overall remarks']='Maoini ya Jumla';
$lang['comment']='Hutumika kama maoni ya mwalimu wa darasa yatatokea chini ya ripoti kutokana na wastani wa mwanafunzi ';
$lang['grade_point'] = "Hatua y daraja";
$lang['grade_gradefrom'] = "Alama kuanzia";
$lang['grade_gradeupto'] = "Alama juu ya";
$lang['grade_note'] = "Maoni";
$lang['overall_note'] = "Maoini ya Jumla";

$lang['action'] = "Hatua";
$lang['select_level']='Chagua kiwango maalum cha darasa kupea daraja.';
$lang['select_classlevel']='Chagua kiwango cha darasa';
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['grade_info']='Habari ya daraja';
$lang['fields_marked']='Sehemu za alama';
$lang['are_mandatory']='Lazima';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_class'] = 'Ongeza daraja';
$lang['update_class'] = 'Sasisha Grade';
$lang['classlevel'] = 'Kiwango cha darasa';
$lang['enter_pension'] = 'Weka Jina La Mafao';
$lang['select_pension']='Chagua Kundi la Fao';
$lang['add_allowance']='Weka Posho';
$lang['allowance_title']='Posho';
$lang['select']='Chagua';
$lang['category']='Daraja';

return $lang;