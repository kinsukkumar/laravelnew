 <?php

$lang['panel_title'] = "Wasifu";
$lang['profile_roll'] = "Namba ya Udahili";
$lang['profile_email'] = "Barua pepe";
$lang['profile_dob'] = "Tarehe ya Kuzaliwa";
$lang['profile_jod'] = "Tarehe ya Kujiunga";
$lang['profile_sex'] = "Jinsia";
$lang['profile_religion'] = "Dini";
$lang['profile_phone'] = "Namba ya Simu";
$lang['profile_address'] = "Anuani";
$lang['edit'] = 'Hariri';
$lang['add'] = 'Ongeza mzazi/mlezi';
$lang['students'] = 'Wanafunzi/Mwanafunzi';
$lang['student_photo'] = 'Picha';
$lang['student_name'] = 'Jina';
$lang['edit_all'] = 'Hariri yote';
$lang['profile_username']='Jina la wasifu';

$lang['relation_to_student']='Uhusuano na Mwanafunzi';
$lang['name']='JIna';
$lang['profession']='Kazi';
$lang['dob']='Tarehe ya Kuzaliwa';
$lang['employer']='Muajiri';
$lang['physical_condition']='Hali ya Kimwili';
$lang['welcome'] = 'Karibu';
$lang['proffesion']='Kazi';
$lang['view'] = 'Tazama';
$lang['_name'] = 'Jina';
$lang['employer'] = 'Mwajiri';
$lang['physical_condition']='Hali ya kimwili';
$lang['relation_to_student']='Uhusiano na mwanafunzi';

$lang['profile_guargian_name'] = "Jina la Mlezi";
$lang['profile_father_name'] = "Jina la Baba";
$lang['profile_mother_name'] = "Jina la Mama";
$lang['profile_father_profession'] = "Kazi ya Baba";
$lang['profile_mother_profession'] = "Kazi ya Mama";
$lang['parent_error'] = "Wazazi bado hwajaongezwa! Tafadhari Ongeza taarifa za mzazi";

$lang['personal_information'] = "Taarifa Binafsi";
$lang['parents_information'] = "Taarifa Binafsi";
return $lang;