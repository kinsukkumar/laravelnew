<?php

$lang['panel_title'] = "Kupanda darasa";
$lang['add_title'] = "Ongeza promotion";
$lang['slno'] = "#";
$lang['promotion_photo'] = "Picha";
$lang['promotion_name'] = "Jina";
$lang['promotion_section'] = "Sehemu";
$lang['promotion_result'] = "Matokeo";
$lang['promotion_pass'] = "Pita";
$lang['promotion_fail'] = "Shindwa";
$lang['promotion_from_classes']='Darasa analotoka';
$lang['promotion_to_classes']='Darasa analoenda';
$lang['promotion_modarate'] = "Bado";
$lang['promotion_phone'] = "Namba ya simu";
$lang['promotion_classes'] = "Darasa";
$lang['promotion_roll'] = "Namba ya Udahili";
$lang['promotion_create_class'] = "Tafadhari tengeneza darasa jipya";
$lang['promotion_select_class'] = "Chagua Darasa";
$lang['promotion_select_student'] = "Chagua Mwanafunzi";
$lang['add_all_promotion'] = "Promotion zote";
$lang['promotion_alert'] = "Notisi";
$lang['promotion_ok'] = "Sawa";
$lang['promotion_success'] = "Promotion Imekubaliwa";
$lang['promotion_emstudent'] = "Hakuna orodha ya wanafunzi";


$lang['action'] = "Hatua";

$lang['add_mark_setting'] = 'Seti Alama ya promotion';
$lang['add_promotion'] = 'Promotion kwenda darasa linalofuata';
$lang['promoted_student'] = "Wanafunzi ambao Hawakupandishwa Darasa linalofuata";

return $lang;