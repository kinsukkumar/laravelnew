<?php

/* List Language  */
$lang['panel_title'] = "Mapato ya Shule";
$lang['panel_category_title'] = "Jedwali la Akaunti";
$lang['add_title'] = "Ongeza Mapato";
$lang['add_category'] = "Ongeza aina za Mapato";
$lang['slno'] = "#";
$lang['payer_name'] = "Jina la Mlipaji";
$lang['revenue_name'] = "Jina";
$lang['amount'] = "Kiasi";
$lang['payer_phone'] = "Namba ya Mlipaji";
$lang['transaction_id'] = "Transaction ID";
$lang['date'] = "Tarehe ya Malipo";
$lang['total'] = "Jumla";
$lang['action'] = "Hatua";
$lang['note'] = "Maelezo";
$lang['receipt'] = "Risiti";
$lang['payer_email'] = "Barua pepe ya Mlipaji";
$lang['fee_type'] = "Aina ya Ada";
$lang['phone'] = "Namba ya simu";
$lang['email'] = "Barua Pepe";
$lang['ref_no'] = "Reference No.";
$lang['payment_method'] = "Namna ya Malipo";
$lang['account_code']='Account Code';
$lang['add_account']='Add Account';
$lang['account_type']='Account Type';
$lang['account_name']='Account Name';

$lang['view'] = 'Tazama';
$lang['edit'] = 'Badili';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_expense'] = 'Transactions';
$lang['category'] = 'Category';
$lang['expense'] = 'Name';
$lang['add_category'] = 'Add Category';
$lang['edit_category'] = 'Edit Category';
$lang['update_category'] = 'Update Category';
$lang['subcategory'] = 'Name';
$lang['update_expense'] = 'Update Expense';
$lang['select_expense'] = 'Select Name';
return $lang;