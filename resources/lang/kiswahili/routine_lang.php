<?php

/* List Language  */
$lang['panel_title'] = "Ratiba";
$lang['add_title'] = "Ongeza Ratiba";
$lang['slno'] = "#";
$lang['routine_classes'] = "Darasa";
$lang['routine_select_classes'] = "Chagua Darasa";
$lang['class_routine']='Ratiba ya darasa';
$lang['fields_marked']='Sehemu zenye alama';
$lang['are_mandatory']='ni za lazima';
$lang['routine_select_section'] = "Chagua Mkondo";
$lang['routine_subject_select'] = "Chagua Somo";
$lang['routine_select_student'] = "Chagua Mwanafunzi";
$lang['routine_all_routine'] = "Ratiba Zote";
$lang['routine_section'] = "Mkondo";
$lang['routine_student'] = "Mwanafunzi";
$lang['routine_subject'] = "Somo";
$lang['routine_day'] = "Siku";
$lang['routine_s_to'] = "Kwenda";
$lang['routine_start_time'] = "Muda wa Kuanza";
$lang['routine_end_time'] = "Muda wa Kumaliza";
$lang['routine_room'] = "Chumba";
$lang['routine_note'] = "Notisi";

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */
$lang['add_routine'] = 'Ongeza Ratiba';
$lang['update_routine'] = 'Hariri';

$lang['sunday'] = "Jumapili";
$lang['monday'] = "Jumatatu";
$lang['tuesday'] = "Jumanne";
$lang['wednesday'] = "Jumatano";
$lang['thursday'] = "Alhamisi";
$lang['friday'] = "Ijumaa";
$lang['saturday'] = "Jumamosi";

$lang['sample_routine_file'] = "Pakua Mfano wa Ratiba ya Shule Hapa..";
return $lang;