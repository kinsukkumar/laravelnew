<?php

/**
 * Description of classlevel_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$lang['panel_title']='Muhula';
$lang['add_title']='Ongeza muhula';
$lang['name']='Muhula';
$lang['start_date']='Tarehe ya kuanza';
$lang['semster/term']='Muhula mfano Muhula wa kwanza';
$lang['semester_info']='Maelezo ya muhula wa shule';
$lang['fields']='Sehemu zenye alama';
$lang['mandatory']='ni za lazima';
$lang['end_date']='Tarehe ya kumaliza';
$lang['note']='Notsi';
$lang['action']='Hatua';
$lang['slno']='#';

$lang['action'] = "Hatua";
$lang['view'] = 'Angalia';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['exam_select_year']='Chagua mwaka';
$lang['select_semester']='Chagua muhula';

/* Add Language */

$lang['class_level'] = 'Ngazi ya darasa';
$lang['academic_year'] = 'Mwaka wa masomo';
$lang['update_level'] = 'Sasisha muhula';
return $lang;