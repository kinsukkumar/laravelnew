<?php

/* List Language  */
$lang['panel_title'] = "Mwanafunzi";
$lang['classlevel']= "Ngazi ya darasa";
$lang['register_student']='Andikisha mwanafunzi';
$lang['excel_student']='Pakia excel';
$lang['view_profile']='Angalia wasifu';
$lang['name']='Majina yote mfano Jane Samson';
$lang['nationality']='Chagua uraia';
$lang['reg_status']='Hali ya usajili';
$lang['close']='Funga';
$lang['class name']='Jina la darasa';
$lang['academic_year']='Mwaka wa masomo';
$lang['number of students']='Idadi ya wanafunzi';
$lang['number of sections']='Idadi ya vipengele';
$lang['download']='Pakua';
$lang['select_city']='Chagua mji';
$lang['Religion']='Dini';
$lang['reg_no']='Andika namba ya udahili';
$lang['new_entry']='Usajili mpya';
$lang['joining']='Kujiunga';
$lang['parent/guardian_info']='Maelezo ya mzazi/mlezi';
$lang['parent_ype']='Chagua aina ya mzazi';
$lang['parent_name']='Jina';
$lang['parent_profession']='Ujuzi';


$lang['previous_records']= 'Rekodi ya mwanafunzi ya shule za awali';
$lang['submit']='Tuma';
$lang['health_condition']='Hali ya afya';
$lang['status']='Hali ya sasa ya afya';
$lang['file']='Pakua sampuli ya faili hapa';
$lang['upload_records']='Pakia wanafunzi kutoka kwenye faili la excel';
$lang['auto_generate']='Zalisha moja kwa moja';
$lang['student_nationality']='Uraia wa mwanafunzi';
$lang['student_city']='Mji anapokaa mwanafunzi';
$lang['student_health']='Afya ya mwanafunzi';
$lang['other_health_problem']='Matatizo mengine ya kiafya';
$lang['student_guardian']='Mlezi wa mwanafunzi';
$lang['parent_type']='Wadhifu wa aina ya mzazi';
$lang['student_admitted_from']="Mwanafuzi amekubaliwa kutoka";
$lang['student_year_finished']="Mwaka mwanafunzi aliomaliza";
$lang['student_region']='Mkoa mwanafunzi anakotokea';
$lang['student_reg_number']='Namba ya usajili ya mwanfunzi';
$lang['health_insurance']='Chagua iliyo sahihi';
$lang['student_other']= 'Nyingine';
$lang['add_title'] = "Ongeza Mwanafunzi";
$lang['physical_condition']='Hali ya kimwili';
$lang['student_registration']='Usahili wa mwanafunzi';
$lang['sregistration']='Mwanafunzi';
$lang['registration']='kuingia';
$lang['iregistration']='Usajili wa taarifa ya mwanafunzi';
$lang['eregistration']='Ingiza taarifa msingi za mwanafunzi';
$lang['phy_cond']='Chagua hali ya kimwili';
$lang['health_status']='Chagua hali ya afya';
$lang['student_insurance']='Bima ya afya';
$lang['slno'] = "#";
$lang['student_photo'] = "Picha";
$lang['student_name'] = "Jina";
$lang['student_email'] = "Barua pepe";
$lang['student_dob'] = "Tarehe ya Kuzaliwa";
$lang['student_jod'] = "Tarehe ya Kujiunga";
$lang['student_sex'] = "Jinsia";
$lang['student_sex_male'] = "Kiume";
$lang['student_sex_female'] = "Kike";
$lang['student_religion'] = "Dini";
$lang['student_phone'] = "Namba ya Simu";
$lang['student_address'] = "Anuani";
$lang['student_classes'] = "Darasa";
$lang['student_roll'] = "Namba ya Udahili";
$lang['student_username'] = "Jina la Kutumia";
$lang['student_password'] = "Neno Siri";
$lang['student_select_class'] = "Chagua Darasa";
$lang['student_guargian'] = "Mlezi";

/* Parent */
$lang['parent_guargian_name'] = "Mlezi";
$lang['parent_father_name'] = "Jina la baba";
$lang['parent_mother_name'] = "Jina la mama";
$lang['parent_father_profession'] = "Kazi ya Baba";
$lang['parent_mother_profession'] = "Kazi ya Mama";
$lang['parent_email'] = "Barua pepe";
$lang['parent_phone'] = "Namba ya Simu";
$lang['parent_address'] = "Anuani";
$lang['parent_username'] = "Jina la Kutumia";
$lang['parent_error'] = "Bado wazazi hwajaongezwa Tafadhari ongeza taarifa zao.";
$lang['other_phone']='Namba Nyingine';

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['parent'] = 'Ongea Mzazi';
$lang['pdf_preview'] = 'Onesha PDF ya Awali';
$lang['idcard'] = 'Namba ya Kitambulisho';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa Barua pepe";
$lang['relation']='Mahusiano';
$lang['date_registered']='Tarehe ya Usajili';

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';

/* Add Language */
$lang['personal_information'] = "Taarifa Binafsi";
$lang['parents_information'] = "Taarifa ya mzazi";
$lang['add_student'] = 'Ongeza mwanfunzi';
$lang['add_parent'] = 'Ongeza mzazi';
$lang['update_student'] = 'Sasisha Mwanafunzi';

$lang['student_section'] = 'Mkondo';
$lang['student_select_section'] = 'Chagua Mkondo';
$lang['student_distance_from_school']='Umbali kwenda Shule';

$lang['student_all_students'] = 'Wanafunzi wote';
$lang['report_information']='Ripoti ya Mitihani';

$lang['interview_title']='Tuma Ujembe kwa Mahojiano';
$lang['time']='Mda';
$lang['date']='Tarehe';

$lang['total_students']='Jumla ya Wanafunzi';
$lang['boys']='Wavulana';
$lang['girls']='Wasichana';
$lang['subject_type']='Aina ya Somo';
$lang['teacher']='Mwalimu';

//INVOICE
$lang['invoice_notpaid'] = "Haijalipwa";
$lang['invoice_partially_paid'] = "Imelipwa kiasi";
$lang['invoice_fully_paid'] = "Imelipwa Kamili";

//export excel
$lang['first_name']='Jina La Kwanza';
$lang['middle_name']='Jina la Kati';
$lang['last_name']='Jina la Mwisho';

$lang['employer']="Mwajiri";
$lang['occupation']="Kazi";
$lang['profession']='Taaluma';
$lang['relation'] = "Uhusiano";

//Student Prem View
$lang['physical']="Matatizo ya kimwili";
$lang['hearing']="Matatizo ya Kusikia";
$lang['disorder']="Ulemavu";
$lang['blind']="Upofu";
$lang['intellectual']="Matatizo ya Akili";
$lang['lowvision']="Low Vision";
$lang['student_info']="Taarifa Za Mwanafunzi";
$lang['parent_info']="Taarifa za Mzazi";
$lang['sex']="Jinsi";
$lang['school']="Shule";
$lang['region']="Mkoa";
$lang['district']="WilayaS";
$lang['number']="Namba";
$lang['name']="Jina";
$lang['male']="Mvulana";
$lang['female']="Msichana";


$lang['payment_report'] = "Taarifa za Malipo";
$lang['allocations'] = "Taarifa Zaidi";
$lang['exam_report'] = "Taarifa za Mitihani";
$lang['parent_report'] = "Taarifa za Mzazi";
$lang['student_report'] = "Taarifa za Mwanafunzi";
$lang['singlereport'] = 'Ripoti ya Mtihani Mmoja';
$lang['schoolreport'] = 'Ripoti ya Shule Kiujumla';
$lang['payment_record'] = 'Kumbukumbu za Malipo';
$lang['select'] = 'Chagua Darasa Kuona Matokeo';

$lang['number']='Namba';
$lang['name']='Jina';

$lang['students_upload'] = " Sehemu hii inakuwezesha kupakia(upload) wanafunzi na wazazi kwenye mfumo kwa Pamoja";
$lang['students_uploads'] = "Sehemu hii inakusaidia kupakia(upload) kwenye mfumo taarifa za Wanafunzi tu";
return $lang;



