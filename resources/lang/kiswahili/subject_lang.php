<?php

/* List Language  */
$lang['panel_title'] = "Somo";
$lang['specify']='Chagua mwalimu wa somo';
$lang['GUIDANCE']='MWONGOZO';
$lang['this']='Hii sehemu inasaidia kufafanua walimu wa masomo kwenye kipengele cha darasa,';
$lang['add_teacher']='Ongeza mwalimu';
$lang['class_name']='Jina la darasa';
$lang['subject']='Somo';
$lang['sections']='Kipengelee';
$lang['teachers']='Walimu';
$lang['class_level']='Ngazi ya darasa';
$lang['exam_select_year']='Chagua mwaka';
$lang['add_title'] = "Ongeza Somo";
$lang['subject_select_classes']='Chagua darasa';
$lang['subject'] = "Somo";
$lang['core']='Msingi';
$lang['option']='Chaguo';
$lang['advanced']='Ya juu';
$lang['advanced_subject_settings']='Mipangilio ya juu ya masomo';
$lang['subject_settings1']='Chagua kama hili somo linahesabiwa kwenye wastani ya jumla ya mwanafunzi au la';
$lang['subject_settings2']='Chagua kama hili somo lina adhabu au la (mfano. GS  & Advanced Math in A-Level).';
$lang['is_counted']='Linahesabika kwenye wastani ?';
$lang['YES']='NDIO';
$lang['NO']='HAPANA';
$lang['included']='Linahesabika kwenye mgawanyo (CSEE & ACSEE tu)?';
$lang['penalty']='Lina adhabu ?';
$lang['subject_register']='Kama huoni somo lako hapa nenda kwenye orodha ya masomo na usajili somo kwanza';
$lang['register']='Usajili wa somo';
$lang['subject_info'] = "Maelezo ya somo";
$lang['fields_marked'] = "Sehemu zenye alama";
$lang['are mandatory'] = "ni za lazima";
$lang['subject_name'] = "Jina la somo mfano hesabu";
$lang['subject_select']='Chagua somo';
$lang['subject_type']='Aina ya somo';
$lang['slno'] = "#";
$lang['subject_class_name'] = "Jina la Darasa";
$lang['subject_teacher_name'] = "Jina la Mwalimu";
$lang['subject_student'] = "Mwanafunzi";
$lang['subject_name'] = "Jina la Somo";
$lang['subject_author'] = "Aina ya Somo";
$lang['subject_code'] = "Namba ya Somo";
$lang['subject_teacher'] = "Walimu";
$lang['subject_classes'] = "Darasa";
$lang['select_all'] = "Yote";
$lang['subject_select_class'] = "Chagua Darasa";
$lang['subject_select_section'] = "Chagua Mkondo";
$lang['subject_select_teacher'] = "Chagua Mwalimu";
$lang['subject_select_student'] = "Chagua Mwanafunzi";


$lang['penaltity'] = "Kuna Adhabu";
$lang['is_counted_ave'] = 'Linajumlishwa Kwenye Wastani';
$lang['is_counted_div'] = 'Linajumlishwa Kwenye Madaraja';
$lang['pass_mark'] = 'Alama za Ufaulu';


$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['close'] = 'Funga';

/* Add Language */

$lang['add_subject'] = 'Ongeza Somo';
$lang['update_subject'] = 'Sasisha Somo';

$lang['add_section_subject'] = 'Ongeza Mwalimu wa Somo';
$lang['academic_year']='Mwaka wa Masomo';
$lang['photo']='Picha';
$lang['employment_type']='Aina ya Ajira';

$lang['arrangement']='Mpangilio';

$lang['please_download'] = 'Samahani pakua mfano wa excel ili kujua sehemu za kujaza. Kumbukua kujaza sehemu zote kwa Usahihi.';
$lang['download_sample'] = 'Pakua Sampuli ya Masomo Hapa..';
$lang['register_subject']='Ongeza Somo';
$lang['register_excel']='Ongeza Masomo Kwa Excel';
$lang['register_necta']='Ongeza Masomo Kwa NECTA';

$lang['use_necta'] = 'Tumia Vigezo Maalumu vya Necta Kuongeza Masomo';
$lang['click_necta'] = ' Bofya Hapa kuongeza faili lenye masomo muundo wa NECTA';
return $lang;