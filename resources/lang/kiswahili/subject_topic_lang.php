<?php

/* List Language  */
$lang['panel_title'] = "Somo_mada";
$lang['add_title'] = "Ongeza somo_mada";
$lang['add_class_subject_topic']="Ongeza darasa la somo_mada";
$lang['slno'] = "#";
$lang['subject_class_name'] = "Jina la darasa";
$lang['subject_topic_teacher_name'] = "Jina la mwalimu";
$lang['subject_help']='Hii sehemu inasaidia kufafanua mada ya somo kwenye darasa ,';
$lang['GUIDANCE']='MWONGOZO';
$lang['click_to_add']='Bonyeza ili kuongeza';
$lang['edit_subject']='Sasisha somo';
$lang['class_level*']='Ngazi ya darasa *';
$lang['Academic year *']='Mwaka wa masomo *';
$lang['Name of Topic']='Jina la mada';
$lang['Students']='Wanafunzi';
$lang['enter topics']='Ingiza mada';
$lang['no_subjects']='Hamna masomo yamefafanuliwa kwenye hili darasa. Tafadhali, ';
$lang['no_semester']='Hamna muhula umeongezwa kwenye ngazi hii, Tafadhali,';
$lang['add_semester(s)']='ongeza muhula';
$lang['subject_add']='ongeza somo kwenye hili darasa';
$lang['subject_topic_student'] = "Mwanafunzi";
$lang['subject_name'] = "Jina la somo";
$lang['subject_topic_type'] = "Aina ya Somo_mada";
$lang['subject_topic_code'] = "Msimbo wa Somo_mada";
$lang['exam_select_year'] = "Chagua mwaka wa masomo";
$lang['class_level'] = "Ngazi ya darasa";
$lang['subject_topic_classes'] = "Darasa";
$lang['semester'] = "Muhula";
$lang['select_all'] = "Vyote";
$lang['subject_topic_select_class'] = "Chagua darasa";
$lang['subject_topic_select_section'] = "Chagua kipengele";
$lang['subject_topic_select'] = "Chagua Somo_mada";
$lang['subject_select_classes'] = "Chagua darasa";
$lang['subject_topic_select_teacher'] = "Chagua mwalimu";
$lang['subject_topic_select_student'] = "Chagua mwanafunzi";

$lang['action'] = "Hatua";
$lang['view'] = 'Angalia';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['close'] = 'Funga';

/* Add Language */

$lang['add_subject_topic'] = 'Ongeza somo_mada';
$lang['update_subject_topic'] = 'Sasisha somo_mada'; 
return $lang;
