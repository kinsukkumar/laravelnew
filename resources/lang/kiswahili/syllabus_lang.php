<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 $lang['add_syylabus'] = 'Ongeza Benchmark ya Mtaala';
 $lang['benchmark'] = 'Benchmark ya Mtaala';
 $lang['add_benchmark'] = 'Ongeza Benchmark';
 $lang['edit_benchmark'] = 'Badili Benchmark';
 $lang['grade'] = 'Daraja';
 $lang['points'] = 'Alama';
 $lang['description'] = 'Maelezo Zaidi';
 $lang['grade_remark'] = 'Maoini';
 $lang['add_syllabus'] = 'Ongeza Mtaala';
 $lang['select_classes'] = 'Chagua Darasa';
 $lang['menu_syllabus'] = 'Mtaala';
 $lang['topic'] = 'Mada';
 $lang['subtopic'] = 'Mada Ndogo';
 $lang['specific_objective'] = 'Lengo Maalum';
 $lang['teaching_and_learning_activity'] = 'Kufundisha na Kujifunza';
 $lang['teaching_and_learning_resources'] = 'zana za Kufundishia na Kusoma';
 $lang['reference'] = 'Rejea';
 $lang['no_of_periods'] = 'Jumla ya Vipindi';
 $lang['select_subject'] = 'Chagua Somo';
 $lang['new_syllabus'] = 'Ongeza Mtaala Mpya';
 $lang['title'] = 'Jina';
 $lang['start_date'] = 'Tarehe ya Kuanza';
 $lang['end_date'] = 'Tarehe ya Kumaliza';
 $lang['code'] = 'msimbo';
 $lang['add_topic'] = 'Ongeza Mada';
 $lang['objectives'] = 'Malengo';
 $lang['add_obj_group'] = 'Ongeza Malengo';
 $lang['academic_name'] = 'Mwaka wa Masomo';
 $lang['add_sub_topic'] = 'Ongeza Mada Ndogo';
 $lang[''] = '';
 $lang['assessment'] = 'Tathmini';
$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['close'] = 'Funga';
 return $lang;

