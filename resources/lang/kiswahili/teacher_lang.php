<?php

/* List Language  */
$lang['panel_title'] = "Mwalimu";
$lang['add_title'] = "Ongeza Mwalimu";
$lang['slno'] = "#";
$lang['teacher_photo'] = "Picha";
$lang['employment']='Aina ya ajira';
$lang['teacher_id']='ID ya mwalimu';
$lang['employment_type'] = "Aina ya ajira";
$lang['both_names']='Majina yote mawili';
$lang['student_employment']='Chagua aina ya ajira';
$lang['email_eg']='Barua pepe halali mfano samson@yahoo.com';
$lang['no_eg']='mfano 655406004';
$lang['mail_eg']='mfano S.L.P 33287, Dar';
$lang['upload_teacher']='Pakia walimu kutoka kwenye faili la excel';
$lang['file_download']='Pakua faili la sampuli hapa';
$lang['submit']='Tuma';
$lang['teacher_name'] = "Jina"; 
$lang['designation'] = 'Cheo';
$lang['add_designation'] = 'Chagua cheo';
$lang['teacher_email'] = "Barua pepe";
$lang['teacher_dob'] = "Tarehe ya Kuzaliwa";
$lang['teacher_sex'] = "Jinsia";
$lang['teacher_sex_male'] = "Kiume";
$lang['teacher_sex_female'] = "Kike";
$lang['student_nationality'] = "Uraia";
$lang['physical_condition'] ='Hali ya kimwili';
$lang['phy_cond'] ='Chagua hali ya kimwili';
$lang['teacher_info'] ='Taarifa za mwalimu';
$lang['view_profile']='Angalia wasifu';
$lang['fields']='Sehemu zilizowekwa';
$lang['marked']='ni za lazima';
$lang['education']='Kiwango cha elimu';
$lang['add_education']='Chagua kiwango cha elimu';
$lang['health_status']='Hali ya afya';
$lang['insurance']='Chagua bima ya afya';
$lang['health_insurance']='Bima ya afya';
$lang['status']='Chagua hali ya afya';
$lang['teacher_religion'] = "Dini";
$lang['teacher_phone'] = "Namba ya simu";
$lang['physic_condition']='Hali ya kimwili';
$lang['teacher_address'] = "Anuani";
$lang['permanent'] = "Ya kudumu";
$lang['part_time'] = "Ya muda";
$lang['internship'] = "Tarajali";
$lang['religion'] = "Dini";
$lang['teacher_jod'] = "Tarehe ya Kujiunga";
$lang['teacher_username'] = "Jina la Kutumia";
$lang['teacher_password'] = "Neno Siri";


$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['print'] = "Chapa";
$lang['pdf_preview'] = "Onesha PDF ya awali";
$lang['idcard'] = "Mamba ya Kitambulisho";
$lang['mail'] = "Tuma PDF kwa barua pepe";

/* Add Language */
$lang['personal_information'] = "Taarifa Binafsi";
$lang['add_teacher'] = 'Ongeza Mwalimu';
$lang['update_teacher'] = 'Sasisha Mwalimu';

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';
$lang['.employment_type']= 'Aina ya ajira';
$lang['exam']='Jina la Mtihani';
$lang['max_score']='Maksi Kubwa';
$lang['min_score']='Maksi Ndogo';
$lang['average']='Wastani';
$lang['class']='Darasa';

$lang['basic_info'] = 'Taarifa Muhimu';
$lang['sent_sms'] = 'SMS Zilizotumwa';
$lang['activity'] = 'Taarifa Zaidi';
$lang['subjects_allocated'] = 'Masomo Anayofundisha';
$lang['students'] = 'Wanafunzi';
$lang['site_act'] = ' Linki Alizotembelea';
$lang['ondate'] = 'Tarehe';
$lang['device'] = 'Kifaa';
$lang['page_visit'] = 'Sehemu Alizoingia';
$lang['location'] = 'Mahali';
$lang['tabular']='Jedwari la Maendeleo ya Masomo';
$lang['dob'] = 'Kuzaliwa';
$lang['role'] = 'Cheo';
$lang['sex'] = 'Jinsia';
$lang['payroll_status'] = "Hali ya Malipo";

return $lang;