<?php

/* List Language  */
$lang['panel_title'] = "Mwanachama";
$lang['panel_title_profile'] = "Profile";
$lang['slno'] = "#";
$lang['tmember_photo'] = "Picha";
$lang['tmember_name'] = "Jina";
$lang['tmember_section'] = "Mkondo";
$lang['tmember_roll'] = "Namba ya Udahili";
$lang['tmember_email'] = "Barua pepe";
$lang['tmember_phone'] = "Namba ya simu ya mkononi";
$lang['tmember_tfee'] = "Ada ya Usafiri";
$lang['tmember_route_name'] = "Jina la Njia";
$lang['tmember_classes'] = "Darasa";
$lang['tmember_select_class'] = "Chagua Darasa";
$lang['classes_select_route_name'] = "Chagua Njia";
$lang['tmember_message'] = "Haujaongezwa bado.";

$lang['tmember_joindate'] = "Tarehe ya Kujiunga";
$lang['tmember_dob'] = "Tarehe ya Kuzaliwa";
$lang['tmember_sex'] = "Jinsia";
$lang['tmember_religion'] = "Dini";
$lang['tmember_address'] = "Anuani";

$lang['tmemmber_transport_routes']='Weka njia ya usafiri';
$lang['tmember_import_routes']='Weka Njia ya usafiri';
$lang['tmember_transport_sample']='Pakua mfano wa faili hapa , Jaza taarifa kama mfano wa huu excel, Pakia file hapa';
$lang['tmember_file']='weka mafaili';
$lang['tmember_information']='Habari:';
$lang['tmember_choose']='Chagua excel faili na upakie hapa';
$lang['tmember_download']='pakua mfano wa faili hapa:';
$lang['tmember_download_excel']='pakua excel';
$lang['upload_member_excel']='pakia wanachama kutoka excel';
$lang['tmember_id']='Kitambulisho';
$lang['tmember_name']='Jina';
$lang['tmember_way']='Njia';
$lang['tmember_route_name']='Jina la Njia';
$lang['tmember_amount']='Jumla Kiasi';
$lang['tmember_discount']='Punguzo';
$lang['tmember_discount_amount']='Punguzo Kiasi';
$lang['tmember_vehicle']='Usafiri';
$lang['tmember_action']='Hatua';

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['tmember'] = 'Usafiri';
$lang['delete'] = 'Futa';
$lang['pdf_preview'] = 'Onesha PDF ya awli';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa barua pepe";

$lang['personal_information'] = "Taarifa Binafsi";

$lang["add_tmember"] = "Ongeza Mwanachama";
$lang["update_tmember"] = "Sasisha Mwanachama";

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';return $lang;