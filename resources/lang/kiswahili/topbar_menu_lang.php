<?php

/* List Language  */
$lang['language'] = "Lugha";
$lang['profile'] = "Ukurasa";
$lang['change_password'] = "Badili Nywila";
$lang['panel_title'] = "Nywila";
$lang['panel'] = "Tabia";
$lang['messages']='Ujumbe mfupi';
$lang['view_profile']='Angalia wasifu';
$lang['Select Academic year']='Chagua mwaka wa masomo';
$lang['academic_year'] = 'Mwaka wa Masomo';
$lang['menu_invoice']='Ankara za Malipo';
$lang['menu_parent_invoice']="Ankara ya mzazi";
$lang['menu_mark_report']='Ripoti za Mitihani';
$lang['edit_profile']='Sasisha wasifu';
$lang['Select_character']='Chagua tabia';
$lang['Select class']='Chagua darasa';
$lang['Select Semester']='Chagua muhula';
$lang['paid_amount']='Hela Iliyolipwa';
$lang['old_password'] = "Andika password ya Zamani";
$lang['new_password'] = "Andika password Mpya";
$lang['re_password'] = "Rudia password Mpya";
$lang['forgot_password']='Umesahau Password';
$lang['questions']='Maswali na majibu';
$lang['username'] = "Nenotumizi";
$lang['school_address']='Anuani ya Shule';
$lang['make_sure']='Tafadhali hakikisha unatumia namba yako ya simu au username iliyopo katika mfumo, wasiliana na shule katika';
$lang['login_desc1']='Ingia kwenye akaunti yako kuendelea.';
$lang['login_desc2']='Kama umesahau au hauna password, tafadhali bofya katika umesahau password au alama ya ufunguo juu hapo kuipata';
$lang['login_social']='Au ingia kupitia taarifa ulizotumiwa kwenye ujumbe';
$lang['cannot_login']='Siwezi Ingia ndani';
$lang['forget_password']='Nimesahau Nenosiri';
$lang['name']='Jina';
$lang['roll']='Namba ya Usajili';
$lang['or'] = "au";
$lang['menu_report'] = 'Ripoti';
$lang['menu_other_report']='Ripoti Nyinginezo';
$lang['lost_password'] = "Bonyeza hapa kupata nywila mpya";
$lang['welcome']='Karibu';
$lang['account_setting']='Mpangilio wa akaunti';
$lang['fees']='Ada';
$lang['official_report']='Ripoti rasmi';
$lang['academy']='Kitaaluma';
$lang['subject_topics']='Mada za masomo';
$lang['topic_mark']='Maksi za mada';
$lang['exam_mark']='Maksi za mitihani';
$lang['assesment']='Tathmini';
$lang['transport_members']='Watumiaji wa usafiri';
$lang['installment']='Kipengee';
$lang['register_fee_details']='Andika maelezo ya ada';
$lang['fee_details'] = 'Mgawanyo wa Ada';
$lang['unsubscribe_student']='Mtoe mwanafunzi';
$lang['payment']='Malipo';
$lang['parent_invoice']='Ankara ya mzazi';
$lang['due_amount']='Deni Lililobaki';
$lang['print_reminder']='Chapisha kumbukumbu';
$lang['other_accounts']='Akaunti nyingine';
$lang['fixed_assests']='Mali za kudumu';
$lang['liabilities']='Madeni';
$lang['capital']='Mtaji';
$lang['account_reports']='Ripoti za akaunti';
$lang['income_statement']='Taarifa ya mapato';
$lang['balance_sheet']='Ripoti ya balance';
$lang['sms_parents']='SMS wazazi';
$lang['add_fees']='Ongeza ada';
$lang['student_reports']='Ripoti za mwanafunzi';

//reset password//
$lang['reset_password']='Badili Nywila';
$lang['reset_calculate']='Kokotoa';
$lang['reset_answer']='Jawabu';
$lang['reset_phone']=$lang['phone'] ='Nambari ya Simu';
$lang['website']='Tovuti';

$lang['reset_email']=$lang['email']='Barua Pepe';
$lang['reset_btn']='Wasilisha';
$lang['reset_backtologin']='Rudi kulogia';
$lang['reset_entercodes']='Ingiza namba za uhakiki';
$lang['reset_complete']='Malizia';
$lang['reset_send']='Barua pepe na meseji zimetumwa. Subiri kwa dakika chache kisha utapokea SMS au angalia barua pepe yako kuona namba za uhakiki';
$lang['class_student'] = "Wanafunzi wa Darasa";


$lang['logout'] = "Logua";
$lang['language'] = "Lugha";
$lang['view_more'] = "Angalia Taarifa Zote";
$lang['la_fs'] = "Una";
$lang['la_ls'] = "Taarifa";
$lang['close'] = "Funga";
$lang['login_hint']="Ingia ndani";
$lang['password']='Nenosiri';
$lang['login_btn']="Logia";
$lang['total']='Jumla';
$lang['slno']='#';
$lang['classes']='Madarasa';
$lang['character_code']='Msimbo wa tabia';
$lang['character_category']='Kikundi cha tabia';
$lang['menu_student_characters']='Tabia za wanafunzi';
$lang['student_character_remark']='Maoni ya tabia ya mwanafunzi';
$lang['description']='Maelezo';
$lang['teacher_name']='Jina la mwalimu';
$lang['action']='Hatua';
$lang['assign_title']='Toa cheo';
$lang['classlevel']='Ngazi ya darasa';
$lang['academic_year']='Mwaka wa masomo';
$lang['character_select_year']='Chagua mwaka';
$lang['semester']='Muhula';
$lang['class']='Darasa';
$lang['character']='Tabia';
$lang['teacherID']='Kitambulisho cha mwalimu';
$lang['assign_student_characters']='Toa tabia kwa mwanafunzi';

$lang['Admin'] = "Admini";
$lang['Student'] = "Mwanafunzi";
$lang['Parent'] = "Mzazi";
$lang['Teacher'] = "Mwalimu";
$lang['Accountant'] = "Mhasibu";
$lang['Librarian'] = "Mkutubi";

$lang['Laboratorian'] = "Mtaalamu wa Maabara";
$lang['Driver'] = "Dereva";
$lang['Guards'] = "Walinzi";
$lang['Matron'] = "Matron";
$lang['Patron'] = "Patron";
$lang['Cook'] = "Mpishi";
$lang['Nurse'] = "Nesi";
$lang['Other'] = "Mwengine";

$lang['success'] = "Imefanikiwa!";
$lang['cmessage'] = "Nywila yako imebadilishwa.";

/* Menu List */
$lang['menu_add'] = 'Ongeza';
$lang['menu_edit'] =$lang['edit'] = 'Hariri';
$lang['menu_view'] = 'Angalia';
$lang['menu_success'] = 'Imefanikiwa';
$lang['menu_dashboard'] = 'Dashibodi';
$lang['menu_student'] = 'Wanafunzi';
$lang['menu_parent'] = 'Wazazi';
$lang['menu_teacher'] = 'Walimu';
$lang['menu_user'] = 'Watumiaji';
$lang['menu_staff']= "Watumiaji";
$lang['menu_classes'] = 'Madarasa';
$lang['menu_section'] = 'Mikondo';
$lang['single_section'] = 'Mkondo';
$lang['menu_subject'] = 'Masomo';
$lang['class_subject']='Masomo kwa Darasa';
$lang['section_subject_teacher']='Walimu wasomo kimkondo';

$lang['menu_classlevel'] = 'Madaraja ya Madarasa';
$lang['menu_semester'] = "Muhula";
$lang['menu_grade'] = 'Madaraja';
$lang['special_grade_names'] = 'Majina ya Madaraja Maalumu';
$lang['special_grade'] = 'Madaraja Maalumu';
$lang['add_special_grade_name'] = 'Ongeza Jina la Daraja Maalumu';
$lang['add_special_grade'] = 'Ongeza Daraja Maalumu';
$lang['select_special_grades'] = 'Chagua Jina la Daraja Maalumu';


$lang['menu_exam'] = 'Mtihani';
$lang['menu_school_exam'] ='Mitihani ya shule';
$lang['menu_exam_definition']='Maelezo ya Mtihani';
$lang['menu_examschedule'] = 'Ratiba';
$lang['menu_create_report'] ='Tengeneza ripoti';
$lang['menu_view_report']= 'Zilizotengenezwa';
$lang['menu_library'] = 'Maktaba';
$lang['menu_library_report']="Ripoti ya maktaba";
$lang['general_report'] ="Ripoti ya jumla";
$lang['other_report']='Ripoti nyingine';
$lang['menu_mark'] = 'Alama';
$lang['menu_routine'] = 'Ratiba';
$lang['menu_attendance'] = 'Mahudhurio';
$lang['menu_characters']='Tabia';
$lang['menu_character_list']="Tabia";
$lang['menu_character_grades']="Madaraja ya tabia";
$lang['definitions']='Mfafanuo';
$lang['religions'] = "Orodha ya dini";
$lang['countries'] ='Nchi';
$lang['cities'] = "Miji";
$lang['health_conditions'] = "Hali za afya";
$lang['physical_disabilities'] = "Ulemavu wa kimwili";
$lang['health_insurance'] = "Bima za afya";
$lang['teacher_education_level']='Kiwango cha elimu cha mwalimu';
$lang['payment_reminder_template']='Template ya kumbukumbu ya malipo';
$lang['occupations'] = "Kazi";
$lang['employment_types'] = "Aina za ajira";
$lang['menu_assign']='Toa';
$lang['menu_assess']='Kutathmini';
$lang['menu_general']='Tathmini ya Jumla';
$lang['menu_member'] = 'Mwanachama';
$lang['menu_books'] = 'Vitabu';
$lang['menu_issue'] = 'Maswala';
$lang['setting']='Mfafanuo wa mfumo na vitu vitumikavyo ';
$lang['menu_fine'] = 'Faini';
$lang['menu_profile'] = 'Ukurasa';
$lang['menu_transport'] = 'Usafiri';
$lang['menu_transport_routes']='Njia za Usafiri';
$lang['menu_hostel'] = 'Bweni';
$lang['menu_account_new']='Akaunti mpya';
$lang['bank_account']='Akaunti ya benki';
$lang['menu_feeclass']='Aina ya ada';
$lang['menu_discount']='Punguzo';
$lang['menu_payment_history']='Malipo';
$lang['menu_global_exchange']='Sarafu za Dunia';
$lang['menu_exam_groups']='Kikundi cha Mtihani';
$lang['exams_class_allocation']='Mgao wa darasa';
$lang['menu_accumulative_report']='Taarifa za pamoja';
$lang['menu_comment_per_student']='Tabia ya mwanafunzi';
$lang['menu_minor_exams']='Mitihani midogo ya shule ';


$lang['menu_receipt']='Risiti';
$lang['menu_category'] = 'Kipengere';
$lang['menu_account'] = 'Akaunti';
$lang['menu_feetype'] = 'Aina ya Ada';
$lang['menu_setfee'] = 'Weka Ada';
$lang['menu_balance'] = 'Salio';
$lang['menu_paymentsettings'] = 'Mpangilio wa malipo';
$lang['no_role']='Hamna jukumu limewekwa. Liweke ili kuchagua ruhusa';
$lang['menu_setting_general']='Mpangilio';
$lang['user_permission']='Ruhusa za mtumiaji';
$lang['select_role'] = "Chagua jukumu";
$lang['role_specified'] = "Jukumu maalum";
$lang['user_role'] = "Jukumu la mtumiaji";
$lang['available_roles'] = "Majukumu yaliyopo";
$lang['role'] = "Jukumu";
$lang['option'] = "Chaguo";
$lang['add_new_role'] = "Ongeza jukumu jipya";
$lang['menu_setting_definition']='Ufafanuzi wa mpangilio';
$lang['payment_type']='Aina ya Malipo';

$lang['menu_expense'] = 'Matumizi';
$lang['menu_revenue']='Mapato';
$lang['menu_notice'] = 'Kalenda ya Shule';
$lang['menu_report'] = 'Ripoti';
$lang['menu_setting'] = 'Mpangilio';
$lang['menu_signature'] = 'Saini';
$lang['menu_vehicle'] = 'usafiri';
$lang['transport_member_details'] = 'Taarifa za mwanachama';

$lang['accountant'] = 'Mhasibu';
$lang['librarian'] = 'Mkutubi';

$lang['upload'] = "Pakia";
$lang['help']='Msaada';


/* Start Update Menu */
$lang['menu_sattendance'] = 'Mahudhurio ya Mwanafunzi';
$lang['menu_tattendance'] = 'Mahudhurio ya Mwalimu';
$lang['menu_eattendance'] = 'Mahudhurio ya Mtihani';
$lang['menu_promotion'] = 'Kupanda darasa';
$lang['menu_media'] = 'Media';
$lang['menu_message'] = 'Meseji';
$lang['menu_smssettings'] = 'Mpangilio wa SMS';
$lang['menu_mailandsms'] = 'Barua pepe / SMS';
$lang['menu_mailandsmstemplate'] = 'Barua pepe / SMS Template';
$lang['menu_invoice'] = 'Ankara';

/* End Update Menu */
/**
 * Inventory navigation
 */
$lang['menu_inventory'] = 'Vitu';
$lang['menu_vendor'] = 'Waujazi';
$lang['menu_stockregister'] = 'Msajiri Vitu';
$lang['menu_message'] = 'Ujumbe';
$lang['administration']='Uongozi wa Shule';
$lang['technical_support']='Msaada wa Kiufundi';
$lang['reach_support']='Msaada kutoka ShuleSoft';
$lang['menu_chart_account'] = 'Akaunti Jedwali';

//payroll
$lang['id_number']='Namba ya kitambulisho';
$lang['salary']='Mshahara';
$lang['print']='Printi';

$lang['terms_for_admission']='Masharti ya Kujiunga';
$lang['admission_status']= 'Hali ya Usajili';
$lang['menu_submit']='Tuma';
$lang['total']='Jumla';
$lang['total_amount']='Jumla ya Kiasi';
$lang['priority']='Kipaumbele';
$lang['select_subject']='Chagua Somo';


$lang['exam_name']='Jina la Mtihani';
$lang['exam_date']='Tarehe ya Mtihani';
$lang['marking_status']='Hali ya Usahihishaji';
$lang['view_report']='Tazama Ripoti';
$lang['view_graph']='Tazama Jedwali';

//invoice
$lang['invoice_number']='Ankara';
$lang['invoice_date']='Tarehe ya Ankara';
$lang['total_paid']='Jumla Iliyolipwa';
$lang['total_unpaid']='Jumla Isiyolipwa';
$lang['payment_status']='Hali';

$lang['reset_success']='Barua pepe na meseji zimetumwa. Subiri kwa dakika chache kisha utapokea SMS au angalia barua pepe yako kuona namba za uhakiki';

$lang['menu_reconciliation']='Maridhiano';
$lang['user_upload']='Weka watu kwa njia ya Excel';

$lang['download']='Pakua';
$lang['upload_from_excel']='Weka kutoka kwenye ekseli';

$lang['menu_syllabus']='Mtaala';
$lang['menu_scheme_of_work']='Azimio la Kazi';
$lang['menu_lesson_plan']='Andalio la Somo';
$lang['menu_benchmark']='Kiashiria';
$lang['menu_inactive_user']='Watu waliohama';
$lang['download']='Pakua';
$lang['upload_from_excel']='Weka kutoka kwenye ekseli';
$lang['menu_inactive_user']='Watu waliohama';
$lang['bankaccount_account_name']='Jina la Akaunti';
$lang['salaries']='Mishahara';
$lang['or']='au';
$lang['comment_per_student']='Maoni kwa Mwanafunzi';
$lang['unapproved_payments']='Malipo ya Wazazi';
//$lang['no_single_report']='Hakuna matokeo yaliyowekwa kwenye ShuleSoft hadi sasa. Tafadhali wasiliana na %s kwa taarifa kamili. ';
//$lang['no_official_report']='Hakuna ripoti rasmi zilizowekwa kwenye ShuleSoft hadi sasa. Tafadhali wasiliana na %s kwa taarifa kamili';

$lang['unapproved_payments']='Malipo ya Wazazi';
$lang['no_single_report']='';
$lang['no_official_report']='';
$lang['save']='Hifadhi';
$lang['opening_balance']='Balansi ya Kuanzia';
$lang['class']='Darasa';
$lang['select_classes']='Chagua Darasa';
$lang['payment_deleted'] = 'Malipo Yaliyofutwa';
$lang['non_invoiced_revenue'] = 'Malipo za Ziada';
$lang['invoice_payment'] = 'Malipo ya Ankara';
$lang['capital'] = 'Mtaji';
$lang['account_reports'] = 'Ripoti za akaunti';
$lang['income_statement'] = 'Taarifa za Mapato';
$lang['cash_flow'] = 'Matumizi ya Fedha';
$lang['balance_sheet'] = 'Balance sheet';

$lang['finacial_statement'] = 'Taarifa za Kifedha';
$lang['balance_report'] = 'Ripoti ya Kiasi Kilichopo';
$lang['payment_report'] = 'Ripoti za Malipo';


$lang['installments'] = "Taarifa za Ada Zote";
$lang['summary'] = "Taarifa kwa Kifupi";
$lang['academic_yearr'] = 'Mwaka wa Masomo';
$lang['class_levels'] = "Ngazi ya Darasa";
$lang['summary'] = "Taarifa kwa Kifupi";

$lang['show_rank'] = "Onyesha Madaraja kwa Kila Somo";
$lang['show_grade'] = "Onyesha Grade kwa Kila Somo";
$lang['show_footer'] = "Onyesha Kwa chini Jumla, wastani na Nafasi Kwa kila somo.";
$lang['subject_name']='Jina la Somo';
$lang['total_mark']='Jumla ya Maksi';
$lang['subject_avg']='Wastani kwa Somo';
$lang['gd']='Ufauli';
$lang['rank']='Nafasi';
$lang['exam_name']='Jina la Mtihani';
$lang['class_name']='Darasa';
$lang['exam_dates']='Tarehe ya Mtihani';
$lang['summary']='Kwa Kifupi';
$lang['section']='MKONDO';
$lang['number_subject']='Jumla ya Masomo';
$lang['student_name']='Jina la Mwanafunzi';

$lang['ondate']='Tarehe';
$lang['page_visit']='Linki alizotembelea';
$lang['browser']='Browser';
$lang['device']='Kifaa';
$lang['location']='Katika Eneo';
$lang['activity'] ='Taarifa za Mtumiaji';

$lang['invoice_n']='Namba ya Ankara';
$lang['total_amount']='Jumla ya Kiasi';
$lang['total_paid']='Kiwango kilicholipwa';
$lang['unpaid']=' Ambazo Hazijalipwa';
$lang['payment_status']='Status ya Malipo';
$lang['payment_due']='Malipo ya Ziada';
$lang['action']='Hatua';
$lang['parent_infos']='Taarifa za Mzazi';
$lang['sms']='Meseji';
$lang['payments']='Malipo';
$lang['students']='Wanafunzi';
$lang['profiles']='Taarifa za Mtumiaji';
$lang['study_days'] = 'Siku za Masomo';
$lang['no_students'] = 'Idadi ya Wanafunzi';
$lang['section_info']='Taarifa za Mkondo';

$lang['total_student']='Jumla ya Wanafunzi';
$lang['created_at']='Iliwekwa Saa';
$lang['created_by']='Iliwekwa na';
$lang['last_update']='Mara ya Mwisho';
$lang['add_exam']='Ongeza Mtihani';
$lang['signature']='Sahihi';
$lang['mark']='Alama';

$lang['sex']='Jinsia';
$lang['dob']='Tarehe Kuzaliwa';
$lang['cell']='Simu';
$lang['edit']='Hariri';
$lang['delete']='Futa';
$lang['view']='Angalia';

$lang['user_contract']='Tarehe ya Mkataba Kuisha';
$lang['last_update']='Mara ya Mwisho';
$lang['add_exam']='Ongeza mtihani';
$lang['signature']='Sahihi';
$lang['mark']='Alama';
$lang['student_name']='Jina la Mwanfunzi';
$lang['number_subject']='Idadi ya Masomo';
$lang['reply']='Jibu Ujumbe';
$lang['employer type']='Aina ya Ajira';
$lang['basic_info']='Taarifa Muhimu';
$lang['sent_sms']='Sent SMS';
$lang['sms_sent']='SMS Zilizotumwa';
$lang['class_level_id']='Daraja la Darasa';
$lang['class_level']='Daraja la Darasa';
$lang['sex']='Jinsia';
$lang['exam_select_year']='Mwaka wa Masomo';
$lang['dob']='Tarehe ya Kuzaliwa';
$lang['class_sign'] = "Sahihi ya Mwalimu wa Darasa";
$lang['search_menu'] = "Tafuta Jina, Ankara, Simu au Barua pepe";
$lang['national_id'] = "Kitambulisho cha Taifa";
$lang['option']='Sio Lazima';
$lang['select_date']='Bofya kuchagua Tarehe';
$lang['subject_teachers']='Ongeza Mwalimu wa Somo';
$lang['subject_teachers_by']='Ongeza Mwalimu wa Somo kwa Excel';
$lang['submit']='Ongeza';
$lang['sex']='Jinsia';
$lang['download_sample']='Pakua Mfano wa Excel Hapa.. ';
$lang['please_download']='Pakua Mfano wa Excel ili kujua maeneo ya kujaza. Kumbuka kujaza taarifa zaote Muhimu kwa Usahihi.';
$lang['Expense']='Matumizi';
$lang['Summary']='Taarifa Zaidi';
$lang['type']='Ania ya Malipo';
$lang['expected_amount']='Kiwanjo Kilichotarajiwa';
$lang['expense_project']='Makadirio ya gharama';
$lang['revenue_received']='Mapato Yaliyopokelewa';
$lang['payments_received']='Malipo Yaliyopokelewa';
$lang['no_payments']='Idadi ya Malipo';
$lang['no_invoice']='Idadi ya Ankara';
$lang['today_collect']='Makusanyo ya Leo';
$lang['week_collect']='Makusanyo ya Wiki';
$lang['month_collect']='Makusanyo ya Mwezi';
$lang['project_collect']='Makadilio ya Makusanyo';
$lang['revenue']='Mapato';
$lang['expense_revenue']='Mapato na Matumizi';
$lang['comment-area']=" Maoni ya Wazazi na Walimu Kuhusu Maendeleo ya Mwanafunzi Kwenye Repoti Hii";
$lang['select_year'] = "Chagua Mwaka wa Masomo";
$lang['step_two'] = "Hatua ya Pili Taarifa za Mzazi";
$lang['step_three'] = "Hatua ya Tatu Taarifa za Shule ya Nyuma";
$lang['step_four'] = "Hatua ya NNe Taarifa za Matokeo ya Shule ya Nyuma";
$lang['student_information'] = "Taarifa za Mwanafunzi";
$lang['age_st']="Mgawanyiko Umri wa Wanafunzi";

$lang['age']="Umri ";
$lang['age_number']="Namba ya Umri wa Mwanafunzi";
$lang['gender_dst'] = "Mgawanyo kwa Jinsia";
$lang['gender_over'] = "Mgawanyo kwa Jinsia kwa Darasa na Level";
$lang['total_student'] = " Jumla ya Wanafunzi";
$lang['total_male'] = "Jumla ya Wavulana";
$lang['total_female'] = " Jumla ya Wasicana";
$lang['total'] = " Jumla";
$lang['male'] = "Wavulana";
$lang['female'] = "Wasicana";
$lang['gender_view'] = "Jinsia Kiujumla";
$lang['gender_cls'] = "Mgawanyo kwa Jinsia kwa Darasa";

$lang['physical'] = "Hali ya Afya ya Mwili";
$lang['student_hel'] = "Repoti ya Afya ya Mwanafunzi";
$lang['no_student'] = "Idadi ya Wanafunzi";
$lang['student_diff'] = "Idadi ya Wanafunzi wenye hali Tofauti za Afya";
$lang['health_cond'] = "Hali ya Afya";
$lang['health_diff'] = "Idadi ya Wanafunzi Wenye Hali Mbaya ya kiafya";
$lang['health_number'] = "Idadi ya Wanafunzi wenye Bima za Afya";
$lang['health_ins'] = "Bima ya Afya";
$lang['health_status'] = " Hali ya sasa Kiafya ";
$lang['status_no'] = "Idadi ya Wanafunzi Kulingana na Hali ya sasa Kiafya";
$lang['h_status'] = "Hali ya sasa Kiafya";

$lang['previous_sch'] = "Taarifa za Shule za Mwanzo";
$lang['previous_name'] = "Taarifa Majina ya Shule za Mwanzo";
$lang['schools'] = "Schools";
$lang['student_previous'] = "Taarifa za Aina ya Shule ya Mwanzo";
$lang['ownership'] = "Umiriki";
$lang['previous_location'] = "Taarifa za Eneo la Shule ya Mwanzo";
$lang['location'] = "Mahali";
$lang['previous_district'] = "Taarifa za Wilaya za Shule ya Mwanzo Zilipo";
$lang['district'] = "Wilaya";

$lang['student_religion'] = "Taarifa za Dini kwa Wanafunzi";
$lang['no_religion'] = "Idadi ya wanafunzi kwa kila Dini";
$lang['religion'] = "Dini";
$lang['g_religion'] = "Dini Kiujumla";
$lang['local_ins'] = "Taarifa za Maeneo";
$lang['student_local'] = "Taarifa za Maeneo ya Wanafunzi";
$lang['local_dst'] = "Maeneo Yote";
$lang['local_detail'] = "Taariza Zaidi";
$lang['student_other'] = "Ripoti Zingine za Mwanafunzi";
$lang['no_parent'] = "Idadi ya Mwanafunzi Kwa Aina ya Wazazi";
$lang['parent_type'] = "Aina ya Mzazi";
$lang['student_active'] = "Hali ya Mwanafunzi Kwenye Mfumo";
$lang['no_status'] = "Idadi ya Wanafunzi na Hali Zao";
$lang['student_status'] = " Wanafunzi Wanaotumika Kwenye Mfumo";
$lang['student_distance'] = "Umbali wa Mwanafunzi Kutoka Shule";
$lang['no_distance'] = "Idadi ya Wanafunzi Kulingana na Umbali kutoka Shule";
$lang['distance'] = "Umbali Kutoka Shule";
$lang['reg_type'] = "Aina ya Usajiri";
$lang['reg_date'] = "Tarehe ya Kusajiri";
$lang['no_joining'] = "Idadi ya Wanafunzi Toka Kujiunga";
$lang['joining'] = "Hali ya Kujiunga ";
$lang['no_date'] = "Idadi ya Wanafunzi Kulingana na Tarehe ya Kujiunga";
$lang['dates'] = "Tarehe ya Kusajiri";
$lang['insight_title'] = "Kipengele Hiki kipo kuzisaidia Shule kufanya Maamuzi sahihi. Kama unahitaji Repoti zingine zaidi kulingana na Mahitaji ya shule yako. Tuma Maombi Kupitia Barua pepe hii";
$lang['month_report'] = "Ripoti ya Mwezi";
$lang['select_module'] = "Chagua Module";
$lang['classlevel'] = "Select Class Level";
$lang['exam_comment'] = "Maoni ya wazazi na Walimu Kuhusu Ripoti ya Mandendeleo ya Mwanafunzi Kitaaluma";
$lang['all_exam_comment'] = "Maoni ya wazazi na Walimu Kuhusu Ripoti ya Mandendeleo ya Mwanafunzi Kitaaluma";
$lang['exam_message'] = " Andika Maoni Yako Hapa Kuhusu Ripoti ya MAtokeo Haya...";
$lang['comment'] = "Tuma Maoni";
$lang['view_member'] = 'Angalia';

$lang['online_exam'] = "Ona Mtihani";
$lang['view_result'] = "Angalia Matokeo";
$lang['score'] = "Alama Alizopata";
$lang['release_result'] = "Weka Matokeo Live";
$lang['unrelease_result'] = "Ondoa MAtokeo Live";
$lang['answer'] = "Amejibu";
$lang['online_exam_results'] = "Angalia Matokeo";
$lang['exam_pass_mark']='Kiwango cha Ufaulu';
$lang['total_time'] = 'Muda';
$lang['topic'] = 'Mada';
$lang['questions'] = "Maswali";

$lang['attachment'] = "Attachment";

$lang['deadline'] = "Kukusanya Mwisho";
$lang['assignment'] = "Kazi/Mazoezi";
$lang['attach'] = "Attachment";
$lang['add_assignment'] = "Ongeza Kazi";
$lang['title'] = "Ujumbe";
$lang['subject'] = "Somo";
$lang['total_submit'] = "Zilizokusanywa";
$lang['total_unsubmitted'] = "Hazijakusanywa";
$lang['total_marked'] = "Zilizo sahihishwa";
$lang['total_unmarked'] = "Hazija sahihishwa";
$lang['teacher'] = "Mwalimu";
$lang['go_back'] = "Rudi Nyuma";
$lang['online_studies'] = "Masomo Mtandao";
$lang['a_type'] = "Aina";
$lang['created_at'] = "Toka Tarehe";
$lang['submited_on'] = "Kukusanywa";
$lang['no_attach'] = 'Jumla ya Kazi';
$lang['parent_status']="Chagua Mzazi Hapa";


$lang['menu_applications'] = "Matumizi";
$lang['health_application'] = "Matumizi  Bima ya Afya";
$lang['add_application'] = "Ongeza Matumizi";
$lang['date_application'] = "Tarehe ya  Matumizi";
$lang['guidance'] = "Mwongozo";
$lang['rapplication']="Unaweza kuongeza mwanafunzi mmoja au zaidi nakuwasilisha";
$lang['total_payment']="Jumla ya Malipo";
$lang['select'] = 'Select here...';
$lang['bank_name']="Akaunti ya Malipo";
$lang['news_board']="Habari na Tangazo";
$lang['support']="Msaada";
$lang['join_school']="Jiunge na ShuleYetu";
$lang['menu_transactions']="Shughuli";
$lang['menu_vendors']="Wachuuzi";
$lang['menu_payroll']="Malipo";
$lang['fixed_assets']="Mali iliyowekwa";
$lang['current_assets']="Mali ya sasa";
$lang['cash_request']="Ombi la pesa";
$lang['register_inventory']="Vitu";
$lang['purchase_inventory']="Ununuzi";
$lang['sale_inventory']="Matumizi";
$lang['tax_status']="Hadhi ya ushuru";
$lang['pension_fund']="Fedha za pensheni";
$lang['menu_allowance']="Posho";
$lang['menu_deduction']="Makato";
$lang['menu_salary']="Mishahara";
$lang['send_sms'] = 'Tuma Message';
$lang['subject_semester'] = 'Muhura';
$lang['subject_credit'] = 'Pointi';
$lang['address']="Anuani";

return $lang;
