<?php

/* List Language  */
$lang['panel_title'] = "Usafiri";
$lang['add_title'] = "Ongeza Usafiri";
$lang['slno'] = "#";
$lang['transport_route'] = "Jina la Njia";
$lang['transport_vehicle'] = "Idadi ya magari";
$lang['transport_fare'] = "Nauli ya Njia";
$lang['transport_note'] = "Kumbuka";

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_transport'] = 'Ongeza Usafiri';
$lang['update_transport'] = 'Sasisha Usafiri';
$lang['add_route_title'] = "Ongeza Ruti";
$lang['class_level_id']='Ngazi ya Darasa';
$lang['select_academic_year']='Mwaka wa Masomo';

$lang['select']='Chagua';
$lang['delete']='Futa';
$lang['view']='Angalia';




$lang['transport_vehicle'] = "Njia za Magari";
$lang['transport_members'] = "Wanachama wa usafiri";


$lang['class_level_id'] = 'Kiwango cha darasa';

// Vehicle registration
$lang['plate_number']='Namba ya gari';
$lang['seats']='Namba za siti';
$lang['driver']='Dereva';
$lang['description']='Maelezo';
$lang['add_vehicle']='Ongeza Usafiri';
$lang['update_vehicle']='Sahisha udafiri';
$lang['select_driver']='Chagua dereva';
$lang['select_conductor']='chagua Kondakta';
$lang['conductor']='Kondakta';
$lang['action']='Hatua';
$lang['no_vehicles'] = 'Idadi ya usafiri';



return $lang;