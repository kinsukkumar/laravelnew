<?php

/* List Language  */
$lang['panel_title'] = "Mtumiaji";
$lang['add_title'] = "Ongeza Mtumiaji";
$lang['slno'] = "#";
$lang['user_photo'] = "Picha";
$lang['user_name'] = "Jina";
$lang['user_email'] = "Barua pepe";
$lang['user_dob'] = "Tarehe ya kuzaliwa";
$lang['user_info'] = "Maelezo ya mtumiaji";
$lang['fields_marked'] = "Sehemu zenye alama";
$lang['are_mandatory']='ni za lazima';
$lang['name_eg']='Jina kamili mfano John Samson';
$lang['religion_option']='Chagua dini';
$lang['email_eg']='Barua pepe halali mfano samson@yahoo.com';
$lang['mail_eg']='S.L.P mfano 33287,Dar';
$lang['no_eg']='mfano 655406004';
$lang['user_upload']='Pakia watumiaji kutoka kwenye faili la excel';
$lang['download']='Pakua';
$lang['sample']='faili la sampuli hapa';
$lang['submit']='Tuma';
$lang['user_sex'] = "Jinsia";
$lang['user_sex_male'] = "Kiume";
$lang['user_sex_female'] = "Kike";
$lang['user_religion'] = "Dini";
$lang['user_phone'] = "Namba ya Simu ya Mkononi";
$lang['user_address'] = "Anuani";
$lang['user_jod'] = "Tarehe ya Kujiunga";
$lang['user_type'] = "Aina";
$lang['user_username'] = "Jina la Kutumia";
$lang['user_password'] = "Neno siri";
$lang['user_role']='Jukumu la mtumiaji';


$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['pdf_preview'] = 'Onesha PDF ya awali';
$lang['idcard'] = 'Namba ya Kitambulisho';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa barua pepe";

/* Add Language */
$lang['personal_information'] = "Taarifa Binafsi";
$lang['add_user'] = 'Ongeza Mtumiaji';
$lang['update_user'] = 'Sasisha Mtumiaji';

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';

$lang['user_info'] = 'Taarifa Muhimu';
$lang['sms_sent'] = 'Meseji zilizotumwa';
$lang['payroll_status'] = "Hali ya Malipo";


return $lang;