<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?= ucwords($siteinfos->sname) ?></title>
        <link rel="SHORTCUT ICON" rel="icon" href="<?= base_url("storage/uploads/images/" . $siteinfos->photo) ?>">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="theme-color" content="#00acac">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script type="text/javascript" src="<?php echo base_url('public/assets/shulesoft/jquery.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/jquery-ui/jquery-ui.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/timepicker/timepicker.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/toastr/toastr.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/js/sweet-alert/sweetalert.min.js'); ?>"></script>

        <!-- Bootstrap js -->
        <script type="text/javascript" src="<?php echo base_url('public/assets/bootstrap/bootstrap.min.js'); ?>"></script> 
        <!--<link rel="stylesheet" href="--><?php //echo base_url('public/assets/materialize.min.css');             ?><!--">-->

        <!--        <link href="--><?php //echo base_url('public/assets/bootstrap/bootstrap.min.css');            ?><!--" rel="stylesheet">-->
        <link href="<?php echo base_url('public/assets/bootstrap/jumbotron.less'); ?>">
        <link href="<?php echo base_url('public/assets/fonts/font-awesome.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/fonts/icomoon.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/style.css'); ?>?v=1" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/datepicker/datepicker.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/timepicker/timepicker.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/hidetable.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/shulesoft.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/responsive.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/fullcalendar/fullcalendar.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/editor/jquery-te-1.4.0.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/toastr/toastr.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/rid.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/jquery-ui/jquery-ui.css'); ?>" rel="stylesheet">
        <!--        <link href="--><?php //echo base_url('public/assets/shulesoft/jquery.mCustomScrollbar.min.css')            ?><!--" rel="stylesheet">-->
        <link href="<?php echo base_url('public/assets/css/sweet-alerts/sweetalert.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/bootstrap/3.3.2/bootstrap.min.css'); ?>" rel="stylesheet">

        <link rel="stylesheet" href="<?= url('public/') ?>/intlTelInput/css/intlTelInput.css">

        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/datepicker/themes/default.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/datepicker/themes/default.date.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2-bootstrap.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/gh-pages.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/jqueryUI/jqueryui.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/custom.css'); ?>?v=2">
        <!--<link rel="stylesheet" href="<?= base_url("public/assets/css/documenter_style.css?v=1") ?>">-->
        <link href="<?php echo base_url('public/assets/css/pnotify/pnotify.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/pnotify/pnotify.buttons.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/pnotify/pnotify.nonblock.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/loader.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/switchery.min.css') ?>" rel="stylesheet">
        <script type="text/javascript">


ajax_setup = function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        async: true,
        cache: false,
        beforeSend: function (xhr) {
            jQuery('#loader-content').show();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            jQuery('#loader-content').hide();
//            var message = '';
//            if (jqXHR.status == 404) {
//                message = "Request is not complete, please try again later.";
//            } else {
//                message = textStatus + ": " + errorThrown + ' Request is not complete, please try again later.';
//            }
//             if (jqXHR.readyState == 0) {
//                message = "No internet connection, or very low internet connectoin, please try again later.";
//            }
//            if (textStatus === "timeout") {
//                message = "Timeout";
//            }
//            swal({
//                type: 'warning',
//                title: 'Something is not right',
//                text: message});

        },
        complete: function (xhr, status) {
            jQuery('#loader-content').hide();
            //we will add conditions in some methods
//            if (document.body.scrollTop > 60 || document.documentElement.scrollTop > 60) {
//                $("HTML, BODY").animate({scrollTop: 0}, 300);
//            }

        }
    });
}
//setTimeout(function(){ jQuery('#loader-content').fadeOut('slow'); }, 3000);
var explode = function () {
    jQuery('#loader-content').fadeOut('slow');
};
setTimeout(explode, 3000);
var root_url = "<?= url('/'); ?>";
$(document).ready(ajax_setup);
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement("style");
    msViewportStyle.appendChild(
            document.createTextNode(
                    "@-ms-viewport{width:auto!important}"
                    )
            );
    document.getElementsByTagName("head")[0].
            appendChild(msViewportStyle);
}
        </script>
    </head>

    <style>

        #valid-msg {
            color: #00C900;
        }
        #error-msg {
            color: red;
        }
        #well{
            display: none;
        }
        #well button{
            display: none;
        }
        .breadcrumb{
            display: none;
        }
    </style>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <!--                            <a href="#" class="site_title center">
                                                            <span>ShuleSoft</span></a>-->

                        </div>
                        <div class="clearfix"></div>

                        <br>
                        <div class="profile">
                            <div class="user-profile">
                                <?php
                                $image = array(
                                    "src" => base_url('storage/uploads/images') . '/' . $siteinfos->photo,
                                    'width' => '0',
                                    'height' => '0',
                                    'alt' => $siteinfos->sname . ' ,ShuleSoft School Management System, INETS Company Limited,Software,Dar es Salaam,Tanzania,School System',
                                    'class' => 'img-circle profile_img',
                                    'style' => 'width: 50px; height: 50px; display: inline !important;'
                                );
                                echo img($image);
                                ?>

                            </div>
                            <div class="profile_info">
                                <br/> <h2><?= $siteinfos->sname ?></h2>
                            </div>
                        </div>
                        <br/>
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu" style="z-index: 6000;">
                            <div class="menu_section">
                                <h3>&nbsp;&nbsp;</h3>
                                <ul class="nav side-menu">
                                    <li><?php
                                        echo anchor('admission/index/null/terms', '<i class="fa fa-file"></i> <span>Terms</span>');
                                        ?>

                                    </li>
                                    <?php
                                     if (set_schema_name() !=set_schema_name()) {
                                        ?>
                                        <li><?php
                                            echo anchor('admission/index', '<i class="fa fa-home"></i> <span>Introduction</span>');
                                            ?>

                                        </li>
                                    <?php } ?>
                                </ul></div>
                        </div>

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">

                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                            <?php
                            if (set_schema_name() !=set_schema_name()) {
                                ?>
                                <ul class="nav navbar-nav navbar-right" id="">
                                    <li class="">
                                        <a  href="<?= base_url('signin/index') ?>" class="btn user-profile dropdown-toggle">Login
                                        </a>

                                    </li>
                                </ul>
                            <?php } ?>
                        </nav>
                    </div>
                </div>

                <div class="right_col" role="main">
                    <div class="content" style="min-width: 28em;">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">

                                <div id="wizard" class="form_wizard wizard_horizontal">
                                    <?php
                                    if (set_schema_name() !=set_schema_name()) {
                                        ?>
                                        <ul class="wizard_steps anchor" id="wizard_steps_div">
                                            <li>
                                                <a href="#step-1" class="selected" isdone="1" rel="1">
                                                    <span class="step_no">1</span>
                                                    <span class="step_descr">
                                                        Step 1<br>
                                                        <small>Introduction</small>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#step-2" class="done" isdone="1" rel="2">
                                                    <span class="step_no">2</span>
                                                    <span class="step_descr">
                                                        Step 2<br>
                                                        <small>Registration</small>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#step-3" class="done" isdone="1" rel="3">
                                                    <span class="step_no">3</span>
                                                    <span class="step_descr">
                                                        Step 3<br>
                                                        <small>Submission</small>
                                                    </span>
                                                </a>
                                            </li>
                                            <!--                                        <li>
                                                                                        <a href="#step-4" class="done" isdone="1" rel="4">
                                                                                            <span class="step_no">4</span>
                                                                                            <span class="step_descr">
                                                                                                Step 4<br>
                                                                                                <small>Submission</small>
                                                                                            </span>
                                                                                        </a>
                                                                                    </li>-->
                                        </ul>

                                    <?php } $subview_page = str_replace('/', '.', $subview);
                                    ?>
                                    <?php if (createRoute() == 'admission@payment' && isset($invoice)) { ?>
                                        <div class="heading">
                                            <div class="col-sm-6">

                                                <button class="btn-cs btn-sm-cs" id="menu_toggle" onclick="printPage()"><span class="fa fa-print"></span> <?= $data->lang->line('print') ?> </button>

                                                <?= $siteinfos->payment_integrated == 1 ? btn_payment('admission/addPayment/' . $invoice->id . '/' . $academic_year_id . '/' . $installment_id, $data->lang->line('payment')) : '' ?>


                                            </div>
                                        </div>
                                    <?php } ?>
                                    @include($subview_page)
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <script> 
                    var BASE_URL = '{{url('')}}';
                    $('.phoneNumber').blur(function () {
                        if ($('.phoneNumber').intlTelInput('isValidNumber')) {
                            $("#phone").val($(".phoneNumber").intlTelInput("getNumber"));
                            $(".username").val("0" + $(".phoneNumber").intlTelInput("getNumber", "intlTelInputUtils.numberFormat.NATIONAL"));
                        } else {
                            $("#phone").val('');
                            $("#username").val('');

                        }
                    });
                </script>

            </div>
        </div>

        @include('components.footer')

    </body>
</html>

