<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>ShuleSoft</title>
        <link rel="SHORTCUT ICON" rel="icon" href="<?= base_url("storage/uploads/images/favicon.png") ?>">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!--<meta name="theme-color" content="#00acac">-->
        <meta name="theme-color" content="#ed3bga">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script type="text/javascript" src="<?php echo base_url('public/assets/shulesoft/jquery.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/jquery-ui/jquery-ui.js'); ?>"></script>


        <!-- Bootstrap js -->
        <script type="text/javascript" src="<?php echo base_url('public/assets/bootstrap/bootstrap.min.js'); ?>"></script> 


        <link href="<?php echo base_url('public/assets/bootstrap/3.3.2/bootstrap.min.css'); ?>" rel="stylesheet">


        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2-bootstrap.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/gh-pages.css'); ?>">

        <!-- Favicon -->
        <!--<link href="{{url('public/assets/images/logo.png')}}" rel="icon">-->
        <link rel="SHORTCUT ICON" href="<?= base_url("storage/uploads/images/") ?>" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800" rel="stylesheet">

        <!-- Icons -->
        <link rel="stylesheet" href="{{url('public/newdesign/vendor/fontawesome-pro/css/all.min.css')}}">

        <!-- Page plugins -->

        <link type="text/css" href="{{url('public/newdesign/vendor/fancybox/css/jquery.fancybox.min.css')}}" defer rel="stylesheet">
        <link rel="stylesheet" href="{{url('public/newdesign/vendor/animate/animate.min.css')}}" async>
        <link type="text/css" href="{{url('public/newdesign/vendor/highlight/css/highlight.min.css')}}" async rel="stylesheet">
        <link type="text/css" href="{{url('public/newdesign/vendor/highlight/css/styles/atom-one-dark.css')}}" async rel="stylesheet">
        <!-- Theme CSS -->
        <link type="text/css" href="{{url('public/newdesign/css/theme.min.css')}}" rel="stylesheet" async>
        <meta name="google-signin-client_id" content="614990663215-uodgs6e9b7n4t1hrst6k0aja23btd72q.apps.googleusercontent.com">
        <script src="{{url('public/newdesign/vendor/jquery/jquery.min.js')}}" ></script>

        <script type="text/javascript">


ajax_setup = function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        async: true,
        cache: false,
        beforeSend: function (xhr) {
            jQuery('#loader-content').show();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            jQuery('#loader-content').hide();

        },
        complete: function (xhr, status) {
            jQuery('#loader-content').hide();

        }
    });
}
//setTimeout(function(){ jQuery('#loader-content').fadeOut('slow'); }, 3000);
var explode = function () {
    jQuery('#loader-content').fadeOut('slow');
};
setTimeout(explode, 3000);
var root_url = "<?= url('/'); ?>";
$(document).ready(ajax_setup);
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement("style");
    msViewportStyle.appendChild(
            document.createTextNode(
                    "@-ms-viewport{width:auto!important}"
                    )
            );
    document.getElementsByTagName("head")[0].
            appendChild(msViewportStyle);
}
        </script>
        <style>
            body{
                font-size: 14px;
            }
        </style>
    </head>

  <body>
        <header class="header-transparent footer footer-dark bg-gradient-primary mb-0" id="header-main">
            <!-- Topbar -->
            <div id="navbar-top-main" class="navbar-top  navbar-dark bg-dark border-bottom">
                <div class="container">
                    <div class="navbar-nav align-items-center">
                        <div class="d-none d-lg-inline-block">
                            <span class="navbar-text mr-3">
                                <a href="#" class="avatar rounded-circle shadow">
                                    <img alt="Image placeholder"  src="{{url('/public/assets/images/default-logo.png')}}">
                                </a>
                            </span>
                        </div>
                        <div>
                            <ul class="nav">

                                <li class="nav-item dropdown ml-lg-2 dropdown-animate" data-toggle="hover">
                                    <a class="nav-link px-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img alt="Image placeholder" src="<?php
                                        $image = session('lang') == '' ? 'english' : session('lang');
                                        echo base_url('storage/uploads/language_image/' . $image . '.png');
                                        ?>">
                                        <span class="d-none d-lg-inline-block">{{$image}}</span>
                                        <span class="d-lg-none">{{$image=='english'?'EN':'SW'}}</span>
                                    </a>

                                </li>
                            </ul>
                        </div>
                        <div class="ml-auto">
                            <ul class="nav">
                                <?php
                                ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="https://support.shulesoft.com"><?= __('support') ?></a>
                                </li>                      

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function changeLanguage(a) {
                    $.get('<?= url('Controller/changeLanguage/') ?>/' + a, null, function (data) {
                        window.location.reload();
                    });
                }
            </script>   
        </header>
        <main style="margin-top: 10em;">
            <div class="row"> 
                <div class="col-lg-3">

                </div>
                <div class="col-lg-6">
                    @yield('content')
                </div>
                <div class="col-lg-3">

                </div>
            </div> 
        </main>

        <footer class="footer footer-dark bg-gradient-primary mb-0">
            <div class="container">
                <div class="row align-items-center justify-content-md-between py-4 mt-4 delimiter-top">
                    <div class="col-md-6">
                        <div class="copyright text-sm font-weight-bold text-center text-md-left">
                            &copy; {{date('Y')}} <a href="http://inetstz.com/" class="font-weight-bold" target="_blank">INETS Co Ltd</a>. All rights reserved.
                        </div>
                    </div>
                    <div class="col-md-6">
                        <ul class="nav justify-content-center justify-content-md-end mt-3 mt-md-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/shulesoft" target="_blank">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="https://instagram.com/shulesoft_" target="_blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://facebook.com/shulesoft" target="_blank">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

    </body>
    <!-- Core -->

    <script src="{{url('public/newdesign/vendor/popper/popper.min.js')}}" async></script>
    <script src="{{url('public/newdesign/vendor/bootstrap/bootstrap.min.js')}}" async></script>
    <script src="{{url('public/newdesign/vendor/sticky-kit/sticky-kit.min.js')}}" async></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js" ></script>




    <!-- Page plugins -->
    <script src="{{url('public/newdesign/vendor/highlight/js/highlight.min.js')}}" async></script>
    <script src="{{url('public/newdesign/vendor/fancybox/js/jquery.fancybox.min.js')}}" ></script>



    @yield('page-footer')

    <!-- Theme JS -->
    <script src="{{url('public/newdesign/js/theme.min.js')}}" defer></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-104437451-1"></script>
    <script>
                window.dataLayer = window.dataLayer || [];
                function gtag() {
                    dataLayer.push(arguments);
                }
                gtag('js', new Date());

                gtag('config', 'UA-104437451-1');

                $(".fancybox").fancybox({});

                const observer = lozad(); // lazy loads elements with default selector as '.lozad'
                observer.observe();

                $(document).ready(function () {
                    $('#show_password').click(function () {

                        var $showPassword = $('#inputPassword');

                        if ($showPassword.attr('type') === 'password' && $showPassword.val() !== '') {
                            $showPassword.attr('type', 'text');
                        } else {
                            $showPassword.attr('type', 'password');
                        }
                    });

                    $('#change_string').click(function () {

                        var result = '';
                        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789';
                        var charactersLength = characters.length;
                        for (var i = 0; i < 4; i++) {
                            result += characters.charAt(Math.floor(Math.random() * charactersLength));
                        }

                        $('#random_string').val(result);

                    });
                });

                var $buoop = {required: {e: -4, f: -3, o: -3, s: -1, c: -3}, insecure: true, unsupported: true, api: 2019.05};
                function $buo_f() {
                    var e = document.createElement("script");
                    e.src = "//browser-update.org/update.min.js";
                    document.body.appendChild(e);
                }
                ;
                try {
                    document.addEventListener("DOMContentLoaded", $buo_f, false)
                } catch (e) {
                    window.attachEvent("onload", $buo_f)
                }



    </script>

    <!-- Start of HubSpot Embed Code -->
    <!--<script type="text/javascript" id="hs-script-loader" defer src="//js.hs-scripts.com/2801781.js" async></script>-->
    <!-- End of HubSpot Embed Code -->

</html>

