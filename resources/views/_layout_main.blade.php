<div id="loader-content">
    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>
</div>
<!DOCTYPE html>
<?php
$url = base_url('storage/uploads/images/' . $siteinfos->photo);
?>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?= ucwords($siteinfos->sname) ?></title>

        <link rel="shortcut icon" href="<?= $url ?>?v=4343434323">
        <link rel="apple-touch-icon" href="<?= $url ?>?v=45454523">
        <link rel="image_src" href="<?= $url ?>?v=323232"> 
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0">
        <meta property="og:type" content= "website" />
        <meta property="og:site_name" content="ShuleSoft" />
        <meta property="og:image" itemprop="image primaryImageOfPage" content="<?= $url ?>?v=73d79a89bded" />
        <meta name="twitter:card" content="summary"/>
        <meta name="twitter:domain" content="shulesoft.com"/>
        <link rel="SHORTCUT ICON" rel="icon" href="<?= $url ?>">
        <meta name="theme-color" content="#00acac">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script type="text/javascript" src="<?php echo base_url('public/assets/shulesoft/jquery.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/jquery-ui/jquery-ui.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/timepicker/timepicker.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/toastr/toastr.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/js/sweet-alert/sweetalert.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/bootstrap/bootstrap.min.js'); ?>" async></script>

        <link href="<?php echo base_url('public/assets/bootstrap/jumbotron.less'); ?>">
        <link href="<?php echo base_url('public/assets/fonts/font-awesome.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/fonts/icomoon.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/new_daterangepicker/daterangepicker.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/timepicker/timepicker.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/hidetable.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/shulesoft.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/responsive.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/fullcalendar/fullcalendar.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/editor/jquery-te-1.4.0.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/toastr/toastr.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/rid.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/jquery-ui/jquery-ui.css'); ?>" rel="stylesheet">
        <!--        <link href="--><?php //echo base_url('public/assets/shulesoft/jquery.mCustomScrollbar.min.css')                                                ?><!--" rel="stylesheet">-->
        <link href="<?php echo base_url('public/assets/css/sweet-alerts/sweetalert.css'); ?>" rel="stylesheet">

        <link href="<?php echo base_url('public/assets/bootstrap/3.3.2/bootstrap.min.css'); ?>" rel="stylesheet">


        <link rel="stylesheet" href="<?php echo base_url('public/build/css/prism.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/build/css/intlTelInput.css?1603274336113'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/build/css/isValidNumber.css?1603274336113'); ?>">


       <!-- Chosen-->
        <link href="<?php echo base_url('public/assets/chosen/chosen.min.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/custom.css'); ?>?v=">

        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/datepicker/themes/default.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/datepicker/themes/default.date.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2-bootstrap.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/gh-pages.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/jqueryUI/jqueryui.css'); ?>">

        <link rel="stylesheet" href="<?= base_url("public/assets/css/documenter_style.css") ?>">
        <link href="<?php echo base_url('public/assets/css/pnotify/pnotify.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/pnotify/pnotify.buttons.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/pnotify/pnotify.nonblock.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/loader.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/switchery.min.css') ?>" rel="stylesheet">

        <!--<link href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css" rel="stylesheet">-->
        <!--<link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">-->


        <script type="text/javascript">


ajax_setup = function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        async: true,
        cache: false,
        beforeSend: function (xhr) {
            jQuery('#loader-content').show();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            jQuery('#loader-content').hide();
//            var message = '';
//            if (jqXHR.status == 404) {
//                message = "Request is not complete, please try again later.";
//            } else {
//                message = textStatus + ": " + errorThrown + ' Request is not complete, please try again later.';
//            }
//             if (jqXHR.readyState == 0) {
//                message = "No internet connection, or very low internet connectoin, please try again later.";
//            }
//            if (textStatus === "timeout") {
//                message = "Timeout";
//            }
//            swal({
//                type: 'warning',
//                title: 'Something is not right',
//                text: message});

        },
        complete: function (xhr, status) {
            jQuery('#loader-content').hide();
            //we will add conditions in some methods
//            if (document.body.scrollTop > 60 || document.documentElement.scrollTop > 60) {
//                $("HTML, BODY").animate({scrollTop: 0}, 300);
//            }

        }
    });
}
//setTimeout(function(){ jQuery('#loader-content').fadeOut('slow'); }, 3000);
var explode = function () {
    jQuery('#loader-content').fadeOut('slow');
};
setTimeout(explode, 3000);
var root_url = "<?= url('/'); ?>";
$(document).ready(ajax_setup);
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement("style");
    msViewportStyle.appendChild(
            document.createTextNode(
                    "@-ms-viewport{width:auto!important}"
                    )
            );
    document.getElementsByTagName("head")[0].
            appendChild(msViewportStyle);
}
<?php
if (in_array(session('table'), ['setting', 'user', 'teacher'])) {
    ?>
    /* --------------------------------------------------------
     Training part
     --------------------------------------------------------   */
    (function () {
        $('<div class="color-picker no-print"><a href="#" class="handle"><i class="fa fa-book "></i></a><div class="settings-header"><h3>Step By Step Training</h3></div><div class="section"><div class="list_training"><ul  class="list-unstyled"><?php
    $trainings = DB::table('admin.training_modules')->orderBy('id', 'asc')->get();
    $i = 1;
    foreach ($trainings as $training) {
        // will be uncommented on new version
        echo '<li><a href="#" class="training_module" id="' . $training->id . '" >' . $training->name . '</a></li>';
        $i++;
    }
    ?><li><a href="#faq" class="color-4" id="faq_training_page" >FAQ</a></li></ul></div><div class="training_content"></div></div></div>').appendTo($('body'));
    })();

    /*Gradient Color*/


    /*Normal Color */
    $(".training_module").click(function () {
        /* System configuration*/
        var id = $(this).attr('id');
        if (id === '0') {
            $('.training_content').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= base_url('help/checklist') ?>",
                data: "id=" + id,
                dataType: "html",
                success: function (data) {
                    // for current time tobe uncommented
                    $('.training_content').html(data).show();
                    $('.list_training').hide();
                }
            });
        }
        return false;
    });

    $("#faq_training_page").click(function () {
        /* FAQ */
        $.ajax({
            type: 'POST',
            url: "<?= base_url('help/faq') ?>",
            data: {'source': 'main'},
            dataType: "html",
            success: function (data) {

                $('.training_content').html(data).show();
                $('.list_training').hide();
            }
        });

    });


    $('.color-picker').animate({
        right: '-239px'
    });

    $('.color-picker a.handle').click(function (e) {
        e.preventDefault();
        var div = $('.color-picker');
        if (div.css('right') === '-239px') {
            $('.color-picker').animate({
                right: '0px'
            });
        } else {
            $('.color-picker').animate({
                right: '-239px'
            });
        }
    });
<?php } ?>
        </script>

        <style>
            @-webkit-viewport{width:device-width}
            @-moz-viewport{width:device-width}
            @-ms-viewport{width:device-width}
            @-o-viewport{width:device-width}
            @viewport{width:device-width}
            .btn-bs-file{
                position:relative;
            }
            .btn-bs-file input[type="file"]{
                position: absolute;
                top: -9999999;
                filter: alpha(opacity=0);
                opacity: 0;
                width:0;
                height:0;
                outline: none;
                cursor: inherit;
            }
            body{
                overflow-x: hidden
            }
            .color-picker {
                position: fixed;
                right: 0;
                top: 150px;
                width: 239px;
                background: #fff;
                z-index: 999999;
                -webkit-transition: all .3s ease; }

            .color-picker a.handle {
                border: 1px solid #2C3E50;
                border-right-color: transperant;
                border-left-color: none;
                position: absolute;
                top: 0;
                right: 239px;
                width: 50px;
                height: 50px;
                text-align: center;
                background: #fff;
                z-index: 9999;
                transition: all ease-in 0.3s; }

            .color-picker a.handle:hover {
                background: #4A6076;
                transition: all ease-in 0.3s; }

            .color-picker a.handle:hover i {
                color: #fff;
                transition: all ease-in 0.3s; }

            .color-picker a.handle i {
                color: #2C3E50;
                font-size: 25px;
                line-height: 50px; }

            .color-picker .settings-header {
                background: #2C3E50;
                height: 50px;
                padding: 15px 34px 0 34px; }

            .color-picker .settings-header h3 {
                color: #fff;
                font-size: 16px;
                line-height: 16px;
                margin-bottom: 0;
                padding: 0 0 5px;
                margin-top: 0; }

            .color-picker .section:last-child {
                border-bottom: 1px solid #2c3e50; }

            .color-picker .section {
                padding: 20px 14px;
                border-bottom: 1px solid #2c3e50;
                border-left: 1px solid #2c3e50;
                overflow: hidden; }

            .color-picker .section h3 {
                font-size: 16px;
                text-transform: none;
                color: #3c3c3c;
                line-height: 16px;
                padding: 0;
                margin-bottom: 20px;
                text-align: left; }

            .color-picker .section i {
                font-size: 16px;
                margin-right: 10px; }

            .color-picker span {
                font-size: 13px;
                color: #9a9a9a; }

            .color-picker .colors a {
                display: block;
                border-radius: 10px;
                width: 30px;
                height: 30px;
                margin-left: 0;
                margin-bottom: 10px;
                margin-right: 5px;
                float: left;
                transition: all ease-in 0.3s; }

            .color-picker .colors a:hover {
                box-shadow: 0 0 2px 1px rgba(247, 54, 121, 0.58);
                transition: all ease-in 0.3s; }

            .color-picker .skin a {
                display: block;
                width: 70px;
                height: 30px;
                margin-left: 0;
                margin-top: 0;
                margin-right: 5px;
                float: left;
                text-align: center;
                line-height: 30px;
                border: 2px transparent solid; }

            .color-picker .skin a.actt {
                border: 2px #FFF solid; }
            </style>
        </head>

        <body class="<?= in_array(device(), ['Android', "iPhone"]) ? 'nav-sm' : 'nav-md' ?>">


            <style>

            .alert{
                z-index: 10000;
            }
            #valid-msg {
                color: #00C900;
            }
            #error-msg {
                color: red;
            }
            @media print {
                #setup_notifications {
                    display: none !important;
                }

            }
        </style>
        <?php
        $data = $data_language;
        ?>

        @include('components.menu')
        @include('layouts.notifications')
        <div class="right_col" role="main" style="min-width: 27em; margin-top: 40px; background:url(<?php echo base_url('public/assets/images/backdrop-shulesoft-home.jpg'); ?>) no-repeat fixed; background-size: cover">
            <div class="content">
                <div class="row">
                    <div class="col-xs-12 col-md-12" id="page">

                        <?php
                        $subview_page = str_replace('/', '.', $subview);
                        ?>
                        @include($subview_page)
                    </div>
                </div>
            </div>
        </div>




        @include('components.footer')
         <!--<p align="center">End of ClickDesk  This page took <?php echo (microtime(true) - LARAVEL_START) ?> seconds to render</p> -->

        <script type="text/javascript">

<?php
if (FALSE) {
    ?>
                swal({
                    title: "Wrong Email Address!",
                    text: "Please write your valid Email address to proceed:",
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    inputPlaceholder: "Write something"
                }, function (inputValue) {
                    if (inputValue === false)
                        return false;
                    if (inputValue === "") {
                        swal.showInputError("You need to write something!");
                        return false
                    }
                    $.getJSON('<?= base_url('profile/update/null') ?>', {email: inputValue}, function (data) {
                        if (data.status == '0') {
                            swal.showInputError("This Email is not Valid!");
                            return false
                        } else {
                            swal("Success!", "You email has been updated to : " + inputValue, "success");
                        }
                    });

                });

    <?php
}

$flash = session('flash');
if ($flash != null) {
    ?>
                load_toast = function () {
                    toast("<?= $flash[1] ?>", "<?= $flash[0] ?>");
                };
                $(document).ready(load_toast);
    <?php
    session(['flash' => null]);
}
?>
        </script>
        <script>
            jQuery('#loader-content').hide();

        </script>

    </body>
</html>