    <?php  if (can_access('add_invoice') || can_access('admission')) { ?>
    <li class="dropdown messages-menu hidden-xs">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
            <i class="fa fa-bell"></i> <span class="badge badge-danger" id="alerts"> 0 </span></a>
        <ul class="dropdown-menu">
            <?php if (can_access('manage_admission')) { ?>
                <li>
                    <a href="<?= base_url('admission/application') ?>">
                        <div><strong><span class="red" id="admission">(0)</span> New Admission Request</strong>             
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <?php
            }
            if (can_access('add_invoice')) {
                ?>
                <li>
                    <a href="<?= base_url('invoices/noInvoice') ?>">
                        <div>
                            <strong><span class="red" id="pending_invoice_students">(0)</span> Students without Invoice</strong>

                        </div>
                    </a>
                </li>
                <li class="divider"></li>
            <?php } ?>
                            
           <?php if (can_access('edit_invoice')) { ?>
                <li>
                    <a href="<?= base_url('invoices/electronic_payments') ?>">
                        <div><strong><span class="blue" id="electronic">(0)</span> Electronic Payment</strong>             
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="<?= base_url('invoices/payment_history/1') ?>">
                        <div><strong><span class="blue" id="manual_payment">(0)</span> Payments recorded manually</strong>             
                        </div>
                    </a>
                </li>


            <?php } ?>      
                
                
            <?php if (can_access('view_student')) { ?>
                <li>
                    <a href="<?= base_url('exam/countExamComment/null') ?>">
                        <div><strong><span class="red" id="examcomment">(0)</span> Exam Report Comments</strong>             
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
            <?php } ?>

                 <?php if (can_access('edit_student')) { ?>
                <li>
                    <a href="<?= base_url('student/duplicates') ?>">
                        <div><strong><span class="red" id="duplicate">(0)</span> Student(s) Duplicated</strong>             
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
            <?php } ?>
                
                
                     
                
                
            <?php if (can_access('view_student')) { ?>
                <li>
                    <a href="<?= base_url('background/birthday') ?>">
                        <div><strong><span class="red" id="birthday">(0)</span> Today's Birthday</strong>             
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
            <?php } ?>
            <li>
                <a class="text-center" href="<?= base_url('background/tasks') ?>"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a>
            </li>
        </ul>
    </li>

   
   <?php 
    
    
    $tagid = trim(session('table')) == 'student' ? 'student_id' : session('table') . 'ID';
    $q = DB::table('admin.updates')->where('created_at', '>', DB::table(session('table'))->where($tagid, session('id'))->first()->created_at)->whereNotIn('id',\App\Model\UserUpdate::where('user_id', session('id'))->where('table', session('table'))->get(['update_id']))->count();
    if ((int) $q || date('m')=='3') {
        ?>
        <style>
            @keyframes shadow-pulse
            {
                0% {
                    box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.2);
                }
                100% {
                    box-shadow: 0 0 0 35px rgba(255,0,0,0.2);
                }
            }

            @keyframes shadow-pulse-big
            {
                0% {
                    box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.1);
                }
                100% {
                    box-shadow: 0 0 0 70px rgba(255,0,0,0.2);
                }
            }

            #animate
            {
                float: left;
                font: 13px/130px 'Barlow Semi Condensed', sans-serif;
                text-transform: uppercase;
                letter-spacing: 1px;
                color: #fff;
                text-align: center;
                animation: shadow-pulse 1s infinite;
            }
            .pulse {
                overflow: visible;
                position: relative;
                float: left;
                top:16px;
            }
            .pulse:before {
                content: '';
                display: block;
                position: absolute;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                background-color: inherit;
                border-radius: inherit;
                transition: opacity .3s, transform .3s;
                animation: pulse-animation 2s cubic-bezier(0.24, 0, 0.38, 1) infinite;


            }
            @keyframes pulse-animation {
                0% {
                    opacity: 1;
                    transform: scale(1);
                }
                50% {
                    opacity: 0;
                    transform: scale(1.5);
                }
                100% {
                    opacity: 0;
                    transform: scale(1.5);
                }
            }
        </style>
        <?php if((int) $q ){ ?>
      
 <a class="btn btn-sm btn-warning pulse pull-left hidden-xs hidden-sm" href="<?= base_url("help/updates") ?>">
        <span>New</span>
        <i class="fa fa-new" aria-hidden="true"></i>

    </a>     
      
  <?php  } ?> 
   
        

    <?php }   ?>
    <script type="text/javascript">
        alerts = function () {
            $.getJSON('<?= url('background/alerts/null') ?>', null, function (data) {
                if (data.total > 0) {
                    $('#admission').html(data.admission);
                    $('#birthday').html(data.birthday);
                    $('#examcomment').html(data.examcomment);
                     $('#duplicate').html(data.students_duplicate);
                     $('#electronic').html(data.electronic_payments);
                     $('#manual_payment').html(data.manual_payments);
                    $('#pending_invoice_students').html(data.students);
                    $('.fa-bell').removeClass('hidden');
                    $('#alerts').html(data.total);
                }
            });
        }
        $(document).ready(alerts);
    </script>
<?php } ?>