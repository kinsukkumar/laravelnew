
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?= ucwords($siteinfos->sname) ?></title>
        <link rel="SHORTCUT ICON" rel="icon" href="<?= base_url("storage/uploads/images/favicon.png") ?>">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="theme-color" content="#00acac">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script type="text/javascript" src="<?php echo base_url('public/assets/shulesoft/jquery.js'); ?>"></script>

        <script type="text/javascript" src="<?php echo base_url('public/assets/bootstrap/bootstrap.min.js'); ?>" async></script>

       <link href="<?php echo base_url('public/assets/shulesoft/shulesoft.css'); ?>" rel="stylesheet">
      
        <link href="<?php echo base_url('public/assets/bootstrap/3.3.2/bootstrap.min.css'); ?>" rel="stylesheet">

       <link rel="stylesheet" href="<?php echo base_url('public/assets/custom.css'); ?>?v=">


        <script type="text/javascript">
var root_url = "<?= url('/'); ?>";
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement("style");
    msViewportStyle.appendChild(
            document.createTextNode(
                    "@-ms-viewport{width:auto!important}"
                    )
            );
    document.getElementsByTagName("head")[0].
            appendChild(msViewportStyle);
}
        </script>

        <style>
            @-webkit-viewport{width:device-width}
            @-moz-viewport{width:device-width}
            @-ms-viewport{width:device-width}
            @-o-viewport{width:device-width}
            @viewport{width:device-width}
            .btn-bs-file{
                position:relative;
            }
            .btn-bs-file input[type="file"]{
                position: absolute;
                top: -9999999;
                filter: alpha(opacity=0);
                opacity: 0;
                width:0;
                height:0;
                outline: none;
                cursor: inherit;
            }
        </style>
 