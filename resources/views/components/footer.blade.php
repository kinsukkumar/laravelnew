<!-- ClickDesk Live Chat Service for websites -->
<script type='text/javascript'>
</script>
<!-- PNotify -->
<script src="<?php echo base_url('public/assets/pnotify/dist/pnotify.js') ?>"></script>
<script src="<?php echo base_url('public/assets/pnotify/dist/pnotify.buttons.js') ?>"></script>
<script src="<?php echo base_url('public/assets/pnotify/dist/pnotify.nonblock.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/chosen/chosen.jquery.min.js'); ?>"></script>
<?php if (isset($title)) { ?>

    @include('components.notify')
<?php }
?>

<?php
if (!in_array(session('table'), ['parent', 'student'])) {
    if (isset($setup_instruction) && !empty($setup_instruction)) {
        ?>
        <style>
            .pnotify_first{
                margin-top: 10em;
            }
        </style>
        <script type="text/javascript">
        $(document).ready(
            new PNotify({
                title: '<?= $setup_instruction['title'] ?>',
                type: 'error',
                text: '<?= $setup_instruction['message'] ?>  <a href="<?= $setup_instruction['link'] ?>">Click here to set</a>',
                nonblock: {
                    nonblock: false
                },
                styling: 'bootstrap3',
                addClass: 'pnotify_first'
            }));
        </script>
        <?php
    }

    if (isset($add_payment['due_date']) && in_array(session('table'), ['setting', 'user'])) {
        ?>

        <style>
            .left_col{
                margin-top:63px;
            }
            .pnotify_custom{
                margin-top: 35em;
            }
            @media print {

                #warning_time{
                    display: none;
                }
            }

        </style>
        <?php if (isset($add_payment['due_date']) && isset($add_payment['expired']) && $add_payment['expired'] == 2) { ?>
<!--            <div class="label label-warning fixed-top col-lg-12 col-md-12 col-sm-12" id="warning_grace_time" style="top:0; position: fixed; z-index: 1000; margin-bottom: 1px; height: 20px; overflow-wrap: break-word; z-index: 1800;"> 
                <a style="color:white;" href="<?= base_url('help/upgrade') ?>">Your ShuleSoft usage extended grace period will expired/expires on <?= date('d M Y', strtotime($add_payment['due_date'])) ?>. Only <?= $add_payment['days_remains'] ?> day(s) remain.
                    We hope you enjoyed using ShuleSoft at your school as much as we loved building it for your school.
                    Please consider making payments before the due date.</a>    <a style="color:black;" href="<?= base_url('help/upgrade') ?>">Click here to upgrade your plan</a></div>
            <script type="text/javascript">
                warning_grace_time = function () {
                    $('#warning_grace_time').fadeOut(16000);
                };
                $(document).ready(warning_grace_time);
            </script>-->

        <?php } else if ($add_payment['expired'] == 1) { ?>
<!--            <div class="label label-danger fixed-top col-lg-12 col-md-12 col-sm-12" id="warning_time" style="top:0; position: fixed; z-index: 1000; margin-bottom: 1px; height: 20px; overflow-wrap: break-word; z-index: 1800;">  
                <a style="color:white;" href="<?= base_url('help/upgrade') ?>">Your ShuleSoft usage expired/expires on <?= date('d M Y', strtotime($add_payment['due_date'])) ?>. 
                    We hope you enjoyed using ShuleSoft at your school as much as we loved building it for your school. 
                    Please consider making payments now.</a>    <a style="color:black;" href="<?= base_url('help/upgrade') ?>">Click here to upgrade your plan</a></div> 
            <script type="text/javascript">
                setTimeout(function () {
                    // window.location.href = '<?= url('help/upgrade') ?>';
                }, 4000);
            </script>   -->
        <?php } else if ($add_payment['expired'] == 3) { ?>
<!--            <div class="label label-warning fixed-top col-lg-12 col-md-12 col-sm-12" id="warning_time" style="top:0; position: fixed; z-index: 1000; margin-bottom: 1px; height: 20px; overflow-wrap: break-word; z-index: 1800;">  
                <a style="color:white;" href="<?= base_url('help/upgrade') ?>">We have received a partial amount from your account. Your ShuleSoft usage expired/expires on <?= date('d M Y', strtotime($add_payment['due_date'])) ?>. 
                    We hope you enjoyed using ShuleSoft at your school as much as we loved building it for your school. 
                    Please consider making payments now.</a>    <a style="color:black;" href="<?= base_url('help/upgrade') ?>">Click here to upgrade your plan</a></div> 
            <script type="text/javascript">
                //                setTimeout(function () {
                //                    // window.location.href = '<?= url('help/upgrade') ?>';
                //                }, 4000);
            </script>   -->
        <?php } else if (set_schema_name() == 'shulesoft.') { ?>
<!--            <div class="label label-warning fixed-top col-lg-12 col-md-12 col-sm-12" id="warning_time" style="top:0; position: fixed; z-index: 1000; margin-bottom: 1px; height: 20px; overflow-wrap: break-word; z-index: 1800;">  
                <a style="color:white;" href="<?= base_url('help/upgrade') ?>">We have received a partial amount from your account. Your ShuleSoft usage expired/expires on <?= date('d M Y', strtotime($add_payment['due_date'])) ?>. 
                    We hope you enjoyed using ShuleSoft at your school as much as we loved building it for your school. 
                    Please consider making payments now.</a>    <a style="color:black;" href="<?= base_url('help/upgrade') ?>">Click here to upgrade your plan</a></div> 
            <script type="text/javascript">
                setTimeout(function () {
                    window.location.href = '<?= url('help/upgrade') ?>';
                }, 4000);
            </script>   -->
        <?php } else {
            ?>
<!--            <div class="label label-danger fixed-top col-lg-12 col-md-12 col-sm-12" id="warning_time" style="top:0; position: fixed; z-index: 1000; margin-bottom: 1px; height: 20px; overflow-wrap: break-word; z-index: 1800;">  
                <a style="color:white;" href="<?= base_url('help/upgrade') ?>">'Your ShuleSoft usage expired/expires on <?= date('d M Y', strtotime($add_payment['due_date'])) ?>. 
                    We hope you enjoyed using ShuleSoft at your school as much as we loved building it for your school. 
                    Please consider making payments before the due date.</a>    <a style="color:black;" href="<?= base_url('help/upgrade') ?>">Click here to upgrade your plan</a></div>
            <script type="text/javascript">
                // setTimeout(function(){  window.location.href = '<?= url('help/upgrade') ?>'; }, 3000);         
            </script>-->
            <?php
        }
    }
}
?>


<a class="btn btn-rounded scroll_top  btn-sm btn-success hidden" style="position: fixed;z-index: 1000;display: block;" onclick="topFunction()" id="to_top" data-tooltip="Go to top">SCROLL TO TOP</a>

<?php
/**
 * We shall use js to fetch a div named share_div and put this content to allow social sharing
 */
?>
<div id="share_div" style="display:none">
    <button class="btn-success btn-sm-cs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="fa fa-share"></span>Share
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        <li><a href="#" style="color:#01E675"><i class="fa fa-whatsapp fa-2x"></i>  Whatsapp</a></li>
        <li><a href="#"><i class="fa fa-telegram fa-2x"></i>Telegram </a></li>
        <li><a href="#"><i class="fa fa-envelope fa-2x"></i>Email</a></li>

    </ul>
</div>

<!-- compose -->
<div class="compose col-md-6 col-xs-12">
    <div class="compose-header">
        Is this system useful, or do you want anything to be added ? , please give your comments

        <button type="button" class="close compose-close">
            <span>×</span>
        </button>
    </div>

    <div class="compose-body">
        <form id="demo-form" data-parsley-validate="" novalidate="">
            <div class="col-xs-12">  <br/>         
                <textarea id="message" required="required" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"></textarea>
            </div>
            <?= csrf_field() ?>
        </form>
    </div>

    <div class="compose-footer">
        <button id="send" class="btn btn-sm btn-success" type="button">Send</button>
    </div>
</div>
<!-- /compose -->

<!-- compose                 -->
<script>

    $('#send').click(function () {
        var feedback = $('#message').val();
        if (feedback == '') {
            swal('Warning', 'Please Write Your Message first');
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= base_url('notice/feedback') ?>",
                data: "feedback=" + feedback,
                dataType: "html",
                success: function (data) {
                    if (data === '0') {
                        new PNotify({
                            title: 'Failure',
                            text: 'Something went Wrong please try again',
                            type: 'error',
                            hide: false,
                            styling: 'bootstrap3'
                        });
                    } else {
                        new PNotify({
                            title: 'Success',
                            text: 'Thank you for your feedback , we received it!',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    }
                    $('#message').val('');
                    $('.compose').slideToggle();
                }
            });
        }
    });
    user_table = function () {
        $('.users_table').DataTable();
    }
//     $('.compose').slideToggle();
    $(document).ready(user_table);</script>
<!-- /compose -->


<script>
    var BASE_URL = '{{url('')}}';</script>

<?php
$phone_detecting_pages = ['parents@add', 'parents@edit', 'teacher@edit', 'teacher@add', 'user@add', 'user@edit'];
if (in_array(createRoute(), $phone_detecting_pages)) {
    ?>
    <script src="<?= url('public') ?>/build/js/prism.js"></script>
    <script src="<?= url('public') ?>/build/js/intlTelInput.js?1603274336113"></script>
    <script  src="<?= url('public') ?>/build/customTelInput.js?v=20"></script>
<?php } ?>
<!--style-->

<!-- Style js -->
<script type="text/javascript" src="<?php echo base_url('public/assets/shulesoft/style.js'); ?>?v=2"></script>


<!-- Jquery datatable js -->
<script type="text/javascript" src="<?php echo base_url('public/assets/datatables/jquery.dataTables.min.js'); ?>"></script>
<!-- Datatable js -->
<!--<script type="text/javascript" src="<?php echo base_url('public/assets/datatables/dataTables.bootstrap.js'); ?>"></script>-->

<script type="text/javascript" src="<?php echo base_url('public/assets/shulesoft/shulesoft.js'); ?>?v=3"></script>

<!-- autocomplete plugin select2 js -->
<script type="text/javascript" src="<?php echo base_url('public/assets/select2/select2.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/custom.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/datepicker/legacy.js'); ?>"></script>
<!--<script type="text/javascript" src="<?php echo base_url('public/assets/shulesoft/jquery.mCustomScrollbar.js'); ?>"></script> -->

<script type="text/javascript" src="<?php echo base_url('public/assets/datepicker/datepicker.js'); ?>"></script>

<!-- Jquery UI jquery -->
<script src="<?php echo base_url('public/assets/jquery-ui/jquery-ui.min.js'); ?>" type="text/javascript" charset="utf-8" async defer></script>

<!-- Jquery gritter -->
<script src="<?php echo base_url('public/assets/js/pnotify/pnotify.js') ?>"></script>
<script src="<?php echo base_url('public/assets/js/pnotify/pnotify.buttons.js') ?>"></script>
<script src="<?php echo base_url('public/assets/js/pnotify/pnotify.nonblock.js') ?>"></script>


<!-- Custom Theme Scripts -->
<!--Fastclick for flawless non laggy click effect in mobile phones-->
<script src="<?php echo base_url('public/assets/js/fastclick.js') ?>"></script>
<script src="<?php echo base_url('public/assets/js/nprogress.js') ?>"></script>
<script src="<?php echo base_url('public/assets/new_daterangepicker/moment.js') ?>"></script>

<script src="<?php echo base_url('public/assets/new_daterangepicker/daterangepicker.js') ?>"></script>
<script src="<?php echo base_url('public/assets/js/intro.min.js'); ?>"></script>
<!-- Switchery -->
<script src="<?php echo base_url('public/assets/js/switchery.min.js') ?>"></script>



<script src="<?php echo base_url('public/assets/fullcalendar/fullcalendar.min.js') ?>"></script>
<script src="<?php echo base_url('public/assets/js/custom.js') ?>"></script>

<div class="mask-body mask-body-dark" data-action="ajax_search_results-close" data-target="#ajax_search_results" hidden></div>

<script>

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#profile_pic').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".imgInp").change(function () {
        readURL(this);
    });
    $(document).ready(function () {

        //$('.calendar,#calendar,#calendar_year,#dob,#jod').attr('type','date');


        $('.calendar').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoApply: true,
            calender_style: "picker_3",
            minDate: moment().subtract(3, 'year'),
            dateFormat: 'YYYY-MM-DD',
            maxDate: moment().add(3, 'year')
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        
        $('.start_attedance_calendar').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoApply: true,
            calender_style: "picker_3",
            //minDate: moment().subtract(3, 'year'),
            dateFormat: 'YYYY-MM-DD',
            maxDate: moment().add(0, 'year')
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#calendar').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoApply: true,
            calender_style: "picker_3",
            minDate: moment().subtract(3, 'year'),
            format: 'YYYY-MM-DD',
            maxDate: moment()
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#calendar_year').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            calender_style: "picker_3",
            minDate: moment().subtract(3, 'year'),
            format: 'YYYY',
            autoApply: true,
            maxDate: moment()
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#dob').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            format: 'YYYY-MM-DD',
            calender_style: "picker_3",
            autoApply: true,
            minDate: moment().subtract(70, 'year'),
            maxDate: moment().subtract(0, 'year')
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#jod').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            format: 'YYYY-MM-DD',
            autoApply: true,
            calender_style: "picker_3",
            minDate: moment().subtract(70, 'year'),
            maxDate: moment().subtract(0, 'year')
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('input').attr('autocomplete', 'off');
    });
    $(function () {
        FastClick.attach(document.body);
    });
    if (typeof NProgress != 'undefined') {
        $(document).ready(function () {
            NProgress.start();
        });
        $(window).load(function () {
            NProgress.done();
        });
    }

    $(window).load(function () {
        // When the page has loaded
        $("body").fadeIn(2000);
    });
    function notify(message) {
        new PNotify({
            title: 'Intructions',
            text: message,
            type: 'info',
            hide: 'false',
            styling: 'bootstrap3',
            addclass: 'dark'
        });
    }

    $(".select2").select2({placeholder: "Select ", maximumSelectionSize: 20});
    $(".guargianID").select2({placeholder: "Select ", maximumSelectionSize: 20});
    $("button[data-select2-open]").click(function () {
        $("#" + $(this).data("select2-open")).select2("open");
    });
</script>


<!--<script type="text/javascript" src="<?php //echo base_url('public/assets/shulesoft/tether.min.js');                                    ?>"></script>-->
<!-- Bootstrap js -->
<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/fc-3.2.5/r-2.2.2/sc-2.0.0/sl-1.3.0/datatables.min.css"/>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/fc-3.2.5/r-2.2.2/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>-->

<!--Jquery Advanced Datatables-->
<script type="text/javascript" src="<?php echo base_url('public/assets/datatables/dataTables.buttons.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/datatables/buttons.print.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/datatables/buttons.flash.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/datatables/jszip.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/datatables/jszip.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/datatables/pdfmake.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/datatables/vfs_fonts.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/datatables/buttons.html5.min.js'); ?>"></script>
<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
<!--<script src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>-->
<!--<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>-->

<link href="<?php echo base_url('public/assets/datatables/buttons.dataTables.min.css'); ?>" rel="stylesheet">
<script src="<?php echo base_url('public/assets/js/notifications/socket.io.js') ?>"></script>


<script type="text/javascript">
    $(document).ready(function(){
        $(".phoneNumber").keyup(function(){
            var input = $(this).val();
            $("#phone").val('0'+ input);
        });
    });
</script>


<script type="text/javascript">

    function header() {
        var x = '';
        return x;
    }
    // $(document).ready(function () {
    //     $('[data-toggle="tooltip"]').tooltip()

    //     $(".phoneNumber").intlTelInput();

    //     //  $("[data-toggle='popover'], [data-hover='popover']").popover();

    // });
    data_table = function () {
        $('.dataTable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                {
                    text: 'PDF',
                    extend: 'pdfHtml5',
                    message: '',
                    orientation: 'landscape',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function (doc) {
                        doc.pageMargins = [10, 10, 10, 10];
                        doc.defaultStyle.fontSize = 7;
                        doc.styles.tableHeader.fontSize = 7;
                        doc.styles.title.fontSize = 9;
                        // Remove spaces around page title
                        doc.content[0].text = doc.content[0].text.trim();
                        // Create a footer
                        doc['footer'] = (function (page, pages) {
                            return {
                                columns: [
                                    'www.shulesoft.com',
                                    {
                                        // This is the right column
                                        alignment: 'right',
                                        text: ['page ', {text: page.toString()}, ' of ', {text: pages.toString()}]
                                    }
                                ],
                                margin: [10, 0]
                            }
                        });
                        // Styling the table: create style object
                        var objLayout = {};
                        // Horizontal line thickness
                        objLayout['hLineWidth'] = function (i) {
                            return .5;
                        };
                        // Vertikal line thickness
                        objLayout['vLineWidth'] = function (i) {
                            return .5;
                        };
                        // Horizontal line color
                        objLayout['hLineColor'] = function (i) {
                            return '#aaa';
                        };
                        // Vertical line color
                        objLayout['vLineColor'] = function (i) {
                            return '#aaa';
                        };
                        // Left padding of the cell
                        objLayout['paddingLeft'] = function (i) {
                            return 4;
                        };
                        // Right padding of the cell
                        objLayout['paddingRight'] = function (i) {
                            return 4;
                        };
                        // Inject the object in the document
                        doc.content[1].layout = objLayout;
                    }
                },
                {extend: 'copyHtml5', footer: true},
                {extend: 'excelHtml5', footer: true},
                {extend: 'csvHtml5', customize: function (csv) {
                        return "ShuleSoft" + csv + "ShuleSoft";
                    }},
                {extend: 'print', footer: true}

            ]
        });
        $('.buttons-html5,.dt-button').addClass('btn btn-success').removeClass('dt-button');
    }
<?php
if (!isset($ajax_data_table) && !in_array(request()->segment(2), ['payment_history'])) {
    ?>
        $(document).ready(data_table);

<?php } ?>
//	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
//			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
//		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
//	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
//
//	ga('create', 'UA-78826711-6', 'auto');
//	ga('send', 'pageview');

</script>

<!-- Select2 -->
<script type="text/javascript">

//    var config = {
//        '.select2'           : {},
//        '.select2-deselect'  : { allow_single_deselect: true },
//        '.select2-no-single' : { disable_search_threshold: 10 },
//        '.select2-no-results': { no_results_text: 'Oops, nothing found!' },
//        '.select2-rtl'       : { rtl: true },
//        '.select2-width'     : { width: '95%' }
//    }
//    for (var selector in config) {
//        $(selector).chosen(config[selector]);
//    }


//var $buoop = {required:{e:-4,f:-3,o:-3,s:-1,c:-3},insecure:true,unsupported:true,api:2019.05 };
//function $buo_f(){
//    var e = document.createElement("script");
//    e.src = "//browser-update.org/update.min.js";
//    document.body.appendChild(e);
//};
//try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
//catch(e){window.attachEvent("onload", $buo_f)}



    $(document).ready(function () {
        $('.nav-sm ul.nav.child_menu').css('display', 'none');
        scrollFunction()
        $(".select2_single").select2({
            placeholder: "Select------",
            allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
            maximumSelectionLength: 400,
            placeholder: "Select",
            allowClear: true
        });
    });
    $('#compose, .compose-close').click(function () {
        $('.compose').slideToggle();
    });

    // When the user scrolls down 60px from the top of the document, show the button
    window.onscroll = function () {
        scrollFunction();
    };
    function scrollFunction() {
        if (document.body.scrollTop > 60 || document.documentElement.scrollTop > 60) {
            $("#to_top").removeClass('hidden');
            $("#to_top").css('display', 'block');
        } else {
            $("#to_top").css('display', 'none');
        }
    }

    // When the user clicks on the button, scroll to the top of t            he document
    function topFunction() {
        //document.body.scrollTop = 0; // For Safai
        //document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera

        $("HTML, BODY").animate({scrollTop: 0}, 300);
    }
    jQuery('#loader-content').hide();

    $("#admitted_from").select2({
        minimumInputLength: 2,
        //  tags: [],
        ajax: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?= url('student/getschools/null') ?>',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (term) {
                return {
                    term: term,
                    token: $('meta[name="csrf-token"]').attr('content')
                };
            },
            results: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    })
                };
            }
        }
    });


    $("#invoice_id").select2({
        minimumInputLength: 2,
        //  tags: [],
        ajax: {
            url: '<?= url('invoices/getstudent_invoices/null') ?>',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (term) {
                return {
                    term: term
                };
            },
            results: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    })
                };
            }
        }
    });
</script>

<style>

    #myBtn {
        display: none; /* Hidden by default */
        position: fixed; /* Fixed/sticky position */
        bottom: 20px; /* Place the button at the bottom of the page */
        right: 30px; /* Place the button 30px from the right */
        z-index: 99; /* Make sure it does not overlap */
        border: none; /* Remove borders */
        outline: none; /* Remove outline */
        cursor: pointer; /* Add a mouse pointer on hover */
        padding: 15px; /* Some padding */

        /* transition */
        -webkit-transition: 1s;
        -moz-transition: 1s;
        transition: 1s;
    }

    #to_top {
        bottom: 20px;
        right: 20px;
        background-color: #09a2a2;
        color: #fff;
        font-size: 12px;
        text-transform: uppercase;
        font-weight: 600;
        letter-spacing: 1.3px;
        text-decoration: none;
        opacity: 1;
        padding: 5px 0 4px;
        width: 140px;
        text-align: center;
        -webkit-backface-visibility: hidden;
        border-radius: 18px;
        -webkit-transition: background .2s ease;
        transition: background .2s ease;
    }
    @media (max-width: 550px) {
        #to_top {
            bottom: 0;
            right:0%;
        }
    }
    git branch
    .scrollable {
        -webkit-overflow-scrolling: touch;
        overflow: scroll;
    }

</style>


<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-messaging.js"></script>
<!-- For an optimal experience using Cloud Messaging, also add the Firebase SDK for Analytics. -->
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-analytics.js"></script>

<script>


    if ('serviceWorker' in navigator) {
//console.log('in the service worker');
        navigator.serviceWorker.register('/firebase-messaging-sw.js', {scope: '/dashboard/'})
                .then(function (registration) {
//console.log('Registration successful, scope is:', registration.scope);
                }).catch(function (err) {
//console.log('Service worker registration failed, error:', err);
        });
    }
    ;

// Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyCrSP0vteP6gBoq7zK63ZsLczt0Z7aEmhk",
        authDomain: "shulesoft-1507541174851.firebaseapp.com",
        databaseURL: "https://shulesoft-1507541174851.firebaseio.com",
        projectId: "shulesoft-1507541174851",
        storageBucket: "shulesoft-1507541174851.appspot.com",
        messagingSenderId: "678078088465",
        appId: "1:678078088465:web:61614ed8bae019f55978b0",
        measurementId: "G-41DVG6JXC0"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    //firebase.analytics();

    const messaging = firebase.messaging();


    messaging
            .requestPermission()
            .then(function () {
                return messaging.getToken();
            })
            .then(function (token) {
                // console.log(" TOKEN: " + token); 
                $.ajax({
                    url: "<?= base_url('user/saveToken') ?>",
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    async: true,
                    cache: false,
                    data: {
                        fcm_token: token
                    },
                    dataType: 'JSON',
                    success: function (response) {
                        //console.log(response);
                    },
                    error: function (err) {
                        //console.log(" Can't do because: " + err);
                    }
                });
            })
            .catch(function (err) {
                //console.log("Unable to get permission to notify.", err);
            });

    messaging.onMessage(function (payload) {
        const noteTitle = payload.notification.title;
        const noteOptions = {
            body: payload.notification.body,
            icon: payload.notification.icon,
        };
        new Notification(noteTitle, noteOptions);
    });

    function count_page_tip(a) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            async: true,
            cache: false,
            beforeSend: function (xhr) {
                jQuery('#loader-content').hide();
            },
            url: "<?= base_url('setting/pageTipView') ?>",
            type: 'POST',
            data: {
                id: a
            },
            success: function (response) {
                //console.log(response);
            },
            error: function (err) {
                //console.log(" Can't do because: " + err);
            }
        });
    }

    page_tips_feedback = function () {

        $('#page_tips_feedback').after('<div class="form-group col-lg-6"><label class="control-label col-md-6 col-sm-6 col-xs-6" for="last-name">Is this helpful ? </label> <div class="col-md-6 col-sm-6 col-xs-6"> <input type="radio" value="1" name="page_tip_radio"  onmousedown="submit_tip_feedback(1)" autocomplete="off"> YES&nbsp;<input type="radio"  name="page_tip_radio"  value="0" onmousedown="submit_tip_feedback(0)" autocomplete="off"> NO</div></div>');

    }
    function submit_tip_feedback(a) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            async: true,
            cache: false,
            beforeSend: function (xhr) {
                jQuery('#loader-content').hide();
            },
            url: "<?= base_url('setting/pageTipView') ?>",
            type: 'POST',
            data: {
                status: a, 'page': '<?= createRoute() ?>'
            },
            success: function (response) {
                //console.log(response);
            },
            error: function (err) {
                //console.log(" Can't do because: " + err);
            }
        });
    }
    $(document).ready(page_tips_feedback);
</script>

<?= isset($page_tip_content) ? $page_tip_content : '' ?>

@yield('js')
</body>
<?php
if (in_array(set_schema_name(), ['public.', 'beta_testing.'])) {
    ?>
                                <!--<script src="https://account.snatchbot.me/script.js"></script><script>window.sntchChat.Init(135004)</script>--> 
<?php } ?>
</html>