
<style>
    @media (max-width: 400px){
        .hidden-sm{
            display: none;
        }
        .user-profile img {
            margin-right: 0px;
        }
        .navbar-brand-img, .navbar-brand>img {
            max-width: 100% !important;
            height: auto !important;
            max-height: 3.5rem !important;
            border: solid 1px white;
            padding: 25px 25px 0 -1px !important;
        }
        .navbar-brand{
            padding: 0 !important;
        }
    }
    @media (max-width: 240px){
        .hidden-xs{
            display: none;
        }
        .nav>li>a {
            padding: 13px 4px 12px;
        }
    }
    .navbar-brand{
        float: left;
        height: 57px;
        padding: 17px 74px;
        font-size: 24px;
    }
    .navbar-brand-img, .navbar-brand>img {
        max-width: 100%;
        max-height: 4.5rem;
        border: solid 1px white;
        padding: 3px;
    }

    @media (min-width: 768px){
        .navbar-vertical.navbar-expand-md .navbar-brand-img {
            max-height: 3rem;
        }}

    #blinkingg {
        animation:blinkingText 1.2s infinite;
    }
    @keyframes blinkingText {
        0%{     color: #ff0000;  padding: 1px; font-weight: bold;  }
        49%{    color: #ffff00; }
        60%{    color: transparent; }
        49%{    color: #ffffff;}
        100%{   color: #ffffff;    }
    }

</style>
<div class="container body">
    <div class="main_container">
        <!--    if you uncomment this, delete the below div. This will make scrowbar fixed
        <div class="col-md-3 left_col menu_fixed">-->

        <div class="col-md-3 left_col">
            <div class="left_col col-sm-12 col-xs-12 col-lg-12 scroll-view" style="padding-right: 0">

                <?php
                if (session('table') == 'teacher') {
                    $class_student = \App\Model\Student::whereIn('classesID', \App\Model\Classes::where('teacherID', session('id'))->get(['classesID']))->where('status', 1)->count();
                }
                $usertype = ucfirst(session("usertype"));
                if (session('table') != null) {
                    $id = trim(session('table')) == 'student' ? 'student_id' : session('table') . 'ID';
                    $user_info = \collect(DB::select('select * FROM ' . set_schema_name() . session('table') . ' where "' . $id . '"=\'' . session('id') . '\' '))->first();
                } else {
                    return redirect('signin/index');
                }
                ?>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side d-print main_menu" style="z-index: 1001;">
                    <?php $server_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost'; ?>
                    @if(preg_match('/schoolbanks/i', $server_name)) 
                    <a href="{{url('/dashboard/index')}}" class="navbar-brand">
                        <?php
                        $fimage = array(
                            "src" => "public/assets/schoolbank_images/SchoolBanks-logo-final-line.png",
                            'width' => '',
                            'height' => '',
                            'class' => 'navbar-brand-img
                            mx-auto img-circle img-responsive',
                            'alt' => "ShuleSoft"
                        );
                        echo img($fimage);
                        ?>
                    </a>
                    @else
                    <a href="{{url('/dashboard/index')}}" class="navbar-brand">
                        <?php
                        $fimage = array(
                            "src" => "storage/uploads/images/" . $siteinfos->photo,
                            'width' => '',
                            'height' => '',
                            'class' => 'navbar-brand-img
                            mx-auto img-circle img-responsive',
                            'alt' => "ShuleSoft",
                            'title' => $siteinfos->sname
                        );
                        echo img($fimage);
                        ?>
                    </a>
                    @endif
                    
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><?php
                                echo anchor('dashboard/index', '<i class="fa fa-home"></i> <span>' . $data->lang->line('menu_dashboard') . '</span>');
                                ?>

                            </li>
                            <?php
                            if (set_schema_name() == 'canossa.') {
//                                echo "<li>";
//                                echo anchor('application/index', '<i class="fa fa-desktop"></i> <span>Applications</span>');
//                                echo " </li>";
                            }
                            ?>
                            <?php
                            //                            $necta = \DB::table('necta')->get();
                            //                            if (count($necta) > 0) {
                            //                                echo "<li>";
                            //                                echo anchor('exam/necta', '<i class="fa fa-desktop"></i> <span>NECTA results</span>');
                            //                                echo " </li>";
                            //                            }
                            ?>
                            <?php
//                            if ($usertype == 'Parent' && (isset($user_info->status) && $user_info->status == 5)) {
//                                echo "<li>";
//                                echo anchor('admission/status', '<i class="fa fa-desktop"></i> <span>Admission Status</span>');
//                                echo " </li>";
//                            }
                            ?>

                            <li id="users">
                                <?php if (can_access('view_student') || can_access('view_parents') || can_access('view_teachers') || can_access('view_users') || can_access('view_sponsors')) { ?>
                                    <a><i class="fa fa-users"></i> <?= $data->lang->line('menu_user'); ?>
                                        <span class="fa fa-chevron-down"></span></a>

                                <?php } ?>
                                <ul class="nav child_menu">
                                    <?php
                                    if (can_access('view_student')) {
                                        echo '<li>';
                                        echo anchor('student/index', '<i class="fa icon-student"></i><span>' . $data->lang->line('menu_student') . '</span>');
                                        echo '</li>';
                                    }
                                    ?>

                                    <?php
                                    if (can_access('view_parents')) {
                                        echo '<li>';
                                        echo anchor('parents/index', '<i class="fa fa-user"></i><span>' . $data->lang->line('menu_parent') . '</span>');
                                        echo '</li>';
                                    }
                                    ?>

                                    <?php
                                    if (can_access('view_teachers')) {
                                        echo '<li>';
                                        echo anchor('teacher/index', '<i class="fa icon-teacher"></i><span>' . $data->lang->line('menu_teacher') . '</span>');
                                        echo '</li>';
                                    }
                                    ?>

                                    <?php
                                    if (can_access('view_users')) {
                                        echo '<li>';
                                        echo anchor('user/index', '<i class="fa fa-users"></i><span>' . $data->lang->line('menu_staff') . '</span>');
                                        echo '</li>';
                                    }
                                    ?>
                                    <?php
                                    if (session('table') == 'teacher') {
                                        if ($class_student > 0) {
                                            echo '<li>';
                                            echo anchor('student/class_student/' . session('id'), '<i class="fa fa-users"> </i> <span>' . $data->lang->line('class_student') . '</span>');
                                            echo '</li>';
                                        }
                                    }
                                    ?>
                                    <?php
                                    if (can_access('view_sponsors') && set_schema_name() == 'shulesoft.' || set_schema_name() == 'silverleaf.') {
                                       echo '<li>';
                                       echo anchor('sponsors/index', '<i class="fa fa-list"></i><span>' . $data->lang->line('sponsor') . '</span>');
                                       echo '</li>';
                                    }
                                    ?>
                                    <?php
                                    /**
                                     * This menu will load all type of user groups/roles (predifines/to be defined)
                                     */
                                    //echo '<li>';
                                    //echo anchor('user/index', '<i class="fa fa-users"></i><span>' . $data->lang->line('menu_user') . '</span>');
                                    //echo '</li>';
                                    ?>

                                </ul>
                            </li>

                            <li id="classes"><?php
                                if (can_access('view_class')) {
                                    echo '<li>';
                                    echo anchor('classes/index', '<i class="fa fa-sitemap"></i><span>' . $data->lang->line('menu_classes') . '</span>');
                                    echo '</li>';
                                }
                                ?>
                            </li>
                            <li id="sections">
                                <?php
                                if (can_access('view_section')) {
                                    echo '<li>';
                                    echo anchor('section/index', '<i class="fa fa-star"></i><span>' . $data->lang->line('menu_section') . '</span>');
                                    echo '</li>';
                                }
                                ?>

                            </li>
                            <?php if (can_access('view_subject')) { ?>
                                <li id="subjects"><a><i class="fa icon-subject"></i><?php echo $data->lang->line('menu_subject') ?> <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <?php
                                        echo '<li>';
                                        echo anchor('subject/all_subject', '<i class="fa icon-subject"></i><span>' . $data->lang->line('menu_subject_list') . '</span>');
                                        echo '</li>';
                                        echo '<li>';
                                        echo anchor('subject/index', '<i class="fa icon-subject"></i><span>' . $data->lang->line('class_subject') . '</span>');
                                        echo '</li>';

                                        echo '<li>';
                                        echo anchor('section_teacher/index', '<i class="fa icon-subject"></i><span>' . $data->lang->line('section_subject_teacher') . '</span>');
                                        echo '</li>';
                                        $f = 1;
                                        if ($f == 1) {
                                            ?>
                                            <li class="active"><a><?php echo $data->lang->line('menu_syllabus') ?> <span class="fa fa-chevron-down"></span></a>
                                                <ul class="nav child_menu" style="display: block;">
                                                    <!--                                                    <li class="sub_menu">
                                                    <?php echo anchor('syllabus/benchmark', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_benchmark') . '</span>'); ?>
                                                                                                        </li>-->
                                                    <li class="sub_menu">
                                                        <?php echo anchor('syllabus/scheme', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_scheme_of_work') . '</span>'); ?>
                                                    </li>

                                                    <!--                                                    <li>
                                                    <?php echo anchor('syllabus/plan', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_lesson_plan') . '</span>'); ?>
                                                                                                        </li>-->
                                                    <li class="sub_menu">
                                                        <?php echo anchor('syllabus/report', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_syllabus_report') . '</span>'); ?>
                                                    </li>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                    </ul></li>
                                <?php
                            }

                            if (can_access('view_grade')) {
                                ?>
                                <li id="grades"><a><i class="fa fa-th"></i><?php echo $data->lang->line('menu_grade') ?> <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <?php
                                        echo '<li>';
                                        echo anchor('grade/index', '<i class="fa fa-th"></i><span>' . $data->lang->line('default_grade') . '</span>');
                                        echo '</li>';
                                        echo '<li>';
                                        echo anchor('special_grade/names', '<i class="fa fa-th"></i><span>' . $data->lang->line('special_grade_names') . '</span>');
                                        echo '</li>';
                                        echo '<li>';
                                        echo anchor('special_grade/index', '<i class="fa fa-th"></i><span>' . $data->lang->line('special_grade') . '</span>');
                                        echo '</li>';
                                        ?>

                                    </ul>
                                    <?php
                                }
                                if (can_access('view_semester')) {
//                                    echo '<li>';
//                                    echo anchor('classlevel/index', '<i class="fa fa-sort"></i><span>' . $data->lang->line('menu_classlevel') . '</span>');
//                                    echo '</li>';
                                    echo '<li id="terms">';
                                    echo anchor('semester/index', '<i class="fa fa-signal"></i><span>' . $data->lang->line('menu_semester') . '</span>');
                                    echo '</li>';
                                }
                                ?>


                                <?php if (can_access('view_exam')) { ?>
                                <li id="exams">
                                    <a>
                                        <i class="fa icon-exam"></i>
                                        <span><?= $data->lang->line('menu_exam'); ?></span>
                                        <span class=" fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li class="active"><a><?php echo $data->lang->line('exams_settings') ?> <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu" style="display: block;">
                                                <li class="sub_menu">
                                                    <?php echo anchor('schoolexam/group', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_exam_groups') . '</span>'); ?>
                                                </li>

                                                <li>
                                                    <?php echo anchor('schoolexam/index', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_school_exam') . '</span>'); ?>
                                                </li>
                                                <li>
                                                    <?php echo anchor('exam/index', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('exams_class_allocation') . '</span>'); ?>
                                                </li>
                                            </ul>
                                        </li>
                                        <!--                                        <li>
                                        <?php echo anchor('schoolexam/index', '<i class="fa fa-pencil"></i><span>' . $data->lang->line('menu_school_exam') . '</span>'); ?>
                                                                                </li>-->
                                        <!--                                        <li>
                                        <?php echo anchor('exam/index', '<i class="fa fa-pencil"></i><span>' . $data->lang->line('menu_exam_definition') . '</span>'); ?>
                                                                                </li>-->
                                        <li>
                                            <?php echo anchor('examschedule/index', '<i class="fa fa-puzzle-piece"></i><span>' . $data->lang->line('menu_examschedule') . '</span>'); ?>
                                        </li>

                                        <li class="active"><a><?php echo $data->lang->line('menu_exam_report') ?> <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu" style="display: block;">
                                                <li>
                                                    <?php echo anchor('exam/report', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_report') . '</span>'); ?>
                                                </li>
                                                <li class="sub_menu">
                                                    <?php echo anchor('exam/combined', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_accumulative_report') . '</span>'); ?>
                                                </li>

                                                <li>
                                                    <?php echo anchor('ExamReport/index', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_view_report') . '</span>'); ?>
                                                </li>
                                            </ul>
                                        </li>
                                        @if(set_schema_name() == 'eboniteinstitute.' || set_schema_name() == 'shulesoft.')
                                        <li class="active"><a><?php echo $data->lang->line('college_exam_report') ?> <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu" style="display: block;">
                                                <li>
                                                    <?php echo anchor('exam_two/semester_report', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_report') . '</span>'); ?>
                                                </li>
                                                <li class="sub_menu">
                                                    <?php echo anchor('exam_two/full_semester_report', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('semester_report') . '</span>'); ?>
                                                </li>
                                                <li class="sub_menu">
                                                    <?php echo anchor('exam_two/grade_report', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('grade_report') . '</span>'); ?>
                                                </li>
                                                <li class="sub_menu">
                                                    <?php echo anchor('exam_two/combined', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('year_report') . '</span>'); ?>
                                                </li>
                                                <li>
                                                    <?php echo anchor('ExamReport/index', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_view_report') . '</span>'); ?>
                                                </li>
                                            </ul>
                                        </li>
                                        @endif
                                        <?php if (can_access('general_character_assessment')) { ?>
                                            <li>
                                                <?php echo anchor('general_character_assessment/index', '<i class="fa fa-users"></i><span>' . $data->lang->line('menu_comment_per_student') . '</span>'); ?>
                                            </li>
                                        <?php } ?>
                                        <li>
                                            <?php echo anchor('schoolexam/minor', '<i class="fa fa-file-o"></i><span>' . $data->lang->line('menu_minor_exams') . '</span>'); ?>
                                        </li>
    <!--                                        <li class="active"><a>Evaluation<span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu" style="display: block;">
    <li class="sub_menu">
                                        <?php //echo anchor('examreport/combined', '<i class="fa fa-clipboard"></i><span>Exams</span>');      ?>
    </li>
    <li class="sub_menu">
                                        <?php //echo anchor('examreport/combined', '<i class="fa fa-clipboard"></i><span>Teachers</span>');      ?>
    </li>

    </ul>
    </li>-->
    <!--                                        <li class="active"><a>NECTA Reports<span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu" style="display: block;">
    <li class="sub_menu">
                                        <?php //echo anchor('exam/combined', '<i class="fa fa-clipboard"></i><span>CA</span>');      ?>
    </li>

    </ul>
    </li>-->
                                        <li>

                                        </li>
                                    </ul>
                                </li>

                            <?php } ?>
                            <?php
                            if (can_access('add_mark')) {
                                echo '<li id="marks">';
                                echo anchor('mark/index', '<i class="fa fa-flask"></i><span>' . $data->lang->line('menu_mark') . '</span>');
                                echo '</li>';
                            }
                            ?>
                            <?php
                            $c = 1;

                            if ($c == 2 && can_access('view_exam')) {
                                ?>
                                <li>
                                    <a>
                                        <i class="fa icon-exam"></i>
                                        <span><?php echo $data->lang->line('academy') ?></span>
                                        <span class=" fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li>

                                            <?php echo anchor('subject_topic/index', '<i class="fa icon-subject"></i><span>' . $data->lang->line('subject_topics') . '</span>'); ?>
                                        </li>
                                        <li>
                                            <?php echo anchor('topic_mark/add', '<i class="fa fa-check"></i><span>' . $data->lang->line('topic_mark') . '</span>'); ?>
                                        </li>
                                        <li>
                                            <?php echo anchor('exam_mark/add', '<i class="fa fa-check"></i><span>' . $data->lang->line('exam_mark') . '</span>'); ?>
                                        </li>

                                        <li>
                                            <?php echo anchor('academy_report/ac_report', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_report') . '</span>'); ?>

                                        </li>

                                    </ul>
                                </li>
                            <?php } ?>


                            <?php
//                            if (can_access('view_examschedule')) {
//                                echo '<li>';
//                                echo anchor('examschedule/index', '<i class="fa fa-puzzle-piece"></i><span>' . $data->lang->line('menu_examschedule') . '</span>');
//                                echo '</li>';
//                            }
                            ?>


                            <?php
                            if ($usertype == "Parent" || $usertype == "Student") {
                                ?>
                                <li>

                                    <?php echo anchor('mark/index', '<i class="fa icon-exam"></i><span>' . $data->lang->line('menu_mark_report') . '</span>'); ?>
                                </li>
                            <?php } ?>


                            <?php
                            if ($usertype == "Student" || $usertype == "Parent" || can_access('view_routine')) {?>

                                <li id="routines"><a><i class="fa icon-routine"></i>
                                <?php echo $data->lang->line('menu_routine') ?> <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <?php
                                    echo '<li>';
                                    echo anchor('routine/index', '<i class="fa fa-th"></i><span>Class Routine</span>');
                                    echo '</li>';
                                    echo '<li>';
                                    echo anchor('routine/daily_routine/index', '<i class="fa fa-th"></i><span>Daily Routine</span>');
                                    echo '</li>';
                                   
                                    ?>

                                </ul>




                           <?php }
                            ?>
                            <?php
                            if ($usertype == "Student" || $usertype == "Parent" || can_access('view_notice')) {
                                echo '<li>';
                                echo anchor('news_board/index', '<i class="fa fa-bullhorn"></i><span>' . $data->lang->line('news_board') . '</span>');
                                echo '</li>';
                            }
                            ?>

                            <?php if ((can_access('view_exam_attendance') || can_access('view_student_attendance') || can_access('view_teacher_attendance') || session('table') == 'teacher') && !in_array(session('table'), ['parent', 'student'])) { ?>
                                <li id="tattendance">
                                    <a>
                                        <i class="fa icon-attendance"></i>
                                        <span><?= $data->lang->line('menu_attendance'); ?> </span>
                                        <span class=" fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <?php if (can_access('view_student_attendance')) { ?>
                                            <li>
                                                <?php echo anchor('sattendance/index', '<i class="fa icon-sattendance"></i><span>' . $data->lang->line('menu_sattendance') . '</span>'); ?>
                                            </li>
                                        <?php } ?>
                                        <?php if (can_access('view_teacher_attendance')) { ?>
                                            <li>
                                                <?php echo anchor('tattendance/index', '<i class="fa icon-tattendance"></i><span>' . $data->lang->line('menu_tattendance') . '</span>'); ?>
                                            </li>
                                        <?php } ?>
                                        <?php if (can_access('view_exam_attendance')) { ?>
                                            <li>
                                                <?php echo anchor('eattendance/index', '<i class="fa icon-eattendance"></i><span>' . $data->lang->line('menu_eattendance') . '</span>'); ?>
                                            </li>
                                        <?php } ?>
                                        <?php if (session('table') == 'teacher' || can_access('view_teacher_on_duty')) { ?>
                                            <li>
                                                <?php echo anchor('tattendance/tod', '<i class="fa fa-bell"></i><span>' . $data->lang->line('menu_tod') . '</span>'); ?>
                                            </li>
                                        <?php } ?>

                                        @if (can_access('view_student_attendance'))
                                        <li>
                                            <?php echo anchor('sattendance/attendance_report', '<i class="fa icon-sattendance"></i><span>' . $data->lang->line('menu_sattendance_report') . '</span>'); ?>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php
                            if ($usertype == "Student" || $usertype == "Parent") {
                                echo '<li id="sattendance">';
                                echo anchor('sattendance/view', '<i class="fa icon-sattendance"></i><span>' . $data->lang->line('menu_attendance') . '</span>');
                                echo '</li>';
                            }
                            ?>

                            <?php if (can_access('add_character') || can_access('assess_character') || can_access('general_character_assessment') || can_access('assign_character')) { ?>
                                <li>
                                    <a>
                                        <i class="fa fa-list"></i>
                                        <span><?= $data->lang->line('menu_characters'); ?></span>
                                        <span class=" fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <?php if (can_access('add_character')) { ?>
                                            <li>
                                                <?php echo anchor('character_categories/index', '<i class="fa fa-list"></i><span>' . $data->lang->line('menu_character_category') . '</span>'); ?>
                                            </li>
                                            <li>
                                                <?php echo anchor('characters/index', '<i class="fa fa-list"></i><span>' . $data->lang->line('menu_character_list') . '</span>'); ?>
                                            </li>
                                            <li>
                                                <?php echo anchor('character_grading/index', '<i class="fa fa-list-ol"></i><span>' . $data->lang->line('menu_character_grades') . '</span>'); ?>
                                            </li>
                                        <?php } ?>
                                        <?php if (can_access('assign_character')) { ?>
                                            <!--                                            <li>
                                            <?php //echo anchor('student_characters/index', '<i class="fa fa-share"></i><span>' . $data->lang->line('menu_assign') . '</span>');       ?>
                                                    </li>-->
                                        <?php } ?>
                                        <?php if (can_access('assess_character')) { ?>
                                            <!--                                            <li>
                                            <?php //echo anchor('student_characters/assess', '<i class="fa fa-user"></i><span>' . $data->lang->line('menu_assess') . '</span>');       ?>
                                                    </li>-->
                                        <?php } ?>
                                        <?php if (false) { ?>
                                            <li>
                                                <?php echo anchor('general_character_assessment/index', '<i class="fa fa-users"></i><span>' . $data->lang->line('menu_general') . '</span>'); ?>
                                            </li>
                                        <?php } ?>
                                        <?php if (can_access('assess_head_teacher')) { ?>
                                            <!--                                            <li>
                                            <?php echo anchor('general_character_assessment/index', '<i class="fa fa-user"></i><span>' . $siteinfos->headname . $data->lang->line('assesment') . '</span>'); ?>
                                                    </li>-->
                                        <?php } ?>
                                        <?php if (can_access('assess_character')) { ?>
                                            <li>
                                                <?php echo anchor('general_character_assessment/assessed', '<i class="fa fa-file-text-o"></i><span>' . $data->lang->line('character_report') . '</span>'); ?>

                                            </li>
                                        <?php } ?>

                                    </ul>
                                </li>
                            <?php } ?>



                            <?php if (can_access('view_library')) { ?>
                                <li id='libraries'>
                                    <a>
                                        <i class="fa icon-library"></i>
                                        <span><?= $data->lang->line('menu_library'); ?></span>
                                        <span class=" fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <?php if (can_access("view_library_members")) { ?>
                                            <li>
                                                <?php echo anchor('lmember/index', '<i class="fa icon-member"></i><span>' . $data->lang->line('menu_member') . '</span>'); ?>
                                            </li>
                                        <?php } ?>
                                        <li>
                                            <?php echo anchor('book/index', '<i class="fa icon-lbooks"></i><span>' . $data->lang->line('menu_books') . '</span>'); ?>
                                        </li>
                                        <?php
                                        echo '<li>';
                                        echo anchor('issue/index', '<i class="fa icon-issue"></i><span>' . $data->lang->line('menu_issue') . '</span>');
                                        echo '</li>';
                                        ?>
                                        <!--                    <li>
                                        <?php echo anchor('issue/fine', '<i class="fa icon-fine"></i><span>' . $data->lang->line('menu_fine') . '</span>'); ?>
                                                </li>-->

                                        <!--                    <li>
                                        <?php echo anchor('issue/fine', '<i class="fa icon-fine"></i><span>' . $data->lang->line('menu_fine') . '</span>'); ?>
                                                </li>-->
                                        <li class="active"><a><?= $data->lang->line('menu_library_report') ?><span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu" style="display: block;">
                                                <li class="sub_menu">
                                                    <?php echo anchor('book/general_report', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('general_report') . '</span>'); ?>
                                                </li>
                                                <li>
                                                    <?php echo anchor('book/book_report', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('other_report') . '</span>'); ?>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if (can_access('view_transport')) { ?>

                                <li id="transports">
                                    <a>
                                        <i class="fa icon-bus"></i>
                                        <span><?= $data->lang->line('menu_transport'); ?></span>
                                        <span class=" fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <?php echo can_access('view_transport') ? ' <li>' . anchor('vehicle/index', '<i class="fa icon-sbus"></i><span>' . $data->lang->line('menu_vehicle') . '</span>') . '</li>' : ''; ?>
                                        <?php echo can_access('view_transport') ? ' <li>' . anchor('transport/index', '<i class="fa icon-sbus"></i><span>' . $data->lang->line('menu_transport_routes') . '</span>') . '</li>' : ''; ?>


                                        <?php //echo can_access('view_transport') ? ' <li>' . anchor('tmember/index', '<i class="fa icon-member"></i><span>' . $data->lang->line('transport_assign_member') . '</span>') . '</li>' : '';   ?>

                                        <?php echo can_access('add_transport_member') ? ' <li>' . anchor('tmember/route_member/', '<i class="fa icon-invoice"></i><span>' . $data->lang->line('transport_member_details') . '</span>') . '</li>' : ''; ?>


                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if (can_access('view_hostel')) { ?>
                                <li id="hostels">
                                    <a>
                                        <i class="fa icon-hhostel"></i>
                                        <span><?= $data->lang->line('menu_hostel'); ?></span>
                                        <span class=" fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li>
                                            <?php echo anchor('hostel/index', '<i class="fa icon-hostel"></i><span>' . $data->lang->line('menu_hostel') . '</span>'); ?>
                                        </li>
                                        <li>
                                            <?php //echo anchor('category/index', '<i class="fa fa-leaf"></i><span>' . $data->lang->line('menu_category') . '</span>');   ?>
                                        </li>
                                        <li>
                                            <?php
                                            if (can_access('view_hostel_member')) {

                                                echo anchor('hmember/index', '<i class="fa icon-member"></i><span>' . $data->lang->line('menu_member') . '</span>');
                                            }
                                            ?>
                                        </li>
                                        <?php if (can_access('view_hostel_member')) { ?>
                                            <li>
                                                <?php
                                                //echo anchor('hostel/report', '<i class="fa icon-member"></i><span>' . $data->lang->line('menu_report') . '</span>');
                                                ?>
                                            </li>
                                        <?php } ?>
                                        <li>
                                            <?php
                                            if ($usertype == "Student") {
                                                echo anchor('hmember/view', '<i class="fa fa-briefcase"></i><span>' . $data->lang->line('menu_profile') . '</span>');
                                            }
                                            ?>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php
                            if ($usertype == "Parent" || $usertype == "Student") {
                                echo '<li>';
                                echo anchor('invoices', '<i class="fa icon-payment"></i><span>' . $data->lang->line('menu_payment_history') . '</span>');
                                echo '</li>';
                            }
                            ?>
                    <?php
                            if ($usertype == "Parent" && set_schema_name() == 'stpeterclaver.' || $usertype == "Student" ) {
                                echo '<li>';
                                echo anchor('wallet/studentWallet/', '<i class="fa fa-tags"></i><span>' . $data->lang->line('pocket_money') . '</span>');
                                echo '</li>';
                            }
                            ?>

                            <?php if (can_access('view_account') && !in_array(session('table'), ['parent', 'student'])) { ?>
                                <li id="accounts"><a><i class="fa icon-feetype"></i><?= $data->lang->line('menu_account') ?> <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="#level1_1"><?= $data->lang->line('fees') ?><span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">

                                                <li class="sub_menu"><?php echo can_access('view_installment') ? anchor('payment_plan/index', '<i class="fa icon-feetype"></i><span>' . $data->lang->line('installment') . '</span>') : ''; ?>
                                                </li>
                                                <li><?php echo can_access('view_feetype') ? anchor('fee/index', '<i class="fa fa-money"></i><span>' . $data->lang->line('add_fees') . '</span>') : ''; ?>
                                                </li>
                                                <li>     
                                                    <?php echo can_access('view_feetype') ? anchor('fee_detail/details', '<i class="fa icon-feetype"></i><span>' . $data->lang->line('fee_details') . '</span>') : ''; ?>
                                                </li>
                                        </li>
                                        <?php echo can_access('add_invoice_discount') ? '<li>' . anchor('fee_discount/index', '<i class="fa icon-invoice"></i><span>' . $data->lang->line('menu_discount') . '</span>') . ' </li>' : ''; ?>
                                        <li>
                                            <?php echo can_access('edit_fee') ? anchor('fee/exclude_fee', '<i class="fa icon-feetype"></i><span>' . $data->lang->line('unsubscribe_student') . '</span>') : ''; ?>
                                        </li>
                                    </ul>
                                </li>

                                <?php if (can_access('view_invoice')) {
                                    ?>
                                    <li class="sub_menu">
                                        <?php echo anchor('invoices/index', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_invoice') . '</span>'); ?>

                                    </li>
                                    <?php
                                }
                                if (can_access('view_assets') || can_access('view_liability') || can_access('view_capital') || can_access('view_revenue') || can_access('view_expense') || can_access('view_cash_request')) {
                                    ?>

                                    <li class=""><a><?= __('topbar_menu_lang.menu_transactions') ?> <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: block;">
                                            <?php
                                            if (can_access('view_revenue')) {
                                                echo ' <li>';
                                                echo anchor('revenue/index', '<i class="fa icon-account"></i><span>' . $data->lang->line('menu_revenue') . '</span>');
                                                echo '</li>';
                                            }

                                            if (can_access('view_expense')) {
                                                echo ' <li>';
                                                echo anchor('expense/index/4', '<i class="fa icon-expense"></i><span>' . $data->lang->line('menu_expense') . '</span>');
                                                echo '</li>';
                                            }



                                            echo can_access('view_assets') ? '<li class="sub_menu">' . anchor('expense/index/1', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('fixed_assets') . '</span>') . ' </li>' : '';
                                            ?>


                                            <?php echo can_access('view_assets') ? '<li class="sub_menu">' . anchor('expense/index/5', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('current_assets') . '</span>') . ' </li>' : ''; ?>

                                            <?php echo can_access('view_liability') ? '<li>' . anchor('expense/index/2', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('liabilities') . '</span>') . '</li>' : ''; ?>


                                            <?php
                                            echo can_access('view_capital') ? '<li>' . anchor('expense/index/3', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('capital') . '</span>') . '</li>' : '';

                                            echo can_access('view_cash_request') ? '<li class="sub_menu">' . anchor('expense/cash_request', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('cash_request') . '</span>') . ' </li>' : '';

                                            echo '<li>';
                                            echo anchor('PaymentController/reconciliation', '<i class="fa icon-promotion"></i><span>' . $data->lang->line('menu_reconciliation') . '</span>');
                                            echo '</li>';
                                            ?>



                                        </ul>
                                    </li>
                                <?php } ?>





                                <?php
                                $schema = str_replace('.', NULL, set_schema_name());

                                if (can_access('view_inventory')) {
                                    ?>

                                    <li><a> <?= $data->lang->line('menu_inventory') ?><span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: block;">
                                            <li class="sub_menu">
                                                <?php echo anchor('vendor/index', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_vendors') . '</span>'); ?>

                                            </li>
                                            <li>
                                                <?php
                                                echo anchor('inventory/index', '<i class="fa icon-expense"></i><span>' . $data->lang->line('register_inventory') . '</span>');
                                                ?>
                                            </li>

                                            <li>
                                                <?php
                                                echo anchor('inventory/purchase', '<i class="fa icon-expense"></i><span>' . $data->lang->line('purchase_inventory') . '</span>');
                                                ?>
                                            </li> 

                                            <li>
                                                <?php
                                                echo anchor('inventory/sale', '<i class="fa icon-expense"></i><span>' . $data->lang->line('sale_inventory') . '</span>');
                                                ?>
                                            </li>

                                            <li>
                                                <?php
                                                echo anchor('wallet/index', '<i class="fa icon-expense"></i><span>' . $data->lang->line('pocket_money') . '</span>');
                                                ?>
                                            </li>

                                        </ul>
                                    </li>

                                <?php } ?>              






                                <?php
                                if (can_access('manage_payroll')) {
                                    ?>
                                    <li class="">
                                        <a>
                                            <?= $data->lang->line('menu_payroll') ?> 
                                            <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: block;">

                                            <?php echo true ? '<li class="sub_menu">' . anchor('payroll/taxes', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('tax_status') . '</span>') . ' </li>' : ''; ?>



                                            <?php echo TRUE ? '<li>' . anchor('payroll/pension', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('pension_fund') . '</span>') . '</li>' : ''; ?>


                                            <?php echo true ? '<li>' . anchor('allowance/index', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_allowance') . '</span>') . '</li>' : ''; ?>


                                            <li class="sub_menu" nav>
                                                <?= anchor('deduction/index', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_deduction') . '</span>') ?>
                                            </li>
                                            <?php
                                          //  if (in_array(set_schema_name(), ['public.', 'beta_testing.', 'anazak.', 'elshaddai.'])) {
                                                ?>
                                                <li class="">
                                                    <a>
                                                        Loans
                                                        <span class="fa fa-chevron-down"></span></a>
                                                    <nav>
                                                        <li class="" >
                                                            <?= anchor('loan/type', '<i class="fa fa-clipboard"></i><span style="color: white; line-height: 25px;"> Loan Types</span>') ?>
                                                        </li>
                                                        <li class="">
                                                            <?= anchor('loan/index', '<i class="fa fa-clipboard"></i><span style="color: white; line-height: 25px;"> Borrowers </span>') ?>
                                                        </li>
                                                    </nav>
                                                </li>
                                            <?php //} ?>

                                            <?php echo true ? '<li>' . anchor('payroll/index', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_salary') . '</span>') . '</li>' : ''; ?>

                                        </ul>
                                    </li>
                                <?php } ?>
                                <li>
                                    <?php echo can_access('view_account_reports') ? anchor('report/account_report', '<i class="fa fa-clipboard"></i><span>Reports </span>') : ''; ?>
                                </li>
                                <li><a href="#level1_1">{{trans('bankaccount_lang.settings')}}<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li class="sub_menu">
                                            <?php echo can_access('view_bankaccount') ? anchor('bankaccount/index', '<i class="fa fa-credit-card"></i><span>' . trans('bankaccount_lang.banking') . '</span>') : ''; ?>
                                        </li>
                                        <li>
                                            <?php echo can_access('view_account_group') ? anchor('group/index', '<i class="fa fa-globe"></i><span>' . $data->lang->line('menu_group') . '</span>') : ''; ?>
                                        </li>
                                        <li>
                                            <?php echo can_access('view_chart_of_account') ? '<a href="' . url('expense/financial_category') . '" data-original-title="Chart of accounts" data-toggle="tooltip" data-placement="right"><i class="fa icon-feetype"></i><span>' . $data->lang->line('menu_chart_account') . '</span></a>' : ''; ?>
                                        </li>
                                        <li>
                                        <li>
                                            <?php echo can_access('edit_fee') ? anchor('fee_detail/opening_balance', '<i class="fa fa-book"></i><span>' . trans('bankaccount_lang.opening_balance') . '</span>') : ''; ?>
                                        </li>
                                        <li>
                                            <?= anchor('accounts/import_export', '<i class="fa fa-gear"></i><span>' . $data->lang->line('import_export') . '</span>'); ?>
                                        </li>
                                        <li>
                                            <?php echo can_access('view_invoice') ? anchor('invoices/invoiceGuide', '<i class="fa fa-globe"></i><span>' . $data->lang->line('menu_invoice_guide') . '</span>') : ''; ?>
                                        </li>

                                </li>

                                <!--                            <li><a href="#level1_2">Taxes</a>
                                                            </li>-->
                            </ul>
                            </li>
                            </ul>
                            </li>


                        <?php } ?>
                        <?php if (in_array(session('table'), ['user', 'teacher']) && !can_access('manage_payroll')) { ?>
                            <li>
                                <?php
                                // if (can_access('view_salary') && !can_access('manage_payroll')) {
                                echo anchor('payroll/salary', '<i class="fa icon-paymentsettings"></i><span>' . $data->lang->line('salaries') . '</span>');
                                //   }
                                ?>
                            </li>
                            <?php if (in_array(set_schema_name(), ['public.', 'beta_testing.', 'elshaddai.'])) { ?>
                                <li class="">
                                    <?= anchor('loan/index', '<i class="fa fa-clipboard"></i><span style="color: white; line-height: 25px;"> Apply Loan </span>') ?>
                                </li>

                                <?php
                            }
                        }
                        if (can_access('view_promotion')) {
                            echo '<li>';
                            echo anchor('promotion/index', '<i class="fa icon-promotion"></i><span>' . $data->lang->line('menu_promotion') . '</span>');
                            echo '</li>';
                        }
                        ?>

                        <?php //if ($usertype == "Student" || can_access('view_media')) {    ?>

                        <li id="resources">
                            <a>
                                <i class="fa fa-film"></i>
                                <span><?= $data->lang->line('menu_media'); ?></span>
                                <span class=" fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li>
                                    <?php echo anchor('media/index', '<i class="fa fa-file-o"></i><span>' . $data->lang->line('menu_files') . '</span>'); ?>
                                </li>

                                <li>
                                    <?php echo anchor('media/livestream', '<i class="fa fa-video-camera"></i><span>' . $data->lang->line('menu_live_studies') . '</span>'); ?>
                                </li>
                                <!--<li>
                                <?php echo anchor('media/books', '<i class="fa fa-file-o"></i><span>' . $data->lang->line('menu_books') . '</span>'); ?>
                                </li>-->

                                <li>
                                    <?php echo anchor('media/manageFiles', '<i class="fa fa-file-o"></i><span>' . $data->lang->line('menu_notes') . '</span>'); ?>
                                </li>
                                <!--                                     <li>
                                <?php echo anchor('media/exams', '<i class="fa fa-file-o"></i><span>' . $data->lang->line('menu_past_papers') . '</span>'); ?>
                                                                    </li>-->
                                <?php if (session('table') == 'student') { ?>
                                    <li>
                                        <?php echo anchor('schoolexam/studentIndex', '<i class="fa fa-book"></i><span>' . $data->lang->line('online_exam') . '</span>'); ?>
                                    </li>
                                    <?php
                                }
                                if (session('table') == 'parent') {
                                    ?>
                                    <li>
                                        <?php echo anchor('schoolexam/indexParent', '<i class="fa fa-book"></i><span>' . $data->lang->line('online_exam') . '</span>'); ?>

                                    </li>
                                <?php } ?>
                                <li>

                                </li>
                                <?php
                                if (in_array(session('table'), ['teacher', 'user', 'setting'])) {
                                    ?>
                                    <li>
                                        <?php echo anchor('schoolexam/minor', '<i class="fa fa-book"></i><span>' . $data->lang->line('online_exam') . '</span>'); ?>
                                    </li>
                                <?php } ?>
                                <li>
                                    <?php echo anchor('media/assignment', '<i class="fa icon-subject"></i><span>' . $data->lang->line('online_studies') . '  </span>'); ?>
                                </li>
                                <li>

                                    <?php echo anchor('media/forum', '<i class="fa fa-comments"></i><span>Online Discussion </span>'); ?>
                                </li>
                                <!-- <li>
                                
                                <?php // echo anchor('media/settings', '<i class="fa fa-comments"></i><span>Settings </span>');     ?>
                                                                 </li>-->
                            </ul>
                        </li>
                        <?php //}
                        ?>

                        <?php if (can_access('view_mailsms')) { ?>
                            <li id="email_sms">
                                <a>
                                    <i class="fa icon-mailandsmstop"></i>
                                    <span><?= $data->lang->line('menu_mailandsms'); ?></span>
                                    <span class=" fa fa-chevron-down"></span>
                                </a>
                                <ul class="nav child_menu">
                                    <li>
                                        <?php echo anchor('/SmsController/show', '<i class="fa fa-envelope"></i><span>' . $data->lang->line('menu_inbox') . '</span>'); ?>
                                    </li>
                                    <li>
                                        <?php echo anchor('mailandsms/add', '<i class="fa fa-envelope"></i><span>' . trans('mailandsms_lang.mailandsms_compose') . '</span>'); ?>
                                    </li>
                                    <li>
                                        <?php echo anchor('mailandsmstemplate/index', '<i class="fa icon-template"></i><span>' . $data->lang->line('menu_mailandsmstemplate') . '</span>'); ?>
                                    </li>
                                    <li>

                                        <?php echo anchor('mailandsms/reminder', '<i class="fa fa-wrench"></i><span>' . $data->lang->line('print_reminder') . '</span>'); ?>



                                    </li>
                                    <li>
                                        <?php echo anchor('smssettings/index', '<i class="fa icon-mailandsms"></i><span>' . trans('mailandsms_lang.sent_and_summary') . '</span>'); ?>
                                    </li>



                                    <li>
                                        <?php echo anchor('smssettings/feedback/2', '<i class="fa fa-user"></i><span>' . $data->lang->line('menu_parents_feedbacks') . '</span>'); ?>
                                    </li>

                                </ul>
                            </li>
                        <?php } ?>


                        <li>
                            <?php
                            $message = new \App\Http\Controllers\Message();
                            $not = $message->getUnreadMessage();
                            $badge_status = $not > 0 ? '<b class="badge bg-red pull-right">' . $not . '</b>' : "";
                            echo anchor('message/index', '<i class="fa fa-envelope"></i><span>' . $data->lang->line('messages') . '</span>' . $badge_status);
                            ?>
                        </li>




                        <?php
                        if (can_access('view_notice')) {
                            echo '<li id="notices">';
                            echo anchor('notice/index', '<i class="fa fa-calendar"></i><span>' . $data->lang->line('menu_notice') . '</span>');
                            echo '</li>';
                        }
                        if (can_access('view_diary')) {
                            echo '<li>';
                            echo anchor('ediary/index', '<i class="fa fa-book"></i><span>' . $data->lang->line('ediary') . '</span>');
                            echo '</li>';
                        }
                        if (can_access('view_system_report')) {
                            ?>

                            <li id='other_reports'>
                                <a>
                                    <i class="fa fa-file"></i>
                                    <span><?= $data->lang->line('menu_other_report'); ?></span>
                                    <span class=" fa fa-chevron-down"></span>
                                </a>
                                <ul class="nav child_menu">
                                    <!--<li>
                                    <?= anchor('report/index', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_report') . '</span>'); ?>
                                    </li>-->

                                    <li>
                                        <?= anchor('report/inactive', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_inactive_user') . '</span>'); ?>
                                    </li>
                                    <li>
                                        <?= anchor('report/students', '<i class="fa fa-users"></i><span>' . $data->lang->line('student_reports') . '</span>'); ?>
                                    </li>
                                    <li>
                                        <?= anchor('student/graduate_student', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_graduate') . '</span>'); ?>
                                    </li>
                                    <li>
                                        <?= anchor('report/default_password', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_default_passwords') . '</span>'); ?>
                                    </li>
                                    <li>
                                        <?= anchor('insight/index', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_insight') . '</span>'); ?>
                                    </li>
                                    <li>
                                        <?= anchor('report/staffs', '<i class="fa fa-list"></i><span>' . $data->lang->line('staff_report') . '</span>'); ?>
                                    </li>
                                    <?php
                                    $set_schema = ['beta_testing.', 'shulesoft.'];
                                    if (in_array(set_schema_name(), $set_schema)) {
                                        ?>
                                        <li>
                                            <?= anchor('report/systemReport', '<i class="fa fa-list"></i><span> System Use Statistics</span>'); ?>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </li>
                            <?php
                        }
                        if (can_access('view_system_setting')) {
                            ?>
                            <li id="settings">
                                <a>
                                    <i class="fa fa-gear"></i>
                                    <span><?= $data->lang->line('menu_setting'); ?></span>
                                    <span class=" fa fa-chevron-down"></span>
                                </a>
                                <ul class="nav child_menu">
                                    <li>
                                        <?= anchor('setting/index#home', '<i class="fa fa-gear"></i><span>' . $data->lang->line('menu_setting_general') . '</span>'); ?>
                                    </li>

                                    <li>
                                        <?= anchor('setting/permission', '<i class="fa fa-users"></i><span>' . $data->lang->line('user_permission') . '</span>'); ?>
                                    </li>
                                    <!--                                    <li>
                                    <?php // anchor('setting/definition', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_setting_definition') . '</span>');       ?>
                                    
                                                                        </li>-->
                                    <!--<li>
                                    <?= anchor('setting/guides', '<i class="fa fa-clipboard"></i><span>' . $data->lang->line('menu_setting_guide') . '</span>'); ?>

                                    </li>-->

                                </ul>
                            </li>
                            <?php
                        }
                        if (can_access('add_signature')) {
                            echo '<li id="signatures">';
                            echo anchor('setting/signature', '<i class="fa fa-pencil-square-o"></i><span>' . $data->lang->line('menu_signature') . '</span>');
                            echo '</li>';
                        }
                        ?>


                        <li id="faq">
                            <a target="_blank" href="<?= $siteinfos->country_id == 198 ? url('https://support.schoolbanks.com') : base_url('help/index') ?>">

                                <i  class="fa fa-info-circle"></i><span><?= $data->lang->line('help') ?> </span>
                            </a>
                        </li>


                        <!--                        <li>
                                                    <a href="<?= base_url('https://t.me/shuleyetubot') ?>"  target="_blank">
                                                        <i  class="fa fa-tags"></i> <span id="blinkingg">Telegram</span>
                                                    </a>
                                                </li>-->
                        <?php
                        if (in_array(set_schema_name(), ['public.', 'beta_testing.']) && !in_array(session('table'), ['student', 'parent'])) {
                            ?>
                           <li>
                                <a href="<?= base_url('WebManagement/index') ?>" >
                                    <i class="fa fa-globe"></i> <span id="blinkingg">Website Management</span>
                                </a>
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <div class="row" style="padding-bottom: 20px;">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class=" bs-example-popovers" style="z-index: 10000; position: fixed; margin-top: 0; margin-left:5px;">


                </div>
            </div>
            <div class="top_nav navbar-fixed-top">

                <!-- Modal content start here -->
                <!--            <div class="modal fade" id="modal-info">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div id="modal-infoPrint">
                                            <div class="modal-header">
                                                Upgrade in Progress
                                            </div>
                                            <div class="modal-body" >
                                                <table>
                                                    <tr>
                                                    <div class="row">

                                                        <div class="col-lg-12">
                                                            <h3><strong><?php
                if (strlen($user_info->name) > 15) {
                    echo 'Hello ' . substr($user_info->name, 0, 15) . "..";
                } else {
                    echo 'Hello ' . $user_info->name;
                }
                ?></strong></h3>

                                                            <h5>We would like to let you know that from 6PM 19th Sept 2018 we are upgrading ShuleSoft especially the Accounts Module to enable you to use the system more effectively and flawlessly.
                ShuleSoft will still be available as usual while the upgrade is under way.
                Please bear with us in case of any inconveniences.
                Don't hesitate to contact us for more information. Thank you!
                </h5>
                                                        </div>
                                                        <br/>
                                                        <div class="col-lg-12">
                                                            <h3><br/><br/><strong><?php
                if (strlen($user_info->name) > 15) {
                    echo 'Habari ' . substr($user_info->name, 0, 15) . "..";
                } else {
                    echo 'Habari ' . $user_info->name;
                }
                ?></strong></h3>

                                                            <h5>
                                                                Tunapenda kukutaarifu kuwa kuanzia  19/09/2018 saa 12 jioni tutakuwa tunafanya maboresho  katika mfumo wa
                                                                ShuleSoft hususani  kipengele cha Accounts ili kukupa urahisi zaidi wa kutumia mfumo. Huduma ya ShuleSoft itakua ikipatikana kama kawaida huku maboresho yakiendelea.
                                                                Tunaomba radhi kwa usumbufu wowote utakaojitokeza. Kwa taarifa zaidi usisite kuwasiliana nasi. Asante
                                                            </h5>
                                                        </div>
                                                    </div>

                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" style="margin-bottom:0px;" onclick="javascript:closeWindow()" data-dismiss="modal"><?= $data->lang->line('close') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                <!-- Modal content End here -->
                <div class="nav_menu" name="topnav" id="topnav">

                    <nav class="" role="navigation">
                        <ul class="nav navbar-nav navbar-right">
                            <div class="nav toggle">
                                <a class="navbar-btn sidebar-toggle" style="color:#00acac;" id="menu_toggle" data-toggle="offcanvas" role="button">
                                    <i class="fa fa-bars"></i>
                                </a>
                            </div>

                            <li style="right:5%; margin-left:3%">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                                   aria-expanded="true">

                                    <?php
                                    $imagearray = array(
                                        "src" => base_url('storage/uploads/images/' . $user_info->photo),
                                        'width' => '0',
                                        'height' => '0',
                                        'class' => 'img-circle responsive-img',
                                        'style' => ''
                                    );
                                    echo img($imagearray);
                                    ?>
                                    <span class="hidden-sm"><?php
                                        if (strlen($user_info->name) > 15) {
                                            echo substr($user_info->name, 0, 15) . "..";
                                        } else {
                                            echo $user_info->name;
                                        }
                                        ?></span>
                                    <span class=" fa fa-chevron-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="<?= base_url("profile/index") ?>">
                                            <i class="fa fa-briefcase"></i>
                                            <?= $data->lang->line("view_profile") ?>
                                        </a>
                                    </li>
                                    <?php if ($usertype == "Parent") { ?>
                                        <li><a href="<?= base_url("parents/edit/" . session('id')) ?>">
                                                <i class="fa fa-edit"></i>
                                                <?= $data->lang->line("edit_profile") ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <li>

                                        <a href="<?= base_url("signin/cpassword") ?>">
                                            <i class="fa fa-lock"></i>

                                            <span><?= $data->lang->line("change_password") ?></span>
                                        </a>

                                    </li>
                                    <?php
                                    if (session('table') != 'parent') {
                                        ?>

                                        <li><a href="<?= base_url('help/index') ?>" >

                                                <i  class="fa fa-info-circle"></i><span><?= $data->lang->line('help') ?> </span>
                                            </a> </li>

                                                <!-- <li>
        
                                                    <a href="<?= base_url("help/training") ?>" ><i  class="fa fa-book"></i> Training</a>
                                                </li>-->

                                    <?php } ?>
                                    <?php
                                    if ($siteinfos->country_id == 1 && (session('table') == 'setting' || can_access('view_invoice')) && !in_array(session('table'), ['student', 'parent'])) {
                                        ?>
                                        <li>

                                            <a href="<?= base_url("help/upgrade") ?>" ><i  class="fa fa-money"></i> My Accounts </a>
                                        </li>
                                    <?php } ?>
                                    <li>
                                        <a href="<?= base_url("signin/signout") ?>" id="log_out" onclick="signOut();">
                                            <div><i class="fa fa-sign-out pull-right"></i></div>
                                            <?= $data->lang->line("logout") ?>
                                        </a>
                                        <?php
                                        $linked_schema = [
                                            'public.', 'canossa.', 'makongo.', 'kamo.', 'atlasschools.', 'geniuskings.', 'barbrojohannson.', 'stpeterclaver.', 'stjosephs.', 'rightwayschools.', 'stfrancisgirls.', 'shepherdsschools.', 'ntonzo.', 'akilistars.', 'hindumandalschools.', 'filbertbayi.', 'enaboishu.'
                                        ];
                                        if (in_array(set_schema_name(), $linked_schema)) {
                                            ?>
                                            <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
                                            <meta name="google-signin-client_id" content="614990663215-uodgs6e9b7n4t1hrst6k0aja23btd72q.apps.googleusercontent.com">
                                            <script>
                                                function signOut() {
                                                    var auth2 = gapi.auth2.getAuthInstance();
                                                    auth2.signOut().then(function () {
                                                        console.log('User signed out.');
                                                    });
                                                }

                                                function onLoad() {
                                                    gapi.load('auth2', function () {
                                                        gapi.auth2.init();
                                                    });
                                                }
                                            </script>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </li>
                            <?php if (!in_array(session('table'), ['parent', 'student','teacher'])) { ?>
                                <li role="presentation" class="dropdown" style=" margin-right: 2%;">
                                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="true">
                                        <i class="fa fa-envelope-o"></i>
                                        <br/><span class="badge bg-red" id="count_badge">0</span>
                                    </a>
                                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">

                                        <span id="reply_content"></span>

                                        <li>
                                            <div class="text-center">
                                                <a href="<?= url('SmsController/show/2') ?>">
                                                    <strong>See All SMS replies</strong>
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>
                            <li role="presentation" class="dropdown notifications-menu">
                                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                                   aria-expanded="false">
                                <!--<span> <?= $data->lang->line("language") ?> </span>-->
                                    &nbsp;<img class="language-img" src="<?php
                                    $image = session('lang') == '' ? 'english' : session('lang');
                                    echo base_url('storage/uploads/language_image/' . $image . '.png');
                                    ?>"
                                               />
                                    <i class="fa fa-chevron-down"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;"><ul class="menu" style="overflow: hidden;width: 100%;height: auto;">
                                                <li class="language" id="english">
                                                    <a href="#level1_1" onclick="changeLanguage('en')">
                                                        <div class="pull-left">
                                                            <img src="<?php echo base_url('storage/uploads/language_image/english.png'); ?>">
                                                        </div>
                                                        <h4>
                                                            English
                                                            <?php if ($image == 'english') echo " <i class='fa fa-check'></i>"; ?>                                            </h4>
                                                    </a>
                                                </li>
                                                <li class="language" id="french">
                                                    <a href="#level1_1" onclick="changeLanguage('sw')">
                                                        <div class="pull-left">
                                                            <img src="<?php echo base_url('storage/uploads/language_image/kiswahili.png'); ?>">
                                                        </div>
                                                        <h4>
                                                            Kiswahili
                                                            <?php if ($image == 'kiswahili') echo " <i class='fa fa-check'></i>"; ?>
                                                        </h4>
                                                    </a>
                                                </li>


                                            </ul><div class="slimScrollBar" style="width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 0px; z-index: 99; right: 1px; height: auto; background: rgb(0, 0, 0);"></div>
                                            <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div></div>
                                    </li>
                                    <li class="footer"></li>
                                </ul>
                            </li>

                            <!-- Messages: style can be found in dropdown.less-->
                            <?php if ($usertype == "Parent") { ?>

                                <li role="presentation" class="dropdown" style=" margin-right: 2%;">
                                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="true">
                                        <i class="fa fa-envelope-o"></i>
                                        <span class="badge bg-red" id="parent_count_badge"></span>
                                    </a>
                                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">

                                        <span id="parent_sms_content"></span>

                                        <li>
                                            <div class="text-center">
                                                <a href="<?= url('profile/index#section-iconbox-2') ?>">
                                                    <strong>See All SMS</strong>
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <script type="text/javascript">
                                    get_parent_sms = function () {
                                        $.getJSON('<?= url('SmsController/parents/null') ?>', null, function (data) {
                                            if (data.count > 0) {
                                                $('#parent_count_badge').html(data.count);
                                                $('#parent_sms_content').html(data.message);
                                            }
                                        });
                                    }
                                    $(document).ready(get_parent_sms);
                                </script>

                                <?php
                            }
                            if (in_array($usertype, array('Student', 'Teacher', "Parent"))) {
                                ?>
                                <li class="dropdown messages-menu hidden-xs">
                                    <span class=" form-control-feedback"></span>
                                    <a href="#level1_1" id="compose">
                                        System feedback</a>
                                </li>
                            <?php } ?>
<!--                            <span class="alert alert-warning" style="margin-left: 20%">Upgrade in Progress :For more Information,  <a class="btn-cs btn-xs btn-success" data-toggle="modal" data-target="#modal-info"><span class="fa fa-floppy-o"></span>click here</a></span>-->
                            @include('components.alerts')
                            <?php if (can_access('view_users') || can_access('view_student') || can_access('view_teachers') || can_access('view_parents')) { ?>

                                @include('components.search')

                            <?php } ?>

                            </div>


                    </nav>
                </div>
            </div>
            <script type="text/javascript">
                menu_setting = function () {

                    //we will add conditions in some methods

                    //  $("HTML, BODY").animate({scrollTop: 0}, 300);
                };


                $(document).ready(menu_setting);
                notifications = function () {
                    $.getJSON('<?= url('SmsController/opened/null') ?>', null, function (data) {
                        if (data.count > 0) {
                            $('#count_badge').html(data.count);
                            $('#reply_content').html(data.message);
                        }
                    });
                }
                function changeLanguage(a) {
                    $.get('<?= url('Controller/changeLanguage/') ?>/' + a, null, function (data) {
                        window.location.reload();
                    });
                }
                $(document).ready(notifications);
                ////////
                //Search

                $(document).on("keyup", "#searchBar input", function (e) {
                    console.log(e.keyCode);
                    if (e.keyCode == 27 || e.keyCode == 13) {
                        closeSearchBar();
                        return;
                    } else {
                        var val = $(this).val();
                        if (val.length) {
                            search(val);
                            $("#searchBar").addClass('search-started');
                        } else {
                            $("#searchBar").removeClass('search-started');
                        }
                    }
                });

                function openSearchBar() {
                    $("body").addClass("open-search");
                    $("#searchBar input").focus();
                }

                function closeSearchBar() {
                    if ($("#searchBar").hasClass('search-started')) {
                        $("#searchBar").removeClass('search-started');
                        setTimeout(function () {
                            $("body").removeClass("open-search");
                        }, 160);
                    } else {
                        $("body").removeClass("open-search");
                        $("#searchBar").removeClass('search-started');
                    }
                }

                function emptySearchBar() {
                    $("#searchBar input").val("").focus();
                    $("#searchBar").removeClass('search-started');
                }

                function search(q) {
                    var form = $("#searchBar form");
                    var url = form.attr('action') + '/' + q;
                    $("#searchResults").addClass("searching");
                    $.get(url, function (data) {
                        $("#searchResults").removeClass("searching");
                        $("#searchResults #results").html(data);
                    });
                }
                var type = window.location.hash.substr(1);
                $('#' + type).css({'border': '1px solid yellow'});




            </script>

