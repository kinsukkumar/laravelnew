
<style>

#ajax_search_results {
    display: none;
    overflow: auto;
    margin-top: 18%;
    position: absolute;
    width: 329px;
    max-height: 200px;
    border-radius: .2rem;
    background: #FFF;
    z-index: 102;

}
.search_row:hover{
    -webkit-transition: all 0.20s ease-in-out;
    -moz-transition: all 0.20s ease-in-out;
    -ms-transition: all 0.20s ease-in-out;
    -o-transition: all 0.20s ease-in-out;
    background-color: #d4d4d4;
}
.mask-body {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    cursor: pointer;
    z-index: 1029;
}
.mask-body-dark {
    background: rgba(31, 45, 61, 0.3);
}
@media (max-width: 415px){
    #general_search {
        width: 226px !important;
        left: 0 !important;
        font-size: 12px;
    }

}

input[type=text] {
    -webkit-transition: all 0.20s ease-in-out;
    -moz-transition: all 0.20s ease-in-out;
    -ms-transition: all 0.20s ease-in-out;
    -o-transition: all 0.20s ease-in-out;
    outline: none;
    padding: 3px 0px 3px 3px;
    margin: 5px 1px 3px 0px;
    border: 1px solid #DDDDDD;
}

.top_search input[type=text]:focus{
    box-shadow: 0 0 5px rgb(15, 191, 150) !important;
    padding: 3px 0px 3px 3px !important;
    margin: 5px 1px 3px 0px !important;
    border: 1px solid rgb(15, 191, 150) !important;
}
</style>
<div style="margin-top: .7%;" class="col-md-offset-3 col-md-3 col-sm-offset-3 col-sm-3 col-xs-offset-2 col-xs-6 pull-left top_search" data-step="2" data-intro="Use this search bar to search for students, parents, teachers and other staff names, phone numbers, emails and invoices, reference number">
<div class="input-group" >
    <input type="text" class="form-control text-center" id="general_search" placeholder="<?= $data->lang->line('search_menu')?>" style="border-radius: 0.2rem; height: 42px; width: 420px;">
    
    <div id="ajax_search_results">


    </div>
</div>

</div>

<script type="text/javascript">
ajax_search_results = function () {
    $('#general_search').keyup(function () {
        var q = $(this).val();
        $.ajax({
            type: 'POST',
            url: "<?= base_url('background/search') ?>",
            data: "q=" + q,
            dataType: "html",
            success: function (data) {
                $('#ajax_search_results').html(data).show();
                $('.mask-body').show();
            }
        });
    });
}
$(document).ready(ajax_search_results);
$("body").on("click", "[data-action]", function(e) {

    e.preventDefault();

    var $this = $(this);
    var action = $this.data('action');
    var target = $this.data('target');

    switch (action) {
        case 'ajax_search_results-close':
            target = $this.data('target');
            $(target).hide();
            $('body').find('.mask-body').remove();
            break;

        case 'search-open':
            target = $this.data('target');
            $this.addClass('active');
            $(target).addClass('show');
            $(target).find('.form-control').focus();
            break;

        case 'search-close':
            target = $this.data('target');
            $('[data-action="search-open"]').removeClass('active');
            $(target).removeClass('show');
            break;

    }
})

</script>