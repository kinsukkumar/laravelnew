
<link href="<?php echo base_url('public/assets/shulesoft/style.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/datepicker/datepicker.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/timepicker/timepicker.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/shulesoft/hidetable.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/shulesoft/shulesoft.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/shulesoft/responsive.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/fullcalendar/fullcalendar.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/editor/jquery-te-1.4.0.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/toastr/toastr.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/shulesoft/rid.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/jquery-ui/jquery-ui.css'); ?>" rel="stylesheet">
<!--        <link href="--><?php //echo base_url('public/assets/shulesoft/jquery.mCustomScrollbar.min.css')        ?><!--" rel="stylesheet">-->
<link href="<?php echo base_url('public/assets/css/sweet-alerts/sweetalert.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/bootstrap/3.3.2/bootstrap.min.css'); ?>" rel="stylesheet">

<link rel="stylesheet" href="<?= url('public/') ?>/intlTelInput/css/intlTelInput.css">

<link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('public/assets/datepicker/themes/default.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('public/assets/datepicker/themes/default.date.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2-bootstrap.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/gh-pages.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('public/assets/jqueryUI/jqueryui.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('public/assets/custom.css'); ?>">

@yield('ajax')
