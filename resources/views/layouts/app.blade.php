<div id="loader-content">
    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>
</div>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= ucwords($siteinfos->sname) ?></title>
        <link rel="SHORTCUT ICON" rel="icon" href="<?= base_url("storage/uploads/images/favicon.png") ?>">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="theme-color" content="#00acac">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script type="text/javascript" src="<?php echo base_url('public/assets/shulesoft/jquery.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/jquery-ui/jquery-ui.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/timepicker/timepicker.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/toastr/toastr.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/assets/js/sweet-alert/sweetalert.min.js'); ?>"></script>

        <!-- Bootstrap js -->
<!--        <script type="text/javascript" src="<?php echo base_url('public/assets/bootstrap/bootstrap.min.js'); ?>"></script> -->
        <!--<link rel="stylesheet" href="--><?php //echo base_url('public/assets/materialize.min.css');            ?><!--">-->
        <script type="text/javascript" src="<?php echo base_url('public/assets/bootstrap/bootstrap.min.js'); ?>" async></script>

        <!--        <link href="--><?php //echo base_url('public/assets/bootstrap/bootstrap.min.css');           ?><!--" rel="stylesheet">-->
        <link href="<?php echo base_url('public/assets/bootstrap/jumbotron.less'); ?>">
        <link href="<?php echo base_url('public/assets/fonts/font-awesome.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/fonts/icomoon.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/datepicker/datepicker.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/timepicker/timepicker.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/hidetable.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/shulesoft.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/responsive.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/fullcalendar/fullcalendar.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/editor/jquery-te-1.4.0.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/toastr/toastr.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/shulesoft/rid.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/jquery-ui/jquery-ui.css'); ?>" rel="stylesheet">
        <!--        <link href="--><?php //echo base_url('public/assets/shulesoft/jquery.mCustomScrollbar.min.css')           ?><!--" rel="stylesheet">-->
        <link href="<?php echo base_url('public/assets/css/sweet-alerts/sweetalert.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/bootstrap/3.3.2/bootstrap.min.css'); ?>" rel="stylesheet">

        <link rel="stylesheet" href="<?= url('public/') ?>/intlTelInput/css/intlTelInput.css">

        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/datepicker/themes/default.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/datepicker/themes/default.date.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2-bootstrap.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/gh-pages.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/jqueryUI/jqueryui.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('public/assets/custom.css'); ?>">
        <link rel="stylesheet" href="<?= base_url("public/assets/css/documenter_style.css") ?>">
        <link href="<?php echo base_url('public/assets/css/pnotify/pnotify.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/pnotify/pnotify.buttons.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/pnotify/pnotify.nonblock.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/loader.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('public/assets/css/switchery.min.css') ?>" rel="stylesheet">

        <script type="text/javascript">
ajax_setup = function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function (xhr) {
            jQuery('#loader-content').show();
        },
        error: function (jqXHR, textStatus, errorThrown) {
//            if (jqXHR.status == 404) {
//                alert("Element not found.");
//            } else {
//                alert("Error: " + textStatus + ": " + errorThrown);
//            }
//if(textStatus==="timeout") {
//            alert("got timeout");
//        } else {
//            alert(textstatus);
//        }
            jQuery('#loader-content').hide();
        },
        complete: function (xhr, status) {
            jQuery('#loader-content').fadeOut();
            if (document.body.scrollTop > 60 || document.documentElement.scrollTop > 60) {
                $("HTML, BODY").animate({scrollTop: 0}, 300);
            }
        }
    });
}
$(document).ready(ajax_setup);
        </script>
        <script type="text/javascript">

            var root_url = "<?= url('/') ?>"
        </script>
        <style>
            .btn-bs-file{
                position:relative;
            }
            .btn-bs-file input[type="file"]{
                position: absolute;
                top: -9999999;
                filter: alpha(opacity=0);
                opacity: 0;
                width:0;
                height:0;
                outline: none;
                cursor: inherit;
            }
        </style>

        @yield('css')
    </head>
    <body class="<?= in_array(device(), ['Android', "iPhone"]) ? 'nav-sm' : 'nav-md' ?>">

        <style>

            #valid-msg {
                color: #00C900;
            }
            #error-msg {
                color: red;
            }
            @media print {
                #setup_notifications {
                    display: none !important;
                }

            }
        </style>
        <?php
        $data = $data_language;
        ?>

        @include('components.menu')
        @include('layouts.notifications')

        <div class="right_col" role="main">
            <div class="content" style="min-width: 28em;">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        <!-- ClickDesk Live Chat Service for websites -->
        <script type='text/javascript'>
        </script>

        @include('components.footer')
         <!--<p align="center">End of ClickDesk  This page took <?php echo (microtime(true) - LARAVEL_START) ?> seconds to render</p> -->
        <script type="text/javascript">
            jQuery(window).load(function () {
                jQuery('#loader-content').fadeOut('slow');
            });
<?php
if (FALSE) {
    ?>
                swal({
                    title: "Wrong Email Address!",
                    text: "Please write your valid Email address to proceed:",
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    inputPlaceholder: "Write something"
                }, function (inputValue) {
                    if (inputValue === false)
                        return false;
                    if (inputValue === "") {
                        swal.showInputError("You need to write something!");
                        return false
                    }
                    $.getJSON('<?= base_url('profile/update/null') ?>', {email: inputValue}, function (data) {
                        if (data.status == '0') {
                            swal.showInputError("This Email is not Valid!");
                            return false
                        } else {
                            swal("Success!", "You email has been updated to : " + inputValue, "success");
                        }
                    });

                });

    <?php
}

$flash = session('flash');
if ($flash != null) {
    ?>
                load_toast = function () {
                    toastr["<?= $flash[0] ?>"]("<?= $flash[1] ?>");
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                }
                $(document).ready(load_toast);
    <?php
    session(['flash' => null]);
}
?>


        </script>
