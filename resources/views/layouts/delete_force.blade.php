<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-pencil"></i> Delete By Force</h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $data->lang->line('menu_dashboard') ?></a></li>
            <li><a href="<?= base_url($return_url) ?>">Back</a></li>
            <li class="active"></li>
        </ol>
    </div><!-- /.box-header -->
    <div>
        <p class="alert alert-warning"><?=$message ?></p>
        <p align="right">
            <a class="btn btn-info"  href="<?= base_url($return_url)?>">No,return back</a>
            <a class="btn btn-success" href="<?= base_url($del_by_force_url)?>">Delete this anyway</a>
        </p>
    </div>
</div>