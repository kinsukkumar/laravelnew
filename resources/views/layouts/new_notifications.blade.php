

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <!--<span class="alert-inner--icon"><i class="fas fa-check"></i></span>-->
    <span class="alert-inner--text"><strong>Success! </strong>{!! $message !!}</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if ($message = session('error'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <!--<span class="alert-inner--icon"><i class="fas fa-times"></i></span>-->
    <span class="alert-inner--text"><strong>Error! </strong>{!! $message !!}</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <!--<span class="alert-inner--icon"><i class="fas fa-times"></i></span>-->
        <span class="alert-inner--text"><strong>Warning! </strong>{!! $message !!}</span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if ($message = Session::get('info'))
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <!--<span class="alert-inner--icon"><i class="fas fa-times"></i></span>-->
        <span class="alert-inner--text"><strong>Info! </strong>{!! $message !!}</span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <!--<span class="alert-inner--icon"><i class="fas fa-times"></i></span>-->
        <span class="alert-inner--text"><strong>Error! </strong>Please check the form below for errors</span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
