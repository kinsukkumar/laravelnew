<style>
    .margin5{
        max-width: 77%;
    }
</style>
@if ($errors->any())
    <div class="alert alert-top alert-danger alert-dismissable margin5">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error:</strong> Please check the form below for errors
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-top alert-success alert-dismissable margin5">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Success:</strong> {{ $message }}
    </div>
@endif

@if ($message = Session::get('error'))
    <div class="alert alert-top alert-danger alert-dismissable margin5">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error:</strong> {!! $message !!}
        <p><a href="https://api.whatsapp.com/send?phone=+255655406004&text={!! $message !!}" target="_blank">Or Click here to Report this <i class="fa fa-whatsapp"></i></a></p>
    </div>
@endif

@if ($message = Session::get('warning'))
    <div class="alert alert-top alert-warning alert-dismissable margin5">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Warning:</strong> {{ $message }}
    </div>
@endif

@if ($message = Session::get('info'))
    <div class="alert alert-top alert-info alert-dismissable margin5">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Info:</strong> {{ $message }}
    </div>
@endif
<script>//$('.margin5').fadeOut(28000)</script>