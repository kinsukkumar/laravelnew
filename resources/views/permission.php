

<div class="error-page">
    <h2 class="headline text-info"> 403</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Sorry,</h3>
        <p>
             You don't have permission to do this action.
            Contact your administrator to grant you permission.  <a href="<?=base_url('dashboard/index')?>">Click to return to dashboard</a>.
        </p>
    </div><!-- /.error-content -->
</div><!-- /.error-page -->