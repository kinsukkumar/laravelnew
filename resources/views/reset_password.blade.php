@extends('new_login_layout')
@section('content')
    <div class="col-md-10 col-lg-5 pt-4">

    <div class="card shadow zindex-100">
        <div class="card-body px-md-5 py-5">
            <div class="text-left mb-5">
                <h6 class="h3">Password reset</h6>
                <p class="text-muted mb-0">Enter your phone number and your answer below to proceed</p>
            </div>
            <span class="clearfix"></span>



            <form method="post">
                <div class="form-group">
                    <label class="form-control-label">Phone Number</label>
                    <div class="input-group input-group-transparent">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" id="input-phone" placeholder="0655406004">
                    </div>

                </div>
                <div class="form-group">
                    <div class="row">

                <div class="input-group col-md-4 input-group-transparent">
                    <input type="text" disabled class="form-control pull-left" id="firstNumber" name="firstNumber" value="<?php echo isset($firstnumber) ? $firstnumber : ''; ?>">

                </div>
                        <div class="input-group col-md-1 input-group-transparent">
                            <p class="text-center"><h3>+</h3></p>
                        </div>

                    <div class="input-group col-md-4 input-group-transparent">
                        <input type="text" disabled class="form-control pull-right " id="secondNumber" name="secondNumber" value="<?php echo isset($firstnumber) ? $firstnumber : ''; ?>">
                    </div>
                        <div class="input-group col-md-1 input-group-transparent">
                            <p class="text-center"><h3>=</h3></p>
                        </div>
                        <div class="input-group col-md-6 input-group-transparent">
                            <input type="text" name="answer" class="form-control pull-right" placeholder="<?= $data_language->line('reset_answer') ?>">

                        </div>

                </div>
                </div>
{{csrf_field()}}
                <div class="text-center mb-3">
                    <button type="submit" class="btn btn-block btn-primary forgotPasswordButton">Reset password</button>
                </div>
                <div class="text-center mb-3">
                    <a  href="{{url('signin/new_login')}}" class="btn btn-block btn-warning">Back to login</a>
                </div>
            </form>

        </div>
    </div>



</div>
    @endsection