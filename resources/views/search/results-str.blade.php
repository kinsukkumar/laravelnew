<?php
	$result_count_str = "";
	$result_count_str .= $result_count > 0 ? $result_count : "No";
	$result_count_str .= $result_count > 1 ? " results" : " result";
	$result_count_str .= " found";
?>
<style>
	a{
		color: inherit;
		font-size: inherit;
	}
	.a-divider{
		margin-top: 15px;
	}
</style>
<h5 class="a-divider">{{$result_count_str}}</h5>

@if($users->count() > 0)
	<h5 class="a-divider">People</h5>
	@foreach($users as $user)
		@if($loop->iteration <= 3)
			<?php
				$item = new stdClass();
				//$item->img = '' . $user->photo;
				$item->link = url("/user/$user->id");
				$item->title = $user->name;
				$item->sub_title = $user->usertype;
			?>
			@include('search.search_item')
		@else
			<?php break; ?>
		@endif
	@endforeach	

	@if($users->count() > 3)
		<?php $rem = $users->count() - 3 ?>
		<a href="{{url('search/'.$q.'/users')}}">View {{$rem}} more users</a>
	@endif
@endif

@if($students->count() > 0)
	<h5 class="a-divider">Students</h5>
	@foreach($students as $student)
		@if($loop->iteration <= 3)
			<?php
				$item = new stdClass();
				$item->link = url("/student/view/$student->student_id/$student->classesID");
            	$item->bg = '#3c3c3c';
				$item->title = $student->name;
				$item->sub_title = 'class 1';
			?>
			@include('search.search_item')
		@else
			<?php break; ?>
		@endif
	@endforeach

	@if($students->count() > 3)
		<?php $rem = $students->count() - 3 ?>
		<a href="{{url('search/'.$q.'/students')}}">View {{$rem}} more students</a>
	@endif
@endif

@if($classes->count() > 0)
	<h5 class="a-divider">Classes</h5>
	@foreach($classes as $class)
		@if($loop->iteration <= 3)
			<?php
				$item = new stdClass();
				$item->bg = '#3c3c3c';
				$item->link = url("/classes/$class->classesID");
				//$item->img = $class_url . "thumbs/" . $class->image_url;
				$item->title = $class->classes;
				//$item->sub_title = $class->teacher()->name;
			?>
			@include('search.search_item')
		@else
			<?php break; ?>
		@endif
	@endforeach

	@if($classes->count() > 3)
		<?php $rem = $classes->count() - 3 ?>
		<a href="{{url('search/'.$q.'/classes')}}">View {{$rem}} more classes</a>
	@endif
@endif

