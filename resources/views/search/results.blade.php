	<?php
		$result_count_str = "";
		$result_count_str .= $result_count > 0 ? $result_count : "No";
		$result_count_str .= $result_count > 1 ? " results" : " result";
		$result_count_str .= " found";
	?>
	<style>
		body{
			background-color: #eee;
		}
		a{
			color: inherit;
			font-size: inherit;
		}

		.content{
			width:100%;max-width: 700px;
			margin: 30px auto;
		}

		#categoryResults .dh-card{
			padding: 30px; 
		}

		#categoryResults .search-item{
			margin-bottom: 30px !important;
		}

		#categoryResults .search-item .search_item_visual{
			height: 100px !important;
			width: 100px !important;
			margin-right: 40px !important;
		}

		#categoryResults #search_item_title{
			margin-top: 10px !important;
			margin-bottom: 6px !important;
			font-size: 1.8em !important;
		}
		
		#categoryResults #search_item_sub_title{
			font-size: 1.4em !important;
		}

		#categoryResults .a-divider{
			font-size: 1.4em !important;
			margin-bottom: 20px;
		}

		#categoryResults .a-divider:first-child{
			font-size: 1.8em !important;
			margin-bottom: 30px !important;
		}
	</style>

	<div id="categoryResults" class="container">
		<div class="content">
			<h5 class="a-divider">{{$result_count_str}}</h5>

			@if($users->count() > 0)

				<div class="dh-card">
					<h5 class="a-divider">Users</h5>
					@foreach($users as $user)
						@if($loop->iteration <= 3)
							<?php
								$item = new stdClass();
								//$item->img = "storage/uploads/images/" . $user_info;
								$item->link = url("/user/view/$user->userID");
								$item->title = $user->name;
								$item->sub_title = $user->usertype;
							?>
							@include('search.search_item')
						@else
							<?php break; ?>
						@endif
					@endforeach	

					@if($users->count() > 3)
						<?php $rem = $users->count() - 3 ?>
						<a href="{{url('search/'.$q.'/users')}}">View {{$rem}} more users</a>
					@endif
				</div>
			@endif

			@if($students->count() > 0)
				<div class="dh-card">
					<h5 class="a-divider">Students</h5>
					@foreach($students as $student)
						@if($loop->iteration <= 3)
							<?php
								$item = new stdClass();
								$item->link = url("/student/$student->student_id/$student->classesID");
								//$item->cover = $student->cover();
								$item->title = $student->title;
								$item->sub_title = $student->user->name;
							?>
							@include('search.search_item')
						@else
							<?php break; ?>
						@endif
					@endforeach

					@if($students->count() > 3)
						<?php $rem = $students->count() - 3 ?>
						<a href="{{url('search/'.$q.'/students')}}">View {{$rem}} more students</a>
					@endif
				</div>
			@endif

			@if($classes->count() > 0)
				<div class="dh-card">
					<h5 class="a-divider">Classes</h5>
					@foreach($classes as $class)
						@if($loop->iteration <= 3)
							<?php
								$item = new stdClass();
								$item->bg = '#3c3c3c';
								$item->link = url("/classes/view/$class->classesID");
								//$item->img = $class_url . "thumbs/" . $class->image_url;
								$item->title = $class->classes;
								$item->sub_title = $class->classes_numeric;
							?>
							@include('search.search_item')
						@else
							<?php break; ?>
						@endif
					@endforeach

					@if($classes->count() > 3)
						<?php $rem = $classes->count() - 3 ?>
						<a href="{{url('search/'.$q.'/classes')}}">View {{$rem}} more classes</a>
					@endif
				</div>
			@endif



		</div>
	</div>