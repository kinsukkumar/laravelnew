
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-lock"></i> <?=$data->lang->line('change_password')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$data->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$data->lang->line('change_password')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8 col-xs-12">
    <?php 
    if(isset($success) && $success !='') {
                        echo "<div class=\"alert alert-success alert-dismissable\">
                            <i class=\"fa fa-ban\"></i>
                            <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                           $success
                        </div>";
                    }
                if($form_validation == "No"){
                } else {
                    if(!empty($form_validation)) {
                        echo "<div class=\"alert alert-danger alert-dismissable\">
                            <i class=\"fa fa-ban\"></i>
                            <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                            $form_validation
                        </div>";
                    }
                }
            ?>
                <form class="form-horizontal" role="form" method="post">

                        <?php 
                            if(form_error($errors,'old_password')) 
                                echo "<div class='form-group has-error' >";
                            else     
                                echo "<div class='form-group' >";
                        ?>
                            <label for="old_password" class="col-sm-2 col-xs-4 control-label">
                                <?=$data->lang->line("old_password")?>
                            </label>
                            <div class="col-sm-6 col-xs-8">
                                <input type="password" class="form-control" id="old_password" name="old_password" required="true">
                            </div>
                            <span class="col-sm-4 col-xs-4 control-label">
                                <?php echo form_error($errors,'old_password'); ?>
                            </span>
                        </div>

                        <?php 
                            if(form_error($errors,'new_password')) 
                                echo "<div class='form-group has-error' >";
                            else     
                                echo "<div class='form-group' >";
                        ?>
                            <label for="new_password" class="col-sm-2 col-xs-4 control-label">
                                <?=$data->lang->line("new_password")?>
                            </label>
                            <div class="col-sm-6 col-xs-8">
                                <input type="password" class="form-control" id="new_password" name="new_password" required="true">
                            </div>
                            <span class="col-sm-4 col-xs-4 control-label">
                                <?php echo form_error($errors,'new_password'); ?>
                            </span>
                        </div>

                        <?php 
                            if(form_error($errors,'re_password')) 
                                echo "<div class='form-group has-error' >";
                            else     
                                echo "<div class='form-group' >";
                        ?>
                            <label for="re_password" class="col-sm-2 col-xs-4 control-label">
                                <?=$data->lang->line("re_password")?>
                            </label>
                            <div class="col-sm-6 col-xs-8">
                                <input type="password" class="form-control" id="re_password" name="re_password" required="true">
                            </div>
                            <span class="col-sm-4 col-xs-4 control-label">
                                <?php echo form_error($errors,'re_password'); ?>
                            </span>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-6 col-xs-offset-4 col-xs-8">
                                <button type="submit" class="btn  btn-success btn-block" ><?=$data->lang->line("change_password")?></button>
                            </div>
                        </div>

                    <?= csrf_field() ?>
</form>
            </div> <!-- col-sm-8 -->
            
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
