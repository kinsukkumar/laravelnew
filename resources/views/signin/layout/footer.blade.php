<?php
$server_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
if (preg_match('/schoolbanks/i', $server_name)) {
    $domain = 'https://www.schoolbanks.net';
    $name = 'AltonSoft Corp';
    $twitter = 'https://click.schoolbanks.com/twitter';
    $facebook = 'https://click.schoolbanks.com/facebook';
    $instagram = 'https://click.schoolbanks.com/instagram';
    $linkedin = 'https://click.schoolbanks.com/linkedin';
    $youtube = 'https://click.schoolbanks.com/youtube';
} else {
    $domain = 'http://www.shulesoft.com';
    $name = 'INETS Company Limited';
    $twitter = 'https://twitter.com/shulesoft';
    $facebook = 'https://facebook.com/shulesoft';
    $instagram = 'https://instagram.com/shulesoft_';
    $linkedin = 'https://click.schoolbanks.com/linkedin';
    $youtube = 'https://click.schoolbanks.com/youtube';
}
?>

<footer class="footer footer-dark bg-gradient-primary mb-0 mt-auto">
    <div class="container">
        <div class="row align-items-center justify-content-md-between py-4 mt-4 delimiter-top">
            <div class="col-md-6">
                <div class="copyright text-sm font-weight-bold text-center text-md-left">
                    &copy; {{date('Y')}} <a href="<?=$domain?>" class="font-weight-bold" target="_blank"><?=$name?></a>. All rights reserved.
                </div>
            </div>
            <div class="col-md-6">
                <ul class="nav justify-content-center justify-content-md-end mt-3 mt-md-0">
                    <li class="nav-item">
                        <a class="nav-link" href="<?=$twitter?>" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="<?=$instagram?>" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=$facebook?>" target="_blank">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<!-- Core -->

<script src="{{url('public/newdesign/vendor/popper/popper.min.js')}}" async></script>
<script src="{{url('public/newdesign/vendor/bootstrap/bootstrap.min.js')}}" async></script>
<script src="{{url('public/newdesign/vendor/sticky-kit/sticky-kit.min.js')}}" async></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js" ></script>

<!-- Page plugins -->
<script src="{{url('public/newdesign/vendor/highlight/js/highlight.min.js')}}" async></script>
<script src="{{url('public/newdesign/vendor/fancybox/js/jquery.fancybox.min.js')}}" ></script>



@yield('page-footer')

<!-- Theme JS -->
<script src="{{url('public/newdesign/js/theme.min.js')}}" defer></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-104437451-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());

gtag('config', 'UA-104437451-1');

$(".fancybox").fancybox({});

const observer = lozad(); // lazy loads elements with default selector as '.lozad'
observer.observe();

$(document).ready(function () {
    $('#show_password').click(function () {

        var $showPassword = $('#inputPassword');

        if ($showPassword.attr('type') === 'password' && $showPassword.val() !== '') {
            $showPassword.attr('type', 'text');
        } else {
            $showPassword.attr('type', 'password');
        }
    });

    $('#change_string').click(function () {

        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789';
        var charactersLength = characters.length;
        for (var i = 0; i < 4; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        $('#random_string').val(result);

    });
});

var $buoop = {required: {e: -4, f: -3, o: -3, s: -1, c: -3}, insecure: true, unsupported: true, api: 2019.05};
function $buo_f() {
    var e = document.createElement("script");
    e.src = "//browser-update.org/update.min.js";
    document.body.appendChild(e);
}
;
try {
    document.addEventListener("DOMContentLoaded", $buo_f, false)
} catch (e) {
    window.attachEvent("onload", $buo_f)
}



</script>
 
@if (preg_match('/schoolbanks/i', $server_name))

@else
    <!-- Start of HubSpot Embed Code -->
    <script type="text/javascript" id="hs-script-loader" defer src="//js.hs-scripts.com/2801781.js" async>
    </script>
    <!-- End of HubSpot Embed Code -->
@endif

</body>
</html>