<?php
$server_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
if (preg_match('/schoolbanks/i', $server_name)) {
    $domain = 'https://support.schoolbanks.com';
    $logo = 'https://trello-attachments.s3.amazonaws.com/5f9203d9bef82a6402df2115/5f9204473d6389195e030e47/f945ddb63be6e699e32b4bbe84a48d5d/SchoolBanks-logo-final-icon%2Btransparent_500.png';
} else {
    $domain = 'https://supports.shulesoft.com';
    $logo=url('/public/assets/images/default-logo.png');
}
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="An online school management system in Dar es Salaam, Tanzania">
        <meta name="author" content="INETS Company Limited | ShuleSoft School Management System">
        <meta name="theme-color" content="#ed3bga">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title><?= $siteinfos->sname ?></title>
        <!-- Favicon -->
        <!--<link href="{{url('public/assets/images/logo.png')}}" rel="icon">-->
        <link rel="SHORTCUT ICON" href="<?= base_url("storage/uploads/images/" . $siteinfos->photo) ?>" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800" rel="stylesheet">

        <!-- Icons -->
        <link rel="stylesheet" href="{{url('public/newdesign/vendor/fontawesome-pro/css/all.min.css')}}">

        <!-- Page plugins -->

        <link type="text/css" href="{{url('public/newdesign/vendor/fancybox/css/jquery.fancybox.min.css')}}" defer rel="stylesheet">
        <link rel="stylesheet" href="{{url('public/newdesign/vendor/animate/animate.min.css')}}" async>
        <link type="text/css" href="{{url('public/newdesign/vendor/highlight/css/highlight.min.css')}}" async rel="stylesheet">
        <link type="text/css" href="{{url('public/newdesign/vendor/highlight/css/styles/atom-one-dark.css')}}" async rel="stylesheet">
        <!-- Theme CSS -->
        <link type="text/css" href="{{url('public/newdesign/css/theme.min.css')}}" rel="stylesheet" async>
        <meta name="google-signin-client_id" content="614990663215-uodgs6e9b7n4t1hrst6k0aja23btd72q.apps.googleusercontent.com">
        <script src="{{url('public/newdesign/vendor/jquery/jquery.min.js')}}" ></script>
        
    </head>

    <body>
        <header class="header-transparent" id="header-main">

            <!-- Search -->
            <div id="search-main" class="navbar-search">
                <div class="container">
                    <!-- Search form -->
                    <form class="navbar-search-form" role="form">
                        <div class="form-group">
                            <div class="input-group input-group-transparent">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Type and hit enter ...">
                            </div>
                        </div>
                    </form>
                    <div class="navbar-search-suggestions">
                        <h6>Search Suggestions</h6>
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a class="list-link" href="#">
                                            <i class="far fa-search"></i>
                                            <span>support</span> in Exam reports
                                        </a>
                                    </li>
                                    <li>
                                        <a class="list-link" href="#">
                                            <i class="far fa-search"></i>
                                            <span>marking</span> in Student marks
                                        </a>
                                    </li>
                                    <li>
                                        <a class="list-link" href="#">
                                            <i class="far fa-search"></i>
                                            <span>make payment</span> in Parents
                                        </a>
                                    </li>
                                    <li>
                                        <a class="list-link" href="#">
                                            <i class="far fa-search"></i>
                                            <span>creating invoices</span> in Accounts
                                        </a>
                                    </li>
                                    <li>
                                        <a class="list-link" href="#">
                                            <i class="far fa-search"></i>
                                            <span>how to clear payments</span> in Accounts
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <!-- Topbar -->
            <div id="navbar-top-main" class="navbar-top  navbar-dark bg-dark border-bottom">
                <div class="container">
                    <div class="navbar-nav align-items-center">
                        <div class="d-none d-lg-inline-block">
                            <span class="navbar-text mr-3">
                                <a href="#" class="avatar rounded-circle shadow">
                                    <img alt="Image placeholder" src="<?=$logo?>" height="48" width="48">
                                </a>
                            </span>
                        </div>

                        <div>

                            <ul class="nav">

                                <li class="nav-item dropdown ml-lg-2 dropdown-animate" data-toggle="hover">
                                    <a class="nav-link px-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img alt="Image placeholder" src="<?php
                                        $image = session('lang') == '' ? 'english' : session('lang');
                                        echo base_url('storage/uploads/language_image/' . $image . '.png');
                                        ?>">
                                        <span class="d-none d-lg-inline-block">{{$image}}</span>
                                        <span class="d-lg-none">{{$image=='english'?'EN':'SW'}}</span>
                                    </a>
                                    @if (!preg_match('/schoolbanks/i', $server_name))
                                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-arrow">
                                            <a href="#" class="dropdown-item" onclick="changeLanguage('sw')">
                                                <img alt="ShuleSoft school management system, school management software, Tanzania, Africa, Dar es Salaam" src="{{url('public/newdesign/img/icons/flags/tz.svg')}}">Kiswahili</a>
                                            <a href="#" class="dropdown-item" onclick="changeLanguage('en')">
                                                <img alt="ShuleSoft school management system, school management software, Tanzania, Africa, Dar es Salaam" src="{{url('public/newdesign/img/icons/flags/us.svg')}}">English</a>
                                        </div>
                                    @endif
                                </li>
                            </ul>
                        </div>
                        <div class="ml-auto">
                            <ul class="nav">

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= $domain ?>" target="_blank"><?= $data->lang->line('support') ?></a>
                                </li>                      
                                <?php
//                        $schema=str_replace('.','', set_schema_name());
//                        $check=DB::table('admin.modules')->where('schema_name',$schema)->where('name','admission')->first();

                                if ($siteinfos->online_admission == 1) {
                                    ?>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= url('admission/index') ?>"><?= $data->lang->line('join_school') ?></a>
                                    </li>
                                <?php } ?>
                                <!-- <li class="nav-item">
                                     <a href="#" class="nav-link" data-action="search-open" data-target="#search-main"><i class="far fa-search"></i></a>
                                 </li>-->
                                <!-- <li class="nav-item">
                                     <a class="nav-link" href="#our-clients"><i class="far fa-user-circle"></i> Login</a>
                                 </li>-->
                            </ul>
                        </div>



                    </div>
                </div>
            </div>



            <script type="text/javascript">
                function changeLanguage(a) {
                    $.get('<?= url('Controller/changeLanguage/') ?>/' + a, null, function (data) {
                        window.location.reload();
                    });
                }
            </script>   
        </header>
        <?php
        if (set_schema_name() != 'jifunze.') {
            ?>
            <style>
                @keyframes shadow-pulse
                {
                    0% {
                        box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.2);
                    }
                    100% {
                        box-shadow: 0 0 0 35px rgba(255,0,0,0.2);
                    }
                }

                @keyframes shadow-pulse-big
                {
                    0% {
                        box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.1);
                    }
                    100% {
                        box-shadow: 0 0 0 70px rgba(255,0,0,0.2);
                    }
                }

                #animate
                {
                    float: left;
                    font: 11px/130px 'Barlow Semi Condensed', sans-serif;
                    text-transform: uppercase;
                    letter-spacing: 1px;
                    color: #fff;
                    text-align: center;
                    animation: shadow-pulse 1s infinite;
                }
                .pulse {
                    overflow: visible;
                    position: relative;
                    float: left;
                    top:16px;
                }
                .pulse:before {
                    content: '';
                    display: block;
                    position: absolute;
                    width: 100%;
                    height: 100%;
                    top: 0;
                    left: 0;
                    background-color: inherit;
                    border-radius: inherit;
                    transition: opacity .3s, transform .3s;
                    animation: pulse-animation 2s cubic-bezier(0.24, 0, 0.38, 1) infinite;


                }
                @keyframes pulse-animation {
                    0% {
                        opacity: 1;
                        transform: scale(1);
                    }
                    50% {
                        opacity: 0;
                        transform: scale(1.5);
                    }
                    100% {
                        opacity: 0;
                        transform: scale(1.5);
                    }
                }
            </style>
            <?php
        }?>