<?php
$linked_schema = [
    'public.', 'canossa.', 'makongo.', 'kamo.', 'atlasschools.', 'geniuskings.', 'barbrojohannson.', 'stpeterclaver.', 'stjosephs.', 'rightwayschools.', 'stfrancisgirls.', 'shepherdsschools.', 'ntonzo.', 'akilistars.', 'hindumandalschools.', 'filbertbayi.', 'enaboishu.'
];
?>
<style>
    @media (min-width: 768px){
        .header-account-page {
            height: 88px !important;
        }
        ul li  .btn {
            padding: 0.8rem 1.85rem !important;

        }
    }
    .leadinModal-content{
        display: none !important;
    }
    .avatar-lg {
        width: 12.5rem !important;
        height: 12.5rem !important;

    }
    @media (max-width: 991.98px){
        .header-account-page {
            padding-top: 5rem !important;
        }
    }
    /* @media (max-width: 400px){
        ul li .btn {
            padding: 1rem 2.54rem !important;
            
        }
    } */


    @media (max-width: 765px){
        ul li .btn {
            /*padding: 1rem 3.56rem;*/
        }
    }
    /*  @media (max-width: 375px){
     ul li .btn {
     padding: 1rem 3.05rem !important;
     }} */


    /*@media (min-width: 321px){*/
    /*ul li .btn {*/
    /*padding: 1rem 3.04rem !important;*/
    /*}*/
    .card {
        border-radius: 0 0 .375rem !important;
    }
    .btn{
        border-radius:0 !important;
        padding: 1rem 0.5rem !important;
    }
    .btn-inner--text{
        padding-top:.8rem;
    }
    .media .avatar {
        width: 15.5rem;
        height: 15.5rem;
    }
    @media (min-width: 768px){
        .col-md-push-6{left:50%}
        .col-md-pull-6{right:50%}
    }
    .nav-tabs {
        border-bottom: none !important;
        display:flex;

    }
    .nav-tabs li{
        display:flex;
        flex:1;

    }
    .nav-tabs li a{
        flex:1;
    }
    .loginBtn {
        box-sizing: border-box;
        position: relative;
        /* width: 13em;  - apply for fixed size */
        margin: 0.2em;
        padding: 0 15px 0 46px;
        border: none;
        text-align: left;
        line-height: 34px;
        white-space: nowrap;
        border-radius: 0.2em;
        font-size: 16px;
        color: #FFF;
    }
    .loginBtn:before {
        content: "";
        box-sizing: border-box;
        position: absolute;
        top: 0;
        left: 0;
        width: 34px;
        height: 100%;
    }
    .loginBtn:focus {
        outline: none;
    }
    .loginBtn:active {
        box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
    }


    /* Facebook */
    .loginBtn--facebook {
        background-color: #4C69BA;
        background-image: linear-gradient(#4C69BA, #3B55A0);
        /*font-family: "Helvetica neue", Helvetica Neue, Helvetica, Arial, sans-serif;*/
        text-shadow: 0 -1px 0 #354C8C;
    }
    .loginBtn--facebook:before {
        border-right: #364e92 1px solid;
        background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png') 6px 6px no-repeat;
    }
    .loginBtn--facebook:hover,
    .loginBtn--facebook:focus {
        background-color: #5B7BD5;
        background-image: linear-gradient(#5B7BD5, #4864B1);
    }


    /* Google */
    .loginBtn--google {
        /*font-family: "Roboto", Roboto, arial, sans-serif;*/
        background: #DD4B39;
    }
    .loginBtn--google:before {
        border-right: #BB3F30 1px solid;
        background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_google.png') 6px 6px no-repeat;
    }
    .loginBtn--google:hover,
    .loginBtn--google:focus {
        background: #E74B37;
    }
    .btn-facebook {
        background: #3B5998;
        border-radius: 0;
        color: #fff;
        border-width: 1px;
        border-style: solid;
        border-color: #263961; 
    }
    .btn-facebook:link, .btn-facebook:visited {
        color: #fff;
    }
    .btn-facebook:active, .btn-facebook:hover {
        background: #263961;
        color: #fff;
    }

</style>

@extends('signin.layout.layout')
@section('content')
<div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0&appId=526011487520671&autoLogAppEvents=1"></script>
    <main>
        <header class="header-account-page bg-gradient-primary d-flex align-items-end">
            <div class="container">
            <div class="row">
                <div class=" col-lg-12">
                    <div class="row align-items-center mb-4">
                        <div class="col-lg-5 mb-4 mb-lg-0">

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </header>

    <section class="slice">
        <div class="container">
            <div class="row">
                <div class="col-lg-6"></div>
                <div class="col-lg-6">
                    <?php
                    if ($siteinfos) {
                        if (set_schema_name() == 'jifunze.' || (int) $siteinfos->enable_self_registration == 1) {
                            ?>

                            <p>    <a class="btn btn-sm btn-warning col-lg-6 col-sm-4 col-xs-4" href="<?= base_url('admission/join_shulesoft') ?>" style="font-size: 11px;"> <span> JOIN <?= str_replace('.', null, ucfirst(set_schema_name())) ?>  (Students and Parents) </span>
                                    <i class="fa fa-new" aria-hidden="true"></i></a> 
                                &nbsp;
                                <a class="btn btn-sm btn-primary col-lg-4" href="<?= base_url('admission/join_teacher') ?>" style="font-size: 11px;"> <span>Teachers Self Registration</span>
                                    <i class="fa fa-new" aria-hidden="true"></i></a>
                            </p>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="row justify-content-center">

                <div class="col-md-6 col-md-push-6">

                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs">
                                <li class="">
                                    <a href="#login" class="active btn btn-secondary btn-icon nav-button" data-toggle="tab">

                                        <span class="btn-inner--icon"><i class="far fa-lock-open"></i></span><br>
                                        <span class="btn-inner--text  d-md-inline-block">{{$data_language->line('login_hint')}}</span></a></li>
                                <li class=""><a href="#forgot_password" class="btn btn-secondary btn-icon nav-button" data-toggle="tab">
                                        <span class="btn-inner--icon"><i class="far fa-key"></i></span><br>
                                        <span class="btn-inner--text  d-md-inline-block"> {{$data_language->line('forgot_password')}}</span></a></li>
                                
                                <?php $server_name = isset($_SERVER['HTTP_HOST'])  ? $_SERVER['HTTP_HOST'] : 'localhost'; ?>
                                @if(!preg_match('/schoolbanks/i', $server_name)) 
                                    <li class=""><a href="#faq" class="btn btn-secondary btn-icon nav-button" data-toggle="tab">
                                        <span class="btn-inner--icon"><i class="far fa-question"></i></span><br>
                                        <span class="btn-inner--text  d-md-inline-block">{{$data_language->line('questions')}}</span></a>
                                    </li>
                                @endif       
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="myTabContent" class="tab-content">

                                <!-- Login tab start -->
                                <div class="tab-pane active in" id="login">
                                    <div class="card shadow zindex-100 ">
                                        <div class="card-body px-md-5">
                                            <div class="text-center mb-4">
                                                <h6 class="h3">{{$data_language->line('welcome')}} </h6>

                                                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none" id="status_message">
                                                    </div>
                                                    @if(!preg_match('/schoolbanks/i', $server_name)) 
                                                        <p class="text-muted mb-0"><small>If you use any of these, click to login or get your login credentials</small></p>
                                                        <p class="text-center" align="center" style="padding-left:20%">
                                                            <span id="my-signin2"></span>
                                                            <li class="text-sm py-1">
                                                                <a href="<?= !in_array(strtolower(device()), ['android', "iphone"]) ? 'https://web.telegram.org/#/im?p=@shulesoft_telegram_bot' : 'https://t.me/shulesoft_telegram_bot' ?>" target="_blank"> <i class="fab fa-telegram fa-2x"></i>Telegram </a> |
                                                                <a target="_blank" href="https://api.whatsapp.com/send?phone=255754406004&text=login_<?= str_replace('.', NULL, set_schema_name()) ?>" style="color:#01E675"> <i class="fab fa-whatsapp fa-2x"></i> WhatsApp</a>
                                                            </li>
                                                        </p>
                                                         <p class="text-muted mb-0"><small>{{$data_language->line('login_social')}}</small></p>
                                                     @endif
                                                    
                                                    <!--<p class="text-muted mb-0">{{$data_language->line('login_desc1')}}<br><small>{{$data_language->line('login_desc2')}}</small></p>-->
                                                @include('layouts.new_notifications')
                                            </div>
                                            <span class="clearfix"></span>

                                            <form  method="post" action="{{url('signin/index')}}">
                                                <div class="form-group">
                                                    <div class="d-flex align-items-center justify-content-between">
                                                        <div>
                                                            <label class="form-control-label">{{$data_language->line('username').' '. $data_language->line('or').' '. $data_language->line('phone')}}</label>
                                                        </div>
                                                    </div>

                                                    <div class="input-group input-group-transparent">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="far fa-user"></i></span>
                                                        </div>
                                                        <input class="form-control" name="username" required id="inputName" placeholder="" value="{{session('username')}}">
                                                    </div>
                                                </div>
                                                {{csrf_field()}}
                                                <div class="form-group mb-4">
                                                    <div class="d-flex align-items-center justify-content-between">
                                                        <div>
                                                            <label class="form-control-label">{{$data_language->line('password')}}</label>
                                                        </div>

                                                    </div>
                                                    <div class="input-group input-group-transparent">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="far fa-key"></i></span>
                                                        </div>
                                                        <input type="password" name="password" required class="form-control" id="inputPassword" placeholder="">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <a href="javascript:void(0)" id="show_password"><i class="far fa-eye"></i></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-center mb-3">
                                                    <button type="submit" class="btn btn-block btn-primary">{{$data_language->line('login_hint')}}</button>

                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!-- Login tab ends-->

                                <!-- Forget password tab starts -->
                                <div class="tab-pane fade" id="forgot_password">
                                    <div class="card shadow zindex-100">
                                        <div class="card-body px-md-5 py-5">
                                            <div class="text-center mb-3">
                                                <h6 class="h3">{{$data_language->line('reset_password')}} </h6>
                                                <p class="text-muted mb-0">Please enter the following details to get a new or forgotten password<br><small>This applies if you have or have never logged in to ShuleSoft</small></p>
                                            </div>
                                            <span class="clearfix"></span>
                                            <form role="form" method="post" class="form-horizontal" action="{{url('reset/index')}}">
                                                <div class="form-group">
                                                    <label class="form-control-label text-left"><?= $data_language->line('reset_phone') ?></label>
                                                    <div class="input-group input-group-transparent">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="far fa-user"></i></span>
                                                        </div>
                                                        <input class="form-control" name="phone" required id="inputPhone" value="{{old('phone')}}">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 mt-0 mb-0">

                                                        <div class="input-group input-group-transparent">
                                                            <input style="border: none !important; font-weight: bolder; cursor: not-allowed; font-size: larger; background-color: #aaaaaa;" name="random_string"  value="{{isset($random_string) ? $random_string : ''}}" required class="form-control" id="random_string">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <a href="javascript:void(0)" id="change_string"><i class="far fa-sync"></i></a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 mt-0">
                                                        <div class="form-group">
                                                            <label class="form-control-label text-left">{{$data_language->line('enter_characters')}}</label>
                                                            <input class="form-control" required type="text" name="answer" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-center mb-3">
                                                    <button type="submit" class="btn btn-block btn-primary forgotPasswordButton">{{$data_language->line('menu_submit')}}</button>
                                                </div>

                                                <?= csrf_field() ?>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- Forget password tab ends-->

                                <!-- Frequently asked questions starts -->
                                <div class="tab-pane fade" id="faq">
                                    <div class="card shadow zindex-100">
                                        <div class="card-body px-md-5 py-5">
                                            <div class="">
                                                <div id="accordion_1" data-accordion="1">
                                                    <div class="card mb-3">
                                                        <div class="card-header py-4" id="heading_1_1" data-toggle="collapse" data-target="#question_1_1" aria-expanded="true" aria-controls="question_1_1">
                                                            <h5 class="heading h5 font-weight-normal mb-0"><i class="far fa-file-pdf mr-3"></i>{{ $data_language->line('forget_password')}}</h5>
                                                        </div>

                                                        <div id="question_1_1" class="collapse" aria-labelledby="heading_1_1" data-parent="#accordion_1" style="">
                                                            <div class="card-body">
                                                                <p>You can get your password by clicking above on the 'Forgot Password' link and enter your phone number and characters you will see below it then click Submit</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="card mb-3">
                                                        <div class="card-header py-4 collapsed" id="heading_1_2" data-toggle="collapse" data-target="#question_1_2" aria-expanded="false" aria-controls="question_1_2">
                                                            <h5 class="heading h5 font-weight-normal mb-0"><i class="far fa-key mr-3 pull-left"></i>{{ $data_language->line('cannot_login')}}</h5>
                                                        </div>

                                                        <div id="question_1_2" class="collapse" aria-labelledby="heading_1_2" data-parent="#accordion_1" style="">
                                                            <div class="card-body">
                                                                <p>{{ $data_language->line('make_sure').' '. $siteinfos->phone}} </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="card mb-3">
                                                        <div class="card-header py-4 collapsed" id="heading_1_3" data-toggle="collapse" data-target="#question_1_3" aria-expanded="false" aria-controls="question_1_3">
                                                            <h5 class="heading h5 font-weight-normal mb-0"><i class="far fa-lock-open mr-3"></i>How do I view my child's report?</h5>
                                                        </div>

                                                        <div id="question_1_3" class="collapse" aria-labelledby="heading_1_3" data-parent="#accordion_1" style="">
                                                            <div class="card-body">
                                                                <p> By using your username and password, log into the system and on the left menu, click 'Student Report' and you will be able to view all associated reports </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card mb-3">
                                                        <div class="card-header py-4 collapsed" id="heading_1_4" data-toggle="collapse" data-target="#question_1_4" aria-expanded="false" aria-controls="question_1_3">
                                                            <h5 class="heading h5 font-weight-normal mb-0"><i class="far fa-lock-open mr-3"></i>How do I change my Email address?</h5>
                                                        </div>

                                                        <div id="question_1_4" class="collapse" aria-labelledby="heading_1_4" data-parent="#accordion_1" style="">
                                                            <div class="card-body">
                                                                <p> Send SMS with key word CHANGE Email:yourmail@example.com to 0655406004. If your phone number is registered, email will be updated. For example, CHANGE Email: johndoe123@gmail.com  </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Frequently asked questions ends-->
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 col-md-pull-6 mt-4 mt-lg-0 mt-md-0 text-center">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pl-lg-4">
                                
                                    @if (preg_match('/schoolbanks/i', $server_name))
                                        <a href="{{base_url('public/assets/schoolbank_images/SchoolBanks-logo.jpg')}}" data-fancybox class="avatar avatar-lg rounded-circle mr-3">
                                            <img src="{{base_url('public/assets/schoolbank_images/SchoolBanks-logo.jpg')}}">
                                        </a>
                                    @else
                                        <a href="{{base_url('storage/uploads/images').'/'.$siteinfos->photo}}" data-fancybox class="avatar avatar-lg rounded-circle mr-3">
                                            <img src="{{base_url('storage/uploads/images').'/'.$siteinfos->photo}}">
                                        </a>
                                    @endif
                                                                                    
                                <div class="mt-2">
                                    <p> <span class="h3 mb-0 d-block hide-md">{{$siteinfos->sname}}</span>
                                        @if(isset($siteinfos->motto))
                                        <span class="">{{title_case($siteinfos->motto)}}</span>
                                        @endif</p>
                                </div>

                                <!-- Product description -->
                                <div class="mt-0">
                                    <small class="text-dark font-weight-bold d-block mb-2 mt-4">{{$data_language->line('school_address')}}</small>
                                    <p class="text-sm lh-150"> @if(isset($siteinfos->box))

                                        <i class="fa fa-map-marker user-profile-icon"></i> P.O.Box <?= $siteinfos->box ?>,<?= ucwords($siteinfos->address) ?>
                                        @endif</p>
                                </div>

                                <!-- Size -->
                                <div class="mt-0">
                                    <small class="d-block font-weight-bold text-dark mb-2">{{$data_language->line('administration')}} | {{$data_language->line('technical_support')}} </small>
                                    <ul class="list-unstyled list-icon">
                                        @if (preg_match('/schoolbanks/i', $server_name))
                                        <li class="text-sm py-1">
                                            <i class="fa fa-envelope"></i> <?= $siteinfos->email ?></p>
                                        </li>
                                        @else
                                        <li class="text-sm py-1">
                                            <p><i class="fa fa-phone"></i><a href="tel:<?= $siteinfos->phone ?>"><?= $siteinfos->phone ?></a>  |  <i class="fa fa-envelope"></i> <?= $siteinfos->email ?></p>
                                        </li>
                                        @endif
                                    </ul>
                                </div>

                                <!-- Color -->
                                <!-- <div class="mt-0">


                                        <ul class="list-unstyled list-icon">
                                            <li class="text-sm py-1">
                                                <p><i class="fa fa-envelope"></i> <?= $siteinfos->email ?></p>
                                            </li>
                                            <li class="text-sm py-1">

                                            </li>
                                        </ul>
                                    </div>-->

                                <!-- ShuleSoft support -->
                                <div class="mt-0">
                                    <small class="d-block font-weight-bold text-dark link-underline-warning"><?= $data_language->line('reach_support') ?></small>
                                    <?php
                                    $server_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
                                    if (preg_match('/schoolbanks/i', $server_name)) {
                                        $domain = strtolower(str_replace('.schoolbanks.net', '', $server_name));
                                        ?>
                                        <ul class="list-unstyled list-icon">
                                            <li class="text-sm py-1">
                                                <i class="far fa-phone mr-3"></i> <a href="tel:+1 470.841.4004"> +1 470.841.4004</a> 
                                                <!--| <a target="_blank" href="https://api.whatsapp.com/send?phone=255655406004" style="color:#01E675"><i class="fab fa-whatsapp fa-2x"></i>WhatsApp</a>-->
                                            </li>
                                            <li class="text-sm py-1">
                                                <i class="far fa-question mr-3"></i><a target="_blank" href="https://support.schoolbanks.com/">Support (Open ticket)</a>
                                            </li>
                                            <li class="text-sm py-1">
                                                <i class="far fa-globe mr-3"></i><a target="_blank" href="https://www.schoolbanks.com">www.schoolbanks.com</a> | <a target="_blank" href="https://www.schoolbanks.com/privacy-policy">Privacy Policy</a>
                                            </li>
                                        </ul>
                                        <?php
                                    } else {
                                        $domain = strtolower(str_replace('.shulesoft.com', '', $server_name));
                                        ?>
                                        <ul class="list-unstyled list-icon">
                                            <li class="text-sm py-1">
                                                <i class="far fa-phone mr-3"></i><a href="tel:+255655406004">+255 754 406 004</a> |<a href="<?= !in_array(strtolower(device()), ['android', "iphone"]) ? 'https://web.telegram.org/#/im?p=@shulesoft_telegram_bot' : 'https://t.me/shulesoft_telegram_bot' ?>" target="_blank"> <i class="fab fa-telegram fa-2x"></i>Telegram Bot </a> | <a target="_blank" href="https://api.whatsapp.com/send?phone=255754406004" style="color:#01E675"> <i class="fab fa-whatsapp fa-2x"></i> WhatsApp</a>
                                            </li>
                                            <li class="text-sm py-1">
                                                <i class="far fa-question mr-3"></i><a target="_blank" href="https://support.shulesoft.com/open.php">Support (Open ticket)</a>
                                            </li>
                                            <li class="text-sm py-1">
                                                <i class="far fa-globe mr-3"></i><a target="_blank" href="https://www.shulesoft.com">www.shulesoft.com</a> | <a target="_blank" href="https://www.shulesoft.com/privacy">Privacy Policy</a>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </script>
<?php
    if (in_array(set_schema_name(), $linked_schema)) {
    ?>
    <script>
    function onSuccess(googleUser) {
    console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
    googleUser.grant({
        scope: 'https://www.googleapis.com/auth/contacts'
    });
    var id_token = googleUser.getAuthResponse().id_token;
    var profile = googleUser.getBasicProfile();
    $.ajax({
        type: 'GET',
        url: '<?= url('/') ?>/signin/new_login/null',
        data: {id: profile.getId(), name: profile.getName(), photo: profile.getImageUrl(), email: profile.getEmail(), action: 'google'},
        dataType: "html",
        success: function (data) {
            if (data == '1') {
                window.location.reload();
            } else if (data == '2') {
                $('#status_message').html('Sorry, Your ShuleSoft account has been disabled. Please contact your Administrator or call us for more information').show();
            } else {
                $('#status_message').html('Sorry: This Email Address (' + profile.getEmail() + ') is not registered in ShuleSoft. Send SMS with your Name and keyword CHANGE Email:' + profile.getEmail() + ' to <?= $siteinfos->phone ?> to update your email in ShuleSoft. ').show();
            }
        }
    });
    }
    function onFailure(error) {
    console.log(error);
    }
    function renderButton() {
    gapi.signin2.render('my-signin2', {
        'scope': 'profile email',
        'width': 240,
        'height': 50,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
    }

    function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
    console.log('statusChangeCallback');
    console.log(response);                   // The current login status of the person.
    if (response.status === 'connected') {   // Logged into your webpage and Facebook.
        //testAPI();
    } else {                                 // Not logged into your webpage or we are unable to tell.
        document.getElementById('status').innerHTML = 'Please log ' +
                'into this webpage.';
    }
    }


    function checkLoginState() {               // Called when a person is finished with the Login Button.
    FB.getLoginStatus(function (response) {   // See the onlogin handler
        statusChangeCallback(response);
    });
    }


    window.fbAsyncInit = function () {
    FB.init({
        appId: '526011487520671',
        cookie: true, // Enable cookies to allow the server to access the session.
        xfbml: true, // Parse social plugins on this webpage.
        version: 'v5.0'           // Use this Graph API version for this call.
    });


    FB.getLoginStatus(function (response) {   // Called after the JS SDK has been initialized.
        statusChangeCallback(response);        // Returns the login status.
    });
    };


    (function (d, s, id) {                      // Load the SDK asynchronously
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id))
        return;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    function testAPI() {                      // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function (response) {
        console.log(response);
        $.ajax({
            type: 'POST',
            url: '<?= url('/') ?>/signin/new_login/null',
            data: {id: response.id, name: response.name, photo: response.public_profile, email: response.email, token: response.id, action: 'facebook'},
            dataType: "html",
            success: function (data) {
                if (data == '1') {
                    window.location.reload();
                } else if (data == '2') {
                    $('#status_message').html('Sorry, Your ShuleSoft account has been disabled. Please contact your Administrator or call us for more information').show();
                } else {
                    $('#status_message').html('Sorry: This Email Address (' + response.email + ') is not registered in ShuleSoft. Send SMS with keyword CHANGE Email:' + response.email + ' to +255655406004 to update your email in ShuleSoft.').show();
                }
            }
        });
        console.log('Successful login for: ' + response.name);
        //document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
    });
    }
    </script>
    <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
<?php } ?>
@endsection

