<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */
Route::any('/attendance/sync', function() {
    $re = request()->except('schema_name');
    DB::table('api.attendance_logs')->insert(['requests' => json_encode($re), 'schema_name' => request('schema_name')]);
    return json_encode(request()->all());
});
Route::get('/param', function() {
    echo sha1('962641603716163' . md5('962641603716163')) . rand(445, 254454);
});
Route::post('/mobile', 'ApiController@index');
Route::post('/attendance', 'Attendance@test');
Route::post('/init', 'PaymentController@api');
Route::post('/webhook', 'PaymentController@selcomWebhook');
Route::post('/telegram', 'Telegramz@index');
Route::post('/whatsapp', 'WhatsAppBot@index');
Route::post('/payment', 'PaymentController@remoteAccess');
Route::post('/init/syncInvoice', 'PaymentController@syncInvoice');
Route::post('/sms', 'SmsController@store');
Route::post('/login', 'ApiController@login');
Route::get('webhook/telegram', 'SmsController@setTelegramWebHook');
Route::post('forward', function() {
    $data = DB::select('select phone, email,name,  case when "table" in (\'parent\',\'teacher\') then \'user\'  else (case when "table"=\'setting\' '
                    . ' then \'super\' else \'admin\' end) end AS usertype,  "schema_name" as school from admin.all_users where "table" !=\'student\' ');
    return json_encode($data);
});

Route::post('notify', 'NotificationController@notify');
Route::post('/cron', function() {
    $data = \DB::table('admin.all_tempfiles')->where('processed', 0)->limit(90)->get();
    foreach ($data as $value) {
        if ($value->controller == 'PaymentController') {
            $class = new \App\Http\Controllers\PaymentController();
            $class->{$value->method}($value->data, $value->id, $value->schema_name);
        }
    }
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

