<?php

//use Illuminate\Support\Facades\File;
//use Illuminate\Support\Facades\Response;
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/*
  Route::get('storage/uploads/images/{filename}', function ($filename)
  {
  return  $path = storage_path('public/uploads/images/' . $filename);

  if (!File::exists($path)) {
  abort(404);
  }

  $file = File::get($path);
  $type = File::mimeType($path);

  $response = Response::make($file, 200);
  $response->header("Content-Type", $type);

  return $response;
 * 
  });
 * 
 * { 
 *     "SN":"AEWD204460015",
 *     "INFO":"Ver 8.0.4.5-20200729,2,2,4,192.168.2.125,10,7,12,0,111"
 * }
 * 
 * {
 *  "SN":"AEWD204460015",
 *  "INFO":"Ver 8.0.4.5-20200729,2,2,3,192.168.2.125,10,7,12,0,111"}
 */

$server_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
$domain = strtolower(str_replace('.shulesoft.com', '', $server_name));
if ($domain == '51.91.251.252:8081') {
    $inputs = file_get_contents("php://input");
    // we only deal with time attendance, here we skipp all issues 
    DB::table('api.attendance_requests')->insert(
            ['body' => json_encode(request()->all()),
                'head' => $inputs
    ]);
    if (strlen(request('SN')) > 4) {
        Route::any('/', 'Attendance@timeAttendance');
        exit;
    }
}


$bad_url = ['acme-challenge', 'rss', 'index.php', 'errors', 'phpR', 'apple-touch', 'assetlinks', '.php', 'public', 'ads.txt'];
foreach ($bad_url as $value) {
    if (preg_match('/' . $value . '/', url()->current())) {
        exit;
    }
}

Route::any('/1488231142:AAGopbeSQgb7-edWdOyorrVl1rFoL_H1dbs/webhook', 'Telegramz@index');
Route::get('/student/getschools/null', function() {
    if (strlen(request('term')) > 1) {
        $sql = "SELECT id,upper(name)|| ' '||upper(type)||' - '||upper(region) as name FROM admin.schools 
			WHERE lower(name) LIKE '%" . str_replace("'", null, strtolower(request('term'))) . "%'
			LIMIT 10";
        die(json_encode(DB::select($sql)));
    }
});
Route::get('/report/quarter/{null}', 'report@quarter');
Route::get('/telegram/webhook', 'TelegramController@activity');
Route::get('/telegram/updates', 'getUpdates@activity');



Route::get('/search', 'SearchController@search');
Route::post('/tmember', 'tmember@assign_transport');
Route::get('/search/{q}', 'SearchController@search');


Auth::routes();

Route::post('/student/print_ids/{student_id}', 'Student@print_ids');
// Route::get('/', function () {
//     return Auth::check() ? response(redirect('dashboard/index')->header('Cache-Control', 'no-store')) : response(redirect('signin/index')->header('Cache-Control', 'no-store'));
// });

if (createRoute() != NULL) {

    $route = explode('@', createRoute());

    $file = app_path() . DIRECTORY_SEPARATOR . 'Http' . DIRECTORY_SEPARATOR . 'Controllers' . DIRECTORY_SEPARATOR . ucfirst($route[0]) . '.php';

    if (file_exists($file)) {

        Route::any('/{controller?}/{method?}/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}/{param6?}/{param7?}', ucfirst(createRoute()));
    } else {
        return view('errors.404');
    }
} else {
  
    return view('errors.404');
}
