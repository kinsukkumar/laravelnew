<?php
/**
 * Telegram Bot example.
 *
 * @author Gabriele Grillo <gabry.grillo@alice.it>
 */
include 'Telegram.php';

// Set the bot TOKEN
$bot_token = '1488231142:AAGopbeSQgb7-edWdOyorrVl1rFoL_H1dbs';
// Instances the class
$telegram = new Telegram($bot_token);

/* If you need to manually take some parameters
*  $result = $telegram->getData();
*  $text = $result["message"] ["text"];
*  $chat_id = $result["message"] ["chat"]["id"];
*/

// Take text and chat_id from the message
$text = $telegram->Text();
$chat_id = $telegram->ChatID();

// Test CallBack
$callback_query = $telegram->Callback_Query();
if ($callback_query !== null && $callback_query != '') {
    $reply = 'Callback value '.$telegram->Callback_Data();
    $content = ['chat_id' => $telegram->Callback_ChatID(), 'text' => $reply];
    $telegram->sendMessage($content);

    $content = ['callback_query_id' => $telegram->Callback_ID(), 'text' => $reply, 'show_alert' => true];
    $telegram->answerCallbackQuery($content);
}

//Test Inline
$data = $telegram->getData();
if ($data['inline_query'] !== null && $data['inline_query'] != '') {
    $query = $data['inline_query']['query'];
    // GIF Examples
    if (strpos('testText', $query) !== false) {
        $results = json_encode([['type' => 'gif', 'id'=> '1', 'gif_url' => 'http://i1260.photobucket.com/albums/ii571/LMFAOSPEAKS/LMFAO/113481459.gif', 'thumb_url'=>'http://i1260.photobucket.com/albums/ii571/LMFAOSPEAKS/LMFAO/113481459.gif']]);
        $content = ['inline_query_id' => $data['inline_query']['id'], 'results' => $results];
        $reply = $telegram->answerInlineQuery($content);
    }

    if (strpos('dance', $query) !== false) {
        $results = json_encode([['type' => 'gif', 'id'=> '1', 'gif_url' => 'https://media.tenor.co/images/cbbfdd7ff679e2ae442024b5cfed229c/tenor.gif', 'thumb_url'=>'https://media.tenor.co/images/cbbfdd7ff679e2ae442024b5cfed229c/tenor.gif']]);
        $content = ['inline_query_id' => $data['inline_query']['id'], 'results' => $results];
        $reply = $telegram->answerInlineQuery($content);
    }
}

// Check if the text is a command
if (!is_null($text) && !is_null($chat_id)) {
    if ($text == '/git') {
        $reply = 'Check me on GitHub: https://github.com/Eleirbag89/TelegramBotPHP';
        // Build the reply array
        $content = ['chat_id' => $chat_id, 'text' => $reply];
        $telegram->sendMessage($content);
    } elseif ($text == '/img') {
        // Load a local file to upload. If is already on Telegram's Servers just pass the resource id
        $img = curl_file_create('test.png', 'image/png');
        $content = ['chat_id' => $chat_id, 'photo' => $img];
        $telegram->sendPhoto($content);
        //Download the file just sended
        $file_id = $message['photo'][0]['file_id'];
        $file = $telegram->getFile($file_id);
        $telegram->downloadFile($file['result']['file_path'], './test_download.png');
    } elseif ($text == '/where') {
        // Send the Catania's coordinate
        $content = ['chat_id' => $chat_id, 'latitude' => '37.5', 'longitude' => '15.1'];
        $telegram->sendLocation($content);
    } elseif ($text == '/inlinekeyboard') {
        // Shows the Inline Keyboard and Trigger a callback on a button press
        $option = [
                [
                $telegram->buildInlineKeyBoardButton('Callback 1', $url = '', $callback_data = '1'),
                $telegram->buildInlineKeyBoardButton('Callback 2', $url = '', $callback_data = '2'),
                ],
            ];

        $keyb = $telegram->buildInlineKeyBoard($option);
        $content = ['chat_id' => $chat_id, 'reply_markup' => $keyb, 'text' => 'This is an InlineKeyboard Test with Callbacks'];
        $telegram->sendMessage($content);
    }
    if ($text == '/start') {
            $reply = ' Welcome. This is ShuleSoft - School Management Software.'. chr(10);
            $reply .=  chr(10). 'Please choose your preferred language'. chr(10);
            $reply .= chr(10);
            $reply .= '/English'. chr(10);
            $reply .= '/Swahili'. chr(10);
            $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
            $telegram->sendMessage($message);
        }elseif($text == '/English') {
            $reply =  'Welcome. ShuleSoft is a scalable and Easy to use School Management Software used by Nursery Schools, Primary Schools, Secondary Schools, and Colleges.';
            $reply .= chr(10);
            $reply .= 'Click the link below to get started'. chr(10);
            $reply .= '/clickHere'. chr(10);
            $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
            $telegram->sendMessage($message);
        }elseif($text == '/Swahili') {
                $reply =  'Karibu. ShuleSoft ni mfumo rahisi unaokuwezesha kusimamia shughuli zote za shule. Mfumo Huu unatumika katika shule za Msingi,Secondary na Vyuo.';
                $reply .= chr(10);
                $reply .= 'Bofya kiunganishi hapo chini ili kuendelea'. chr(10);
                $reply .= '/Bofya Hapa'. chr(10);
                $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
                $telegram->sendMessage($message);
            }elseif($text == '/clickHere') {
                $reply =  'Do you have a ShuleSoft account in any school that use ShuleSoft?';
                $reply .= chr(10);
                $reply .= '/Yes'. chr(10);
                $reply .= '/No'. chr(10);
                $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
                $telegram->sendMessage($message);
            }elseif($text == '/Bofya') {
                $reply =  'Je una akaunti kwenye shule mfumo wetu wa shulesoft?';
                $reply .= chr(10);
                $reply .= '/Ndio'. chr(10);
                $reply .= '/Hapana'. chr(10);
                $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
                $telegram->sendMessage($message);
            }elseif($text == '/No') {
                $reply = 'Click link below to get Help. '.chr(10);
                $reply .= '/about' . chr(10);
                $reply .= '/loginHelp' . chr(10);
                $reply .= '/changeEmail' . chr(10);
                $reply .= '/ourContact' . chr(10);
                $reply .= '/examReport' . chr(10);
                $reply .= '/Payment report' . chr(10);
                $reply .= '/forgotPassword' . chr(10);
                $reply .= '/ask' . chr(10);
                $reply .= '/MoreHelp' . chr(10);
                $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
                $telegram->sendMessage($message);
            }elseif($text == '/Hapana') {
                $reply = 'Click link below to get Help. '.chr(10);
                $reply .= '/Kuhusu Shulesoft' . chr(10);
                $reply .= '/Jinsi ya Kuingia' . chr(10);
                $reply .= '/Badili Email' . chr(10);
                $reply .= '/Mawasiliano' . chr(10);
                $reply .= '/Ripoti za Mitihani' . chr(10);
                $reply .= '/Ripoti, za Malipo' . chr(10);
                $reply .= '/Kubadili Password' . chr(10);
                $reply .= '/Uliza' . chr(10);
                $reply .= '/Msaada Zaidi' . chr(10);
                $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
                $telegram->sendMessage($message);
            }elseif ($text == '/Yes') {
            $reply = 'Kindly write your phone number';
          $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
          $telegram->sendMessage($message);
    } elseif ($text == '/Ndio') {
        $reply = 'Samahani Ingiza namba yako hapa 👇👇👇';
      $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
      $telegram->sendMessage($message);
    } elseif ($text == '/about' || $text == 'About Shulesoft') {
        $reply = 'ShuleSoft is an extensible, scalable and easy to use school management system that simplifies school operations and interconnects parents, teachers, students and other school stakeholders. Currently used by more than 380 Schools.'. chr(10);
        $reply .= chr(10);
        $reply .= 'In order to use ShuleSoft to access your school syetm you are required to be registered.';
         $reply .= 'Vist Our Website: https://www.shulesoft.com/' . chr(10);
        $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
        $telegram->sendMessage($message);
    } elseif ($text == '/menu') {
        $reply = 'Click link below to get Help. '.chr(10);
        $reply .= '/about' . chr(10);
        $reply .= '/loginHelp' . chr(10);
        $reply .= '/changeEmail' . chr(10);
        $reply .= '/ourContact' . chr(10);
        $reply .= '/examReport' . chr(10);
        $reply .= '/Payment report' . chr(10);
        $reply .= '/forgotPassword' . chr(10);
        $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
        $telegram->sendMessage($message);
      }elseif ($text == '/ourContact') {
        $reply = 'To know more please give us a call, drop us an email or visit our offices.' . chr(10);
        $reply .= 'Email:- support@shulesoft.com ' . chr(10);
        $reply .= 'Phone:- +255 655 406 004 / +255 754 406 004' . chr(10);
        $reply .= 'Website: https://www.shulesoft.com/' . chr(10);
        $reply .= '/menu' . chr(10);
        $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
        $telegram->sendMessage($message);
      }elseif ($text == '/changeEmail') {
        $reply = 'Send SMS with key word CHANGE Email:yourmail@example.com to 0655406004. If your phone number is registered, email will be updated.';
        $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
                  $telegram->sendMessage($message);
    } elseif ($text == '/loginHelp' || $text == 'Login Help') {
        $reply = 'Make sure you are using your registered username or reset your password using your phone number, otherwise contact the school ';
        $reply .= chr(10);
        $reply .= '/forgotPassword';
        $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
                  $telegram->sendMessage($message);
    } elseif ($text == '/examReport') {
        $reply = 'By using your username and password, log into the system and on the left menu, click *Student Report* and you will be able to view all associated reports';
        $reply .= chr(10);
        $reply .= '/Payment report';
        $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
        $telegram->sendMessage($message);
    } elseif ($text == '/enterSchoolName') {
        $reply = 'By using your username and password, log into the system and on the left menu, click *Student Report* and you will be able to view all associated reports';
        $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
        $telegram->sendMessage($message);
    }elseif ($text == '/enterPhoneNumber') {
        $reply = 'By using your username and password, log into the system and on the left menu, click *Student Report* and you will be able to view all associated reports';
        $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
        $telegram->sendMessage($message);
    } elseif ($text == '/Help') {
     $reply = 'Visit Us on: https://www.shulesoft.com/contact-us';
     // Build the reply array
     $content = ['chat_id' => $chat_id, 'text' => $reply];
     $telegram->sendMessage($content);
     // Create option for the custom keyboard. Array of array string
     $option = [['About Shulesoft', 'Login Help'], ['', 'Contact Us']];
     // Get the keyboard
     $keyb = $telegram->buildKeyBoardHide($option);
     $content = ['chat_id' => $chat_id, 'reply_markup' => $keyb, 'text' => $reply];
     $telegram->sendMessage($content);
 } elseif ($text == '/ask') {
               
     $reply = 'Please select one of the buttons below to proceed. ';
 // Create option for the custom keyboard. Array of array string
//  $option = [['About Shulesoft', 'Login Help'], ['View Exam Report', 'Contact Us']];
 $option = [
     ['About Shulesoft'], 
     ['Does ShuleSoft work on mobile devices?'],
     ['How do I access my account?'], 
     ['What happens if I lose or forget my username and password?'], 
     ['What kind of support does ShuleSoft offer?'],
     ['Can my son or daughter look at their grades?'],
     ['Is ShuleSoft Support or offer Digital Learning platform?']
 ]; 
 // Get the keyboard
 $keyb =  $telegram->buildKeyBoard($option);
 $content = ['chat_id' => $chat_id, 'reply_markup' => $keyb, 'text' => $reply];
 $telegram->sendMessage($content);

} elseif($text == 'Does ShuleSoft work on mobile devices?'){
 $reply = 'Yes! As long as your device has an internet connection, Most major browsers and operating systems are supported, ShuleSoft work on Desktop, tablets and mobile phones. For parents and students, we also offer a free mobile app (available on Android) specifically designed for mobile devices.';
 $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
 $telegram->sendMessage($message);
} elseif($text == 'How do I access my account?'){
$reply = 'ShuleSoft will provide login credentials to staff,  teacher, parent or student. If you are a teacher, parent or student whose school uses Shulesoft system you’ll need to contact one of your school’s administrators to receive your login credentials.';
$message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
$telegram->sendMessage($message);
} elseif($text == 'What happens if I lose or forget my username and password?'){
 $reply = 'Click “Forgot Password” to reset your password. If you don’t know your username or school name, please contact one of your school’s administrators. For security reasons, Shulesoft staff are unable to provide login credentials over the phone or via email.';
 $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
 $telegram->sendMessage($message);
} elseif($text == 'What kind of support does ShuleSoft offer?'){
 $reply ='We offer technical support to parents, students and staff members.  This includes video calls, SMS and emails depending on your need.  Our support team is also available with the direct messaging button in the lower right corner of every page.';
 $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
 $telegram->sendMessage($message);

} elseif($text == 'Can my son or daughter look at their Exam reports?'){
 $reply ='Yes! ShuleSoft offers separate parent and student accounts that both have access to student grades. For students old enough to use a computer, the Student Panel is simple to use and the content is entirely easy to navigate.';
 $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
 $telegram->sendMessage($message);
} elseif($text == 'Is ShuleSoft Support or offer Digital Learning platform?'){
 $reply ='There are 9 major Packages that help schools on Digital Learning.' .chr(10);
 $reply .= '1. Live Studies to stream live videos to students. ' . chr(10);
 $reply .= '2. Class Notes allow teachers to upload notes and Students can read notes online' . chr(10);
 $reply .= '3. Online Exams, Students can easily perform exams online and get result on-time ' . chr(10);
 $reply .= '4. Online Discussion Forum, This allows students to ask different questions & answers. ' . chr(10);
 $reply .= '5. Home packages management, students can view, work on them and submit back to the system.' . chr(10);
 $reply .= '6. SMS and Emails communication ' . chr(10);
 $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
 $telegram->sendMessage($message);
}elseif ($text == '/Payment') {
    $reply = 'By using your username and password, log into the system and on the left menu, click *Student Report* and you will be able to view all associated reports';
    $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
    $telegram->sendMessage($message);
  }elseif ($text == '/resetPassord') {
    $reply = 'What happens if I lose or forget my username and password?'. chr(10);
    $reply .= 'You can get your password by clicking above on the *Forgot Password* link and enter your phone number and characters you will see below it then click Submit . You will receive an Email or SMS with username & Password.'. chr(10);
    $reply .= ' If you don’t know your username or school ID, please contact one of your school’s administrators.'; 
    $reply .= chr(10);
    $reply .= '/loginHelp';
    $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
    $telegram->sendMessage($message);
  }elseif($text == ' Can I try Shulesoft before paying it?'){
    $reply = 'Definitely, you can try a free demo trial to get a deeper understanding of all shulesoft features'. chr(10);
    $reply .='Our demo is free for 14 days. Try today!'. chr(10);
    $reply .= 'You want to talk to us? 📱'. chr(10);
    $reply .= '/ourContact'. chr(10);
    $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
    $telegram->sendMessage($message);
}elseif($text == '/MoreHelp'){
    $reply = 'Great. If its anything concerning ShuleSoft,  we are happy to assist you 😇'. chr(10);
    $reply .= chr(10);
    $reply .= 'Phone:- +255655406004 / +255754406004' . chr(10);
    $reply .= 'Email:- support@shulesoft.com ' . chr(10);
    $reply .= 'Website: https://www.shulesoft.com/' . chr(10);
    $reply .= '/menu' . chr(10);
    $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
    $telegram->sendMessage($message);
  }else{
    $reply = 'Unrecognized command. Please use /start to begin.'. chr(10);
    $reply .= chr(10);
    $reply .= '/start' . chr(10);
    $reply .= '/menu' . chr(10);
    $reply .= '/MoreHelp' . chr(10);
    $message = array('chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML');
    $telegram->sendMessage($message);
  }


}
